# VHFMGR service
#
# VERSION       VHFMGR-0.1.1

# use an alpine image from docker-hub.
# FROM anapsix/alpine-java
FROM adoptopenjdk/openjdk11:alpine-jre

MAINTAINER Andrea Formica

ENV USR svom
ENV SVOM_GID 208

ENV vhfmgr_version 1.1-SNAPSHOT
ENV vhfmgr_dir /home/${USR}/vhfmgr
ENV catalina_base /tmp
ENV data_dir /home/${USR}/data
ENV gradle_version 6.7
ENV TZ GMT

ENV ssl_dir /home/${USR}/security

RUN mkdir -p ${vhfmgr_dir} \
  && mkdir -p ${data_dir}/web \
  && mkdir -p ${data_dir}/dump \
  && mkdir -p ${data_dir}/logs \
  && mkdir -p ${ssl_dir}

RUN addgroup -g $SVOM_GID $USR \
    && adduser -S -u $SVOM_GID -G $USR -h /home/$USR $USR

### This is if you need to create a full war on docker
#ADD . ${vhfmgr_dir}
#RUN cd /opt/swagger_vhfmgr && ./gradlew clean :vhfmgrApi-web:build && cp ./vhfmgrApi-web/build/libs/vhfmgr.war ${vhfmgr_dir}/

## This works if using an externally generated war, in the local directory
ADD vhfmgrApi-web/build/libs/vhfmgr.war ${vhfmgr_dir}/vhfmgr.war
ADD certificates ${ssl_dir}/certificates
ADD swagger_schemas/swagger/yaml/vhfmgrapi_full.yaml ${data_dir}/web/api.yaml
ADD logback.xml.vhfmgr /home/${USR}/logback.xml

VOLUME "${data_dir}"
EXPOSE 8080

COPY ./entrypoint.sh /home/${USR}
COPY ./create-properties.sh /home/${USR}
RUN chown -R $USR:$SVOM_GID /home/${USR}

USER ${USR}
WORKDIR /home/${USR}

ENTRYPOINT  [ "./entrypoint.sh" ]
