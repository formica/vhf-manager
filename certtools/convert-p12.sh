openssl pkcs12 -in $1.p12 -nocerts -out $1.key.pem -nodes
openssl pkcs12 -in $1.p12 -clcerts -nokeys -out $1.cert.pem
cat $1.cert.pem $1.key.pem >> $1.client.pem

