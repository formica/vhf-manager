HOSTNAME=svomtest.svom.eu
CERT_CA_ROOT=/Users/formica/svomtest.svom.eu.ssl/root-selfsigned
MODE=$1

function generateSelfSigned {
  cd $CERT_CA_ROOT
  ### Generate a key without password and use it for CSR
  openssl genrsa -out intermediate/private/${HOSTNAME}.key.pem 2048
  chmod 400 intermediate/private/${HOSTNAME}.key.pem
  openssl req -config intermediate/openssl.cnf -key intermediate/private/${HOSTNAME}.key.pem -new -sha256 -out intermediate/csr/${HOSTNAME}.csr.pem

  ### Create the signed certificate from the csr
  openssl ca -config intermediate/openssl.cnf \
        -extensions server_cert -days 375 -notext -md sha256 \
        -in intermediate/csr/${HOSTNAME}.csr.pem \
        -out intermediate/certs/${HOSTNAME}.cert.pem
  chmod 444 intermediate/certs/${HOSTNAME}.cert.pem
}

function generateSelfSignedKeystore {
  ### Generate the pkcs12
###  openssl pkcs12 -export -inkey ${HOSTNAME}.key.pem -in ${HOSTNAME}.cert.pem -out ${HOSTNAME}.pkcs12
###  keytool -importkeystore -srckeystore ${HOSTNAME}.pkcs12 -srcstoretype PKCS12 -destkeystore keystore.jks
  ### Concatenate certificates
  cat ${HOSTNAME}.cert.pem intermediate.cert.pem ca.cert.pem > certificates.txt

  openssl pkcs12 -export -inkey ${HOSTNAME}.key.pem -in certificates.txt -out certificates-bundle.pkcs12
  keytool -importkeystore -srckeystore certificates-bundle.pkcs12 -srcstoretype PKCS12 -destkeystore keystore.jks
  keytool -changealias -keystore  keystore.jks -alias 1
}

function generateCnesKeystore {
  ### CNES signed
  cat svomtest_svom_eu.pem ac_tech2026.pem ac_racine.pem > cnes-certificates.txt

  openssl pkcs12 -export -inkey ${HOSTNAME}.key.pem -in cnes-certificates.txt -out cnes-certificates-bundle.pkcs12
  keytool -importkeystore -srckeystore cnes-certificates-bundle.pkcs12 -srcstoretype PKCS12 -destkeystore keystore.jks
  keytool -changealias -keystore  keystore.jks -alias 1
}

if [[ "$MODE" == "selfsigned" ]]; then
  echo "Generate self signed..."

fi

if [[ "$MODE" == "cnes" ]]; then
  echo "Generate CNES Keystore for host from ${HOSTNAME}.key.pem and ${HOSTNAME}.cert.pem"
  generateCnesKeystore
fi
