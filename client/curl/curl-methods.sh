host=$1
apiname="api"

function json_data() {
  cat <<EOF
{ "" }
EOF
}

function update_scheduler_status() {
  echo "Execute $1 : update status of scheduler $2 to $3"
  resp=`curl -X PUT -H "Accept: application/json" -H "Content-Type: application/json" "${host}/${apiname}/schedulers/$2?status=$3"`
  echo "Received response $resp"
}


function manager_service_status() {

  echo "Execute $1 : manager service status "
  echo "curl dump: curl -X GET -H \"Content-Type: application/json\" \"${host}/mgmt/health\""
  if [ -n "${AUTH_HEADER+1}" ]; then
    resp=`curl -X GET -H $AUTH_HEADER -H "Content-Type: application/json" "${host}/mgmt/health"`
  else
    resp=`curl -X GET -H "Content-Type: application/json" "${host}/mgmt/health"`
  fi
  echo "Received response $resp"
}

###
# Main script
echo "Use host = $host apiname $apiname"
echo "Execute $2"
if [ "$2" == "list" ]; then
  echo "Use commands: update_scheduler_status,.. "
  echo "check health: manager_service_status,.. "
else
  echo "run function $2"
  $2 "${@:2}"
fi
