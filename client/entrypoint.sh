#!/bin/sh
# Use in docker
#   - docker build -t vhfmgr-client:1.0 .
#   - docker run -v /tmp:/data -e vhfmgr_server_url='http://svomtest.svom.fr:9091/api' -i -t vhfmgr-client:1.0
#
export CDMS_HOME=$HOME
echo "setting server ${vhfmgr_server_url}"
export CDMS_HOST="${vhfmgr_server_url}"
cd $HOME
echo "running script from $PWD" > /data/out.txt
echo `eval date` >> /data/out.txt
python ./utils/test-vhfmgr.py >> /data/out.txt
