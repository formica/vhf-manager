# vhfmgrapi.ApidsApi

All URIs are relative to *http://localhost:8080/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_packet_apid**](ApidsApi.md#create_packet_apid) | **POST** /apids | Saves PacketApid
[**find_packet_apids**](ApidsApi.md#find_packet_apids) | **GET** /apids/{id} | Finds PacketApid by id
[**list_packet_apids**](ApidsApi.md#list_packet_apids) | **GET** /apids | Finds a packet APID list.
[**update_packet_apids**](ApidsApi.md#update_packet_apids) | **PUT** /apids/{id} | Update PacketApid by id


# **create_packet_apid**
> PacketApidDto create_packet_apid(body)

Saves PacketApid

Create a new packet APID description entry in the DB.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.ApidsApi()
body = vhfmgrapi.PacketApidDto() # PacketApidDto | A json string that is used to construct a packetapiddto object: { name: xxx, ... }

try:
    # Saves PacketApid
    api_response = api_instance.create_packet_apid(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApidsApi->create_packet_apid: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PacketApidDto**](PacketApidDto.md)| A json string that is used to construct a packetapiddto object: { name: xxx, ... } | 

### Return type

[**PacketApidDto**](PacketApidDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_packet_apids**
> PacketApidDto find_packet_apids(id)

Finds PacketApid by id

Search for a resource by ID

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.ApidsApi()
id = 56 # int | packet apid

try:
    # Finds PacketApid by id
    api_response = api_instance.find_packet_apids(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApidsApi->find_packet_apids: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| packet apid | 

### Return type

[**PacketApidDto**](PacketApidDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_packet_apids**
> list[PacketApidDto] list_packet_apids(by=by, page=page, size=size, sort=sort)

Finds a packet APID list.

This method allows to perform search and sorting.Arguments: by=<pattern>, page={ipage}, size={isize}, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.ApidsApi()
by = 'none' # str | by: the search pattern {none} (optional) (default to none)
page = 0 # int | page: the page number {0} (optional) (default to 0)
size = 1000 # int | size: the page size {1000} (optional) (default to 1000)
sort = 'packetName:ASC' # str | sort: the sort pattern {apid:DESC} (optional) (default to packetName:ASC)

try:
    # Finds a packet APID list.
    api_response = api_instance.list_packet_apids(by=by, page=page, size=size, sort=sort)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApidsApi->list_packet_apids: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none} | [optional] [default to none]
 **page** | **int**| page: the page number {0} | [optional] [default to 0]
 **size** | **int**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **str**| sort: the sort pattern {apid:DESC} | [optional] [default to packetName:ASC]

### Return type

[**list[PacketApidDto]**](PacketApidDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_packet_apids**
> PacketApidDto update_packet_apids(id, body)

Update PacketApid by id

Search for a resource by ID and update fields

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.ApidsApi()
id = 56 # int | packet apid
body = vhfmgrapi.PacketApidDto() # PacketApidDto | A json string that is used to construct a packetapiddto object: { name: xxx, ... }

try:
    # Update PacketApid by id
    api_response = api_instance.update_packet_apids(id, body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApidsApi->update_packet_apids: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| packet apid | 
 **body** | [**PacketApidDto**](PacketApidDto.md)| A json string that is used to construct a packetapiddto object: { name: xxx, ... } | 

### Return type

[**PacketApidDto**](PacketApidDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

