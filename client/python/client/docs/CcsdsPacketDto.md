# CcsdsPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ccsds_id** | **int** |  | [optional] 
**ccsds_version_num** | **int** |  | [optional] 
**ccsds_type** | **int** |  | [optional] 
**ccsds_sec_head_flag** | **int** |  | [optional] 
**ccsds_apid** | **int** |  | [optional] 
**ccsds_g_flag** | **int** |  | [optional] 
**ccsds_counter** | **int** |  | [optional] 
**ccsds_plength** | **int** |  | [optional] 
**ccsds_crc** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


