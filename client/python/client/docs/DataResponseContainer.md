# DataResponseContainer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data_request** | [**DataRequest**](DataRequest.md) |  | 
**data_flag** | [**DataFlag**](DataFlag.md) |  | 
**size** | **int** |  | 
**data** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


