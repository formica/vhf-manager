# FrameHeaderPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**f_id** | **int** |  | [optional] 
**tframe_version** | **int** |  | [optional] 
**space_craft_id** | **int** |  | [optional] 
**vc_id** | **int** |  | [optional] 
**oc_flag** | **int** |  | [optional] 
**mcf_count** | **int** |  | [optional] 
**vcf_count** | **int** |  | [optional] 
**df_status** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


