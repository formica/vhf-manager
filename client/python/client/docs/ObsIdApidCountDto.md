# ObsIdApidCountDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apid** | **int** |  | [optional] 
**packet_name** | **str** |  | [optional] 
**packet_obsid** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**counted_packets** | **int** |  | [optional] [default to -1]
**expected_packets** | **int** |  | [optional] [default to -1]
**insertion_time** | **int** |  | [optional] 
**update_time** | **int** |  | [optional] 
**obsid_min_time** | **int** |  | [optional] 
**obsid_max_time** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


