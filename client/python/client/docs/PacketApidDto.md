# PacketApidDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apid** | **int** |  | [optional] 
**packet_name** | **str** |  | [optional] 
**packet_description** | **str** |  | [optional] 
**class_name** | **str** |  | [optional] 
**instrument** | **str** |  | [optional] 
**category** | **str** |  | [optional] 
**expected_packets** | **int** |  | [optional] [default to -1]
**priority** | **int** |  | [optional] [default to -1]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


