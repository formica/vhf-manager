# PrimaryHeaderPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ph_id** | **int** |  | [optional] 
**packet_time** | **int** |  | [optional] 
**packet_obsid** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


