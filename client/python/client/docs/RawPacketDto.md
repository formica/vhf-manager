# RawPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pktsize** | **int** |  | [optional] 
**packet** | **str** |  | [optional] 
**pktformat** | **str** |  | [optional] [default to 'HEX']
**pktflag** | **str** |  | [optional] [default to 'NOT_DECODED']
**insertion_time** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


