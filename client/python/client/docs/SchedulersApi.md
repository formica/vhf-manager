# vhfmgrapi.SchedulersApi

All URIs are relative to *http://localhost:8080/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_schedulers**](SchedulersApi.md#create_schedulers) | **POST** /schedulers | Saves Schedulers
[**list_schedulers**](SchedulersApi.md#list_schedulers) | **GET** /schedulers | Finds a Schedulers list.
[**list_schedulers_exec**](SchedulersApi.md#list_schedulers_exec) | **GET** /schedulers/runs | Finds last execution information summary.
[**update_scheduler_status**](SchedulersApi.md#update_scheduler_status) | **PUT** /schedulers/{id} | Update Scheduler by name


# **create_schedulers**
> VhfSchedulerStatusDto create_schedulers(body)

Saves Schedulers

Create a new Schedulers entry in the DB.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.SchedulersApi()
body = vhfmgrapi.VhfSchedulerStatusDto() # VhfSchedulerStatusDto | A json string that is used to construct a schedulerdto object: { name: xxx, ... }

try:
    # Saves Schedulers
    api_response = api_instance.create_schedulers(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SchedulersApi->create_schedulers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**VhfSchedulerStatusDto**](VhfSchedulerStatusDto.md)| A json string that is used to construct a schedulerdto object: { name: xxx, ... } | 

### Return type

[**VhfSchedulerStatusDto**](VhfSchedulerStatusDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_schedulers**
> list[VhfSchedulerStatusDto] list_schedulers(by=by)

Finds a Schedulers list.

Find all schedulers

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.SchedulersApi()
by = 'none' # str | by: the search pattern {none} (optional) (default to none)

try:
    # Finds a Schedulers list.
    api_response = api_instance.list_schedulers(by=by)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SchedulersApi->list_schedulers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none} | [optional] [default to none]

### Return type

[**list[VhfSchedulerStatusDto]**](VhfSchedulerStatusDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_schedulers_exec**
> list[VhfSchedulerMonDto] list_schedulers_exec(by=by)

Finds last execution information summary.

Find last execution information.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.SchedulersApi()
by = 'none' # str | by: the search pattern {none} (optional) (default to none)

try:
    # Finds last execution information summary.
    api_response = api_instance.list_schedulers_exec(by=by)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SchedulersApi->list_schedulers_exec: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none} | [optional] [default to none]

### Return type

[**list[VhfSchedulerMonDto]**](VhfSchedulerMonDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_scheduler_status**
> VhfSchedulerStatusDto update_scheduler_status(id, status)

Update Scheduler by name

Search for a resource by ID and update its content

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.SchedulersApi()
id = 'id_example' # str | scheduler name
status = 'ACTIVE' # str | status: the status to set (ACTIVE,IDLE) (default to ACTIVE)

try:
    # Update Scheduler by name
    api_response = api_instance.update_scheduler_status(id, status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SchedulersApi->update_scheduler_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| scheduler name | 
 **status** | **str**| status: the status to set (ACTIVE,IDLE) | [default to ACTIVE]

### Return type

[**VhfSchedulerStatusDto**](VhfSchedulerStatusDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

