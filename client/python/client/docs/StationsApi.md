# vhfmgrapi.StationsApi

All URIs are relative to *http://localhost:8080/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_vhf_ground_stations**](StationsApi.md#create_vhf_ground_stations) | **POST** /stations | Saves VhfGroundStation
[**find_vhf_ground_stations**](StationsApi.md#find_vhf_ground_stations) | **GET** /stations/{id} | Finds VhfGroundStationDto by id
[**list_vhf_ground_stations**](StationsApi.md#list_vhf_ground_stations) | **GET** /stations | Finds a VhfGroundStationDtos list.
[**update_vhf_ground_station**](StationsApi.md#update_vhf_ground_station) | **PUT** /stations/{id} | Update Station by id


# **create_vhf_ground_stations**
> VhfGroundStationDto create_vhf_ground_stations(body)

Saves VhfGroundStation

Create new station

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.StationsApi()
body = vhfmgrapi.VhfGroundStationDto() # VhfGroundStationDto | A json string that is used to construct a vhfgroundstationdto object: { name: xxx, ... }

try:
    # Saves VhfGroundStation
    api_response = api_instance.create_vhf_ground_stations(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StationsApi->create_vhf_ground_stations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**VhfGroundStationDto**](VhfGroundStationDto.md)| A json string that is used to construct a vhfgroundstationdto object: { name: xxx, ... } | 

### Return type

[**VhfGroundStationDto**](VhfGroundStationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_vhf_ground_stations**
> VhfGroundStationDto find_vhf_ground_stations(id)

Finds VhfGroundStationDto by id

Search for a specific resource

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.StationsApi()
id = 56 # int | station id

try:
    # Finds VhfGroundStationDto by id
    api_response = api_instance.find_vhf_ground_stations(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StationsApi->find_vhf_ground_stations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| station id | 

### Return type

[**VhfGroundStationDto**](VhfGroundStationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_vhf_ground_stations**
> list[VhfGroundStationDto] list_vhf_ground_stations(by=by, page=page, size=size, sort=sort)

Finds a VhfGroundStationDtos list.

This method allows to perform search and sorting.Arguments: by=<pattern>, page={ipage}, size={isize}, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.StationsApi()
by = 'none' # str | by: the search pattern {none} (optional) (default to none)
page = 0 # int | page: the page number {0} (optional) (default to 0)
size = 1000 # int | size: the page size {1000} (optional) (default to 1000)
sort = 'stationId:ASC' # str | sort: the sort pattern {stationId:ASC} (optional) (default to stationId:ASC)

try:
    # Finds a VhfGroundStationDtos list.
    api_response = api_instance.list_vhf_ground_stations(by=by, page=page, size=size, sort=sort)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StationsApi->list_vhf_ground_stations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none} | [optional] [default to none]
 **page** | **int**| page: the page number {0} | [optional] [default to 0]
 **size** | **int**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **str**| sort: the sort pattern {stationId:ASC} | [optional] [default to stationId:ASC]

### Return type

[**list[VhfGroundStationDto]**](VhfGroundStationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_vhf_ground_station**
> VhfGroundStationDto update_vhf_ground_station(id, body)

Update Station by id

Search for a resource by ID and update fields

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.StationsApi()
id = 56 # int | station id
body = vhfmgrapi.VhfGroundStationDto() # VhfGroundStationDto | A json string that is used to construct a VhfGroundStationDto object: { name: xxx, ... }

try:
    # Update Station by id
    api_response = api_instance.update_vhf_ground_station(id, body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling StationsApi->update_vhf_ground_station: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| station id | 
 **body** | [**VhfGroundStationDto**](VhfGroundStationDto.md)| A json string that is used to construct a VhfGroundStationDto object: { name: xxx, ... } | 

### Return type

[**VhfGroundStationDto**](VhfGroundStationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

