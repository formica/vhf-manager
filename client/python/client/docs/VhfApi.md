# vhfmgrapi.VhfApi

All URIs are relative to *http://localhost:8080/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**decode_packet**](VhfApi.md#decode_packet) | **GET** /vhf/packet/decode | Decode a VHF packet.
[**find_obsid_list**](VhfApi.md#find_obsid_list) | **GET** /vhf/obsids | Finds vhf obsid list filled from scheduler.
[**find_vhf_transfer_frames**](VhfApi.md#find_vhf_transfer_frames) | **GET** /vhf/{frame} | Finds vhf transfer frame by ID.
[**get_vhf_packets**](VhfApi.md#get_vhf_packets) | **GET** /vhf/packet/{hash} | Finds vhf packet by HASH.
[**list_vhf_counters**](VhfApi.md#list_vhf_counters) | **GET** /vhf/count | Finds a list of summary data (ObsIdApidCountDto).
[**list_vhf_raw_packets**](VhfApi.md#list_vhf_raw_packets) | **GET** /vhf/rawpackets | Finds a RawPacketDtos lists.
[**list_vhf_station_status**](VhfApi.md#list_vhf_station_status) | **GET** /vhf/stationstatus | Finds a VhfStationStatusDtos lists.
[**list_vhf_station_status_count**](VhfApi.md#list_vhf_station_status_count) | **GET** /vhf/stationstatus/count | Finds a VhfStationStatusCountDtos lists.
[**list_vhf_transfer_frames**](VhfApi.md#list_vhf_transfer_frames) | **GET** /vhf | Finds a VhfTransferFrameDtos lists.
[**save_packet_from_stream**](VhfApi.md#save_packet_from_stream) | **POST** /vhf/upload | Saves VhfTransferFrame from binary stream
[**search_vhf_packets**](VhfApi.md#search_vhf_packets) | **GET** /vhf/packet/search | Finds vhf packet by query.


# **decode_packet**
> HTTPResponse decode_packet(format=format, pkt=pkt)

Decode a VHF packet.

This method utilize an hexa string to test decoding at server level.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
format = 'swig' # str | The format of the input data (optional) (default to swig)
pkt = 'none' # str | pkt: the packet in hexa {none}. (optional) (default to none)

try:
    # Decode a VHF packet.
    api_response = api_instance.decode_packet(format=format, pkt=pkt)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->decode_packet: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**| The format of the input data | [optional] [default to swig]
 **pkt** | **str**| pkt: the packet in hexa {none}. | [optional] [default to none]

### Return type

[**HTTPResponse**](HTTPResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_obsid_list**
> list[ObsIdApidCountDto] find_obsid_list(by, page=page, size=size, sort=sort)

Finds vhf obsid list filled from scheduler.

This method will search for obsid list.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
by = 'none' # str | by: the search pattern {none}.  List of accepted fields: apidAndTimeRange:[apid,ts,te]. (default to none)
page = 0 # int | page: the page number {0} (optional) (default to 0)
size = 1000 # int | size: the page size {1000} (optional) (default to 1000)
sort = 'packetObsid:ASC' # str | sort: the sort pattern {packetObsid:ASC} (optional) (default to packetObsid:ASC)

try:
    # Finds vhf obsid list filled from scheduler.
    api_response = api_instance.find_obsid_list(by, page=page, size=size, sort=sort)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->find_obsid_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none}.  List of accepted fields: apidAndTimeRange:[apid,ts,te]. | [default to none]
 **page** | **int**| page: the page number {0} | [optional] [default to 0]
 **size** | **int**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **str**| sort: the sort pattern {packetObsid:ASC} | [optional] [default to packetObsid:ASC]

### Return type

[**list[ObsIdApidCountDto]**](ObsIdApidCountDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_vhf_transfer_frames**
> VhfTransferFrameDto find_vhf_transfer_frames(frame)

Finds vhf transfer frame by ID.

This method will search for transfer frame.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
frame = 789 # int | id of the frame

try:
    # Finds vhf transfer frame by ID.
    api_response = api_instance.find_vhf_transfer_frames(frame)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->find_vhf_transfer_frames: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **frame** | **int**| id of the frame | 

### Return type

[**VhfTransferFrameDto**](VhfTransferFrameDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_vhf_packets**
> VhfBasePacket get_vhf_packets(hash, x_vhf_pkt_format=x_vhf_pkt_format)

Finds vhf packet by HASH.

This method will search for a VHF packet, in binary format.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
hash = 'hash_example' # str | hash of the packet
x_vhf_pkt_format = 'binary' # str | header parameter containing the requested output format: binary, decoded, ... (optional) (default to binary)

try:
    # Finds vhf packet by HASH.
    api_response = api_instance.get_vhf_packets(hash, x_vhf_pkt_format=x_vhf_pkt_format)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->get_vhf_packets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hash** | **str**| hash of the packet | 
 **x_vhf_pkt_format** | **str**| header parameter containing the requested output format: binary, decoded, ... | [optional] [default to binary]

### Return type

[**VhfBasePacket**](VhfBasePacket.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_vhf_counters**
> list[ObsIdApidCountDto] list_vhf_counters(apid=apid, obsid=obsid, tfrom=tfrom, tto=tto)

Finds a list of summary data (ObsIdApidCountDto).

This method allows to perform filtering on apids and obsid, also a time period can be selected

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
apid = 'all' # str | apid: the apid name {all}. (optional) (default to all)
obsid = 0 # int | obsid: the observation id number {0}; 0 means ignore it (optional) (default to 0)
tfrom = 789 # int | from: the beginning of the selected time interval {now-1h} (optional)
tto = 789 # int | to: the end of the selected time interval {now} (optional)

try:
    # Finds a list of summary data (ObsIdApidCountDto).
    api_response = api_instance.list_vhf_counters(apid=apid, obsid=obsid, tfrom=tfrom, tto=tto)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->list_vhf_counters: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apid** | **str**| apid: the apid name {all}. | [optional] [default to all]
 **obsid** | **int**| obsid: the observation id number {0}; 0 means ignore it | [optional] [default to 0]
 **tfrom** | **int**| from: the beginning of the selected time interval {now-1h} | [optional] 
 **tto** | **int**| to: the end of the selected time interval {now} | [optional] 

### Return type

[**list[ObsIdApidCountDto]**](ObsIdApidCountDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_vhf_raw_packets**
> list[RawPacketDto] list_vhf_raw_packets(by=by, page=page, size=size, sort=sort)

Finds a RawPacketDtos lists.

This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
by = 'none' # str | by: the search pattern {none}.  List of accepted fields: flag, insertionTime. (optional) (default to none)
page = 0 # int | page: the page number {0} (optional) (default to 0)
size = 1000 # int | size: the page size {1000} (optional) (default to 1000)
sort = 'insertionTime:DESC' # str | sort: the sort pattern {insertionTime:DESC} (optional) (default to insertionTime:DESC)

try:
    # Finds a RawPacketDtos lists.
    api_response = api_instance.list_vhf_raw_packets(by=by, page=page, size=size, sort=sort)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->list_vhf_raw_packets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none}.  List of accepted fields: flag, insertionTime. | [optional] [default to none]
 **page** | **int**| page: the page number {0} | [optional] [default to 0]
 **size** | **int**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **str**| sort: the sort pattern {insertionTime:DESC} | [optional] [default to insertionTime:DESC]

### Return type

[**list[RawPacketDto]**](RawPacketDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_vhf_station_status**
> list[VhfStationStatusDto] list_vhf_station_status(by=by, page=page, size=size, sort=sort)

Finds a VhfStationStatusDtos lists.

This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
by = 'none' # str | by: the search pattern {none}.  List of accepted fields: stationTime, idStation, doppler. (optional) (default to none)
page = 0 # int | page: the page number {0} (optional) (default to 0)
size = 1000 # int | size: the page size {1000} (optional) (default to 1000)
sort = 'idStation:ASC' # str | sort: the sort pattern {idStation:ASC} (optional) (default to idStation:ASC)

try:
    # Finds a VhfStationStatusDtos lists.
    api_response = api_instance.list_vhf_station_status(by=by, page=page, size=size, sort=sort)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->list_vhf_station_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none}.  List of accepted fields: stationTime, idStation, doppler. | [optional] [default to none]
 **page** | **int**| page: the page number {0} | [optional] [default to 0]
 **size** | **int**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **str**| sort: the sort pattern {idStation:ASC} | [optional] [default to idStation:ASC]

### Return type

[**list[VhfStationStatusDto]**](VhfStationStatusDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_vhf_station_status_count**
> list[VhfStationStatusCountDto] list_vhf_station_status_count(_from=_from, to=to)

Finds a VhfStationStatusCountDtos lists.

This method allows to perform search and sorting. Arguments: from,to.  The from and to parameters can also be provided as now and now-xh.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
_from = 'now-1h' # str | from: the start time range in which the packets should be selected. (optional) (default to now-1h)
to = 'now' # str | to: the end time range in which the packets should be selected. (optional) (default to now)

try:
    # Finds a VhfStationStatusCountDtos lists.
    api_response = api_instance.list_vhf_station_status_count(_from=_from, to=to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->list_vhf_station_status_count: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_from** | **str**| from: the start time range in which the packets should be selected. | [optional] [default to now-1h]
 **to** | **str**| to: the end time range in which the packets should be selected. | [optional] [default to now]

### Return type

[**list[VhfStationStatusCountDto]**](VhfStationStatusCountDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_vhf_transfer_frames**
> list[VhfTransferFrameDto] list_vhf_transfer_frames(by=by, page=page, size=size, sort=sort, dateformat=dateformat)

Finds a VhfTransferFrameDtos lists.

This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
by = 'none' # str | by: the search pattern {none}.  List of accepted fields: receptionTime, isFrameValid, isFrameDuplicate, stationId, apid. (optional) (default to none)
page = 0 # int | page: the page number {0} (optional) (default to 0)
size = 1000 # int | size: the page size {1000} (optional) (default to 1000)
sort = 'frameId:ASC' # str | sort: the sort pattern {frameId:ASC} (optional) (default to frameId:ASC)
dateformat = 'ms' # str | The format of the input time fields: e.g. yyyyMMdd'T'HHmmssz (optional) (default to ms)

try:
    # Finds a VhfTransferFrameDtos lists.
    api_response = api_instance.list_vhf_transfer_frames(by=by, page=page, size=size, sort=sort, dateformat=dateformat)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->list_vhf_transfer_frames: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none}.  List of accepted fields: receptionTime, isFrameValid, isFrameDuplicate, stationId, apid. | [optional] [default to none]
 **page** | **int**| page: the page number {0} | [optional] [default to 0]
 **size** | **int**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **str**| sort: the sort pattern {frameId:ASC} | [optional] [default to frameId:ASC]
 **dateformat** | **str**| The format of the input time fields: e.g. yyyyMMdd&#39;T&#39;HHmmssz | [optional] [default to ms]

### Return type

[**list[VhfTransferFrameDto]**](VhfTransferFrameDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **save_packet_from_stream**
> HTTPResponse save_packet_from_stream(format=format, stream=stream)

Saves VhfTransferFrame from binary stream

Upload a binary packet

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
format = 'binary' # str | The format of the input data (optional) (default to binary)
stream = '/path/to/file.txt' # file |  (optional)

try:
    # Saves VhfTransferFrame from binary stream
    api_response = api_instance.save_packet_from_stream(format=format, stream=stream)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->save_packet_from_stream: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**| The format of the input data | [optional] [default to binary]
 **stream** | **file**|  | [optional] 

### Return type

[**HTTPResponse**](HTTPResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_vhf_packets**
> VhfPacketSearchDto search_vhf_packets(by, page=page, size=size, sort=sort, x_vhf_pkt_format=x_vhf_pkt_format, dateformat=dateformat)

Finds vhf packet by query.

This method will search for a VHF packet, in binary format.

### Example
```python
from __future__ import print_function
import time
import vhfmgrapi
from vhfmgrapi.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vhfmgrapi.VhfApi()
by = 'none' # str | by: the search pattern {none}.  List of accepted fields: [apid,packetTime,insertionTime, obsid, apidname]. (default to none)
page = 0 # int | page: the page number {0} (optional) (default to 0)
size = 1000 # int | size: the page size {1000} (optional) (default to 1000)
sort = 'insertionTime:ASC' # str | sort: the sort pattern {insertionTime:ASC} (optional) (default to insertionTime:ASC)
x_vhf_pkt_format = 'binary' # str | header parameter containing the requested output format: binary, decoded, raw... (optional) (default to binary)
dateformat = 'yyyyMMdd'T'HHmmssz' # str | The format of the input time fields (optional) (default to yyyyMMdd'T'HHmmssz)

try:
    # Finds vhf packet by query.
    api_response = api_instance.search_vhf_packets(by, page=page, size=size, sort=sort, x_vhf_pkt_format=x_vhf_pkt_format, dateformat=dateformat)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VhfApi->search_vhf_packets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **str**| by: the search pattern {none}.  List of accepted fields: [apid,packetTime,insertionTime, obsid, apidname]. | [default to none]
 **page** | **int**| page: the page number {0} | [optional] [default to 0]
 **size** | **int**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **str**| sort: the sort pattern {insertionTime:ASC} | [optional] [default to insertionTime:ASC]
 **x_vhf_pkt_format** | **str**| header parameter containing the requested output format: binary, decoded, raw... | [optional] [default to binary]
 **dateformat** | **str**| The format of the input time fields | [optional] [default to yyyyMMdd&#39;T&#39;HHmmssz]

### Return type

[**VhfPacketSearchDto**](VhfPacketSearchDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

