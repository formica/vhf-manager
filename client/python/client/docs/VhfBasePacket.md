# VhfBasePacket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pktformat** | **str** |  | 
**pkttype** | **str** |  | 
**hash_id** | **str** |  | 
**insertion_time** | **int** |  | [optional] 
**pktsize** | **int** |  | 
**packet** | **str** | A string representation of the packet content (can be JSON or base64 string, or RAW for hexa string) | 
**uri** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


