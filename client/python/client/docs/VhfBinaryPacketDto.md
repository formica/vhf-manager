# VhfBinaryPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**binarypacket** | **str** | The byte array representation of input packet, this parameter is optional | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


