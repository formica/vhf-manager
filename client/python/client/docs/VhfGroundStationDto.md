# VhfGroundStationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**station_id** | **int** |  | [optional] 
**station_name** | **str** |  | [optional] 
**idstation** | **str** |  | [optional] 
**location** | **str** |  | [optional] 
**loc** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**macaddress** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


