# VhfPacketSearchDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**criteria** | **str** |  | 
**size** | **int** |  | 
**pkttype** | **str** |  | 
**packets** | [**list[VhfBasePacket]**](VhfBasePacket.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


