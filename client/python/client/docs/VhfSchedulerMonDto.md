# VhfSchedulerMonDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sched_job_id** | **str** |  | [optional] 
**insertion_time** | **datetime** |  | [optional] 
**update_time** | **datetime** |  | [optional] 
**sched_exec** | **int** |  | [optional] 
**sched_time_spent** | **int** |  | [optional] 
**since** | **int** |  | [optional] 
**pkt_num** | **int** |  | [optional] 
**exec_description** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


