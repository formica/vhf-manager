# VhfSchedulerStatusDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sched_name** | **str** |  | [optional] 
**sched_status** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


