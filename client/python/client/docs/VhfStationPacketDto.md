# VhfStationPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationid** | **str** |  | [optional] 
**softversion** | **str** |  | [optional] 
**data** | **str** |  | [optional] 
**encoding** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


