# VhfStationStatusCountDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_station** | **int** |  | [optional] 
**min_time** | **int** |  | [optional] 
**max_time** | **int** |  | [optional] 
**npackets** | **int** |  | [optional] 
**avg_corr_power** | **float** |  | [optional] 
**std_corr_power** | **float** |  | [optional] 
**avg_doppler** | **float** |  | [optional] 
**std_doppler** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


