# VhfStationStatusDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sstatus_id** | **int** |  | [optional] 
**id_station** | **int** |  | [optional] 
**channel** | **int** |  | [optional] 
**padding1** | **int** |  | [optional] 
**station_time** | **int** |  | [optional] 
**corr_power** | **int** |  | [optional] 
**padding2** | **int** |  | [optional] 
**doppler** | **float** |  | [optional] 
**dop_drift** | **float** |  | [optional] 
**srerror** | **float** |  | [optional] 
**reed_solomon_corr** | **int** |  | [optional] 
**ck_sum** | **int** |  | [optional] 
**padding3** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


