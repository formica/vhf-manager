# VhfTransferFrameDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**frame_id** | **int** |  | [optional] 
**reception_time** | **int** |  | [optional] 
**is_frame_valid** | **bool** |  | [optional] [default to False]
**is_frame_duplicate** | **bool** |  | [optional] [default to False]
**dup_frame_id** | **int** |  | [optional] 
**apid** | [**PacketApidDto**](PacketApidDto.md) |  | [optional] 
**station** | [**VhfGroundStationDto**](VhfGroundStationDto.md) |  | [optional] 
**frame_header** | [**FrameHeaderPacketDto**](FrameHeaderPacketDto.md) |  | [optional] 
**ccsds** | [**CcsdsPacketDto**](CcsdsPacketDto.md) |  | [optional] 
**header** | [**PrimaryHeaderPacketDto**](PrimaryHeaderPacketDto.md) |  | [optional] 
**station_status** | [**VhfStationStatusDto**](VhfStationStatusDto.md) |  | [optional] 
**packet** | [**VhfBinaryPacketDto**](VhfBinaryPacketDto.md) |  | [optional] 
**packet_json** | [**VhfDecodedPacketDto**](VhfDecodedPacketDto.md) |  | [optional] 
**binary_hash** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


