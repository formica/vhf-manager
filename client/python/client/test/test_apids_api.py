# coding: utf-8

"""
    VHF REST API setting in JAX-RS

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: andrea.formica@cern.ch
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import vhfmgrapi
from vhfmgrapi.api.apids_api import ApidsApi  # noqa: E501
from vhfmgrapi.rest import ApiException


class TestApidsApi(unittest.TestCase):
    """ApidsApi unit test stubs"""

    def setUp(self):
        self.api = vhfmgrapi.api.apids_api.ApidsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_create_packet_apid(self):
        """Test case for create_packet_apid

        Saves PacketApid  # noqa: E501
        """
        pass

    def test_find_packet_apids(self):
        """Test case for find_packet_apids

        Finds PacketApid by id  # noqa: E501
        """
        pass

    def test_list_packet_apids(self):
        """Test case for list_packet_apids

        Finds a packet APID list.  # noqa: E501
        """
        pass

    def test_update_packet_apids(self):
        """Test case for update_packet_apids

        Update PacketApid by id  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
