# coding: utf-8

"""
    VHF REST API setting in JAX-RS

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: andrea.formica@cern.ch
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import vhfmgrapi
from vhfmgrapi.api.vhf_api import VhfApi  # noqa: E501
from vhfmgrapi.rest import ApiException


class TestVhfApi(unittest.TestCase):
    """VhfApi unit test stubs"""

    def setUp(self):
        self.api = vhfmgrapi.api.vhf_api.VhfApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_decode_packet(self):
        """Test case for decode_packet

        Decode a VHF packet.  # noqa: E501
        """
        pass

    def test_find_obsid_list(self):
        """Test case for find_obsid_list

        Finds vhf obsid list filled from scheduler.  # noqa: E501
        """
        pass

    def test_find_vhf_transfer_frames(self):
        """Test case for find_vhf_transfer_frames

        Finds vhf transfer frame by ID.  # noqa: E501
        """
        pass

    def test_get_vhf_packets(self):
        """Test case for get_vhf_packets

        Finds vhf packet by HASH.  # noqa: E501
        """
        pass

    def test_list_vhf_counters(self):
        """Test case for list_vhf_counters

        Finds a list of summary data (ObsIdApidCountDto).  # noqa: E501
        """
        pass

    def test_list_vhf_raw_packets(self):
        """Test case for list_vhf_raw_packets

        Finds a RawPacketDtos lists.  # noqa: E501
        """
        pass

    def test_list_vhf_station_status(self):
        """Test case for list_vhf_station_status

        Finds a VhfStationStatusDtos lists.  # noqa: E501
        """
        pass

    def test_list_vhf_station_status_count(self):
        """Test case for list_vhf_station_status_count

        Finds a VhfStationStatusCountDtos lists.  # noqa: E501
        """
        pass

    def test_list_vhf_transfer_frames(self):
        """Test case for list_vhf_transfer_frames

        Finds a VhfTransferFrameDtos lists.  # noqa: E501
        """
        pass

    def test_save_packet_from_stream(self):
        """Test case for save_packet_from_stream

        Saves VhfTransferFrame from binary stream  # noqa: E501
        """
        pass

    def test_search_vhf_packets(self):
        """Test case for search_vhf_packets

        Finds vhf packet by query.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
