# coding: utf-8

"""
    VHF REST API setting in JAX-RS

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: andrea.formica@cern.ch
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import vhfmgrapi
from vhfmgrapi.models.vhf_transfer_frame_dto import VhfTransferFrameDto  # noqa: E501
from vhfmgrapi.rest import ApiException


class TestVhfTransferFrameDto(unittest.TestCase):
    """VhfTransferFrameDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testVhfTransferFrameDto(self):
        """Test VhfTransferFrameDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = vhfmgrapi.models.vhf_transfer_frame_dto.VhfTransferFrameDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
