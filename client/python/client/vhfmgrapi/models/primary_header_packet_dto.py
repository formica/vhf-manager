# coding: utf-8

"""
    VHF REST API setting in JAX-RS

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: andrea.formica@cern.ch
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class PrimaryHeaderPacketDto(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'ph_id': 'int',
        'packet_time': 'int',
        'packet_obsid': 'int'
    }

    attribute_map = {
        'ph_id': 'phId',
        'packet_time': 'packetTime',
        'packet_obsid': 'packetObsid'
    }

    def __init__(self, ph_id=None, packet_time=None, packet_obsid=None):  # noqa: E501
        """PrimaryHeaderPacketDto - a model defined in Swagger"""  # noqa: E501

        self._ph_id = None
        self._packet_time = None
        self._packet_obsid = None
        self.discriminator = None

        if ph_id is not None:
            self.ph_id = ph_id
        if packet_time is not None:
            self.packet_time = packet_time
        if packet_obsid is not None:
            self.packet_obsid = packet_obsid

    @property
    def ph_id(self):
        """Gets the ph_id of this PrimaryHeaderPacketDto.  # noqa: E501


        :return: The ph_id of this PrimaryHeaderPacketDto.  # noqa: E501
        :rtype: int
        """
        return self._ph_id

    @ph_id.setter
    def ph_id(self, ph_id):
        """Sets the ph_id of this PrimaryHeaderPacketDto.


        :param ph_id: The ph_id of this PrimaryHeaderPacketDto.  # noqa: E501
        :type: int
        """

        self._ph_id = ph_id

    @property
    def packet_time(self):
        """Gets the packet_time of this PrimaryHeaderPacketDto.  # noqa: E501


        :return: The packet_time of this PrimaryHeaderPacketDto.  # noqa: E501
        :rtype: int
        """
        return self._packet_time

    @packet_time.setter
    def packet_time(self, packet_time):
        """Sets the packet_time of this PrimaryHeaderPacketDto.


        :param packet_time: The packet_time of this PrimaryHeaderPacketDto.  # noqa: E501
        :type: int
        """

        self._packet_time = packet_time

    @property
    def packet_obsid(self):
        """Gets the packet_obsid of this PrimaryHeaderPacketDto.  # noqa: E501


        :return: The packet_obsid of this PrimaryHeaderPacketDto.  # noqa: E501
        :rtype: int
        """
        return self._packet_obsid

    @packet_obsid.setter
    def packet_obsid(self, packet_obsid):
        """Sets the packet_obsid of this PrimaryHeaderPacketDto.


        :param packet_obsid: The packet_obsid of this PrimaryHeaderPacketDto.  # noqa: E501
        :type: int
        """

        self._packet_obsid = packet_obsid

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PrimaryHeaderPacketDto, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PrimaryHeaderPacketDto):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
