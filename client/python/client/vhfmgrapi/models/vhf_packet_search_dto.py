# coding: utf-8

"""
    VHF REST API setting in JAX-RS

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1.0.0
    Contact: andrea.formica@cern.ch
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from vhfmgrapi.models.vhf_base_packet import VhfBasePacket  # noqa: F401,E501


class VhfPacketSearchDto(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'criteria': 'str',
        'size': 'int',
        'pkttype': 'str',
        'packets': 'list[VhfBasePacket]'
    }

    attribute_map = {
        'criteria': 'criteria',
        'size': 'size',
        'pkttype': 'pkttype',
        'packets': 'packets'
    }

    def __init__(self, criteria=None, size=None, pkttype=None, packets=None):  # noqa: E501
        """VhfPacketSearchDto - a model defined in Swagger"""  # noqa: E501

        self._criteria = None
        self._size = None
        self._pkttype = None
        self._packets = None
        self.discriminator = None

        self.criteria = criteria
        self.size = size
        self.pkttype = pkttype
        self.packets = packets

    @property
    def criteria(self):
        """Gets the criteria of this VhfPacketSearchDto.  # noqa: E501


        :return: The criteria of this VhfPacketSearchDto.  # noqa: E501
        :rtype: str
        """
        return self._criteria

    @criteria.setter
    def criteria(self, criteria):
        """Sets the criteria of this VhfPacketSearchDto.


        :param criteria: The criteria of this VhfPacketSearchDto.  # noqa: E501
        :type: str
        """
        if criteria is None:
            raise ValueError("Invalid value for `criteria`, must not be `None`")  # noqa: E501

        self._criteria = criteria

    @property
    def size(self):
        """Gets the size of this VhfPacketSearchDto.  # noqa: E501


        :return: The size of this VhfPacketSearchDto.  # noqa: E501
        :rtype: int
        """
        return self._size

    @size.setter
    def size(self, size):
        """Sets the size of this VhfPacketSearchDto.


        :param size: The size of this VhfPacketSearchDto.  # noqa: E501
        :type: int
        """
        if size is None:
            raise ValueError("Invalid value for `size`, must not be `None`")  # noqa: E501

        self._size = size

    @property
    def pkttype(self):
        """Gets the pkttype of this VhfPacketSearchDto.  # noqa: E501


        :return: The pkttype of this VhfPacketSearchDto.  # noqa: E501
        :rtype: str
        """
        return self._pkttype

    @pkttype.setter
    def pkttype(self, pkttype):
        """Sets the pkttype of this VhfPacketSearchDto.


        :param pkttype: The pkttype of this VhfPacketSearchDto.  # noqa: E501
        :type: str
        """
        if pkttype is None:
            raise ValueError("Invalid value for `pkttype`, must not be `None`")  # noqa: E501

        self._pkttype = pkttype

    @property
    def packets(self):
        """Gets the packets of this VhfPacketSearchDto.  # noqa: E501


        :return: The packets of this VhfPacketSearchDto.  # noqa: E501
        :rtype: list[VhfBasePacket]
        """
        return self._packets

    @packets.setter
    def packets(self, packets):
        """Sets the packets of this VhfPacketSearchDto.


        :param packets: The packets of this VhfPacketSearchDto.  # noqa: E501
        :type: list[VhfBasePacket]
        """
        if packets is None:
            raise ValueError("Invalid value for `packets`, must not be `None`")  # noqa: E501

        self._packets = packets

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(VhfPacketSearchDto, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, VhfPacketSearchDto):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
