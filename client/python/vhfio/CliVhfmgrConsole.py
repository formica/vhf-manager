'''
Created on Nov 24, 2017

@author: formica
'''

import cmd

import sys,os
import readline
import logging
import atexit
import json
# arguments parser
import argparse
from datetime import datetime
from svom.messaging import VhfDbIo
from vhfutils import *
from pip._vendor.pyparsing import empty

log = logging.getLogger( __name__ )
log.setLevel( logging.INFO )

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter( logging.Formatter( format ) )
log.addHandler( handler )

sys.path.append(os.path.join(sys.path[0],'..'))
historyFile = '.vhfconsole_hist'

class VhfConsoleUI(cmd.Cmd):
    """Simple command processor example."""
    cm = None
    prompt ='(Vhf): '
    homehist = os.getenv('CDMS_HISTORY_HOME', os.environ["HOME"])
    histfile = os.path.join( homehist, historyFile)
    host=None
    loc_parser=None
    import re
    rr = r"""
        ([<|>|:]+)  # match one of the symbols
    """
    rr = re.compile(rr, re.VERBOSE)
    log.info('creating an instance of console')

    def init_history(self, histfile):
        readline.parse_and_bind( "tab: complete" )
        readline.set_history_length( 100 )
        if hasattr( readline, "read_history_file" ):
            try:
                readline.read_history_file( histfile )
            except IOError:
                pass
            atexit.register( self.save_history, histfile )

    def save_history(self, histfile):
        print ('Saving history in %s' % histfile)
        readline.write_history_file( histfile )

    def set_host(self, url):
        self.host = url

    def get_args(self, line=None):
        argv=line.split()
        log.info(f'parse arguments {argv}')
        self.loc_parser = argparse.ArgumentParser(description="parser options for cli",add_help=False)
        group = self.loc_parser.add_mutually_exclusive_group()
        group.add_argument("-v", "--verbose", action="store_true")
        group.add_argument("-q", "--quiet", action="store_true")
        self.loc_parser.add_argument('cmd', nargs='?', default='iovs')
        self.loc_parser.add_argument('-h', '--help', action="store_true", help='show this help message')
        self.loc_parser.add_argument("-n", "--name", default='%', help="the name pattern for the search")
        self.loc_parser.add_argument("-I", "--id", help="the id number")
        self.loc_parser.add_argument("--params", nargs='+', help="the string containing k=v pairs for apids or stations creation")
        self.loc_parser.add_argument("--inpfile", help="the input file to upload")
        self.loc_parser.add_argument("--page", default="0", help="the page number.")
        self.loc_parser.add_argument("--size", default="100", help="the page size.")
        self.loc_parser.add_argument("--sort", default="name:ASC",help="the sort parameter (depend on the selection).")
        self.loc_parser.add_argument("-f", "--format", help="the output format, use <help> for details")
        return self.loc_parser.parse_args(argv)

    def do_connect(self,url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        self.cm = VhfDbIo(server_url=url)
        log.info(f'Connected to {url}')

    def do_ls(self, pattern):
        """ls <datatype> [-n name] [-I id] [other options: --size, --page, --sort, --format]
        Search for data collection of different kinds: apids, stations.
        datatype: apids, stations
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = None
        dname = None
        did = None
        log.info(f'ls uses pattern {pattern}')
        if pattern:
            args = self.get_args(pattern)
            if args.help:
                self.loc_parser.print_help()
                return
            cmd = args.cmd
            log.info (f'Searching {cmd}')
            if args.name:
                dname = args.name
            if args.id:
                did = args.id

            cdic = {}
            if cmd == 'apids':
                if dname is None:
                    dname = "%"
                cdic['name'] = dname
                if 'name' in args.sort:
                    args.sort = 'packetName:ASC'
                log.info(f'Search apids with args {args} and criteria {cdic}')
                out = self.cm.search_apid_info(page=args.page,size=args.size,sort=args.sort,**cdic)
            elif cmd == 'stations':
                if 'name' in args.sort:
                    args.sort = 'name:ASC'
                print(f'Using name {dname} and dic {cdic}')
                out = self.cm.search_stations(page=args.page,size=args.size,sort=args.sort,**cdic)
        else:
            log.info ('Search all apids')
            out = self.cm.search_apid_info()
        print(out)

    def do_decode(self, pattern):
        """decode [hexa string]
        Decode the input Hexadecimal string"""
        if pattern:
            print ("Decoding %s " % pattern)
        else:
            print ('Cannot decode empty string')
        out = self.cm.decode(pattern)
        print (out)

    def do_create(self, pattern):
        """create <datatype> -n xxx -I 5 --params a=v,b=w,...
        Create a new apid or ground station, using a series of k=val pairs, separated
        by commas
        """
        out = None
        if pattern:
            args = self.get_args(pattern)
            if args.help:
                self.loc_parser.print_help()
                return
            cmd = args.cmd
            log.info (f'Creating {cmd}')
            fmt = args.format
            tname = None
            if args.name:
                tname = args.name
            pdic = {}
            strlist = None
            if args.params:
                strlist = ' '.join(args.params)
                log.info(f'splitting arg {strlist}')
                pdic = dict(map(lambda x: x.split('='), strlist.split(',')))
            if cmd == 'apids':
                log.info(f'Create apid {tname} {args.id} {pdic}')
                if args.inpfile:
                    apid_dic = {}
                    with open(args.inpfile,'r') as f:
                        apid_dic = json.load(f)
                        for k in apid_dic.keys():
                            ap=apid_dic[k]
                            ap.pop('apid', None)
                            out = self.cm.create_apid(name=ap['packetName'], apid=k, **ap)
                else:
                    out = self.cm.create_apid(name=tname, apid=args.id, **pdic)
            elif cmd == 'stations':
                log.info(f'Create station {tname} {args.id} {pdic}')
                out = self.cm.create_station(name=tname, id=args.id, **pdic)
            else:
                print(f'Command {cmd} is not recognized in this context')

        else:
            log.info ('Cannot create object without arguments')
        print(f'Response is : {out}')

    def do_store(self, pattern):
        """store [file name, format string]
        Store packet using file: ex. store ../data/vhf/eclalertl1.txt.txt,hexa"""
        if pattern:
            print ("Storing %s " % pattern)
        else:
            print ('Cannot store empty string')
        out = self.cm.store(pattern)
        print (out)

    def do_update(self, pattern):
        """update <datatype> -n xxx -I 5 --params a=v,b=w,...
        Create a new apid or ground station, using a series of k=val pairs, separated
        by commas
        """
        out = None
        if pattern:
            args = self.get_args(pattern)
            if args.help:
                self.loc_parser.print_help()
                return
            cmd = args.cmd
            log.info (f'Creating {cmd}')
            fmt = args.format
            pdic = {}
            strlist = None
            if args.params:
                strlist = ' '.join(args.params)
                log.info(f'splitting arg {strlist}')
            pdic = dict(map(lambda x: x.split('='), strlist.split(',')))
            log.info(f'created arguments dictionary {pdic}')
            if cmd == 'apids':
                log.info(f'Update apid {args.id} {pdic}')
                out = self.cm.update_apid(apid=args.id, **pdic)
            elif cmd == 'stations':
                log.info(f'Update station {args.id} {pdic}')
            elif cmd == 'schedulers':
                log.info(f'Update scheduler status {pdic}')
                if pdic['status']:
                    out = self.cm.update_scheduler(name=args.id,status=pdic['status'])
                else:
                    log.error('Cannot update scheduler, parameter status is missing')
            else:
                print(f'Command {cmd} is not recognized in this context')

        else:
            log.info ('Cannot update object without arguments')
        print(f'Response is : {out}')

    def do_ls_vhf(self,line):
        """ls_vhf [by=param:pattern,sort=param:DESC[ASC], page=numpage, size=pagesize]
        List packets from VHF. The param can be [id,receptionTime,packetTime,apid,apidName,isFrameDuplicate,isFrameValid,stationId]"""
        out = self.cm.ls_vhf(line)
        print (out)

    def do_get_pkt(self,line):
        """get_pkt [hash=xxx,x-vhf-pkt-format=binary or decoded]
        Get the original packet from Svom satellite, either in binary or decoded format"""
        out = self.cm.get_pkt(line)
        print ('==========')
        print (out)

    def do_count_vhf(self,line):
        """count_vhf [apid=xxx,obsid>=yyy,tfrom=0,tto=someunixtime]
        Get the counters by obsID and APID """
        out = self.cm.count_vhf(line)
        print ('==========')
        print (out)

    def do_exit(self, line):
        return True
    def do_quit(self, line):
        return True
    def emptyline(self):
        pass

    def preloop(self):
        self.init_history(self.histfile)

    def postloop(self):
        print

if __name__ == '__main__':
        # Parse arguments
    parser = argparse.ArgumentParser(description='Crest browser.')
    parser.add_argument('--host', default='localhost',
                        help='Host of the Crest service (default: svomtest.svom.fr)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: crestapi)')
    parser.add_argument('--port', default='8090',
                        help='Port of the Crest service (default: 8090)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    args = parser.parse_args()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/{3}".format(prot,args.host,args.port,args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST']=host
    ui = VhfConsoleUI()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info("Activating socks on localhost:3129 ; if you want another address please set CDMS_SOCKS_HOST and _PORT env vars")
        ui.socks()

    ui.cmdloop()
