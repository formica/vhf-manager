'''
Created on Nov 24, 2017

@author: formica
'''

import sys, os
import readline
import logging
import atexit
import json
# arguments parser
import argparse
from datetime import datetime
from svom.messaging import VhfDbIo
from vhfutils import *
from pip._vendor.pyparsing import empty

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter(logging.Formatter(format))
log.addHandler(handler)

sys.path.append(os.path.join(sys.path[0], '..'))
historyFile = '.vhfconsole_hist'


class VhfCli(object):

    def __init__(self, server_url=None):
        if server_url is None:
            server_url = 'http://localhost:8080/api'

        cm = None
        self.host = server_url
        loc_parser = None
        import re
        rr = r"""
            ([<|>|:]+)  # match one of the symbols
        """
        rr = re.compile(rr, re.VERBOSE)

    def set_host(self, url):
        self.host = url

    def set_parser(self, prs):
        self.loc_parser = prs

    def do_connect(self, url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        self.cm = VhfDbIo(server_url=url, max_tries=1, use_tokens=True)
        log.info(f'Connected to {url}')

    def do_ls(self, args=None):
        """ls <datatype> [-n name] [-I id] [other options: --size, --page, --sort, --format]
        Search for data collection of different kinds: apids, stations.
        datatype: apids, stations
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = None
        dname = None
        did = None
        jsonout = None
        log.info(f'ls uses args {args}')
        if args:
            if 'type' in args:
                cmd = args['type']
            if 'name' in args:
                dname = args['name']
            if 'id' in args:
                did = args['id']
            fields = []
            if 'fields' in args:
                log.debug('fields is %s' % args.get('fields'))
                if 'none' == args['fields']:
                    fields = []
                else:
                    fields = args.get('fields').split(',')
            if 'json' in args:
                jsonout = args['json']
            cdic = {}
            if cmd == 'apids':
                if dname is None:
                    dname = "%"
                cdic['name'] = dname
                if 'name' in args['sort']:
                    args['sort'] = 'packetName:ASC'
                log.info(f'Search apids with args {args} and criteria {cdic}')
                out = self.cm.search_apid_info(page=args['page'], size=args['size'], sort=args['sort'], **cdic)
            elif cmd == 'stations':
                if 'name' in args['sort']:
                    args['sort'] = 'name:ASC'
                print(f'Using name {dname} and dic {cdic}')
                out = self.cm.search_stations(page=args['page'], size=args['size'], sort=args['sort'], **cdic)
        else:
            log.info('Search all apids')
            out = self.cm.search_apids()
        server_print(out, format=fields, jsonfile=jsonout)

    def do_decode(self, pattern):
        """decode [hexa string]
        Decode the input Hexadecimal string"""
        if pattern:
            print("Decoding %s " % pattern)
        else:
            print('Cannot decode empty string')
        out = self.cm.decode(pattern)
        print(out)

    def do_create(self, args):
        """create <datatype> -n xxx -I 5 --params a=v,b=w,...
        Create a new apid or ground station, using a series of k=val pairs, separated
        by commas
        """
        out = None
        if args:
            if 'type' in args:
                cmd = args['type']
            log.info(f'Creating object for type {cmd}')
            pdic = {}
            # Check if the user needs hints on fields
            if 'params' in args and args['params'] is not None:
                pararr = args['params'].split(',')
                for par in pararr:
                    kv = par.split('=')
                    pdic[kv[0]] = kv[1]
            if cmd == 'apids':
                log.info(f'Create (or update) apids from file')
                if 'inpfile' in args:
                    apid_dic = {}
                    apidsfile = args['inpfile']
                    apiddic = read_definitions(apidsfile)
                    if args['cnes']:
                        for key in apiddic:
                            log.info(f'Found key {key} in apids file')
                            apid_num = apiddic[key]['identifiers']['CcsdsApid']
                            descr = apiddic[key]['components']['TmPktHeader']['CcsdsTmHeadr']['CcsdsApid']['values'][
                                apid_num]
                            new_apid = {'packetName': key, 'packetDescription': descr, 'instrument': ''}
                            if 'ancillary_data' in apiddic[key]:
                                log.info(f'Found apid ancillary data for {apid_num}')
                                ancillary_data = apiddic[key]['ancillary_data']
                                new_apid['expectedPackets'] = ancillary_data['buffer_size']
                                new_apid['priority'] = ancillary_data['priority']
                            componentsdic = apiddic[key]['components']
                            if 'InstrumentMode' in componentsdic:
                                log.info(f'Found instrument mode for {apid_num}')
                                new_apid['instrument'] += 'InstrumentMode,'
                            if 'SatAttitudeQ0' in componentsdic or 'SatAttitudeLCQ0' in componentsdic or \
                                    'QuatSatAtt' in componentsdic:
                                log.info(f'Found Attitude mode for {apid_num}')
                                new_apid['instrument'] += 'Attitude,'
                            if 'SatPositionX' in componentsdic or 'SatPositionLon':
                                log.info(f'Found Position mode for {apid_num}')
                                new_apid['instrument'] += 'Position,'
                            resp = self.cm.get_apid_info(apid_num)
                            if 'code' in resp and resp['code'] >= 400:
                                log.error(f'Error in search {apid_num}...try to insert')
                                out = self.cm.create_apid(name=key, apid=apid_num, **new_apid)
                            else:
                                log.info(f'Update apid {apid_num} using {new_apid}')
                                out = self.cm.update_apid(apid=apid_num, **new_apid)
                    else:

                        log.info(f'Found list of apids of size {apiddic["size"]}')
                        apid_list = apiddic['resources']
                        for apid_entry in apid_list:
                            key = apid_entry['name']
                            apid_num = apid_entry['apid']
                            new_apid = apid_entry
                            del new_apid['apid']
                            resp = self.cm.get_apid_info(apid_num)
                            if 'code' in resp and resp['code'] >= 400:
                                log.error(f'Error in search {apid_num}...try to insert')
                                del new_apid['name']
                                out = self.cm.create_apid(name=key, apid=apid_num, **new_apid)
                            else:
                                log.info(f'Update apid {apid_num} using {new_apid}')
                                out = self.cm.update_apid(apid=apid_num, **new_apid)


            elif cmd == 'stations':
                log.info(f'Create station...')
                if 'inpfile' in args:
                    stat_list = []
                    station_file = args['inpfile']
                    stat_set = read_definitions(station_file)
                    log.info(f'Found list of stations of size {stat_set["size"]}')
                    stat_list = stat_set['resources']
                    for stat in stat_list:
                        print(f'Found CNES station {stat}')
                        idkey = 'id'
                        namkey = 'name'
                        altkey = 'altitude'
                        lonkey = 'longitude'
                        latkey = 'latitude'
                        counkey = 'country'
                        lockey = 'location'
                        minelkey = 'minElevationAngle'

                        if 'ID' in stat.keys():
                            idkey = 'ID'
                            namkey = 'Name'
                            altkey = 'Altitude'
                            lonkey = 'Longitude'
                            latkey = 'Latitude'
                            counkey = 'Country'
                            lockey = 'Location'
                            minelkey = 'MinElevationAngle'

                        statid = int(stat[idkey], 16)
                        if isinstance(stat[altkey], str) and 'TBC' in stat[altkey]:
                            stat[altkey] = '0.0'
                        alt = float(stat[altkey])
                        pdic = {'id': stat[idkey], 'altitude': alt, 'longitude': float(stat[lonkey]),
                                'latitude': float(stat[latkey]), 'country': stat[counkey],
                                'location': stat[lockey], 'minElevationAngle': float(stat[minelkey])}
                        out = self.cm.create_station(name=stat[namkey], station_id=statid, **pdic)
            else:
                print(f'Command {cmd} is not recognized in this context')

        else:
            log.info('Cannot create object without arguments')
        print(f'Response is : {out}')

    def do_store(self, pattern):
        """store [file name, format string]
        Store packet using file: ex. store ../data/vhf/eclalertl1.txt.txt,hexa"""
        if pattern:
            print("Storing %s " % pattern)
        else:
            print('Cannot store empty string')
        out = self.cm.store(pattern)
        print(out)

    def do_update(self, pattern):
        """update <datatype> -n xxx -I 5 --params a=v,b=w,...
        Create a new apid or ground station, using a series of k=val pairs, separated
        by commas
        """
        out = None
        if pattern:
            args = self.get_args(pattern)
            if args.help:
                self.loc_parser.print_help()
                return
            cmd = args.cmd
            log.info(f'Creating {cmd}')
            fmt = args.format
            pdic = {}
            strlist = None
            if args.params:
                strlist = ' '.join(args.params)
                log.info(f'splitting arg {strlist}')
            pdic = dict(map(lambda x: x.split('='), strlist.split(',')))
            log.info(f'created arguments dictionary {pdic}')
            if cmd == 'apids':
                log.info(f'Update apid {args.id} {pdic}')
                out = self.cm.update_apid(apid=args.id, **pdic)
            elif cmd == 'stations':
                log.info(f'Update station {args.id} {pdic}')
            elif cmd == 'schedulers':
                log.info(f'Update scheduler status {pdic}')
                if pdic['status']:
                    out = self.cm.update_scheduler(name=args.id, status=pdic['status'])
                else:
                    log.error('Cannot update scheduler, parameter status is missing')
            else:
                print(f'Command {cmd} is not recognized in this context')

        else:
            log.info('Cannot update object without arguments')
        print(f'Response is : {out}')

    def do_ls_vhf(self, line):
        """ls_vhf [by=param:pattern,sort=param:DESC[ASC], page=numpage, size=pagesize]
        List packets from VHF. The param can be [id,receptionTime,packetTime,apid,apidName,isFrameDuplicate,isFrameValid,stationId]"""
        out = self.cm.ls_vhf(line)
        print(out)

    def do_get_pkt(self, line):
        """get_pkt [hash=xxx,x-vhf-pkt-format=binary or decoded]
        Get the original packet from Svom satellite, either in binary or decoded format"""
        out = self.cm.get_pkt(line)
        print('==========')
        print(out)

    def do_count_vhf(self, line):
        """count_vhf [apid=xxx,obsid>=yyy,tfrom=0,tto=someunixtime]
        Get the counters by obsID and APID """
        out = self.cm.count_vhf(line)
        print('==========')
        print(out)

    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            # Configuration

            # Remove this if you don't plan to "deactivate" the proxy later
            #        default_socket = socket.socket
            # Set up a proxy
            #            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            print('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            print('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Vhf browser.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['ls', 'create'], default='ls')
    parser.add_argument('--type', choices=['apids', 'stations'], default='apids')
    parser.add_argument('--host', default='svom-vhfmgr.lal.in2p3.fr',
                        help='Host of the VhfMgr service (default: svom-vhfmgr.lal.in2p3.fr)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api) Ignored in this client.')
    parser.add_argument('--port', default='443',
                        help='Port of the AlignMon service (default: 443)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument('--cnes', action="store_true", help='use cnes db format from decoder')
    parser.add_argument("--params", help="the string containing k=v pairs for tags or global tags creation")
    parser.add_argument("--cut", help="additional selection parameters. e.g. since>1000,until<2000")
    parser.add_argument("--inpfile", help="the input file to upload")
    parser.add_argument("--json", help="the output json file to write")
    parser.add_argument("--since", help="the since time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00")
    parser.add_argument("--until", help="the until time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00")
    parser.add_argument("--page", default="0", help="the page number.")
    parser.add_argument("--size", default="100", help="the page size.")
    parser.add_argument("--sort", default="none", help="the sort parameter (depend on the selection).")
    parser.add_argument("-f", "--fields", default='none',
                        help="the list of fields to show, separated with a comma. Use -f help to get the available fields.")
    parser.add_argument("-H", "--header", default="BLOB", help="set header request ...")

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    prot = "http"
    if args.ssl:
        prot = "https"
    #    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    host = "{0}://{1}/{2}".format(prot, args.host, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host
    ui = VhfCli()
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    ui.set_parser(parser)
    argsdic = vars(args)
    if args.fields:
        if args.fields == 'help':
            if args.cmd == 'tags':
                print(f'Fields for {args.cmd} are {tagfieldsdictypes.keys()}')
            else:
                print('not available for this command')
            sys.exit()

    if args.params:
        if args.params == 'help':
            print_help(args.type)
            sys.exit()
    if args.cmd in ['ls']:
        log.info(f'Launch ls command on {args.type}')
        ui.do_ls(argsdic)
    elif args.cmd in ['create']:
        log.info(f'Launch create command on {args.type}')
        ui.do_create(argsdic)
    else:
        log.info(f'Cannot launch command {args.cmd}')
