'''
Created on Nov 24, 2017

@author: formica
'''

import sys,os
import logging
import atexit
from beautifultable import BeautifulTable
from termcolor import colored
import json

apidsfieldsdicheader = {
    'apid': {'key': '{apid:8s}', 'val': 'Apid', 'field': '{apid:8d}', 'type': 'int'},
    'packetName': {'key': '{packetName:30s}', 'val': 'Name', 'field': '{packetName:30s}', 'type': 'str'},
    'packetDescription': {'key': '{packetDescription:50s}', 'val': 'Description', 'field': '{packetDescription:50s}', 'type': 'str'},
    'className': {'key': '{className:10s}', 'val': 'Class', 'field': '{className:10s}', 'type': 'str'},
    'instrument': {'key': '{instrument:10s}', 'val': 'Instrument', 'field': '{instrument:10s}', 'type': 'str'},
    'productName': {'key': '{productName:20s}', 'val': 'Product', 'field': '{productName:20s}', 'type': 'str'},
    'category': {'key': '{category:10s}', 'val': 'Category', 'field': '{category:10s}', 'type': 'str'},
    'expectedPackets': {'key': '{expectedPackets:8s}', 'val': 'Expected', 'field': '{expectedPackets:8d}', 'type': 'int'},
    'priority': {'key': '{priority:8s}', 'val': 'Priority', 'field': '{priority:8d}',
                        'type': 'int'},
}
def server_print(cdata, format=[], jsonfile=None):
    if cdata is None or 'size' not in cdata.keys():
        print('Cannot find results to print')
        return

    if jsonfile:
        print(f'Dump json data to output file {jsonfile}')
        with open(jsonfile, 'w') as local_file:
            json.dump(cdata, local_file)

    size=cdata['size']
    dataarr = []
    if 'resources' in cdata:
        print(f'Retrieved {size} lines')
        dataarr = cdata['resources']
    if (cdata['format'] == 'PacketApidSetDto'):
        prettyprint(format,apidsfieldsdicheader,dataarr)
    else:
        prettyprint(format=[], headerdic={}, cdata=dataarr)

def prettyprint(format, headerdic, cdata, mw=190):
    try:
        x = BeautifulTable(maxwidth=mw)
        x.set_style(BeautifulTable.STYLE_MARKDOWN)
        fmt = format
        if len(format) == 0:
            fmt = headerdic.keys()
        colheadfmt = []
        for a in fmt:
            colheadfmt.append(colored(a, 'red'))
        if cdata is None or len(cdata) == 0:
            nx = BeautifulTable(maxwidth=mw)
            nx.rows.append([i for i in colheadfmt])
            print(nx)
        else:
            firstrow = True
            for xt in cdata:
                line = []
                for k in fmt:
                    line.append(xt[k])
                if len(line) == 0:
                    if firstrow:
                        fmt = []
                        for k in xt.keys():
                            colheadfmt.append(colored(k, 'green'))
                            fmt.append(k)
                            line.append(xt[k])
                    firstrow = False
                x.rows.append(line)
            x.columns.header = colheadfmt
            x.columns.alignment = BeautifulTable.ALIGN_RIGHT
            print(x)
    except Exception as e:
        print(f'Cannot print using format {format} and header dic {headerdic}: {e}')


def dprint(format, headerdic, cdata):
    if len(format) == 0:
        format = headerdic.keys()
    headerfmtstr = ' '.join([headerdic[k]['key'] for k in format])
    # print(f'The format string is {headerfmtstr}')
    headic = {}
    for k in format:
        headic[k] = headerdic[k]['val']
        # print(f'Using format {headic[k]} for {k}')
    print(headerfmtstr.format(**headic))
    # print('Use format %s' % format)
    fmtstr = ' '.join([headerdic[k]['field'] for k in format])
    # print('Format string %s'%fmtstr)
    for xt in cdata:
        adic = {}
        for k in format:
            if xt[k] is None:
                if headerdic[k]['type'] in ['int', 'long', 'float']:  # True
                    xt[k] = -1
                else:
                    xt[k] = ' - '
            adic[k] = xt[k]
        # print('Use dictionary %s'%adic)
        print(fmtstr.format(**adic))

def read_definitions(filename):
    import json
    with open(filename) as f_in:
        return (json.load(f_in))
