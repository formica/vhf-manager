'''
Created on Nov 24, 2017

@author: formica
'''

import cmd

import sys,os
import readline
import logging
import atexit
# arguments parser
import argparse

log = logging.getLogger('VhfConsole')
log.setLevel( logging.DEBUG )

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter( logging.Formatter( format ) )
log.addHandler( handler )

sys.path.append(os.path.join(sys.path[0],'..'))
historyFile = '.vhfconsole_hist'

from VhfManagementApi import VhfConsole

class VhfConsoleUI(cmd.Cmd):
    """Simple command processor example."""
    cm = VhfConsole()
    prompt ='(Vhf): '
    homehist = os.getenv('CDMS_HISTORY_HOME', os.environ["HOME"])
    histfile = os.path.join( homehist, historyFile)
    log.info('creating an instance of console')

    def init_history(self, histfile):
        readline.parse_and_bind( "tab: complete" )
        readline.set_history_length( 100 )
        if hasattr( readline, "read_history_file" ):
            try:
                readline.read_history_file( histfile )
            except IOError:
                pass
            atexit.register( self.save_history, histfile )

    def setHost(self,url):
        self.cm.connect(url)

    def save_history(self, histfile):
        print ('Saving history in %s' % histfile)
        readline.write_history_file( histfile )

    def do_connect(self,url):
        """connect [url]
        Use the url for server connections"""
        out = self.cm.connect(url)
        print(out)

    def do_ls_apids(self, pattern):
        """ls_apids [name pattern]
        Search for apids which contain the input pattern"""
        if pattern:
            print ("Searching %s " % pattern)
        else:
            print ('Search all apids')
        out = self.cm.ls_apids(pattern)
        print (out)

    def do_ls_stations(self, pattern):
        """ls_stations [name pattern]
        Search for stations which contain the input pattern"""
        if pattern:
            print ("Searching %s " % pattern)
        else:
            print ('Search all stations')
        out = self.cm.ls_stations(pattern)
        print (out)

    def do_decode(self, pattern):
        """decode [hexa string]
        Decode the input Hexadecimal string"""
        if pattern:
            print ("Decoding %s " % pattern)
        else:
            print ('Cannot decode empty string')
        out = self.cm.decode(pattern)
        print (out)

    def do_store(self, pattern):
        """store [file name, format string]
        Store packet using file: ex. store ../data/vhf/eclalertl1.txt.txt,hexa"""
        if pattern:
            print ("Storing %s " % pattern)
        else:
            print ('Cannot store empty string')
        out = self.cm.store(pattern)
        print (out)

    def do_create_apid(self,line):
        """createapid [apid:the apid,packet_name:the name, .....]
        Save the APID."""
        out = self.cm.create_apid(line)
        print (out)

    def do_create_apid_list(self,line):
        """create_apid_list [file name]
        Save the APID from file."""
        out = self.cm.create_apid_list(line)
        print (out)

    def do_create_station(self,line):
        """create_station [station_id=the id,station_name=the name, idstation=the code.....]
        Save the Station."""
        out = self.cm.create_station(line)
        print (out)

    def do_create_station_list(self,line):
        """do_create_station_list [file name]
        Save the Ground Stations from file."""
        out = self.cm.create_station_list(line)
        print (out)

    def do_ls_vhf(self,line):
        """ls_vhf [by=param:pattern,sort=param:DESC[ASC], page=numpage, size=pagesize]
        List packets from VHF. The param can be [id,receptionTime,packetTime,apid,apidName,isFrameDuplicate,isFrameValid,stationId]"""
        out = self.cm.ls_vhf(line)
        print (out)

    def do_get_pkt(self,line):
        """get_pkt [hash=xxx,x-vhf-pkt-format=binary or decoded]
        Get the original packet from Svom satellite, either in binary or decoded format"""
        out = self.cm.get_pkt(line)
        print ('==========')
        print (out)

    def do_count_vhf(self,line):
        """count_vhf [apid=xxx,obsid>=yyy,tfrom=0,tto=someunixtime]
        Get the counters by obsID and APID """
        out = self.cm.count_vhf(line)
        print ('==========')
        print (out)

    def do_exit(self, line):
        return True
    def do_quit(self, line):
        return True
    def emptyline(self):
        pass

    def preloop(self):
        self.init_history(self.histfile)

    def postloop(self):
        print

if __name__ == '__main__':

        # Parse arguments
    parser = argparse.ArgumentParser(description='VHF packets browser.')
    parser.add_argument('--host', default='svomtest.svom.fr',
                        help='Host of the VHF manager service (default: svomtest.svom.fr)')
    parser.add_argument('--port', default='9091',
                        help='Port of the VHF manager service (default: 9091)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    args = parser.parse_args()

    prot = "http"
    if args.ssl:
        prot = "https"
    host = "{0}://{1}:{2}/api".format(prot,args.host,args.port)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST']=host
    ui = VhfConsoleUI()
    log.info('Start application')
    ui.setHost(host)
    if args.socks:
        log.info("Activating socks on localhost:3129 ; if you want another address please set CDMS_SOCKS_HOST and _PORT env vars")
        ui.socks()

    ui.cmdloop()
