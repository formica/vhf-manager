import sys
import os
import re
import time

# python 2 and python 3 compatibility library
from six import iteritems
from bisect import bisect_left

import json
import logging
import datetime
import csv


sys.path.append(os.path.join(sys.path[0],'..'))

from vhfmgrapi.api import ApidsApi, StationsApi, VhfApi
from vhfmgrapi.models import VhfGroundStationDto, PacketApidDto, ObsIdApidCountDto, HTTPResponse
from vhfmgrapi import ApiClient
from vhfmgrapi.rest import ApiException
from vhfmgrapi.configuration import Configuration

import cdms_profile

MAX_OBJTYPE_LENGTH=1000

DefaultSnapshot = 2500000000

module_log = logging.getLogger('VhfConsole')

def printDic(adiction):
    fmt = ''
    #print('Try to print %s' % adiction)
    for (k, v) in adiction.items():
        if hasattr(v,"to_dict"):
            fmt = '{0}\n ==> {1}: {2}'.format(fmt,k,printDic(v.to_dict()))
        elif isinstance(v,dict):
            fmt = '{0}\n ==> {1}: {2}'.format(fmt,k,printDic(v))
        else:
            fmt = '{0} {1}={2} '.format(fmt,k,v)
    return fmt

class VhfManagementAbstract(object):

    '''Abstract class providing list of methods for vhf data management'''
    def createApid(self):
        pass
        raise NotImplementedError('Method not implemented here')
    def listApids(self):
        pass
        raise NotImplementedError('Method not implemented here')
    def findApid(self):
        pass
        raise NotImplementedError('Method not implemented here')
    def createStation(self):
        pass
        raise NotImplementedError('Method not implemented here')
    def listStations(self):
        pass
        raise NotImplementedError('Method not implemented here')
    def listVhf(self):
        pass
        raise NotImplementedError('Method not implemented here')
    def countVhf(self):
        pass
        raise NotImplementedError('Method not implemented here')
    def decodeHexa(self):
        pass
        raise NotImplementedError('Method not implemented here')

    def activatesocks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = int(os.getenv('CDMS_SOCKS_PORT', 3129))
        try:
            import socket
            import socks # you need to install pysocks (use the command: pip install pysocks)
# Configuration

# Remove this if you don't plan to "deactivate" the proxy later
#        default_socket = socket.socket
# Set up a proxy
#            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            print ('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST,SOCKS5_PROXY_PORT))
        except Exception as inst:
            print(type(inst))
            print(inst)
            print ('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST,SOCKS5_PROXY_PORT))

class ApidDtoWrap():

    def __init__(self, tdto):
        self._tdto = tdto

    def to_str(self):
        msg = "%s: %s; %s %s %s %s %s %s" % (self._tdto.apid, self._tdto.packet_name, self._tdto.packet_description,self._tdto.class_name,self._tdto.instrument,self._tdto.category,self._tdto.priority,self._tdto.expected_packets)
        return msg

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

class StationDtoWrap():

    def __init__(self, tdto):
        self._tdto = tdto

    def to_str(self):
        msg = "%s: %s; %s %s %s %s %s" % (self._tdto.station_id, self._tdto.station_name, self._tdto.idstation,self._tdto.location,self._tdto.loc, self._tdto.description,self._tdto.macaddress)
        return msg

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

class VhfPacketDtoWrap():

    def __init__(self, tdto, data):
        self._tdto = tdto

    def to_str(self):
        msg = "%s: %s; %s %s => " % (self._tdto.hash_id, self._tdto.pktformat, self._tdto.discriminator,self._tdto.insertion_time)
        ro = self._tdto.get_real_child_model(data)
        msg = "%s %s" % (msg, ro.to_str())
        return msg

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

class VhfTransferFrameDtoWrap():
    '''
            'frame_id': 'int',
            'reception_time': 'datetime',
            'is_frame_valid': 'bool',
            'is_frame_duplicate': 'bool',
            'dup_frame_id': 'int',
            'apid': 'PacketApidDto',
            'station': 'VhfGroundStationDto',
            'frame_header': 'FrameHeaderPacketDto',
            'ccsds': 'CcsdsPacketDto',
            'decoded': 'VhfDecodedPacketDto',
    '''
    def __init__(self, tdto):
        self._tdto = tdto

    def to_str(self):
#        import yaml
#        msg = yaml.dump(self._tdto.to_dict(), default_flow_style=False)
        msg='vhf: '
        dobjstr = printDic(self._tdto.to_dict())
        msg = '{0} {1}\n===================================='.format(msg,dobjstr)
#        msg = "%s: %s; %s %s %s" % (self._tdto.frame_id, self._tdto.reception_time, self._tdto.is_frame_valid,self._tdto.is_frame_duplicate,self._tdto.dup_frame_id)
#        msg = "%s\n station     : %s; %s %s" % (msg,self._tdto.station.station_id, self._tdto.station.station_name, self._tdto.station.idstation)
#        msg = "%s\n ccsds       : %s; %s %s %s" % (msg,self._tdto.ccsds.ccsds_id, self._tdto.ccsds.ccsds_apid, self._tdto.ccsds.ccsds_plength,self._tdto.ccsds.ccsds_counter)
        return msg

class VhfPacketDtoWrap():
    '''
            'hash_id': 'string',
            'packet': 'string',
    '''
    def __init__(self, tdto):
        self._tdto = tdto

    def to_str(self):
#        import yaml
#        msg = yaml.dump(self._tdto.to_dict(), default_flow_style=False)
        msg='packet: '
        dobjstr = printDic(self._tdto.to_dict())
        msg = '{0} {1}\n===================================='.format(msg,dobjstr)
#        msg = "%s: %s; %s %s %s" % (self._tdto.frame_id, self._tdto.reception_time, self._tdto.is_frame_valid,self._tdto.is_frame_duplicate,self._tdto.dup_frame_id)
#        msg = "%s\n station     : %s; %s %s" % (msg,self._tdto.station.station_id, self._tdto.station.station_name, self._tdto.station.idstation)
#        msg = "%s\n ccsds       : %s; %s %s %s" % (msg,self._tdto.ccsds.ccsds_id, self._tdto.ccsds.ccsds_apid, self._tdto.ccsds.ccsds_plength,self._tdto.ccsds.ccsds_counter)
        return msg

class VhfObsCountDtoWrap():
    '''
            'obsid': 'integer',
            'apid': 'integer',
            ....
    '''
    def __init__(self, tdto):
        self._tdto = tdto

    def to_str(self):
#        import yaml
#        msg = yaml.dump(self._tdto.to_dict(), default_flow_style=False)
        msg='vhf counter by obsID and APID: '
        dobjstr = printDic(self._tdto.to_dict())
        msg = '{0} {1}\n===================================='.format(msg,dobjstr)
#        msg = "%s: %s; %s %s %s" % (self._tdto.frame_id, self._tdto.reception_time, self._tdto.is_frame_valid,self._tdto.is_frame_duplicate,self._tdto.dup_frame_id)
#        msg = "%s\n station     : %s; %s %s" % (msg,self._tdto.station.station_id, self._tdto.station.station_name, self._tdto.station.idstation)
#        msg = "%s\n ccsds       : %s; %s %s %s" % (msg,self._tdto.ccsds.ccsds_id, self._tdto.ccsds.ccsds_apid, self._tdto.ccsds.ccsds_plength,self._tdto.ccsds.ccsds_counter)
        return msg

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()


class CMApi(VhfManagementAbstract):

    def __init__(self):
        print ('Initialise CMApi...')
        self._sockson=False
        self.urlsvc = os.getenv('CDMS_HOST', 'http://svomtest.svom.fr:9091/api')
        if os.getenv('CDMS_SOCKS', False) and not self._sockson:
            self.activatesocks()
            self._sockson = True
        self._config = Configuration()
        self._config.host = self.urlsvc
        self._config.ssl_ca_cert = os.getenv('SSL_CA_CERT', None)
        self._config.cert_file = os.getenv('SSL_CLIENT_CERT', None)
        self._config.key_file = os.getenv('SSL_CLIENT_KEY', None)
        self.api_client = ApiClient(self._config)
        self._mlog = logging.getLogger('VhfConsole.api.cm')
        self._mlog.info('Use configuration for server: %s %s ' % (self.urlsvc,self._config.ssl_ca_cert))
        self._mlog.info('Finished init of CMApi')

    def socks(self):
        self.activatesocks()
        self._sockson = True

    def connectServer(self,url):
        self.urlsvc = url
        self._config.host = self.urlsvc
        self.api_client = ApiClient(self._config)

    def connectServerWithConfig(self,config):
        self._mlog.info('Change ApiClient configuration in CMApi: %s ' % config)
        self.api_client = ApiClient(config)


    def listStations(self,stationpattern):

        api_instance = StationsApi(self.api_client)
        try:
            searchby='idstation:{0}'.format(stationpattern)
            api_response = api_instance.list_vhf_ground_stations(by=searchby)
            return api_response
        except ApiException as e:
            print ("Exception when calling StationsApi->list_vhf_ground_stations: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def findStation(self,id_num):
        api_instance = StationsApi(self.api_client)
        try:
            api_response = api_instance.find_vhf_ground_stations(id_num)
            return api_response
        except ApiException as e:
            print ("Exception when calling StationsApi->find_vhf_ground_stations: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def listApids(self,apidpattern):
        api_instance = ApidsApi(self.api_client)
        try:
            searchby='packet_name:{0}'.format(apidpattern)
            api_response = api_instance.list_packet_apids(by=searchby)
            return api_response
        except ApiException as e:
            print ("Exception when calling ApidsApi->list_packet_apids: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def listVhf(self,pattern):
        args = []
        res = None
        api_instance = VhfApi(self.api_client)
        if ',' in pattern:
            args = pattern.split(',')
        try:
            self._mlog.info('list vhf packets using %s' % args)
            inpdic = { 'by' : 'isFrameValid:true','sort' : 'receptionTime:DESC', 'size' : '10', 'page' : '0' }
            for ar in args:
                (k,v) = (ar.split('=')[0],ar.split('=')[1])
                inpdic[k] = v
                self._mlog.info('Fill dictionary with key=%s and value=%s' % (k,v))
            self._mlog.info('  : input dictionary %s' % inpdic)
            api_response = api_instance.list_vhf_transfer_frames(by=inpdic['by'],sort=inpdic['sort'],page=inpdic['page'],size=inpdic['size'])
            return api_response
        except ApiException as e:
            print ("Exception when calling VhfApi->list_vhf_transfer_frames: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def countVhf(self,pattern):
        args = []
        res = None
        api_instance = VhfApi(self.api_client)
        if ',' in pattern:
            args = pattern.split(',')
        try:
            self._mlog.info('list obsid counts for vhf packets using %s' % args)
            inpdic = { 'tfrom' : '0','tto' : '1644101904000', 'apid' : 'all', 'obsid' : '0' }
            for ar in args:
                (k,v) = (ar.split('=')[0],ar.split('=')[1])
                inpdic[k] = v
                self._mlog.info('Fill dictionary with key=%s and value=%s' % (k,v))
            self._mlog.info('  : input dictionary %s' % inpdic)
            api_response = api_instance.list_vhf_counters(tfrom=inpdic['tfrom'],tto=inpdic['tto'],apid=inpdic['apid'],obsid=inpdic['obsid'])
            return api_response
        except ApiException as e:
            print ("Exception when calling VhfApi->list_vhf_counters: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def getPacket(self,pattern):
        args = []
        res = None
        api_instance = VhfApi(self.api_client)
        if ',' in pattern:
            args = pattern.split(',')
        try:
            self._mlog.info('get vhf packet using args %s' % args)
            inpdic = { 'hash' : 'none','x_vhf_pkt_format' : 'decoded' }
            for ar in args:
                (k,v) = (ar.split('=')[0],ar.split('=')[1])
                inpdic[k] = v
                self._mlog.info('Fill dictionary with key=%s and value=%s' % (k,v))
            self._mlog.info('  : input dictionary %s' % inpdic)
            api_response = api_instance.get_vhf_packets(hash=inpdic['hash'],x_vhf_pkt_format=inpdic['x_vhf_pkt_format'])
            #self._mlog.info(' Received response %s' % api_response)
            return api_response
        except ApiException as e:
            print ("Exception when calling VhfApi->get_vhf_packets: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def findApid(self,apidid):
        api_instance = ApidsApi(self.api_client)
        try:
            api_response = api_instance.find_packet_apids(apidid)
            return api_response
        except ApiException as e:
            print ("Exception when calling ApidsApi->find_packet_apids: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def createApidsV1(self,**kwargs):
        all_params = [ 'apid', 'packet_name','packet_description', 'class_name', 'instrument', 'category', 'expected_packets', 'priority' ]
        apidparams = {}
        params = locals()
        for key, val in params['kwargs'].items():
            self._mlog.info('createApidsV1: use params %s %s' % (key,val))
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method select_iovs" % key
                )
            apidparams[key] = val
        del params['kwargs']

        self._mlog .debug('Creating apid dto %s %s ' % (apidparams['apid'],apidparams))
        try:
            apid = self.findApid(params['apid'])
            self._mlog .debug('Apid found: %s ...' % (apid.packet_name))
            return apid
        except Exception as e:
            self._mlog .debug('Apid with id: %s does not exists...creating it now....' % apidparams['apid'])

        api_instance = ApidsApi(self.api_client)
        try:
            self._mlog .debug('create dto: %s ...' % (apidparams))
            dto = PacketApidDto(**apidparams)
            #msg = ('dto resource %s ' ) % (dto.to_dict())
            self._mlog.info('Create %s ' % dto)
            api_response = api_instance.create_packet_apid(dto)
            return api_response
        except ApiException as e:
            print ("Exception when calling ApidsApi->create_packet_apid: %s\n" % e)
            if e.status == 303: # the global tag already exists....
                return e.body
            raise

    def createApids(self,apid_num,packet_name,**kwargs):
        all_params = [ 'packet_description', 'class_name', 'instrument', 'category', 'expected_packets', 'priority' ]
        params = locals()
        for key, val in params['kwargs'].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method select_iovs" % key
                )
            params[key] = val
        del params['kwargs']
        apid_params = { 'class_name' : 'none', 'instrument' : 'none', 'packet_description' : 'none', 'category' : 'none', 'priority' : -1, 'expected_packets': 1 }
        apid_params['apid'] = apid_num
        apid_params['packet_name'] = packet_name

        if 'packet_description' in params:
            apid_params['packet_description'] = params['packet_description']
        if 'class_name' in params:
            apid_params['class_name'] = params['class_name']
        if 'instrument' in params:
            apid_params['instrument'] = params['instrument']
        if 'category' in params:
            apid_params['category'] = params['category']
        if 'expected_packets' in params:
            apid_params['expected_packets'] = params['expected_packets']
        if 'priority' in params:
            apid_params['priority'] = params['priority']

        self._mlog .debug('Creating apid dto %s %s ' % (apid_num,apid_params))
        try:
            apid = self.findApid(apid_params['apid'])
            self._mlog .debug('Apid found: %s ...' % (apid.packet_name))
            return apid
        except Exception as e:
            self._mlog .debug('Apid with id: %s does not exists...creating it now....' % apid_num)

        api_instance = ApidsApi(self.api_client)
        try:
            dto = PacketApidDto(apid=int(apid_num),packet_name=packet_name,packet_description=apid_params['packet_description'], class_name=apid_params['class_name'], instrument=apid_params['instrument'], category=apid_params['category'])
            #msg = ('dto resource %s ' ) % (dto.to_dict())
            self._mlog.info('Create %s ' % dto)
            api_response = api_instance.create_packet_apid(dto)
            return api_response
        except ApiException as e:
            print ("Exception when calling ApidsApi->create_packet_apid: %s\n" % e)
            if e.status == 303: # the global tag already exists....
                return e.body
            raise

    def createStations(self,id_num,station_name,idstat,**kwargs):
        all_params = [ 'description', 'location', 'loc', 'macaddress' ]

        params = locals()
        for key, val in params['kwargs'].items():
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method select_iovs" % key
                )
            params[key] = val
        del params['kwargs']
        stid_params = { 'description' : 'none', 'location' : 'none', 'loc' : 'none', 'macaddress' : 'none' }

        stid_params['station_id'] = id_num
        stid_params['station_name'] = station_name
        stid_params['idstation'] = idstat

        if 'description' in params:
            stid_params['description'] = params['description']
        if 'location' in params:
            stid_params['location'] = params['location']
        if 'loc' in params:
            stid_params['loc'] = params['loc']
        if 'macaddress' in params:
            stid_params['macaddress'] = params['macaddress']
        self._mlog .debug('Creating station dto %s %s ' % (id_num,stid_params))
        try:
            stid = self.findStation(stid_params['station_id'])
            self._mlog .debug('Station id found: %s ...' % (stid.station_name))
            return stid
        except Exception as e:
            self._mlog .debug('Station with id: %s does not exists...creating it now....' % id_num)

        api_instance = StationsApi(self.api_client)
        try:
            ###station_id=None, station_name=None, idstation=None, location=None, loc=None, description=None, macaddress=None
            dto = VhfGroundStationDto(station_id=int(id_num),station_name=station_name,idstation=idstat,description=stid_params['description'], location=stid_params['location'], loc=stid_params['loc'], macaddress=stid_params['macaddress'])
            #msg = ('dto resource %s ' ) % (dto.to_dict())
            self._mlog.info('Storing dto %s ' % dto)
            api_response = api_instance.create_vhf_ground_stations(dto)
            return api_response
        except ApiException as e:
            print ("Exception when calling StationsApi->create_vhf_ground_stations: %s\n" % e)
            if e.status == 303: # the station already exists....
                return e.body
            raise

    def decodeHexa(self,hexastring):
        # test decoding
        api_instance = VhfApi(self.api_client)
        try:
            formatargs='swig'
            hs = hexastring
            if ',' in hexastring:
                args = hexastring.split(',')
                formatargs = args[1]
                hs = args[0]
            self._mlog.info('Decoding hexa %s using format %s' % (hs,formatargs))
            api_response = api_instance.decode_packet(pkt=hs,format=formatargs)
            return api_response
        except ApiException as e:
            print ("Exception when calling VhfApi->decode_packet: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

    def storeVhf(self,fname,format):
        # test decoding
        api_instance = VhfApi(self.api_client)
        try:
            formatargs='hexa'
            if format is not None:
                formatargs=format
            self._mlog.info('Store packet from file %s using format %s' % (fname,formatargs))
            api_response = api_instance.save_packet_from_stream(stream=fname,format=formatargs)
            return api_response
        except ApiException as e:
            print ("Exception when calling VhfApi->save_packet_from_stream: %s\n" % e)
        except Exception as inst:
            print(type(inst))
            print(inst)
        return None

class Info(dict):
    """
    This class extends a dictionary with a format string for its representation.
    It is used as the return value for the inspection commands, such that one
    can both query for the inspected object's information:

      res = tool.ls( '/a' )
      print res['name']

    as well as obtain a standard printout of the information:

      print res

    """
    def __init__( self, fmt = None ):
        super( Info, self ).__init__()
        self.format = fmt

    def __str__( self ):
        if self.format is None:
            return super( Info, self ).__str__()
        else:
            return self.format % self

    def setformat(self,fmt):
        self.format = fmt

    def format(self):
        return self.format

class InfoList(list):
    """
    This class extends a list with a custom string representation.
    """
    def __str__( self ):
        return '\n'.join( [ str(i) for i in self ] )

    def __repr__( self ):
        return str( self )




class VhfConsole(CMApi):
    def __init__(self):
        print ('Initialise VhfConsole and the management API...')
        self._log = logging.getLogger('VhfConsole.api')
        self._log.info('Initialized logger')
        CMApi.__init__(self)

    def connect(self,url):
        self.connectServer(url)
        return 'Use connection to %s' % url

    def ls_apids(self,pattern):
        res = self.listApids(pattern)
        wrapres = []
        if res is not None:
            for x in res:
                wrapit = ApidDtoWrap(x)
                wrapres.append(wrapit)
        return InfoList(wrapres)

    def create_apid(self,bodyargs):
        args = bodyargs.split(',')
        dtodic = { 'apid' : args[0].split('=')[1] , 'packet_name' : args[1].split('=')[1]}
        self._log.info('creating apid ....%s ' % dtodic['packet_name'])
        try:
            self._log.info('creating apid ....%s' % dtodic['packet_name'])
            inpdic = { 'class_name' : 'none', 'instrument' : 'none', 'packet_description' : 'none', 'category' : 'none', 'priority' : -1, 'expected_packets': 1 }
            for ar in args:
                (k,v) = (ar.split('=')[0],ar.split('=')[1])
                inpdic[k] = v
                self._log.info('Fill dictionary with key=%s and value=%s' % (k,v))
            inpdic['apid'] = int(dtodic['apid'])
            inpdic['packet_name'] = dtodic['packet_name']
            self.createApidsV1(**inpdic)
        except:
            print('Exception in creating apid')

        return self.ls_apids(dtodic['packet_name'])

    def create_apid_list(self, fname):
        self._log.info('creating apids from file %s ' % fname)
        if 'json' in fname:
            try:
                with open(fname, 'r') as jsfile:
                    for row in jsfile:
                        self._log.info('reading %s' % (row))
                        self._log.info('    create apid with %s' % (row))
                        apiddic = json.loads(row)
                        self._log.info('    create dictionary from json %s' % (apiddic))
                        pdic = {}
                        refmap = PacketApidDto().attribute_map
                        for k,v in refmap.items():
                            self._log.info('    clean attribute %s %s'% (k,v))
                            if k not in apiddic.keys():
                                self._log.info('    convert key %s into %s for value %s %s'% (k,refmap[k],v, apiddic[v]))
                                pdic[k] = apiddic[v]
                            else:
                                pdic[k] = apiddic[k]
                        self._log.info('Use dictionary %s '% (pdic))
                        self.createApidsV1(**pdic)
            except Exception as e:
                print('Exception in creating apid list %s' % e)
        else:
            try:
                with open(fname, 'r') as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=':')
                    for row in spamreader:
                        self._log.info('reading %s' % (row))
                        self._log.info('    create apid with id %s' % (row[1].strip()))
                        apel = []
                        apel.append(row[1].strip())
                        apel.append(row[0].strip())
                        self._log.info('    use %s' % (','.join(apel)))
                        self.create_apid('apid=%s,packet_name=%s' % (apel[0],apel[1]))
            except:
                print('Exception in creating apid list')

        return self.ls_apids('%')


    def ls_stations(self,pattern):
        res = self.listStations(pattern)
        wrapres = []
        if res is not None:
            for x in res:
                wrapit = StationDtoWrap(x)
                wrapres.append(wrapit)
        return InfoList(wrapres)

    def ls_vhf(self,bodyargs):
        res = self.listVhf(bodyargs)
        wrapres = []
        if res is not None:
            for x in res:
                #print('Found %s' % x.to_dict())
                wrapit = VhfTransferFrameDtoWrap(x)
                wrapres.append(wrapit.to_str())
        wrapres.append('Retrived a total of %s vhf packets from db' % len(res))
        return InfoList(wrapres)

    def count_vhf(self,bodyargs):
        res = self.countVhf(bodyargs)
        wrapres = []
        if res is not None:
            for x in res:
                #print('Found %s' % x.to_dict())
                wrapit = VhfCountDtoWrap(x)
                wrapres.append(wrapit.to_str())
        wrapres.append('Retrived a total of %s obsID/APID counters from db' % len(res))
        return InfoList(wrapres)


    def get_pkt(self,bodyargs):
        res = self.getPacket(bodyargs)
        wrapres = []
        if res is not None:
            wrapit = VhfPacketDtoWrap(res)
            wrapres.append(wrapit)
        return InfoList(wrapres)

    def decode(self,pattern):
        res = self.decodeHexa(pattern)
        wrapres = []
        if res is not None:
            print('Found result %s' % res)
            wrapres.append(res)
        return InfoList(wrapres)

    def store(self,pattern):
        args=[pattern, 'hexa']
        if ',' in pattern:
            args = pattern.split(',')
        res = self.storeVhf(args[0],args[1])
        wrapres = []
        if res is not None:
            print('Found result %s' % res)
            wrapit = VhfTransferFrameDtoWrap(res)
            wrapres.append(wrapit)
        return InfoList(wrapres)

    def create_station(self,bodyargs):
        '''
                'station_id': 'int',
                'station_name': 'str',
                'idstation': 'str',
                'location': 'str',
                'loc': 'str',
                'description': 'str',
                'macaddress': 'str'
        '''
        args = bodyargs.split(',')
        dtodic = { 'station_id' : args[0].split('=')[1] , 'station_name' : args[1].split('=')[1], 'idstation' : args[2].split('=')[1]}
        self._log.info('creating station ....%s ' % dtodic['station_name'])
        try:
            self._log.info('creating station ....%s' % dtodic['station_name'])
            inpdic = { 'location' : 'none', 'loc' : 'none', 'description' : 'none', 'macaddress' : 'none' }
            for ar in args:
                (k,v) = (ar.split('=')[0],ar.split('=')[1])
                inpdic[k] = v
                self._log.info('Fill dictionary with key=%s and value=%s' % (k,v))
            self.createStations(inpdic['station_id'],inpdic['station_name'], inpdic['idstation'], location=inpdic['location'], loc=inpdic['loc'], description=inpdic['description'], macaddress=inpdic['macaddress'])
        except:
            print('Exception in creating station')

        return self.ls_stations(inpdic['idstation'])

    def create_station_list(self, fname):
        self._log.info('creating stations from file %s ' % fname)
        try:
            with open(fname, 'r') as csvfile:
                spamreader = csv.reader(csvfile, delimiter=',')
                irow=0
                header_keys=[]
                for row in spamreader:
                    if irow==0:
                        header_keys = row
                        self._log.info('header is %s' % (header_keys))
                        irow=irow+1
                    else:
                        self._log.info('reading %s' % (row))
                        self._log.info('    create station with id %s' % (row[0].strip()))
                        bodyargs=''
                        ii=0
                        for el in row:
                            arg='%s=%s' % (header_keys[ii],el.strip())
                            ii=ii+1
                            if len(bodyargs)>0:
                                bodyargs='%s,%s' % (bodyargs,arg)
                            else:
                                bodyargs='%s' % (arg)
                        self._log.info('    use %s' % (bodyargs))
                        self.create_station(bodyargs)
        except:
            print('Exception in creating station list')

        return self.ls_stations('%')


##cmapi.readPayloadFromFile('test.json')
