'''
Created on Nov 24, 2017

@author: formica
'''

import cmd

import sys,os
import readline
import logging
import atexit


log = logging.getLogger('VhfManagerClient')
log.setLevel( logging.DEBUG )

handler = logging.StreamHandler()
format = "%(levelname)s:%(name)s: %(message)s"
handler.setFormatter( logging.Formatter( format ) )
log.addHandler( handler )

sys.path.append(os.path.join(sys.path[0],'..'))

from VhfManagementApi import VhfConsole,CMApi

homedir = os.getenv('CDMS_HOME', os.environ["HOME"])

apidsdata = homedir+"/data/apids/apids.txt"
stationsdata = homedir+"/data/stations/stations.txt"

cm = VhfConsole()
print('=========')
print ('Create apids and dump them')
cm.create_apid_list(apidsdata)
resp = cm.ls_apids('%')
print ('Apids list %s '%resp)
print('=========')
print ('Create stations and dump them')
cm.create_station_list(stationsdata)
resp = cm.ls_stations('%')
print ('Stations list %s '%resp)
