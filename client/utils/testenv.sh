#export CERT_DIR=/etc/docker/certs.d/svomtest.svom.fr:5543
#export REG_PORT=5543
#export CERT_DIR=$HOME/etc/x509
#export SVOM_PW=xxx
export SSL_CLIENT_KEY=/home/formica/svom-ca/svomtest.svom.fr.ssl/STSVOMT1.key.pem
export SSL_CLIENT_CERT=/home/formica/svom-ca/svomtest.svom.fr.ssl/STSVOMT1.cert.pem
export SSL_CA_CERT=/home/formica/svom-ca/svomtest.svom.fr.ssl/svomtest_svom_fr-bundle.pem
export CDMS_HOST=https://svomtest.svom.fr:6443/api
