
import os
import sys
from VhfManagementApi import CMApi, ApidDtoWrap, StationDtoWrap
from vhfmgrapi.configuration import Configuration

cm = CMApi()

def init_api():
    config = Configuration()
    config.host = 'http://svomtest.svom.fr:9091/api'
    cm.connectServerWithConfig(config)

def test_ls_apids():
    res = cm.listApids('')
    if res is not None:
        for x in res:
            wrapit = ApidDtoWrap(x)
            print('Found : %s ' % wrapit)

def test_ls_stations():
    res = cm.listStations('')
    if res is not None:
        for x in res:
            wrapit = StationDtoWrap(x)
            print('Found : %s ' % wrapit)


init_api()
test_ls_apids()
test_ls_stations()
