
import os
import sys

def convertHexaFile(fname, outname):
    blines=[]
    with open(fname,'r') as f:
        line = f.readline()
        print(line)
        if line is not None:
            bline = bytes.fromhex(line)
            blines.append(bline)
    with open(outname,'wb') as fo:
        for al in blines:
            fo.write(al)


convertHexaFile('../data/vhf/vhfhexa.txt','../data/vhf/vhfbin.txt')
