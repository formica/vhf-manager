#!/bin/sh

## -Dlogging.config=/data/logs/logback.xml
echo "Setting JAVA_OPTS from file javaopts.properties"
joptfile=./javaopts.properties
echo "use opt : "
cat $joptfile
if [ -e $joptfile ]; then
   export JAVA_OPTS=
   while read line; do JAVA_OPTS="$JAVA_OPTS -D$line"; done < $joptfile
fi
if [ -e ./create-properties.sh ]; then
### . ./create-properties.sh
   echo "Skip properties creation for secrets"
fi
if [ -z "$vhfmgr_dir" ]; then
   vhfmgr_dir=$PWD/vhfmgrApi-web/build/libs
fi 

echo "$USER is starting server with JAVA_OPTS : $JAVA_OPTS from user directory $PWD"
if [ x"$1" = x"" ]; then
    sh -c "java $JAVA_OPTS -jar ${vhfmgr_dir}/vhfmgr.war 2>>/tmp/err.log"
else
    sh -c "$@"
fi
