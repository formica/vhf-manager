package io.swagger.client.api

import io.swagger.client.model._
import com.typesafe.config.ConfigFactory

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.core.structure.PopulationBuilder

import java.io.File

import scala.collection.mutable

class StationsApiSimulation extends Simulation {

    def getCurrentDirectory = new File("").getAbsolutePath
    def userDataDirectory = getCurrentDirectory + "/src/gatling/resources/data"

    // basic test setup
    val configName = System.getProperty("testConfig", "baseline")
    val config = ConfigFactory.load(configName).withFallback(ConfigFactory.load("default"))
    val hostName = config.getString("performance.hostName")
    val durationSeconds = config.getInt("performance.durationSeconds")
    val rampUpSeconds = config.getInt("performance.rampUpSeconds")
    val rampDownSeconds = config.getInt("performance.rampDownSeconds")
    val authentication = config.getString("performance.authorizationHeader")
    val acceptHeader = config.getString("performance.acceptType")
    val contentTypeHeader = config.getString("performance.contentType")
    val rateMultiplier = config.getDouble("performance.rateMultiplier")
    val instanceMultiplier = config.getDouble("performance.instanceMultiplier")

    // global assertion data
    val globalResponseTimeMinLTE = config.getInt("performance.global.assertions.responseTime.min.lte")
    val globalResponseTimeMinGTE = config.getInt("performance.global.assertions.responseTime.min.gte")
    val globalResponseTimeMaxLTE = config.getInt("performance.global.assertions.responseTime.max.lte")
    val globalResponseTimeMaxGTE = config.getInt("performance.global.assertions.responseTime.max.gte")
    val globalResponseTimeMeanLTE = config.getInt("performance.global.assertions.responseTime.mean.lte")
    val globalResponseTimeMeanGTE = config.getInt("performance.global.assertions.responseTime.mean.gte")
    val globalResponseTimeFailedRequestsPercentLTE = config.getDouble("performance.global.assertions.failedRequests.percent.lte")
    val globalResponseTimeFailedRequestsPercentGTE = config.getDouble("performance.global.assertions.failedRequests.percent.gte")
    val globalResponseTimeSuccessfulRequestsPercentLTE = config.getDouble("performance.global.assertions.successfulRequests.percent.lte")
    val globalResponseTimeSuccessfulRequestsPercentGTE = config.getDouble("performance.global.assertions.successfulRequests.percent.gte")

// Setup http protocol configuration
    val httpConf = http
        .baseURL(hostName)
        .doNotTrackHeader("1")
        .acceptLanguageHeader("en-US,en;q=0.5")
        .acceptEncodingHeader("gzip, deflate")
        .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
        .acceptHeader(acceptHeader)
        .contentTypeHeader(contentTypeHeader)

    // set authorization header if it has been modified from config
    if(!authentication.equals("~MANUAL_ENTRY")){
        httpConf.authorizationHeader(authentication)
    }

    // Setup all the operations per second for the test to ultimately be generated from configs
    val createVhfGroundStationsPerSecond = config.getDouble("performance.operationsPerSecond.createVhfGroundStations") * rateMultiplier * instanceMultiplier
    val findVhfGroundStationsPerSecond = config.getDouble("performance.operationsPerSecond.findVhfGroundStations") * rateMultiplier * instanceMultiplier
    val listVhfGroundStationsPerSecond = config.getDouble("performance.operationsPerSecond.listVhfGroundStations") * rateMultiplier * instanceMultiplier

    val scenarioBuilders: mutable.MutableList[PopulationBuilder] = new mutable.MutableList[PopulationBuilder]()

    // Set up CSV feeders
    val createVhfGroundStationsBodyFeeder = csv(userDataDirectory + File.separator + "createVhfGroundStations-bodyParams.csv", escapeChar = '\\').random
    val findVhfGroundStationsPATHFeeder = csv(userDataDirectory + File.separator + "findVhfGroundStations-pathParams.csv").random
    val listVhfGroundStationsQUERYFeeder = csv(userDataDirectory + File.separator + "listVhfGroundStations-queryParams.csv").random

    // Setup all scenarios

    
    val scncreateVhfGroundStations = scenario("createVhfGroundStationsSimulation")
        .feed(createVhfGroundStationsBodyFeeder)
        .exec(http("createVhfGroundStations")
        .httpRequest("POST","/stations")
        .body(StringBody(VhfGroundStationDto.toStringBody("${stationId}","${ipAddress}","${location}","${thread}")))
        )

    // Run scncreateVhfGroundStations with warm up and reach a constant rate for entire duration
    scenarioBuilders += scncreateVhfGroundStations.inject(
        rampUsersPerSec(1) to(createVhfGroundStationsPerSecond) during(rampUpSeconds),
        constantUsersPerSec(createVhfGroundStationsPerSecond) during(durationSeconds),
        rampUsersPerSec(createVhfGroundStationsPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scnfindVhfGroundStations = scenario("findVhfGroundStationsSimulation")
        .feed(findVhfGroundStationsPATHFeeder)
        .exec(http("findVhfGroundStations")
        .httpRequest("GET","/stations/${id}")
)

    // Run scnfindVhfGroundStations with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnfindVhfGroundStations.inject(
        rampUsersPerSec(1) to(findVhfGroundStationsPerSecond) during(rampUpSeconds),
        constantUsersPerSec(findVhfGroundStationsPerSecond) during(durationSeconds),
        rampUsersPerSec(findVhfGroundStationsPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scnlistVhfGroundStations = scenario("listVhfGroundStationsSimulation")
        .feed(listVhfGroundStationsQUERYFeeder)
        .exec(http("listVhfGroundStations")
        .httpRequest("GET","/stations")
        .queryParam("by","${by}")
        .queryParam("sort","${sort}")
        .queryParam("size","${size}")
        .queryParam("page","${page}")
)

    // Run scnlistVhfGroundStations with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnlistVhfGroundStations.inject(
        rampUsersPerSec(1) to(listVhfGroundStationsPerSecond) during(rampUpSeconds),
        constantUsersPerSec(listVhfGroundStationsPerSecond) during(durationSeconds),
        rampUsersPerSec(listVhfGroundStationsPerSecond) to(1) during(rampDownSeconds)
    )

    setUp(
        scenarioBuilders.toList
    ).protocols(httpConf).assertions(
        global.responseTime.min.lte(globalResponseTimeMinLTE),
        global.responseTime.min.gte(globalResponseTimeMinGTE),
        global.responseTime.max.lte(globalResponseTimeMaxLTE),
        global.responseTime.max.gte(globalResponseTimeMaxGTE),
        global.responseTime.mean.lte(globalResponseTimeMeanLTE),
        global.responseTime.mean.gte(globalResponseTimeMeanGTE),
        global.failedRequests.percent.lte(globalResponseTimeFailedRequestsPercentLTE),
        global.failedRequests.percent.gte(globalResponseTimeFailedRequestsPercentGTE),
        global.successfulRequests.percent.lte(globalResponseTimeSuccessfulRequestsPercentLTE),
        global.successfulRequests.percent.gte(globalResponseTimeSuccessfulRequestsPercentGTE)
    )
}
