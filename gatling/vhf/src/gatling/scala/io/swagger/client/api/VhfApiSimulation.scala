package io.swagger.client.api

import io.swagger.client.model._
import com.typesafe.config.ConfigFactory

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.core.structure.PopulationBuilder

import java.io.File

import scala.collection.mutable

class VhfApiSimulation extends Simulation {

    def getCurrentDirectory = new File("").getAbsolutePath
    def userDataDirectory = getCurrentDirectory + "/src/gatling/resources/data"

    // basic test setup
    val configName = System.getProperty("testConfig", "baseline")
    val config = ConfigFactory.load(configName).withFallback(ConfigFactory.load("default"))
    val durationSeconds = config.getInt("performance.durationSeconds")
    val rampUpSeconds = config.getInt("performance.rampUpSeconds")
    val rampDownSeconds = config.getInt("performance.rampDownSeconds")
    val authentication = config.getString("performance.authorizationHeader")
    val acceptHeader = config.getString("performance.acceptType")
    val contentTypeHeader = config.getString("performance.contentType")
    val rateMultiplier = config.getDouble("performance.rateMultiplier")
    val instanceMultiplier = config.getDouble("performance.instanceMultiplier")

    // global assertion data
    val globalResponseTimeMinLTE = config.getInt("performance.global.assertions.responseTime.min.lte")
    val globalResponseTimeMinGTE = config.getInt("performance.global.assertions.responseTime.min.gte")
    val globalResponseTimeMaxLTE = config.getInt("performance.global.assertions.responseTime.max.lte")
    val globalResponseTimeMaxGTE = config.getInt("performance.global.assertions.responseTime.max.gte")
    val globalResponseTimeMeanLTE = config.getInt("performance.global.assertions.responseTime.mean.lte")
    val globalResponseTimeMeanGTE = config.getInt("performance.global.assertions.responseTime.mean.gte")
    val globalResponseTimeFailedRequestsPercentLTE = config.getDouble("performance.global.assertions.failedRequests.percent.lte")
    val globalResponseTimeFailedRequestsPercentGTE = config.getDouble("performance.global.assertions.failedRequests.percent.gte")
    val globalResponseTimeSuccessfulRequestsPercentLTE = config.getDouble("performance.global.assertions.successfulRequests.percent.lte")
    val globalResponseTimeSuccessfulRequestsPercentGTE = config.getDouble("performance.global.assertions.successfulRequests.percent.gte")

// Setup http protocol configuration
    val httpConf = http
        .baseURL("http://localhost:8080/vhfmgr/v1/")
        .doNotTrackHeader("1")
        .acceptLanguageHeader("en-US,en;q=0.5")
        .acceptEncodingHeader("gzip, deflate")
        .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
        .acceptHeader(acceptHeader)
        .contentTypeHeader(contentTypeHeader)

    // set authorization header if it has been modified from config
    if(!authentication.equals("~MANUAL_ENTRY")){
        httpConf.authorizationHeader(authentication)
    }

    // Setup all the operations per second for the test to ultimately be generated from configs
    val findVhfTransferFramesPerSecond = config.getDouble("performance.operationsPerSecond.findVhfTransferFrames") * rateMultiplier * instanceMultiplier
    val getVhfBinaryPacketsPerSecond = config.getDouble("performance.operationsPerSecond.getVhfBinaryPackets") * rateMultiplier * instanceMultiplier
    val getVhfDecodedPacketsPerSecond = config.getDouble("performance.operationsPerSecond.getVhfDecodedPackets") * rateMultiplier * instanceMultiplier
    val listVhfTransferFramesPerSecond = config.getDouble("performance.operationsPerSecond.listVhfTransferFrames") * rateMultiplier * instanceMultiplier
    val saveHexaVhfTransferFramesPerSecond = config.getDouble("performance.operationsPerSecond.saveHexaVhfTransferFrames") * rateMultiplier * instanceMultiplier
    val savePacketFromStreamPerSecond = config.getDouble("performance.operationsPerSecond.savePacketFromStream") * rateMultiplier * instanceMultiplier
    val saveVhfTransferFramesPerSecond = config.getDouble("performance.operationsPerSecond.saveVhfTransferFrames") * rateMultiplier * instanceMultiplier
    val storeVhfStationPacketPerSecond = config.getDouble("performance.operationsPerSecond.storeVhfStationPacket") * rateMultiplier * instanceMultiplier

    val scenarioBuilders: mutable.MutableList[PopulationBuilder] = new mutable.MutableList[PopulationBuilder]()

    // Set up CSV feeders
    val findVhfTransferFramesPATHFeeder = csv(userDataDirectory + File.separator + "findVhfTransferFrames-pathParams.csv").random
    val getVhfBinaryPacketsHEADERFeeder = csv(userDataDirectory + File.separator + "getVhfBinaryPackets-headerParams.csv").random
    val getVhfBinaryPacketsPATHFeeder = csv(userDataDirectory + File.separator + "getVhfBinaryPackets-pathParams.csv").random
    val getVhfDecodedPacketsPATHFeeder = csv(userDataDirectory + File.separator + "getVhfDecodedPackets-pathParams.csv").random
    val listVhfTransferFramesQUERYFeeder = csv(userDataDirectory + File.separator + "listVhfTransferFrames-queryParams.csv").random
    val saveHexaVhfTransferFramesFORMFeeder = csv(userDataDirectory + File.separator + "saveHexaVhfTransferFrames-formParams.csv").random
    val savePacketFromStreamFORMFeeder = csv(userDataDirectory + File.separator + "savePacketFromStream-formParams.csv").random
    val saveVhfTransferFramesFORMFeeder = csv(userDataDirectory + File.separator + "saveVhfTransferFrames-formParams.csv").random
    val storeVhfStationPacketBodyFeeder = csv(userDataDirectory + File.separator + "storeVhfStationPacket-bodyParams.csv", escapeChar = '\\').random

    // Setup all scenarios

    
    val scnfindVhfTransferFrames = scenario("findVhfTransferFramesSimulation")
        .feed(findVhfTransferFramesPATHFeeder)
        .exec(http("findVhfTransferFrames")
        .httpRequest("GET","/vhf/${frame}")
)

    // Run scnfindVhfTransferFrames with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnfindVhfTransferFrames.inject(
        rampUsersPerSec(1) to(findVhfTransferFramesPerSecond) during(rampUpSeconds),
        constantUsersPerSec(findVhfTransferFramesPerSecond) during(durationSeconds),
        rampUsersPerSec(findVhfTransferFramesPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scngetVhfBinaryPackets = scenario("getVhfBinaryPacketsSimulation")
        .feed(getVhfBinaryPacketsHEADERFeeder)
        .feed(getVhfBinaryPacketsPATHFeeder)
        .exec(http("getVhfBinaryPackets")
        .httpRequest("GET","/vhf/packet/${hash}")
        .header("X-VHF-PktFormat","${X-VHF-PktFormat}")
)

    // Run scngetVhfBinaryPackets with warm up and reach a constant rate for entire duration
    scenarioBuilders += scngetVhfBinaryPackets.inject(
        rampUsersPerSec(1) to(getVhfBinaryPacketsPerSecond) during(rampUpSeconds),
        constantUsersPerSec(getVhfBinaryPacketsPerSecond) during(durationSeconds),
        rampUsersPerSec(getVhfBinaryPacketsPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scngetVhfDecodedPackets = scenario("getVhfDecodedPacketsSimulation")
        .feed(getVhfDecodedPacketsPATHFeeder)
        .exec(http("getVhfDecodedPackets")
        .httpRequest("GET","/vhf/decodedpacket/${hash}")
)

    // Run scngetVhfDecodedPackets with warm up and reach a constant rate for entire duration
    scenarioBuilders += scngetVhfDecodedPackets.inject(
        rampUsersPerSec(1) to(getVhfDecodedPacketsPerSecond) during(rampUpSeconds),
        constantUsersPerSec(getVhfDecodedPacketsPerSecond) during(durationSeconds),
        rampUsersPerSec(getVhfDecodedPacketsPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scnlistVhfTransferFrames = scenario("listVhfTransferFramesSimulation")
        .feed(listVhfTransferFramesQUERYFeeder)
        .exec(http("listVhfTransferFrames")
        .httpRequest("GET","/vhf")
        .queryParam("by","${by}")
        .queryParam("size","${size}")
        .queryParam("page","${page}")
        .queryParam("sort","${sort}")
)

    // Run scnlistVhfTransferFrames with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnlistVhfTransferFrames.inject(
        rampUsersPerSec(1) to(listVhfTransferFramesPerSecond) during(rampUpSeconds),
        constantUsersPerSec(listVhfTransferFramesPerSecond) during(durationSeconds),
        rampUsersPerSec(listVhfTransferFramesPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scnsaveHexaVhfTransferFrames = scenario("saveHexaVhfTransferFramesSimulation")
        .feed(saveHexaVhfTransferFramesFORMFeeder)
        .exec(http("saveHexaVhfTransferFrames")
        .httpRequest("POST","/vhf/hexa")
        .formParam("packet","${packet}")
)

    // Run scnsaveHexaVhfTransferFrames with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnsaveHexaVhfTransferFrames.inject(
        rampUsersPerSec(1) to(saveHexaVhfTransferFramesPerSecond) during(rampUpSeconds),
        constantUsersPerSec(saveHexaVhfTransferFramesPerSecond) during(durationSeconds),
        rampUsersPerSec(saveHexaVhfTransferFramesPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scnsavePacketFromStream = scenario("savePacketFromStreamSimulation")
        .feed(savePacketFromStreamFORMFeeder)
        .exec(http("savePacketFromStream")
        .httpRequest("POST","/vhf/upload")
        .formParam("stream","${stream}")
)

    // Run scnsavePacketFromStream with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnsavePacketFromStream.inject(
        rampUsersPerSec(1) to(savePacketFromStreamPerSecond) during(rampUpSeconds),
        constantUsersPerSec(savePacketFromStreamPerSecond) during(durationSeconds),
        rampUsersPerSec(savePacketFromStreamPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scnsaveVhfTransferFrames = scenario("saveVhfTransferFramesSimulation")
        .feed(saveVhfTransferFramesFORMFeeder)
        .exec(http("saveVhfTransferFrames")
        .httpRequest("POST","/vhf")
        .formParam("file","${file}")
)

    // Run scnsaveVhfTransferFrames with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnsaveVhfTransferFrames.inject(
        rampUsersPerSec(1) to(saveVhfTransferFramesPerSecond) during(rampUpSeconds),
        constantUsersPerSec(saveVhfTransferFramesPerSecond) during(durationSeconds),
        rampUsersPerSec(saveVhfTransferFramesPerSecond) to(1) during(rampDownSeconds)
    )

    
    val scnstoreVhfStationPacket = scenario("storeVhfStationPacketSimulation")
        .feed(storeVhfStationPacketBodyFeeder)
        .exec(http("storeVhfStationPacket")
        .httpRequest("POST","/vhf/packet")
        .body(StringBody(VhfStationPacketDto.toStringBody("${format}","${message}","${station}","${packet}")))
        )

    // Run scnstoreVhfStationPacket with warm up and reach a constant rate for entire duration
    scenarioBuilders += scnstoreVhfStationPacket.inject(
        rampUsersPerSec(1) to(storeVhfStationPacketPerSecond) during(rampUpSeconds),
        constantUsersPerSec(storeVhfStationPacketPerSecond) during(durationSeconds),
        rampUsersPerSec(storeVhfStationPacketPerSecond) to(1) during(rampDownSeconds)
    )

    setUp(
        scenarioBuilders.toList
    ).protocols(httpConf).assertions(
        global.responseTime.min.lte(globalResponseTimeMinLTE),
        global.responseTime.min.gte(globalResponseTimeMinGTE),
        global.responseTime.max.lte(globalResponseTimeMaxLTE),
        global.responseTime.max.gte(globalResponseTimeMaxGTE),
        global.responseTime.mean.lte(globalResponseTimeMeanLTE),
        global.responseTime.mean.gte(globalResponseTimeMeanGTE),
        global.failedRequests.percent.lte(globalResponseTimeFailedRequestsPercentLTE),
        global.failedRequests.percent.gte(globalResponseTimeFailedRequestsPercentGTE),
        global.successfulRequests.percent.lte(globalResponseTimeSuccessfulRequestsPercentLTE),
        global.successfulRequests.percent.gte(globalResponseTimeSuccessfulRequestsPercentGTE)
    )
}
