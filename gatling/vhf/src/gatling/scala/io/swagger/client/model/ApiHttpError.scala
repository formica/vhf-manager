
package io.swagger.client.model


case class ApiHttpError (
    _message: Option[String],
    _code: Option[Integer]
)
object ApiHttpError {
    def toStringBody(var_message: Object, var_code: Object) =
        s"""
        | {
        | "message":$var_message,"code":$var_code
        | }
        """.stripMargin
}
