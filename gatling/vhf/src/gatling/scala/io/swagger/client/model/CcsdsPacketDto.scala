
package io.swagger.client.model


case class CcsdsPacketDto (
    _ccsdsId: Option[Long],
    _ccsdsApid: Option[Integer],
    _ccsdsGFlag: Option[Integer],
    _ccsdsCounter: Option[Integer],
    _ccsdsPlength: Option[Integer],
    _ccsdsCrc: Option[Integer]
)
object CcsdsPacketDto {
    def toStringBody(var_ccsdsId: Object, var_ccsdsApid: Object, var_ccsdsGFlag: Object, var_ccsdsCounter: Object, var_ccsdsPlength: Object, var_ccsdsCrc: Object) =
        s"""
        | {
        | "ccsdsId":$var_ccsdsId,"ccsdsApid":$var_ccsdsApid,"ccsdsGFlag":$var_ccsdsGFlag,"ccsdsCounter":$var_ccsdsCounter,"ccsdsPlength":$var_ccsdsPlength,"ccsdsCrc":$var_ccsdsCrc
        | }
        """.stripMargin
}
