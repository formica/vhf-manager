
package io.swagger.client.model


case class FrameHeaderPacketDto (
    _fId: Option[Long],
    _satId: Option[Integer],
    _vcId: Option[Integer],
    _counters: Option[Integer],
    _dfStatus: Option[Integer]
)
object FrameHeaderPacketDto {
    def toStringBody(var_fId: Object, var_satId: Object, var_vcId: Object, var_counters: Object, var_dfStatus: Object) =
        s"""
        | {
        | "fId":$var_fId,"satId":$var_satId,"vcId":$var_vcId,"counters":$var_counters,"dfStatus":$var_dfStatus
        | }
        """.stripMargin
}
