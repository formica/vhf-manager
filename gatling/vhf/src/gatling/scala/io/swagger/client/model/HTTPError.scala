
package io.swagger.client.model


case class HTTPError (
    _message: String,
    _code: Integer
)
object HTTPError {
    def toStringBody(var_message: Object, var_code: Object) =
        s"""
        | {
        | "message":$var_message,"code":$var_code
        | }
        """.stripMargin
}
