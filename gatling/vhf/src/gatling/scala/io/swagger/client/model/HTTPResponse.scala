
package io.swagger.client.model


case class HTTPResponse (
    _message: String,
    _action: String,
    _code: Integer,
    _id: Option[String]
)
object HTTPResponse {
    def toStringBody(var_message: Object, var_action: Object, var_code: Object, var_id: Object) =
        s"""
        | {
        | "message":$var_message,"action":$var_action,"code":$var_code,"id":$var_id
        | }
        """.stripMargin
}
