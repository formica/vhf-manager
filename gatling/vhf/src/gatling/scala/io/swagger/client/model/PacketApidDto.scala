
package io.swagger.client.model


case class PacketApidDto (
    _apid: Option[Integer],
    _packetName: Option[String],
    _packetDescription: Option[String],
    _className: Option[String]
)
object PacketApidDto {
    def toStringBody(var_apid: Object, var_packetName: Object, var_packetDescription: Object, var_className: Object) =
        s"""
        | {
        | "apid":$var_apid,"packetName":$var_packetName,"packetDescription":$var_packetDescription,"className":$var_className
        | }
        """.stripMargin
}
