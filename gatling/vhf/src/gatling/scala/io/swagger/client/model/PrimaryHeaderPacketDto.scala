
package io.swagger.client.model


case class PrimaryHeaderPacketDto (
    _phId: Option[Long],
    _packetTime: Option[Long],
    _packetIobsid: Option[Long]
)
object PrimaryHeaderPacketDto {
    def toStringBody(var_phId: Object, var_packetTime: Object, var_packetIobsid: Object) =
        s"""
        | {
        | "phId":$var_phId,"packetTime":$var_packetTime,"packetIobsid":$var_packetIobsid
        | }
        """.stripMargin
}
