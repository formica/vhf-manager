
package io.swagger.client.model

import java.util.Date

case class VhfBasePacket (
    _pktformat: String,
    _hashId: String,
    _insertionTime: Option[Date],
    _uri: Option[String]
)
object VhfBasePacket {
    def toStringBody(var_pktformat: Object, var_hashId: Object, var_insertionTime: Object, var_uri: Object) =
        s"""
        | {
        | "pktformat":$var_pktformat,"hashId":$var_hashId,"insertionTime":$var_insertionTime,"uri":$var_uri
        | }
        """.stripMargin
}
