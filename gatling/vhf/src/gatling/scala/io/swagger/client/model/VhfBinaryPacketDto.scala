
package io.swagger.client.model

import java.util.Date

case class VhfBinaryPacketDto (
    _pktformat: String,
    _hashId: String,
    _insertionTime: Option[Date],
    _uri: Option[String],
    _pktsize: Option[Long],
    /* A string representation of the packet content (can be JSON or base64 string) */
    _packet: Option[String],
    /* The byte array representation of input packet, this parameter is optional */
    _binarypacket: Option[String]
)
object VhfBinaryPacketDto {
    def toStringBody(var_pktformat: Object, var_hashId: Object, var_insertionTime: Object, var_uri: Object, var_pktsize: Object, var_packet: Object, var_binarypacket: Object) =
        s"""
        | {
        | "pktformat":$var_pktformat,"hashId":$var_hashId,"insertionTime":$var_insertionTime,"uri":$var_uri,"pktsize":$var_pktsize,"packet":$var_packet,"binarypacket":$var_binarypacket
        | }
        """.stripMargin
}
