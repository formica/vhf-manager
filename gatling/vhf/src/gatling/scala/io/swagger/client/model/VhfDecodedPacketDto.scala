
package io.swagger.client.model

import java.util.Date

case class VhfDecodedPacketDto (
    _pktformat: String,
    _hashId: String,
    _insertionTime: Option[Date],
    _uri: Option[String],
    _pktsize: Option[Long],
    /* A string representation of the packet content (can be JSON or base64 string) */
    _packet: Option[String]
)
object VhfDecodedPacketDto {
    def toStringBody(var_pktformat: Object, var_hashId: Object, var_insertionTime: Object, var_uri: Object, var_pktsize: Object, var_packet: Object) =
        s"""
        | {
        | "pktformat":$var_pktformat,"hashId":$var_hashId,"insertionTime":$var_insertionTime,"uri":$var_uri,"pktsize":$var_pktsize,"packet":$var_packet
        | }
        """.stripMargin
}
