
package io.swagger.client.model


case class VhfGroundStationDto (
    _stationId: Option[Integer],
    _ipAddress: Option[String],
    _location: Option[String],
    _thread: Option[String]
)
object VhfGroundStationDto {
    def toStringBody(var_stationId: Object, var_ipAddress: Object, var_location: Object, var_thread: Object) =
        s"""
        | {
        | "stationId":$var_stationId,"ipAddress":$var_ipAddress,"location":$var_location,"thread":$var_thread
        | }
        """.stripMargin
}
