
package io.swagger.client.model


case class VhfStationPacketDto (
    _message: Option[String],
    _station: Option[String],
    _packet: Option[String],
    _format: Option[String]
)
object VhfStationPacketDto {
    def toStringBody(var_message: Object, var_station: Object, var_packet: Object, var_format: Object) =
        s"""
        | {
        | "message":$var_message,"station":$var_station,"packet":$var_packet,"format":$var_format
        | }
        """.stripMargin
}
