
package io.swagger.client.model


case class VhfStationStatusDto (
    _sstatusId: Option[Long],
    _sync: Option[Integer],
    _nrj: Option[Integer],
    _receiverStatus: Option[Integer],
    _dummy: Option[Integer],
    _reedSolomon: Option[Integer],
    _cagVoltage: Option[Integer],
    _stationTime: Option[Integer]
)
object VhfStationStatusDto {
    def toStringBody(var_sstatusId: Object, var_sync: Object, var_nrj: Object, var_receiverStatus: Object, var_dummy: Object, var_reedSolomon: Object, var_cagVoltage: Object, var_stationTime: Object) =
        s"""
        | {
        | "sstatusId":$var_sstatusId,"sync":$var_sync,"nrj":$var_nrj,"receiverStatus":$var_receiverStatus,"dummy":$var_dummy,"reedSolomon":$var_reedSolomon,"cagVoltage":$var_cagVoltage,"stationTime":$var_stationTime
        | }
        """.stripMargin
}
