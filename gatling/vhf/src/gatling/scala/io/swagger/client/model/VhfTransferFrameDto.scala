
package io.swagger.client.model

import java.util.Date

case class VhfTransferFrameDto (
    _frameId: Option[Long],
    _receptionTime: Option[Date],
    _isFrameValid: Option[Boolean],
    _isFrameDuplicate: Option[Boolean],
    _dupFrameId: Option[Long],
    _apid: Option[PacketApidDto],
    _station: Option[VhfGroundStationDto],
    _frameHeader: Option[FrameHeaderPacketDto],
    _ccsds: Option[CcsdsPacketDto],
    _header: Option[PrimaryHeaderPacketDto],
    _stationStatus: Option[VhfStationStatusDto],
    _packet: Option[VhfBinaryPacketDto],
    _binaryHash: Option[String]
)
object VhfTransferFrameDto {
    def toStringBody(var_frameId: Object, var_receptionTime: Object, var_isFrameValid: Object, var_isFrameDuplicate: Object, var_dupFrameId: Object, var_apid: Object, var_station: Object, var_frameHeader: Object, var_ccsds: Object, var_header: Object, var_stationStatus: Object, var_packet: Object, var_binaryHash: Object) =
        s"""
        | {
        | "frameId":$var_frameId,"receptionTime":$var_receptionTime,"isFrameValid":$var_isFrameValid,"isFrameDuplicate":$var_isFrameDuplicate,"dupFrameId":$var_dupFrameId,"apid":$var_apid,"station":$var_station,"frameHeader":$var_frameHeader,"ccsds":$var_ccsds,"header":$var_header,"stationStatus":$var_stationStatus,"packet":$var_packet,"binaryHash":$var_binaryHash
        | }
        """.stripMargin
}
