docker run -d -e JAVA_OPTS="$1" \
-v /data/svom/var/log/vhf:/home/svom/data/logs \
-v /data/svom/var/web/vhf:/home/svom/data/web \
-v /data/svom/var/log/vhf:/home/svom/data/dump \
-p 6443:6443 \
--name svom-vhfmgr svomtest.svom.fr:5543/vhfmgr:1.0 
