inputfile=$1
echo "Setting properties from file $inputfile"
export JAVA_OPTS=
while read line; do echo $line; JAVA_OPTS="$JAVA_OPTS -D$line"; done < $inputfile
export JAVA_OPTS
export vhfmgr_dir=$PWD/vhfmgrApi-web/build/libs
