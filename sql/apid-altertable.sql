--
-- PostgreSQL database dump
--
ALTER TABLE public.vhf_packet_apid RENAME COLUMN PRODUCT TO DESTINATION;
ALTER TABLE public.vhf_packet_apid ADD COLUMN DETAILS character VARYING(4000);
ALTER TABLE public.vhf_packet_apid ADD COLUMN ALIAS character VARYING(100);
ALTER TABLE public.vhf_packet_apid ALTER COLUMN INSTRUMENT TYPE character VARYING(100);


