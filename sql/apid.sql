--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.21
-- Dumped by pg_dump version 11.2

-- Started on 2019-08-18 17:03:21 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2055 (class 0 OID 16437)
-- Dependencies: 181
-- Data for Name: vhf_packet_apid; Type: TABLE DATA; Schema: public; Owner: svom
--

COPY public.vhf_packet_apid (apid, category, class, expected_packets, instrument, descr, name, pcat, pid, priority, tid) FROM stdin;
587	VHF	none	1	SVOM	none	ECLRECURR2	0	0	-1	0
612	VHF	none	1	SVOM	none	MXTRECURR1	0	0	-1	0
519	VHF	none	1	SVOM	none	VTRECURR1	0	0	-1	0
520	VHF	none	1	SVOM	none	VTRECURR2	0	0	-1	0
610	VHF	none	1	SVOM	none	MXTPHOTDATA	0	0	-1	0
0	VHF	none	1	SVOM	none	IDLEFRAME	0	0	-1	0
586	VHF	none	1	SVOM	none	ECLRECURR1	0	0	-1	0
550	VHF	none	1	SVOM	none	GRMRECURR1	0	0	-1	0
576	VHF	TmVhfAlert	1	SVOM	none	ECLALERTL1	0	0	-1	0
577	VHF	none	22	SVOM	none	ECLLCURHP1	0	0	-1	0
546	VHF	none	1	SVOM	none	GRMLCURHIP	0	0	-1	0
608	VHF	none	1	SVOM	none	MXTPOSITIO	0	0	-1	0
609	VHF	none	10	SVOM	none	MXTPHOTONL	0	0	-1	0
579	VHF	none	1	SVOM	none	ECLALDESC1	0	0	-1	0
580	VHF	none	1	SVOM	none	ECLALDESC2	0	0	-1	0
581	VHF	none	1	SVOM	none	ECLALDESC3	0	0	-1	0
582	VHF	none	1	SVOM	none	ECLALDESC4	0	0	-1	0
583	VHF	none	1	SVOM	none	ECLALDESC5	0	0	-1	0
578	VHF	none	42	SVOM	none	ECLLCURLP1	0	0	-1	0
513	VHF	none	1	SVOM	none	VTATTCHART	0	0	-1	0
548	VHF	none	1	SVOM	none	GRMLCURLOP	0	0	-1	0
584	VHF	none	49	SVOM	none	ECLSUBIMAG	0	0	-1	0
514	VHF	none	1	SVOM	none	VTFCHARTR	0	0	-1	0
515	VHF	none	1	SVOM	none	VTFCHARTV	0	0	-1	0
517	VHF	none	1	SVOM	none	VT1SUBIMAR1	0	0	-1	0
521	VHF	none	1	SVOM	none	VT1SUBIMAR2	0	0	-1	0
516	VHF	none	1	SVOM	none	VTFCHARTVR	0	0	-1	0
518	VHF	none	1	SVOM	none	VT2SUBIMAGR1	0	0	-1	0
999	VHF	none	1	SVOM	test apid	TEST999	0	0	-1	0
\.


-- Completed on 2019-08-18 17:03:22 CEST

--
-- PostgreSQL database dump complete
--

