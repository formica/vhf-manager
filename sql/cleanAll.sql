--delete from obsid_apid_count;
--delete from vhf_ccsds_frame;
--delete from vhf_header_packet;
--delete from vhf_frameheader_packet;
delete from public.vhf_station_status;
delete from public.vhf_notification;
delete from public.vhf_transfer_frame;
delete from public.vhf_binary_packet;
delete from public.vhf_decoded_packet;
delete from public.vhf_raw_packet;
delete from public.vhf_sat_position;
delete from public.vhf_sat_attitude;

