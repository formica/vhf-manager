CREATE TABLE public.vhf_sat_attitude (
    att_id bigint NOT NULL,
    sat_attitudeq0 numeric(10,4),
    sat_attitudeq1 numeric(10,4),
    sat_attitudeq2 numeric(10,4),
    sat_attitudeq3 numeric(10,4),
    insertion_time timestamp without time zone,
    apid_name character varying(100) NOT NULL,
    packet_time bigint NOT NULL,
    PRIMARY KEY (att_id)
);

ALTER TABLE public.vhf_sat_attitude OWNER TO svom_cea;

grant update insert select on table public.vhf_sat_attitude to vhf_w;

CREATE TABLE public.vhf_sat_position (
    pos_id bigint NOT NULL,
    sat_position_x numeric(12,4),
    sat_position_y numeric(12,4),
    sat_position_z numeric(12,4),
    sat_position_lon numeric(10,4),
    sat_position_lat numeric(10,4),
    insertion_time timestamp without time zone,
    apid_name character varying(100) NOT NULL,
    packet_time bigint NOT NULL,
    PRIMARY KEY (pos_id)
);

ALTER TABLE public.vhf_sat_position OWNER TO svom_cea;

grant update, insert, select on table public.vhf_sat_position to vhf_w;
