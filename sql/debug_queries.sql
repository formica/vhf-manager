select tf.frame_id,tf.is_valid,tf.is_duplicate,tf.hash_id,tf.raw_packet,
    rp.pkt_flag,tf.frame_status,rp.insertion_time, dp.pkt_data
from vhf_transfer_frame tf, vhf_raw_packet rp, vhf_decoded_packet dp
where tf.raw_packet=rp.pkt_data and dp.hash_id=tf.hash_id
    and rp.pkt_flag='TO_BE_DECODED' order by rp.insertion_time DESC;