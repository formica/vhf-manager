--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.21
-- Dumped by pg_dump version 11.2

-- Started on 2019-08-18 17:06:12 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2059 (class 0 OID 16416)
-- Dependencies: 178
-- Data for Name: vhf_ground_station; Type: TABLE DATA; Schema: public; Owner: svom
--

COPY public.vhf_ground_station (station_id, station_description, id_station, loc_short, location, mac_address, station_name) FROM stdin;
1	p	0x1	p	p	00:01	test
8	p	0x8	p	p	00:08	test8
9	Unknown vhf	0x00000009	Unknown	FSC	0.0.0.9	auto-added-9
2	p	0x3	p	p	00:02	test3
7	Unknown vhf	0x00000007	Unknown	FSC	0.0.0.0	auto-added
28	Unknown vhf	0x00000028	Unknown	FSC	0.0.0.28	auto-added-28
998	test station	0x998	FR	paris	x:y:z	test_vhf
3	test station	0x03	FR	fsc	0:0:0:3	st03
4	test station	0x04	FR	fsc	0:0:0:4	st04
5	test station	0x05	FR	fsc	0:0:0:5	st05
39	Unknown vhf	0x00000039	Unknown	FSC	0.0.0.39	auto-added-39
10	test station	0x10	FR	fsc	0:0:0:10	st10
11	test station	0x11	FR	fsc	0:0:0:11	st11
12	test station	0x12	FR	fsc	0:0:0:12	st12
13	test station	0x13	FR	fsc	0:0:0:13	st13
42	Unknown vhf	0x00000042	Unknown	FSC	0.0.0.42	auto-added-42
14	test station	0x14	FR	fsc	0:0:0:14	st14
15	test station	0x15	FR	fsc	0:0:0:15	st15
16	test station	0x16	FR	fsc	0:0:0:16	st16
17	test station	0x17	FR	fsc	0:0:0:17	st17
18	test station	0x18	FR	fsc	0:0:0:18	st18
19	test station	0x19	FR	fsc	0:0:0:19	st19
20	test station	0x20	FR	fsc	0:0:0:20	st20
21	test station	0x21	FR	fsc	0:0:0:21	st21
22	test station	0x22	FR	fsc	0:0:0:22	st22
23	test station	0x23	FR	fsc	0:0:0:23	st23
24	test station	0x24	FR	fsc	0:0:0:24	st24
25	test station	0x25	FR	fsc	0:0:0:25	st25
26	test station	0x26	FR	fsc	0:0:0:26	st26
31	test station	0x31	FR	fsc	0:0:0:31	st31
32	test station	0x32	FR	fsc	0:0:0:32	st32
30	Unknown vhf	0x00000030	Unknown	FSC	0.0.0.30	auto-added-30
33	test station	0x33	FR	fsc	0:0:0:33	st33
34	test station	0x34	FR	fsc	0:0:0:34	st34
35	test station	0x35	FR	fsc	0:0:0:35	st35
36	test station	0x36	FR	fsc	0:0:0:36	st36
37	test station	0x37	FR	fsc	0:0:0:37	st37
38	test station	0x38	FR	fsc	0:0:0:38	st38
40	test station	0x40	FR	fsc	0:0:0:40	st40
41	test station	0x41	FR	fsc	0:0:0:41	st41
45	test station	0x45	FR	fsc	0:0:0:45	st45
29	Unknown vhf	0x00000029	Unknown	FSC	0.0.0.29	auto-added-29
43	Unknown vhf	0x00000043	Unknown	FSC	0.0.0.43	auto-added-43
46	test station	0x46	FR	fsc	0:0:0:46	st46
47	test station	0x47	FR	fsc	0:0:0:47	st47
48	test station	0x48	FR	fsc	0:0:0:48	st48
49	test station	0x49	FR	fsc	0:0:0:49	st49
50	test station	0x50	FR	fsc	0:0:0:50	st50
51	test station	0x51	FR	fsc	0:0:0:51	st51
52	test station	0x52	FR	fsc	0:0:0:52	st52
27	Unknown vhf	0x00000027	Unknown	FSC	0.0.0.27	auto-added-27
53	test station	0x53	FR	fsc	0:0:0:53	st53
54	test station	0x54	FR	fsc	0:0:0:54	st54
6	p	0x6	p	p	00:06	test6
55	test station	0x55	FR	fsc	0:0:0:55	st55
44	Unknown vhf	0x00000044	Unknown	FSC	0.0.0.44	auto-added-44
56	test station	0x56	FR	fsc	0:0:0:56	st56
57	test station	0x57	FR	fsc	0:0:0:57	st57
58	test station	0x58	FR	fsc	0:0:0:58	st58
59	test station	0x59	FR	fsc	0:0:0:59	st59
\.


-- Completed on 2019-08-18 17:06:17 CEST

--
-- PostgreSQL database dump complete
--

