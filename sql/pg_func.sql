
--
-- Name: delete_packets(); Type: FUNCTION; Schema: public; Owner: svom_cea
--

CREATE OR REPLACE FUNCTION public.delete_packets() RETURNS character
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag char;
   BEGIN
      statusflag = '1';
	  delete from vhf_decoded_packet;
	  delete from vhf_raw_packet;
	  delete from vhf_notification;
	  delete from vhf_station_status;
	  delete from svom_burst_id where burst_id != 'none';
	  delete from vhf_transfer_frame;
	  delete from vhf_binary_packet;
	  delete from vhf_sat_position;
	  delete from vhf_sat_attitude;
      RETURN statusflag;
   END;
   $$;


ALTER FUNCTION public.delete_packets() OWNER TO svom_cea;

--
-- Name: delete_packetsbyapid(); Type: FUNCTION; Schema: public; Owner: svom_cea
-- Remarks: should not put any commit statement in functions. They already start a transaction.
--

CREATE OR REPLACE FUNCTION public.delete_packetsbyapid(apid INTEGER) RETURNS INTEGER
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag INTEGER;
     rec record;
     subrec record;
     query text;
     subqry text;
     v_error_stack text;
   BEGIN
      statusflag := 0;
      query := 'select * from vhf_transfer_frame where apid_id = $1';
	  for rec in execute query using apid
        loop
            subqry := 'select * from vhf_station_status where frame_id = $1';
            for subrec in execute subqry using rec.frame_id
                loop
                    raise notice 'remove station status and notif for frame id: %', subrec.frame_id;
                    delete from vhf_station_status where frame_id = rec.frame_id;
                    delete from vhf_notification where frame_id = rec.frame_id;
                    delete from svom_burst_id where frame_id = rec.frame_id;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where apid_id = $1 and is_duplicate = false';
      for rec in execute query using apid
        loop
            delete from vhf_transfer_frame where hash_id = rec.hash_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.apid_id;
            subqry := 'select * from vhf_binary_packet where hash_id = $1';
            for subrec in execute subqry using rec.hash_id
                loop
                    raise notice 'remove binary and decoded for hash : %', subrec.hash_id;
                    delete from vhf_binary_packet where hash_id = rec.hash_id;
                    delete from vhf_decoded_packet where hash_id = rec.hash_id;
            end loop;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where apid_id = $1 and is_duplicate = true';
      for rec in execute query using apid
        loop
            delete from vhf_transfer_frame where frame_id = rec.frame_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.apid_id;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove dup raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;

      RETURN statusflag;
   EXCEPTION
    when others then
    begin
        raise notice 'Exception in removing packet for apid %', apid;
        GET STACKED DIAGNOSTICS v_error_stack = PG_EXCEPTION_CONTEXT;
        RAISE WARNING 'The stack trace of the error is: "%"', v_error_stack;
    end;
    RETURN 0;
   END;
   $$;


ALTER FUNCTION public.delete_packetsbyapid OWNER TO svom_cea;
GRANT EXECUTE ON FUNCTION public.delete_packetsbyapid(integer) TO vhf_w;

--
-- Name: delete_packetsbyobsid(); Type: FUNCTION; Schema: public; Owner: svom_cea
--

CREATE OR REPLACE FUNCTION public.delete_packetsbyobsidnum(obsid NUMERIC) RETURNS numeric
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag numeric;
     rec record;
     subrec record;
     query text;
     subqry text;
     v_error_stack text;
   BEGIN
      statusflag := 0;
      query := 'select * from vhf_transfer_frame where packet_obsid_num = $1';
	  for rec in execute query using obsid
        loop
            subqry := 'select * from vhf_station_status where frame_id = $1';
            for subrec in execute subqry using rec.frame_id
                loop
                    raise notice 'remove station status and notif for frame id: %', subrec.frame_id;
                    delete from vhf_station_status where frame_id = rec.frame_id;
                    delete from vhf_notification where frame_id = rec.frame_id;
                    delete from svom_burst_id where frame_id = rec.frame_id;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where packet_obsid_num = $1 and is_duplicate = false';
      for rec in execute query using obsid
        loop
            delete from vhf_transfer_frame where hash_id = rec.hash_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.packet_obsid_num;
            subqry := 'select * from vhf_binary_packet where hash_id = $1';
            for subrec in execute subqry using rec.hash_id
                loop
                    raise notice 'remove binary and decoded for hash : %', subrec.hash_id;
                    delete from vhf_binary_packet where hash_id = rec.hash_id;
                    delete from vhf_decoded_packet where hash_id = rec.hash_id;
            end loop;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where packet_obsid_num = $1 and is_duplicate = true';
      for rec in execute query using obsid
        loop
            delete from vhf_transfer_frame where frame_id = rec.frame_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.packet_obsid_num;
            subqry := 'select * from vhf_binary_packet where hash_id = $1';
            for subrec in execute subqry using rec.hash_id
                loop
                    raise notice 'remove binary and decoded for hash : %', subrec.hash_id;
                    delete from vhf_binary_packet where hash_id = rec.hash_id;
                    delete from vhf_decoded_packet where hash_id = rec.hash_id;
            end loop;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;

      RETURN statusflag;
   EXCEPTION
    when others then
    begin
        raise notice 'Exception in removing obsid %', obsid;
        GET STACKED DIAGNOSTICS v_error_stack = PG_EXCEPTION_CONTEXT;
        RAISE WARNING 'The stack trace of the error is: "%"', v_error_stack;
    end;
    RETURN 0;
   END;
   $$;


--
-- Name: delete_packetsbyobsid(); Type: FUNCTION; Schema: public; Owner: svom_cea
--

CREATE OR REPLACE FUNCTION public.delete_packetsbyobsid(obsid NUMERIC) RETURNS numeric
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag numeric;
     rec record;
     subrec record;
     query text;
     subqry text;
     v_error_stack text;
   BEGIN
      statusflag := 0;
      query := 'select * from vhf_transfer_frame where packet_obsid = $1';
	  for rec in execute query using obsid
        loop
            subqry := 'select * from vhf_station_status where frame_id = $1';
            for subrec in execute subqry using rec.frame_id
                loop
                    raise notice 'remove station status and notif for frame id: %', subrec.frame_id;
                    delete from vhf_station_status where frame_id = rec.frame_id;
                    delete from vhf_notification where frame_id = rec.frame_id;
                    delete from svom_burst_id where frame_id = rec.frame_id;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where packet_obsid = $1 and is_duplicate = false';
      for rec in execute query using obsid
        loop
            delete from vhf_transfer_frame where hash_id = rec.hash_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.packet_obsid;
            subqry := 'select * from vhf_binary_packet where hash_id = $1';
            for subrec in execute subqry using rec.hash_id
                loop
                    raise notice 'remove binary and decoded for hash : %', subrec.hash_id;
                    delete from vhf_binary_packet where hash_id = rec.hash_id;
                    delete from vhf_decoded_packet where hash_id = rec.hash_id;
            end loop;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where packet_obsid = $1 and is_duplicate = true';
      for rec in execute query using obsid
        loop
            delete from vhf_transfer_frame where frame_id = rec.frame_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.packet_obsid;
            subqry := 'select * from vhf_binary_packet where hash_id = $1';
            for subrec in execute subqry using rec.hash_id
                loop
                    raise notice 'remove binary and decoded for hash : %', subrec.hash_id;
                    delete from vhf_binary_packet where hash_id = rec.hash_id;
                    delete from vhf_decoded_packet where hash_id = rec.hash_id;
            end loop;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;

      RETURN statusflag;
   EXCEPTION
    when others then
    begin
        raise notice 'Exception in removing obsid %', obsid;
        GET STACKED DIAGNOSTICS v_error_stack = PG_EXCEPTION_CONTEXT;
        RAISE WARNING 'The stack trace of the error is: "%"', v_error_stack;
    end;
    RETURN 0;
   END;
   $$;


ALTER FUNCTION public.delete_packetsbyobsid OWNER TO svom_cea;
GRANT EXECUTE ON FUNCTION public.delete_packetsbyobsid(numeric) TO vhf_w;


ALTER FUNCTION public.delete_packetsbyobsidnum OWNER TO svom_cea;
GRANT EXECUTE ON FUNCTION public.delete_packetsbyobsidnum(numeric) TO vhf_w;