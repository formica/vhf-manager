/Applications/pgAdmin 4.app/Contents/SharedSupport/pg_dump --file "/tmp/vhfdb-bkp.sql" --host "svom-fsc-0.lal.in2p3.fr" --port "20032" --username "svom" --no-password --verbose --format=p --schema-only "vhfdb"

## Dump one table
/Applications/pgAdmin 4.app/Contents/SharedSupport/pg_dump --file "/Users/formica/Public/git-cea/vhfmgr/sql/apid.sql" --host "svom-fsc-0.lal.in2p3.fr" --port "20032" --username "svom" --no-password --verbose --format=p --data-only --table "public.vhf_packet_apid" "vhfdb
"
## dump with password (simple svom no special characters)
/Applications/pgAdmin\ 4.app/Contents/SharedSupport/pg_dump --file "/Users/formica/Public/git-cea/vhfmgr/sql/ground_station.sql" --host "svom-fsc-0.lal.in2p3.fr" --port "20032" --username "svom" --verbose --format=p --data-only --table "public.vhf_ground_station" "vhfdb"
