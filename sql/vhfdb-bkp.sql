--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.21
-- Dumped by pg_dump version 11.2

-- Started on 2019-08-18 16:58:51 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 187 (class 1259 OID 16489)
-- Name: ccsds_header_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.ccsds_header_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ccsds_header_sequence OWNER TO svom;

--
-- TOC entry 188 (class 1259 OID 16491)
-- Name: counter_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.counter_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.counter_sequence OWNER TO svom;

--
-- TOC entry 189 (class 1259 OID 16493)
-- Name: frame_header_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.frame_header_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.frame_header_sequence OWNER TO svom;

--
-- TOC entry 190 (class 1259 OID 16495)
-- Name: frame_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.frame_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.frame_sequence OWNER TO svom;

--
-- TOC entry 191 (class 1259 OID 16497)
-- Name: notif_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.notif_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notif_sequence OWNER TO svom;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 16385)
-- Name: obsid_apid_counts; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.obsid_apid_counts (
    counter_id bigint NOT NULL,
    apid integer NOT NULL,
    counted_packets integer,
    expected_packets integer,
    insertion_time timestamp without time zone,
    obsid_max_time bigint,
    obsid_min_time bigint,
    name character varying(20) NOT NULL,
    pkt_obs_id bigint,
    status character varying(50) NOT NULL,
    update_time timestamp without time zone
);


ALTER TABLE public.obsid_apid_counts OWNER TO svom;

--
-- TOC entry 192 (class 1259 OID 16499)
-- Name: primary_header_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.primary_header_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.primary_header_sequence OWNER TO svom;

--
-- TOC entry 193 (class 1259 OID 16501)
-- Name: rawpkt_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.rawpkt_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rawpkt_sequence OWNER TO svom;

--
-- TOC entry 194 (class 1259 OID 16503)
-- Name: station_sequence; Type: SEQUENCE; Schema: public; Owner: svom
--

CREATE SEQUENCE public.station_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.station_sequence OWNER TO svom;

--
-- TOC entry 174 (class 1259 OID 16390)
-- Name: vhf_binary_packet; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_binary_packet (
    hash_id character varying(255) NOT NULL,
    insertion_time timestamp without time zone,
    pkt_data character varying(512),
    pkt_format character varying(50),
    pkt_size integer
);


ALTER TABLE public.vhf_binary_packet OWNER TO svom;

--
-- TOC entry 175 (class 1259 OID 16398)
-- Name: vhf_ccsds_frame; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_ccsds_frame (
    ccsds_id bigint NOT NULL,
    ccsds_apid integer,
    ccsds_counter integer,
    ccsds_crc integer,
    ccsdsgflag integer,
    ccsds_plength integer,
    ccsds_sec_head_flag integer,
    ccsds_type integer,
    ccsds_version_num integer,
    frame_id bigint
);


ALTER TABLE public.vhf_ccsds_frame OWNER TO svom;

--
-- TOC entry 176 (class 1259 OID 16403)
-- Name: vhf_decoded_packet; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_decoded_packet (
    hash_id character varying(255) NOT NULL,
    insertion_time timestamp without time zone,
    pkt_data character varying(4000) NOT NULL,
    pkt_format character varying(50),
    pkt_size bigint
);


ALTER TABLE public.vhf_decoded_packet OWNER TO svom;

--
-- TOC entry 177 (class 1259 OID 16411)
-- Name: vhf_frameheader_packet; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_frameheader_packet (
    frheader_id bigint NOT NULL,
    df_status integer,
    mcf_count integer,
    oc_flag integer,
    space_craft_id integer,
    tframe_version integer,
    vc_id integer,
    vcf_count integer,
    frame_id bigint
);


ALTER TABLE public.vhf_frameheader_packet OWNER TO svom;

--
-- TOC entry 178 (class 1259 OID 16416)
-- Name: vhf_ground_station; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_ground_station (
    station_id integer NOT NULL,
    station_description character varying(500),
    id_station character varying(20) NOT NULL,
    loc_short character varying(10),
    location character varying(255) NOT NULL,
    mac_address character varying(30) NOT NULL,
    station_name character varying(30) NOT NULL
);


ALTER TABLE public.vhf_ground_station OWNER TO svom;

--
-- TOC entry 179 (class 1259 OID 16424)
-- Name: vhf_header_packet; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_header_packet (
    ph_id bigint NOT NULL,
    packet_obsid_type integer,
    packet_obsid_num integer,
    packet_obsid bigint,
    packet_time bigint,
    frame_id bigint
);


ALTER TABLE public.vhf_header_packet OWNER TO svom;

--
-- TOC entry 180 (class 1259 OID 16429)
-- Name: vhf_notification; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_notification (
    notif_id bigint NOT NULL,
    insertion_time timestamp without time zone,
    notif_format character varying(50),
    notif_message character varying(1024),
    pkt_flag character varying(50),
    notif_size integer,
    frame_id bigint
);


ALTER TABLE public.vhf_notification OWNER TO svom;

--
-- TOC entry 181 (class 1259 OID 16437)
-- Name: vhf_packet_apid; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_packet_apid (
    apid integer NOT NULL,
    category character varying(50),
    class character varying(200),
    expected_packets integer,
    instrument character varying(50),
    descr character varying(100),
    name character varying(20) NOT NULL,
    pcat integer,
    pid integer,
    priority integer,
    tid integer
);


ALTER TABLE public.vhf_packet_apid OWNER TO svom;

--
-- TOC entry 182 (class 1259 OID 16442)
-- Name: vhf_raw_packet; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_raw_packet (
    raw_id bigint NOT NULL,
    insertion_time timestamp without time zone,
    pkt_data character varying(512),
    pkt_flag character varying(50),
    pkt_format character varying(50),
    pkt_size integer
);


ALTER TABLE public.vhf_raw_packet OWNER TO svom;

--
-- TOC entry 183 (class 1259 OID 16450)
-- Name: vhf_scheduler_mon; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_scheduler_mon (
    sched_job_id character varying(255) NOT NULL,
    exec_description character varying(4096),
    insertion_time timestamp without time zone,
    pkt_num bigint,
    sched_exec bigint,
    sched_time_spent bigint,
    pkt_since bigint,
    update_time timestamp without time zone
);


ALTER TABLE public.vhf_scheduler_mon OWNER TO svom;

--
-- TOC entry 184 (class 1259 OID 16458)
-- Name: vhf_scheduler_status; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_scheduler_status (
    sched_name character varying(255) NOT NULL,
    insertion_time timestamp without time zone,
    sched_status character varying(255),
    update_time timestamp without time zone NOT NULL
);


ALTER TABLE public.vhf_scheduler_status OWNER TO svom;

--
-- TOC entry 185 (class 1259 OID 16466)
-- Name: vhf_station_status; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_station_status (
    sstatus_id bigint NOT NULL,
    channel integer,
    ck_sum integer,
    corr_power integer,
    dop_drift real,
    doppler real,
    id_station integer,
    padding1 integer,
    padding2 integer,
    padding3 integer,
    reed_solomon_corr integer,
    srerror real,
    station_time bigint,
    frame_id bigint
);


ALTER TABLE public.vhf_station_status OWNER TO svom;

--
-- TOC entry 186 (class 1259 OID 16471)
-- Name: vhf_transfer_frame; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.vhf_transfer_frame (
    frame_id bigint NOT NULL,
    hash_id character varying(512),
    dup_frame_id bigint,
    is_duplicate boolean NOT NULL,
    is_valid boolean NOT NULL,
    raw_packet character varying(520),
    reception_time timestamp without time zone,
    apid_id integer NOT NULL,
    station_id integer NOT NULL
);


ALTER TABLE public.vhf_transfer_frame OWNER TO svom;

--
-- TOC entry 1957 (class 2606 OID 16389)
-- Name: obsid_apid_counts obsid_apid_counts_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.obsid_apid_counts
    ADD CONSTRAINT obsid_apid_counts_pkey PRIMARY KEY (counter_id);


--
-- TOC entry 1959 (class 2606 OID 16480)
-- Name: obsid_apid_counts uk4pi81t3ryecwmy92q6cxgyyej; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.obsid_apid_counts
    ADD CONSTRAINT uk4pi81t3ryecwmy92q6cxgyyej UNIQUE (apid, pkt_obs_id, obsid_min_time);


--
-- TOC entry 1981 (class 2606 OID 16488)
-- Name: vhf_packet_apid uk_77b80nf2etelrgw4wgw51fw61; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_packet_apid
    ADD CONSTRAINT uk_77b80nf2etelrgw4wgw51fw61 UNIQUE (name);


--
-- TOC entry 1969 (class 2606 OID 16484)
-- Name: vhf_ground_station uk_bqwhrh9swwhm501ljjpyqd6yv; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT uk_bqwhrh9swwhm501ljjpyqd6yv UNIQUE (mac_address);


--
-- TOC entry 1971 (class 2606 OID 16486)
-- Name: vhf_ground_station uk_ewn8rgc74igteqc4pgp5dbc1s; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT uk_ewn8rgc74igteqc4pgp5dbc1s UNIQUE (station_name);


--
-- TOC entry 1973 (class 2606 OID 16482)
-- Name: vhf_ground_station uk_pjl811sxbrp2xue1obanj7rk4; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT uk_pjl811sxbrp2xue1obanj7rk4 UNIQUE (id_station);


--
-- TOC entry 1961 (class 2606 OID 16397)
-- Name: vhf_binary_packet vhf_binary_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_binary_packet
    ADD CONSTRAINT vhf_binary_packet_pkey PRIMARY KEY (hash_id);


--
-- TOC entry 1963 (class 2606 OID 16402)
-- Name: vhf_ccsds_frame vhf_ccsds_frame_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_ccsds_frame
    ADD CONSTRAINT vhf_ccsds_frame_pkey PRIMARY KEY (ccsds_id);


--
-- TOC entry 1965 (class 2606 OID 16410)
-- Name: vhf_decoded_packet vhf_decoded_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_decoded_packet
    ADD CONSTRAINT vhf_decoded_packet_pkey PRIMARY KEY (hash_id);


--
-- TOC entry 1967 (class 2606 OID 16415)
-- Name: vhf_frameheader_packet vhf_frameheader_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_frameheader_packet
    ADD CONSTRAINT vhf_frameheader_packet_pkey PRIMARY KEY (frheader_id);


--
-- TOC entry 1975 (class 2606 OID 16423)
-- Name: vhf_ground_station vhf_ground_station_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT vhf_ground_station_pkey PRIMARY KEY (station_id);


--
-- TOC entry 1977 (class 2606 OID 16428)
-- Name: vhf_header_packet vhf_header_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_header_packet
    ADD CONSTRAINT vhf_header_packet_pkey PRIMARY KEY (ph_id);


--
-- TOC entry 1979 (class 2606 OID 16436)
-- Name: vhf_notification vhf_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_notification
    ADD CONSTRAINT vhf_notification_pkey PRIMARY KEY (notif_id);


--
-- TOC entry 1983 (class 2606 OID 16441)
-- Name: vhf_packet_apid vhf_packet_apid_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_packet_apid
    ADD CONSTRAINT vhf_packet_apid_pkey PRIMARY KEY (apid);


--
-- TOC entry 1985 (class 2606 OID 16449)
-- Name: vhf_raw_packet vhf_raw_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_raw_packet
    ADD CONSTRAINT vhf_raw_packet_pkey PRIMARY KEY (raw_id);


--
-- TOC entry 1987 (class 2606 OID 16457)
-- Name: vhf_scheduler_mon vhf_scheduler_mon_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_scheduler_mon
    ADD CONSTRAINT vhf_scheduler_mon_pkey PRIMARY KEY (sched_job_id);


--
-- TOC entry 1989 (class 2606 OID 16465)
-- Name: vhf_scheduler_status vhf_scheduler_status_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_scheduler_status
    ADD CONSTRAINT vhf_scheduler_status_pkey PRIMARY KEY (sched_name);


--
-- TOC entry 1991 (class 2606 OID 16470)
-- Name: vhf_station_status vhf_station_status_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_station_status
    ADD CONSTRAINT vhf_station_status_pkey PRIMARY KEY (sstatus_id);


--
-- TOC entry 1993 (class 2606 OID 16478)
-- Name: vhf_transfer_frame vhf_transfer_frame_pkey; Type: CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT vhf_transfer_frame_pkey PRIMARY KEY (frame_id);


--
-- TOC entry 1996 (class 2606 OID 16515)
-- Name: vhf_header_packet fk2o9s4lfarqinusdm0p7d9pe7k; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_header_packet
    ADD CONSTRAINT fk2o9s4lfarqinusdm0p7d9pe7k FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- TOC entry 1999 (class 2606 OID 16530)
-- Name: vhf_transfer_frame fk3x5b2h1shl0tfiwu2phujfvin; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT fk3x5b2h1shl0tfiwu2phujfvin FOREIGN KEY (apid_id) REFERENCES public.vhf_packet_apid(apid);


--
-- TOC entry 1997 (class 2606 OID 16520)
-- Name: vhf_notification fkeapxn92tirx9yiby6mtolybf0; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_notification
    ADD CONSTRAINT fkeapxn92tirx9yiby6mtolybf0 FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- TOC entry 2000 (class 2606 OID 16535)
-- Name: vhf_transfer_frame fki6ykt9i0kw1ab7tqmt5amyqf2; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT fki6ykt9i0kw1ab7tqmt5amyqf2 FOREIGN KEY (hash_id) REFERENCES public.vhf_binary_packet(hash_id);


--
-- TOC entry 1995 (class 2606 OID 16510)
-- Name: vhf_frameheader_packet fkji2veg4u7ehabmhuxv6yc69dv; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_frameheader_packet
    ADD CONSTRAINT fkji2veg4u7ehabmhuxv6yc69dv FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- TOC entry 1998 (class 2606 OID 16525)
-- Name: vhf_station_status fkng8siav17brd61pxboq40sw28; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_station_status
    ADD CONSTRAINT fkng8siav17brd61pxboq40sw28 FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- TOC entry 2001 (class 2606 OID 16540)
-- Name: vhf_transfer_frame fkqv5s562ygis8rm99yowix7rub; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT fkqv5s562ygis8rm99yowix7rub FOREIGN KEY (station_id) REFERENCES public.vhf_ground_station(station_id);


--
-- TOC entry 1994 (class 2606 OID 16505)
-- Name: vhf_ccsds_frame fkt2s3axely4bnloa0u3gsoca28; Type: FK CONSTRAINT; Schema: public; Owner: svom
--

ALTER TABLE ONLY public.vhf_ccsds_frame
    ADD CONSTRAINT fkt2s3axely4bnloa0u3gsoca28 FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: svom
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM svom;
GRANT ALL ON SCHEMA public TO svom;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-08-18 16:58:54 CEST

--
-- PostgreSQL database dump complete
--

