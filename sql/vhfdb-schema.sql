--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: delete_packets(); Type: FUNCTION; Schema: public; Owner: svom_cea
--

CREATE FUNCTION OR REPLACE public.delete_packets() RETURNS character
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag char;
   BEGIN
      statusflag = '1';
	  delete from vhf_decoded_packet;
	  delete from vhf_raw_packet;
	  delete from vhf_notification;
	  delete from vhf_station_status;
	  delete from svom_burst_id;
	  delete from vhf_transfer_frame;
	  delete from vhf_binary_packet;
	  delete from vhf_sat_position;
	  delete from vhf_sat_attitude;
      RETURN statusflag;
   END;
   $$;


ALTER FUNCTION public.delete_packets() OWNER TO svom_cea;

--
-- Name: delete_packetsbyobsid(); Type: FUNCTION; Schema: public; Owner: svom_cea
-- Remarks: should not put any commit statement in functions. They already start a transaction.
--

CREATE OR REPLACE FUNCTION public.delete_packetsbyobsid(obsid INTEGER) RETURNS INTEGER
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag INTEGER;
     rec record;
     subrec record;
     query text;
     subqry text;
     v_error_stack text;
   BEGIN
      statusflag := 0;
      query := 'select * from vhf_transfer_frame where packet_obsid_num = $1';
	  for rec in execute query using obsid
        loop
            subqry := 'select * from vhf_station_status where frame_id = $1';
            for subrec in execute subqry using rec.frame_id
                loop
                    raise notice 'remove station status and notif for frame id: %', subrec.frame_id;
                    delete from vhf_station_status where frame_id = rec.frame_id;
                    delete from vhf_notification where frame_id = rec.frame_id;
                    delete from svom_burst_id where frame_id = rec.frame_id;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where packet_obsid_num = $1 and is_duplicate = false';
      for rec in execute query using obsid
        loop
            delete from vhf_transfer_frame where hash_id = rec.hash_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.packet_obsid_num;
            subqry := 'select * from vhf_binary_packet where hash_id = $1';
            for subrec in execute subqry using rec.hash_id
                loop
                    raise notice 'remove binary and decoded for hash : %', subrec.hash_id;
                    delete from vhf_binary_packet where hash_id = rec.hash_id;
                    delete from vhf_decoded_packet where hash_id = rec.hash_id;
            end loop;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where packet_obsid_num = $1 and is_duplicate = true';
      for rec in execute query using obsid
        loop
            delete from vhf_transfer_frame where frame_id = rec.frame_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.packet_obsid_num;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove dup raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;

      RETURN statusflag;
   EXCEPTION
    when others then
    begin
        raise notice 'Exception in removing obsid %', obsid;
        GET STACKED DIAGNOSTICS v_error_stack = PG_EXCEPTION_CONTEXT;
        RAISE WARNING 'The stack trace of the error is: "%"', v_error_stack;
    end;
    RETURN 0;
   END;
   $$;


ALTER FUNCTION public.delete_packetsbyobsid OWNER TO svom_cea;
GRANT EXECUTE ON FUNCTION public.delete_packetsbyobsid(integer) TO vhf_w;

--
-- Name: delete_packetsbyapid(); Type: FUNCTION; Schema: public; Owner: svom_cea
-- Remarks: should not put any commit statement in functions. They already start a transaction.
--

CREATE OR REPLACE FUNCTION public.delete_packetsbyapid(apid INTEGER) RETURNS INTEGER
    LANGUAGE plpgsql
    AS $$   DECLARE
     statusflag INTEGER;
     rec record;
     subrec record;
     query text;
     subqry text;
     v_error_stack text;
   BEGIN
      statusflag := 0;
      query := 'select * from vhf_transfer_frame where apid_id = $1';
	  for rec in execute query using apid
        loop
            subqry := 'select * from vhf_station_status where frame_id = $1';
            for subrec in execute subqry using rec.frame_id
                loop
                    raise notice 'remove station status and notif for frame id: %', subrec.frame_id;
                    delete from vhf_station_status where frame_id = rec.frame_id;
                    delete from vhf_notification where frame_id = rec.frame_id;
                    delete from svom_burst_id where frame_id = rec.frame_id;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where apid_id = $1 and is_duplicate = false';
      for rec in execute query using apid
        loop
            delete from vhf_transfer_frame where hash_id = rec.hash_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.apid_id;
            subqry := 'select * from vhf_binary_packet where hash_id = $1';
            for subrec in execute subqry using rec.hash_id
                loop
                    raise notice 'remove binary and decoded for hash : %', subrec.hash_id;
                    delete from vhf_binary_packet where hash_id = rec.hash_id;
                    delete from vhf_decoded_packet where hash_id = rec.hash_id;
            end loop;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;
      query := 'select * from vhf_transfer_frame where apid_id = $1 and is_duplicate = true';
      for rec in execute query using apid
        loop
            delete from vhf_transfer_frame where frame_id = rec.frame_id;
            statusflag := statusflag + 1;
      	    raise notice '% - %', rec.frame_id, rec.apid_id;
            subqry := 'select * from vhf_raw_packet where pkt_data = $1';
            for subrec in execute subqry using rec.raw_packet
                loop
                    raise notice 'remove dup raw packets: %', subrec.pkt_data;
                    delete from vhf_raw_packet where pkt_data = rec.raw_packet;
            end loop;
      end loop;

      RETURN statusflag;
   EXCEPTION
    when others then
    begin
        raise notice 'Exception in removing packet for apid %', apid;
        GET STACKED DIAGNOSTICS v_error_stack = PG_EXCEPTION_CONTEXT;
        RAISE WARNING 'The stack trace of the error is: "%"', v_error_stack;
    end;
    RETURN 0;
   END;
   $$;


ALTER FUNCTION public.delete_packetsbyapid OWNER TO svom_cea;
GRANT EXECUTE ON FUNCTION public.delete_packetsbyapid(integer) TO vhf_w;

--
-- Name: counter_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.counter_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.counter_sequence OWNER TO svom_cea;

--
-- Name: frame_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.frame_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.frame_sequence OWNER TO svom_cea;

--
-- Name: notif_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.notif_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notif_sequence OWNER TO svom_cea;

--
-- Name: rawpkt_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.rawpkt_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rawpkt_sequence OWNER TO svom_cea;

--
-- Name: station_sequence; Type: SEQUENCE; Schema: public; Owner: svom_cea
--

CREATE SEQUENCE public.station_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.station_sequence OWNER TO svom_cea;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: svom_burst_id; Type: TABLE; Schema: public; Owner: svom
--

CREATE TABLE public.svom_burst_id (
    burst_id character varying(50) NOT NULL,
    alert_time_tb bigint NOT NULL,
    alert_time_tb0_abs_seconds bigint NOT NULL,
    burst_end_time bigint,
    burst_start_time bigint,
    ecl_obsid bigint NOT NULL,
    insertion_time timestamp without time zone,
    packet_time bigint NOT NULL,
    frame_id bigint
);

ALTER TABLE public.svom_burst_id OWNER TO svom_cea;
--
-- Name: vhf_binary_packet; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_binary_packet (
    hash_id character varying(255) NOT NULL,
    insertion_time timestamp without time zone,
    pkt_data character varying(512),
    pkt_format character varying(50),
    pkt_size integer
);


ALTER TABLE public.vhf_binary_packet OWNER TO svom_cea;

--
-- Name: vhf_decoded_packet; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_decoded_packet (
    hash_id character varying(255) NOT NULL,
    insertion_time timestamp without time zone,
    pkt_data character varying(4000) NOT NULL,
    pkt_format character varying(50),
    pkt_size bigint,
    pkt_type character varying(50)
);


ALTER TABLE public.vhf_decoded_packet OWNER TO svom_cea;

--
-- Name: vhf_ground_station; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_ground_station (
    station_id integer NOT NULL,
    description character varying(500),
    identifier character varying(20) NOT NULL,
    name character varying(30),
    location character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    longitude numeric(12,5),
    latitude numeric(12,5),
    altitude numeric(12,5),
    min_elevation_angle numeric(12,6),
    mac_address character varying(30) NOT NULL
);


ALTER TABLE public.vhf_ground_station OWNER TO svom_cea;

--
-- Name: vhf_notification; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_notification (
    notif_id bigint NOT NULL,
    insertion_time timestamp without time zone,
    notif_format character varying(50),
    notif_message character varying(1024),
    pkt_flag character varying(50),
    notif_size integer,
    frame_id bigint
);


ALTER TABLE public.vhf_notification OWNER TO svom_cea;

--
-- Name: vhf_packet_apid; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_packet_apid (
    apid integer NOT NULL,
    category character varying(50),
    class character varying(200),
    expected_packets integer,
    instrument character varying(100),
    descr character varying(100),
    alias character varying(100),
    details character varying(4000),
    name character varying(100) NOT NULL,
    pcat integer,
    pid integer,
    priority integer,
    tid integer,
    destination character varying(200)
);


ALTER TABLE public.vhf_packet_apid OWNER TO svom_cea;

--
-- Name: vhf_raw_packet; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_raw_packet (
    raw_id bigint NOT NULL,
    insertion_time timestamp without time zone,
    pkt_data character varying(512),
    pkt_flag character varying(50),
    pkt_format character varying(50),
    pkt_size integer
);


ALTER TABLE public.vhf_raw_packet OWNER TO svom_cea;

--
-- Name: vhf_sat_attitude; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_sat_attitude (
    att_id bigint NOT NULL,
    sat_attitudeq0 numeric(10,4),
    sat_attitudeq1 numeric(10,4),
    sat_attitudeq2 numeric(10,4),
    sat_attitudeq3 numeric(10,4),
    insertion_time timestamp without time zone,
    apid_name character varying(100) NOT NULL,
    packet_time bigint NOT NULL
);


ALTER TABLE public.vhf_sat_attitude OWNER TO svom_cea;

--
-- Name: vhf_sat_position; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_sat_position (
    pos_id bigint NOT NULL,
    sat_position_x numeric(12,4),
    sat_position_y numeric(12,4),
    sat_position_z numeric(12,4),
    sat_position_lon numeric(10,4),
    sat_position_lat numeric(10,4),
    insertion_time timestamp without time zone,
    apid_name character varying(100) NOT NULL,
    packet_time bigint NOT NULL
);


ALTER TABLE public.vhf_sat_position OWNER TO svom_cea;

--
-- Name: vhf_scheduler_mon; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_scheduler_mon (
    sched_job_id character varying(255) NOT NULL,
    exec_description character varying(4096),
    insertion_time timestamp without time zone,
    pkt_num bigint,
    sched_exec bigint,
    sched_time_spent bigint,
    pkt_since bigint,
    update_time timestamp without time zone,
    min_obsid bigint
);


ALTER TABLE public.vhf_scheduler_mon OWNER TO svom_cea;

--
-- Name: vhf_scheduler_status; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_scheduler_status (
    sched_name character varying(255) NOT NULL,
    insertion_time timestamp without time zone,
    sched_status character varying(255),
    update_time timestamp without time zone NOT NULL,
    sched_busy character varying(255)
);


ALTER TABLE public.vhf_scheduler_status OWNER TO svom_cea;

--
-- Name: vhf_station_status; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_station_status (
    sstatus_id bigint NOT NULL,
    channel integer,
    ck_sum integer,
    corr_power integer,
    dop_drift real,
    doppler real,
    id_station integer,
    padding1 integer,
    padding2 integer,
    padding3 integer,
    reed_solomon_corr integer,
    srerror real,
    station_time bigint,
    frame_id bigint
);


ALTER TABLE public.vhf_station_status OWNER TO svom_cea;

--
-- Name: vhf_transfer_frame; Type: TABLE; Schema: public; Owner: svom_cea
--

CREATE TABLE public.vhf_transfer_frame (
    frame_id bigint NOT NULL,
    hash_id character varying(512),
    burst_id character varying(50),
    dup_frame_id bigint,
    is_duplicate boolean NOT NULL,
    is_valid boolean NOT NULL,
    insertion_time timestamp without time zone,
    apid_id integer NOT NULL,
    station_id integer NOT NULL,
    notif_status character varying(10),
    frame_status character varying(1024),
    ccsds_apid integer,
    ccsds_counter integer,
    ccsds_crc integer,
    ccsdsgflag integer,
    ccsds_plength integer,
    ccsds_sec_head_flag integer,
    ccsds_type integer,
    ccsds_version_num integer,
    packet_obsid bigint,
    packet_time bigint,
    packet_obsid_type integer,
    packet_obsid_num integer,
    instrument_mode integer,
    df_status integer,
    mcf_count integer,
    oc_flag integer,
    space_craft_id integer,
    tframe_version integer,
    vc_id integer,
    vcf_count integer,
    raw_packet character varying(520)
) ;
--PARTITION BY RANGE (reception_time);


ALTER TABLE public.vhf_transfer_frame OWNER TO svom_cea;

--CREATE TABLE public.vhf_transfer_frame_y2020 PARTITION OF public.vhf_transfer_frame
--    FOR VALUES FROM ('2020-01-01') TO ('2020-12-31');

--
-- Name: vhf_packet_apid uk_apid_name; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_packet_apid
    ADD CONSTRAINT uk_apid_name UNIQUE (name);


--
-- Name: vhf_ground_station uk_bqwhrh9swwhm501ljjpyqd6yv; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT uk_station_name UNIQUE (name);


--
-- Name: vhf_ground_station uk_ewn8rgc74igteqc4pgp5dbc1s; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT uk_station_id UNIQUE (identifier);


--
-- Name: vhf_ground_station uk_pjl811sxbrp2xue1obanj7rk4; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT uk_id_station UNIQUE (station_id);


--
-- Name: vhf_binary_packet vhf_binary_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_binary_packet
    ADD CONSTRAINT vhf_binary_packet_pkey PRIMARY KEY (hash_id);


--
-- Name: vhf_decoded_packet vhf_decoded_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_decoded_packet
    ADD CONSTRAINT vhf_decoded_packet_pkey PRIMARY KEY (hash_id);


--
-- Name: vhf_ground_station vhf_ground_station_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_ground_station
    ADD CONSTRAINT vhf_ground_station_pkey PRIMARY KEY (station_id);


--
-- Name: vhf_notification vhf_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_notification
    ADD CONSTRAINT vhf_notification_pkey PRIMARY KEY (notif_id);


--
-- Name: vhf_packet_apid vhf_packet_apid_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_packet_apid
    ADD CONSTRAINT vhf_packet_apid_pkey PRIMARY KEY (apid);


--
-- Name: vhf_raw_packet vhf_raw_packet_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_raw_packet
    ADD CONSTRAINT vhf_raw_packet_pkey PRIMARY KEY (raw_id);


--
-- Name: vhf_sat_attitude vhf_sat_attitude_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_sat_attitude
    ADD CONSTRAINT vhf_sat_attitude_pkey PRIMARY KEY (att_id);


--
-- Name: vhf_sat_position vhf_sat_position_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_sat_position
    ADD CONSTRAINT vhf_sat_position_pkey PRIMARY KEY (pos_id);


--
-- Name: vhf_scheduler_mon vhf_scheduler_mon_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_scheduler_mon
    ADD CONSTRAINT vhf_scheduler_mon_pkey PRIMARY KEY (sched_job_id);


--
-- Name: vhf_scheduler_status vhf_scheduler_status_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_scheduler_status
    ADD CONSTRAINT vhf_scheduler_status_pkey PRIMARY KEY (sched_name);


--
-- Name: vhf_station_status vhf_station_status_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_station_status
    ADD CONSTRAINT vhf_station_status_pkey PRIMARY KEY (sstatus_id);


--
-- Name: vhf_transfer_frame vhf_transfer_frame_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT vhf_transfer_frame_pkey PRIMARY KEY (frame_id);

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT vhf_transfer_frame_rawpkey UNIQUE (raw_packet);
--
-- Name: vhf_transfer_frame fk_burst_id; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--
ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT fk_burst_id FOREIGN KEY (burst_id) REFERENCES public.svom_burst_id(burst_id);

--
-- Name: svom_burst_id svom_burst_id_pkey; Type: CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.svom_burst_id
    ADD CONSTRAINT svom_burst_id_pkey PRIMARY KEY (burst_id);

--
-- Name: svom_burst_id fk_svom_burst_id_frame_id; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--
ALTER TABLE ONLY public.svom_burst_id
    ADD CONSTRAINT fk_svom_burst_id_frame_id FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- Name: idx_att_packet_time; Type: INDEX; Schema: public; Owner: svom_cea
--

CREATE INDEX idx_att_packet_time ON public.vhf_sat_attitude USING btree (packet_time);


--
-- Name: idx_packet_obsid; Type: INDEX; Schema: public; Owner: svom_cea
--

CREATE INDEX idx_packet_obsid ON public.vhf_transfer_frame USING btree (packet_obsid);


--
-- Name: idx_packet_time; Type: INDEX; Schema: public; Owner: svom_cea
--

CREATE INDEX idx_packet_time ON public.vhf_transfer_frame USING btree (packet_time);

--
-- Name: idx_insertion_time; Type: INDEX; Schema: public; Owner: svom_cea
--

CREATE INDEX idx_insertion_time ON public.vhf_transfer_frame USING btree (insertion_time);

--
-- Name: idx_pos_packet_time; Type: INDEX; Schema: public; Owner: svom_cea
--

CREATE INDEX idx_pos_packet_time ON public.vhf_sat_position USING btree (packet_time);


--
-- Name: vhf_transfer_frame_hashid; Type: INDEX; Schema: public; Owner: svom_cea
--

CREATE INDEX vhf_transfer_frame_hashid ON public.vhf_transfer_frame USING hash (hash_id);


--
-- Name: vhf_transfer_frame fk_apid_id; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT fk_apid_id FOREIGN KEY (apid_id) REFERENCES public.vhf_packet_apid(apid);

--
-- Name: vhf_transfer_frame fk_apid_id; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--
-- I remove this constraint to speed up the delete. In any case the association being automatic
-- there is no need probably for this. It can be enforced though. But at the level of data model
-- in Java this is only a String, it does not connect to the burstId table.
-- ALTER TABLE ONLY public.vhf_transfer_frame
--    ADD CONSTRAINT fk_burst_id FOREIGN KEY (burst_id) REFERENCES public.svom_burst_id(burst_id);

--
-- Name: vhf_transfer_frame fk_frame_hashid; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT fk_frame_hashid FOREIGN KEY (hash_id) REFERENCES public.vhf_binary_packet(hash_id);


--
-- Name: vhf_notification fk_notif_frame_id; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_notification
    ADD CONSTRAINT fk_notif_frame_id FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- Name: vhf_station_status fk_station_frame_id; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_station_status
    ADD CONSTRAINT fk_station_frame_id FOREIGN KEY (frame_id) REFERENCES public.vhf_transfer_frame(frame_id);


--
-- Name: vhf_transfer_frame fs_station_id; Type: FK CONSTRAINT; Schema: public; Owner: svom_cea
--

ALTER TABLE ONLY public.vhf_transfer_frame
    ADD CONSTRAINT fs_station_id FOREIGN KEY (station_id) REFERENCES public.vhf_ground_station(station_id);


--
-- Name: SEQUENCE counter_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.counter_sequence TO vhf_w;


--
-- Name: SEQUENCE frame_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.frame_sequence TO vhf_w;


--
-- Name: SEQUENCE notif_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.notif_sequence TO vhf_w;


--
-- Name: SEQUENCE rawpkt_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.rawpkt_sequence TO vhf_w;


--
-- Name: SEQUENCE station_sequence; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,USAGE ON SEQUENCE public.station_sequence TO vhf_w;


--
-- Name: TABLE vhf_binary_packet; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_binary_packet TO vhf_w;


--
-- Name: TABLE vhf_decoded_packet; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_decoded_packet TO vhf_w;


--
-- Name: TABLE vhf_ground_station; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_ground_station TO vhf_w;


--
-- Name: TABLE vhf_notification; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_notification TO vhf_w;


--
-- Name: TABLE vhf_packet_apid; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_packet_apid TO vhf_w;


--
-- Name: TABLE vhf_raw_packet; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_raw_packet TO vhf_w;


--
-- Name: TABLE vhf_sat_attitude; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_sat_attitude TO vhf_w;


--
-- Name: TABLE vhf_sat_position; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_sat_position TO vhf_w;


--
-- Name: TABLE vhf_scheduler_mon; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_scheduler_mon TO vhf_w;


--
-- Name: TABLE vhf_scheduler_status; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_scheduler_status TO vhf_w;


--
-- Name: TABLE vhf_station_status; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_station_status TO vhf_w;


--
-- Name: TABLE vhf_transfer_frame; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.vhf_transfer_frame TO vhf_w;

--
-- Name: TABLE svom_burst_id; Type: ACL; Schema: public; Owner: svom_cea
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.svom_burst_id TO vhf_w;

--
-- Insert default none entry in burstid
--
INSERT INTO SVOM_BURST_ID (burst_id, alert_time_tb, alert_time_tb0_abs_seconds, burst_end_time, burst_start_time, ecl_obsid, insertion_time, packet_time) VALUES ('none', 0 ,0 ,0 ,0, 0, now(), 0);

--
-- PostgreSQL database dump complete
--
