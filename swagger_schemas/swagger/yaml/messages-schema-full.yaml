openapi: 3.0.0
info:
  version: 1.0.0-oas3
  title: Svom Message Model
  termsOfService: 'http://svom.swagger.fr/terms/'
  contact:
    email: andrea.formica@cern.ch
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
tags:
  - name: messages
paths:
  /messages:
    post:
      tags:
        - messages
      summary: DataNotification.
      description: fake method
      operationId: fake01
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DataNotification'
        description: A json string that is used to construct a dto object
        required: true
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/DataSource'
            application/xml:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/DataSource'
  /messages/errormessage:
    post:
      tags:
        - messages
      summary: ErrorMessage.
      description: fake method
      operationId: fake04
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ErrorMessage'
        description: A json string that is used to construct a dto object
        required: true
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ServiceDescriptor'
            application/xml:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ServiceDescriptor'
  /messages/svommessage:
    post:
      tags:
        - messages
      summary: SvomMessage.
      description: fake method
      operationId: fake02
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InfoMessage'
        description: A fake body
        required: true
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/InfoMessage'
            application/xml:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/InfoMessage'
servers:
  - url: 'https://virtserver.swaggerhub.com/andreaformica/Svom-message-schemes/1.0.0'
  - url: 'http://virtserver.swaggerhub.com/andreaformica/Svom-message-schemes/1.0.0'
components:
  schemas:
    any:
      description: Used for schema free data
      type: object
      properties: {}
    DateIso:
      description: >-
        Generic type for the Dates. Using Date as classname make the generator
        including java import in the Py code
      type: string
      format: date-time
    ProcStatus:
      type: string
      enum:
        - Idle
        - Waiting
        - Running
        - Queued
        - Pending
        - Failed
        - Starting
        - Complete
        - Stopping
    PipelineStatus:
      type: string
      enum:
        - Idle
        - Waiting
        - Running
        - Starting
        - Stopping
    ProcMode:
      type: string
      enum:
        - todo
    ErrorLevel:
      type: string
      enum:
        - WARNING
        - ERROR
    InputData:
      type: array
      items:
        type: string
    DataSource:
      type: string
      enum:
        - VHF
        - BandX
        - Fpoc
    DataRequest:
      type: array
      items:
        type: string
    DataFlag:
      type: array
      items:
        type: string
    ResourceLocator:
      description: TBD
      type: string
    DataGroup:
      description: Descriptor of spacecraft target e.g. OBSID or BURSTID
      type: object
      required:
        - id
        - class
      properties:
        identifier:
          type: string
        class:
          $ref: >-
            http://saada.unistra.fr/svom/schema/vocabulary.json#/definitions/DataGroupClass
      example:
        identifier: sb123456
        class: OBS-ID
    Target:
      description: Input data for pipeline processing
      type: object
      required:
        - data_group
        - process
        - message
      properties:
        collected_data:
          $ref: '#/components/schemas/DataGroup'
        process:
          $ref: >-
            http://saada.unistra.fr/svom/schema/vocabulary.json#/definitions/Process
        message:
          type: string
      example:
        collected_data:
          identifier: sb123456
          class: OBS-ID
        process: QHR-ECLGRM
        message: '{''debug'': True}'
    ServiceDescriptor:
      description: Descriptor of a service running in a Docker
      type: object
      required:
        - name
        - uri
        - instrument
        - program
        - creation_date
        - version
      properties:
        name:
          type: string
        uri:
          description: 'Unique descriptorof the servive: svom://a/bc#x/y/z'
          type: string
          pattern: '^svom:/(/[a-z0-9-._]+)+((#[a-z0-9-._]+)(/[a-z0-9-._]+)*)$'
        instrument:
          description: Instruments whose data are processed by the service
          type: array
          items:
            $ref: >-
              http://saada.unistra.fr/svom/schema/vocabulary.json#/definitions/Instrument
        program:
          $ref: >-
            http://saada.unistra.fr/svom/schema/vocabulary.json#/definitions/Program
        creation_date:
          $ref: '#/components/schemas/DateIso'
        version:
          type: string
        links:
          description: Free text giving additional information such the Swagger file
          type: array
          items:
            type: string
    ProcessingDescriptor:
      description: Descriptor of the current processing
      type: object
      required:
        - process_id
        - process_name
        - target
        - submission_date
        - processing_status
      properties:
        process_id:
          type: string
        process_name:
          type: string
        target:
          $ref: '#/components/schemas/Target'
        submission_date:
          $ref: '#/components/schemas/DateIso'
        completion_date:
          $ref: '#/components/schemas/DateIso'
        input_data:
          $ref: '#/components/schemas/InputData'
        processing_status:
          $ref: '#/components/schemas/ProcStatus'
        message:
          description: aditionnal free text
          type: string
    ProcessingReport:
      description: Descriptor of the current processing
      type: object
      required:
        - message_class
        - message_date
        - content
      properties:
        message_class:
          type: string
          default: ProcessingReport
          enum:
            - ProcessingReport
        message_date:
          $ref: '#/components/schemas/DateIso'
        content:
          type: object
          required:
            - service_descriptor
            - processing_descriptor
          properties:
            service_descriptor:
              $ref: '#/components/schemas/ServiceDescriptor'
            processing_descriptor:
              $ref: '#/components/schemas/ProcessingDescriptor'
            messages:
              description: aditionnal free text
              type: array
              items:
                type: string
    ProcessingAck:
      description: Processing startup acknowledgment
      type: object
      required:
        - message_class
        - message_date
        - content
      properties:
        message_class:
          type: string
          default: ProcessingAck
          enum:
            - ProcessingAck
        message_date:
          $ref: '#/components/schemas/DateIso'
        content:
          type: object
          required:
            - service_descriptor
            - processing_descriptor
          properties:
            service_descriptor:
              $ref: '#/components/schemas/ServiceDescriptor'
            processing_descriptor:
              $ref: '#/components/schemas/ProcessingDescriptor'
    ServiceStatus:
      description: Descriptor of a service running in a Docker
      type: object
      required:
        - message_class
        - message_date
        - content
      properties:
        message_class:
          type: string
          default: ServiceStatus
          enum:
            - ServiceStatus
        message_date:
          $ref: '#/components/schemas/DateIso'
        content:
          type: object
          required:
            - service_descriptor
            - activity
            - status
            - date
          properties:
            service_descriptor:
              $ref: '#/components/schemas/ServiceDescriptor'
            activity:
              $ref: '#/components/schemas/ProcStatus'
            date:
              $ref: '#/components/schemas/DateIso'
            status:
              $ref: '#/components/schemas/PipelineStatus'
            info:
              description: 'Additional information on current processing (free) '
              type: string
    DataResponseContainer:
      type: object
      required:
        - data_request
        - data_flag
        - size
      properties:
        data_request:
          $ref: '#/components/schemas/DataRequest'
        data_flag:
          $ref: '#/components/schemas/DataFlag'
        size:
          type: integer
        data:
          type: array
          items:
            type: string
    DataRequestParameters:
      type: object
      required:
        - target
        - start_time
        - startDate
        - end_time
      properties:
        target:
          $ref: '#/components/schemas/Target'
        start_time:
          $ref: '#/components/schemas/DateIso'
        end_time:
          $ref: '#/components/schemas/DateIso'
        info:
          type: string
    ErrorMessage:
      type: object
      required:
        - message_class
        - message_date
        - content
      properties:
        message_class:
          type: string
          default: ErrorMessage
          enum:
            - ErrorMessage
        message_date:
          $ref: '#/components/schemas/DateIso'
        content:
          type: object
          required:
            - service_descriptor
            - processing_descriptor
            - http_error_code
            - level
            - info
            - date
            - message
          properties:
            service_descriptor:
              $ref: '#/components/schemas/ServiceDescriptor'
            processing_descriptor:
              $ref: '#/components/schemas/ProcessingDescriptor'
            http_error_code:
              type: integer
            level:
              $ref: '#/components/schemas/ErrorLevel'
            info:
              type: string
            date:
              $ref: '#/components/schemas/DateIso'
            message:
              type: string
    InfoMessage:
      type: object
      required:
        - message_class
        - message_date
        - content
      properties:
        message_class:
          type: string
          enum:
            - InfoMessage
        message_date:
          $ref: '#/components/schemas/DateIso'
        content:
          type: object
          required:
            - service_descriptor
            - message
          properties:
            service_descriptor:
              $ref: '#/components/schemas/ServiceDescriptor'
            message:
              type: string
    DataNotification:
      type: object
      required:
        - message_class
        - message_date
        - content
      properties:
        message_class:
          type: string
          default: DataNotification
          enum:
            - DataNotification
        message_date:
          $ref: '#/components/schemas/DateIso'
        content:
          type: object
          required:
            - data_stream
            - data_provider
            - product_card
            - date
            - obsid
            - message
            - resource_locator
          properties:
            data_stream:
              $ref: >-
                http://saada.unistra.fr/svom/schema/vocabulary.json#/definitions/DataStream
            data_provider:
              $ref: >-
                http://saada.unistra.fr/svom/schema/vocabulary.json#/definitions/DataProvider
            product_card:
              type: string
            date:
              $ref: '#/components/schemas/DateIso'
            message:
              type: string
            obsid:
              type: string
            resource_locator:
              $ref: '#/components/schemas/ResourceLocator'
    ServiceReport:
      type: object
      required:
        - message_class
        - message_date
        - content
      properties:
        message_class:
          type: string
          default: ServiceReport
          enum:
            - ServiceReport
        message_date:
          $ref: '#/components/schemas/DateIso'
        content:
          type: object
          required:
            - service_descriptor
            - sequences
          properties:
            service_descriptor:
              $ref: '#/components/schemas/ServiceDescriptor'
            sequences:
              type: array
              items:
                $ref: '#/components/schemas/ProcessingDescriptor'