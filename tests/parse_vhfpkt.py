import re,json
import base64


def parsefile(filename):
    pktarr=[]

    with open(filename) as f:
        for line in f:
            if '' == line:
                continue
            pktline=re.match(r'.*packet=([0-9,A-Z]+)',line)
            if pktline is None:
                print('Cannot parse line %s' % line)
                continue
                    #print('Parsed sql query %s for line %s' % (coolline.groups(),line))
            hexastr = pktline.group(1)
            rowdic={ 'station':'10.0.0.1','format': 'hexa','message': 'store packet','packet':hexastr}
            pktarr.append(rowdic)
    return pktarr

def dumppayloadcsv(pktarr):
    csvlist=[]
    for t in pktarr:
        outline = ('\\\"%s\\\",\\\"%s\\\",\\\"%s\\\",\\\"%s\\\"' % (t['station'],t['format'],t['message'],t['packet']))
        csvlist.append(outline)
    with open('packet.csv', 'w') as outfile:
        print('station,format,message,packet',file=outfile)
        for line in csvlist:
            print(line,file=outfile)


pktarr = parsefile('upload-eclvhf.sh')
#dumppayloadcsv('sqldata.txt')
dumppayloadcsv(pktarr)
