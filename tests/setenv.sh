#!/bin/sh 
## Script to initialize a server connection if not present
shost=localhost
sport=8080
smode=$1
sps=$2
if [[ -z "$smode" ]]; then
	echo "need a mode in input"
fi

vhfmgrsrvwar=./vhfmgrApi-web-1.0-SNAPSHOT.war
function startsrv {
	mode=$1
	authent=$2
	if [[ $mode == "default" ]]; then
		$sport=8090
	fi
	java -Dspring.profiles.active=$mode $authent -jar $vhfmgrsrvwar > .vhfmgrlog 2>&1 &
}

spid=
vhfmgrps=""
if [[ $smode == "default" ]]; then
	$sport=8090
else
	vhfmgrps="-Dvhfmgr.db.password=$sps"
fi

sstatus=`curl -s --head http://${shost}:${sport}/vhfmgrapi | head -n 1`
if [[ -z "${sstatus// }" ]]; then
	echo "Server is down...starting up in mode $smode and auth $vhfmgrps"
	startsrv $smode $vhfmgrps & echo $! > run.pid
	cat run.pid
else
	echo "Server is up...kill it"
	spid=`cat run.pid`
	echo "Killing process....$spid"
	kill $spid
fi