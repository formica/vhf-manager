# vhfmgrApi-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>fr.svom.vhf.client</groupId>
    <artifactId>vhfmgrApi-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "fr.svom.vhf.client:vhfmgrApi-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/vhfmgrApi-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import fr.svom.vhf.client.*;
import fr.svom.vhf.client.auth.*;
import fr.svom.vhf.swagger.model.*;
import fr.svom.vhf.client.swagger.api.ApidsApi;

import java.io.File;
import java.util.*;

public class ApidsApiExample {

    public static void main(String[] args) {
        
        ApidsApi apiInstance = new ApidsApi();
        Integer id = 56; // Integer | station id
        try {
            PacketApidDto result = apiInstance.findPacketApids(id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ApidsApi#findPacketApids");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8080/vhf/v1/manager*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ApidsApi* | [**findPacketApids**](docs/ApidsApi.md#findPacketApids) | **GET** /apids/{id} | Finds PacketApid by id
*ApidsApi* | [**listPacketApids**](docs/ApidsApi.md#listPacketApids) | **GET** /apids | Finds a VhfGroundStationDtos lists.
*StationsApi* | [**createVhfGroundStations**](docs/StationsApi.md#createVhfGroundStations) | **POST** /stations | Saves VhfGroundStation
*StationsApi* | [**findVhfGroundStations**](docs/StationsApi.md#findVhfGroundStations) | **GET** /stations/{id} | Finds VhfGroundStation by id
*StationsApi* | [**listVhfGroundStations**](docs/StationsApi.md#listVhfGroundStations) | **GET** /stations | Finds a VhfGroundStationDtos lists.
*VhfApi* | [**findVhfTransferFrames**](docs/VhfApi.md#findVhfTransferFrames) | **GET** /vhf/{frame} | Finds vhf transfer frame by ID.
*VhfApi* | [**listVhfTransferFrames**](docs/VhfApi.md#listVhfTransferFrames) | **GET** /vhf | Finds a VhfTransferFrameDtos lists.
*VhfApi* | [**savePacketFromStream**](docs/VhfApi.md#savePacketFromStream) | **POST** /vhf/upload | Saves VhfTransferFrame from binary stream
*VhfApi* | [**saveVhfTransferFrames**](docs/VhfApi.md#saveVhfTransferFrames) | **POST** /vhf | Saves VhfTransferFrame from file


## Documentation for Models

 - [CcsdsPacketDto](docs/CcsdsPacketDto.md)
 - [PacketApidDto](docs/PacketApidDto.md)
 - [PrimaryHeaderPacketDto](docs/PrimaryHeaderPacketDto.md)
 - [VhfGroundStationDto](docs/VhfGroundStationDto.md)
 - [VhfStationStatusDto](docs/VhfStationStatusDto.md)
 - [VhfTransferFrameDto](docs/VhfTransferFrameDto.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

andrea.formica@cern.ch

