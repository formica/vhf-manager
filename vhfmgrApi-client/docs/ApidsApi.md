# ApidsApi

All URIs are relative to *http://localhost:8080/vhf/v1/manager*

Method | HTTP request | Description
------------- | ------------- | -------------
[**findPacketApids**](ApidsApi.md#findPacketApids) | **GET** /apids/{id} | Finds PacketApid by id
[**listPacketApids**](ApidsApi.md#listPacketApids) | **GET** /apids | Finds a VhfGroundStationDtos lists.


<a name="findPacketApids"></a>
# **findPacketApids**
> PacketApidDto findPacketApids(id)

Finds PacketApid by id

Search for a precise resource

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.ApidsApi;


ApidsApi apiInstance = new ApidsApi();
Integer id = 56; // Integer | station id
try {
    PacketApidDto result = apiInstance.findPacketApids(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ApidsApi#findPacketApids");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| station id |

### Return type

[**PacketApidDto**](PacketApidDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="listPacketApids"></a>
# **listPacketApids**
> List&lt;PacketApidDto&gt; listPacketApids(by, page, size, sort)

Finds a VhfGroundStationDtos lists.

This method allows to perform search and sorting.Arguments: by&#x3D;&lt;pattern&gt;, page&#x3D;{ipage}, size&#x3D;{isize}, sort&#x3D;&lt;sortpattern&gt;. The pattern &lt;pattern&gt; is in the form &lt;param-name&gt;&lt;operation&gt;&lt;param-value&gt;       &lt;param-name&gt; is the name of one of the fields in the dto       &lt;operation&gt; can be [&lt; : &gt;] ; for string use only [:]        &lt;param-value&gt; depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for &lt;pattern&gt;.      The pattern &lt;sortpattern&gt; is &lt;field&gt;:[DESC|ASC]

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.ApidsApi;


ApidsApi apiInstance = new ApidsApi();
String by = "none"; // String | by: the search pattern {none}
Integer page = 0; // Integer | page: the page number {0}
Integer size = 1000; // Integer | size: the page size {1000}
String sort = "name:ASC"; // String | sort: the sort pattern {apid:DESC}
try {
    List<PacketApidDto> result = apiInstance.listPacketApids(by, page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ApidsApi#listPacketApids");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **String**| by: the search pattern {none} | [optional] [default to none]
 **page** | **Integer**| page: the page number {0} | [optional] [default to 0]
 **size** | **Integer**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **String**| sort: the sort pattern {apid:DESC} | [optional] [default to name:ASC]

### Return type

[**List&lt;PacketApidDto&gt;**](PacketApidDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

