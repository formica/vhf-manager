
# CcsdsPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ccsdsId** | **Long** |  |  [optional]
**frameVersionNumber** | **Integer** |  |  [optional]
**spaceCraftID** | **Integer** |  |  [optional]
**virtualChannelID** | **Integer** |  |  [optional]
**operationCtrlFlag** | **Integer** |  |  [optional]
**masterChannelFrameCount** | **Integer** |  |  [optional]
**virtualChannelFrameCount** | **Integer** |  |  [optional]
**secondaryHeaderFlag** | **Integer** |  |  [optional]
**synchronizationFlag** | **Integer** |  |  [optional]
**packetOrderFlag** | **Integer** |  |  [optional]
**segmentLengthID** | **Integer** |  |  [optional]
**firstHeaderPointer** | **Integer** |  |  [optional]
**crcone** | **Integer** |  |  [optional]
**crctwo** | **Integer** |  |  [optional]



