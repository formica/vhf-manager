
# PacketApidDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apid** | **Integer** |  |  [optional]
**packetName** | **String** |  |  [optional]
**packetDescription** | **String** |  |  [optional]
**className** | **String** |  |  [optional]



