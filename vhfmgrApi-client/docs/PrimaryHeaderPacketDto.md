
# PrimaryHeaderPacketDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phId** | **Long** |  |  [optional]
**packetVersionNumber** | **Integer** |  |  [optional]
**packetType** | **Integer** |  |  [optional]
**secondaryHeaderFlag** | **Integer** |  |  [optional]
**apid** | **Integer** |  |  [optional]
**segmentationFlag** | **Integer** |  |  [optional]
**srcSequenceCount** | **Integer** |  |  [optional]
**packetLength** | **Integer** |  |  [optional]
**spaceCraftTime** | **Long** |  |  [optional]



