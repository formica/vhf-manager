# StationsApi

All URIs are relative to *http://localhost:8080/vhf/v1/manager*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVhfGroundStations**](StationsApi.md#createVhfGroundStations) | **POST** /stations | Saves VhfGroundStation
[**findVhfGroundStations**](StationsApi.md#findVhfGroundStations) | **GET** /stations/{id} | Finds VhfGroundStation by id
[**listVhfGroundStations**](StationsApi.md#listVhfGroundStations) | **GET** /stations | Finds a VhfGroundStationDtos lists.


<a name="createVhfGroundStations"></a>
# **createVhfGroundStations**
> VhfGroundStationDto createVhfGroundStations(body)

Saves VhfGroundStation

Create new station

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.StationsApi;


StationsApi apiInstance = new StationsApi();
VhfGroundStationDto body = new VhfGroundStationDto(); // VhfGroundStationDto | A json string that is used to construct a vhfgroundstationdto object: { name: xxx, ... }
try {
    VhfGroundStationDto result = apiInstance.createVhfGroundStations(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationsApi#createVhfGroundStations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**VhfGroundStationDto**](VhfGroundStationDto.md)| A json string that is used to construct a vhfgroundstationdto object: { name: xxx, ... } |

### Return type

[**VhfGroundStationDto**](VhfGroundStationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="findVhfGroundStations"></a>
# **findVhfGroundStations**
> VhfGroundStationDto findVhfGroundStations(id)

Finds VhfGroundStation by id

Search for a specific resource

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.StationsApi;


StationsApi apiInstance = new StationsApi();
Integer id = 56; // Integer | station id
try {
    VhfGroundStationDto result = apiInstance.findVhfGroundStations(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationsApi#findVhfGroundStations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| station id |

### Return type

[**VhfGroundStationDto**](VhfGroundStationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="listVhfGroundStations"></a>
# **listVhfGroundStations**
> List&lt;VhfGroundStationDto&gt; listVhfGroundStations(by, page, size, sort)

Finds a VhfGroundStationDtos lists.

This method allows to perform search and sorting.Arguments: by&#x3D;&lt;pattern&gt;, page&#x3D;{ipage}, size&#x3D;{isize}, sort&#x3D;&lt;sortpattern&gt;. The pattern &lt;pattern&gt; is in the form &lt;param-name&gt;&lt;operation&gt;&lt;param-value&gt;       &lt;param-name&gt; is the name of one of the fields in the dto       &lt;operation&gt; can be [&lt; : &gt;] ; for string use only [:]        &lt;param-value&gt; depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for &lt;pattern&gt;.      The pattern &lt;sortpattern&gt; is &lt;field&gt;:[DESC|ASC]

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.StationsApi;


StationsApi apiInstance = new StationsApi();
String by = "none"; // String | by: the search pattern {none}
Integer page = 0; // Integer | page: the page number {0}
Integer size = 1000; // Integer | size: the page size {1000}
String sort = "stationId:ASC"; // String | sort: the sort pattern {stationId:ASC}
try {
    List<VhfGroundStationDto> result = apiInstance.listVhfGroundStations(by, page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationsApi#listVhfGroundStations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **String**| by: the search pattern {none} | [optional] [default to none]
 **page** | **Integer**| page: the page number {0} | [optional] [default to 0]
 **size** | **Integer**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **String**| sort: the sort pattern {stationId:ASC} | [optional] [default to stationId:ASC]

### Return type

[**List&lt;VhfGroundStationDto&gt;**](VhfGroundStationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

