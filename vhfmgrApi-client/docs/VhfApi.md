# VhfApi

All URIs are relative to *http://localhost:8080/vhf/v1/manager*

Method | HTTP request | Description
------------- | ------------- | -------------
[**findVhfTransferFrames**](VhfApi.md#findVhfTransferFrames) | **GET** /vhf/{frame} | Finds vhf transfer frame by ID.
[**listVhfTransferFrames**](VhfApi.md#listVhfTransferFrames) | **GET** /vhf | Finds a VhfTransferFrameDtos lists.
[**savePacketFromStream**](VhfApi.md#savePacketFromStream) | **POST** /vhf/upload | Saves VhfTransferFrame from binary stream
[**saveVhfTransferFrames**](VhfApi.md#saveVhfTransferFrames) | **POST** /vhf | Saves VhfTransferFrame from file


<a name="findVhfTransferFrames"></a>
# **findVhfTransferFrames**
> VhfTransferFrameDto findVhfTransferFrames(frame)

Finds vhf transfer frame by ID.

This method will search for transfer frame.

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.VhfApi;


VhfApi apiInstance = new VhfApi();
Long frame = 789L; // Long | id of the frame
try {
    VhfTransferFrameDto result = apiInstance.findVhfTransferFrames(frame);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VhfApi#findVhfTransferFrames");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **frame** | **Long**| id of the frame |

### Return type

[**VhfTransferFrameDto**](VhfTransferFrameDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="listVhfTransferFrames"></a>
# **listVhfTransferFrames**
> List&lt;VhfTransferFrameDto&gt; listVhfTransferFrames(by, page, size, sort)

Finds a VhfTransferFrameDtos lists.

This method allows to perform search and sorting.Arguments: by&#x3D;&lt;pattern&gt;, page&#x3D;{ipage}, size&#x3D;{isize}, sort&#x3D;&lt;sortpattern&gt;. The pattern &lt;pattern&gt; is in the form &lt;param-name&gt;&lt;operation&gt;&lt;param-value&gt;       &lt;param-name&gt; is the name of one of the fields in the dto       &lt;operation&gt; can be [&lt; : &gt;] ; for string use only [:]        &lt;param-value&gt; depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for &lt;pattern&gt;.      The pattern &lt;sortpattern&gt; is &lt;field&gt;:[DESC|ASC]

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.VhfApi;


VhfApi apiInstance = new VhfApi();
String by = "none"; // String | by: the search pattern {none}
Integer page = 0; // Integer | page: the page number {0}
Integer size = 1000; // Integer | size: the page size {1000}
String sort = "name:ASC"; // String | sort: the sort pattern {name:ASC}
try {
    List<VhfTransferFrameDto> result = apiInstance.listVhfTransferFrames(by, page, size, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VhfApi#listVhfTransferFrames");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **by** | **String**| by: the search pattern {none} | [optional] [default to none]
 **page** | **Integer**| page: the page number {0} | [optional] [default to 0]
 **size** | **Integer**| size: the page size {1000} | [optional] [default to 1000]
 **sort** | **String**| sort: the sort pattern {name:ASC} | [optional] [default to name:ASC]

### Return type

[**List&lt;VhfTransferFrameDto&gt;**](VhfTransferFrameDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml

<a name="savePacketFromStream"></a>
# **savePacketFromStream**
> VhfTransferFrameDto savePacketFromStream(stream)

Saves VhfTransferFrame from binary stream

Upload a binary packet

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.VhfApi;


VhfApi apiInstance = new VhfApi();
File stream = new File("/path/to/file.txt"); // File | 
try {
    VhfTransferFrameDto result = apiInstance.savePacketFromStream(stream);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VhfApi#savePacketFromStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stream** | **File**|  | [optional]

### Return type

[**VhfTransferFrameDto**](VhfTransferFrameDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

<a name="saveVhfTransferFrames"></a>
# **saveVhfTransferFrames**
> VhfTransferFrameDto saveVhfTransferFrames(file)

Saves VhfTransferFrame from file

Upload a file containing binary packet

### Example
```java
// Import classes:
//import fr.svom.vhf.client.ApiException;
//import fr.svom.vhf.client.swagger.api.VhfApi;


VhfApi apiInstance = new VhfApi();
File file = new File("/path/to/file.txt"); // File | 
try {
    VhfTransferFrameDto result = apiInstance.saveVhfTransferFrames(file);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VhfApi#saveVhfTransferFrames");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**|  | [optional]

### Return type

[**VhfTransferFrameDto**](VhfTransferFrameDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

