
# VhfGroundStationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationId** | **Integer** |  |  [optional]
**ipAddress** | **String** |  |  [optional]
**location** | **String** |  |  [optional]
**thread** | **String** |  |  [optional]



