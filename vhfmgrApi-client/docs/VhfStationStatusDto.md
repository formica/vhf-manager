
# VhfStationStatusDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sstatusId** | **Long** |  |  [optional]
**sync** | **Integer** |  |  [optional]
**nrj** | **Integer** |  |  [optional]
**receiverStatus** | **Integer** |  |  [optional]
**dummy** | **Integer** |  |  [optional]
**reedSolomon** | **Integer** |  |  [optional]
**cagVoltage** | **Integer** |  |  [optional]
**stationTime** | **Integer** |  |  [optional]



