
# VhfTransferFrameDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**frameId** | **Long** |  |  [optional]
**receptionTime** | [**DateTime**](DateTime.md) |  |  [optional]
**isFrameValid** | **Boolean** |  |  [optional]
**apid** | [**PacketApidDto**](PacketApidDto.md) |  |  [optional]
**station** | [**VhfGroundStationDto**](VhfGroundStationDto.md) |  |  [optional]
**ccsds** | [**CcsdsPacketDto**](CcsdsPacketDto.md) |  |  [optional]
**header** | [**PrimaryHeaderPacketDto**](PrimaryHeaderPacketDto.md) |  |  [optional]
**stationStatus** | [**VhfStationStatusDto**](VhfStationStatusDto.md) |  |  [optional]
**binaryHash** | **String** |  |  [optional]



