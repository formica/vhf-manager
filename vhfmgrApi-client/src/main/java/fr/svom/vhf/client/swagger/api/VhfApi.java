/*
 * VHF REST API setting in JAX-RS
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: andrea.formica@cern.ch
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package fr.svom.vhf.client.swagger.api;

import fr.svom.vhf.client.ApiCallback;
import fr.svom.vhf.client.ApiClient;
import fr.svom.vhf.client.ApiException;
import fr.svom.vhf.client.ApiResponse;
import fr.svom.vhf.client.Configuration;
import fr.svom.vhf.client.Pair;
import fr.svom.vhf.client.ProgressRequestBody;
import fr.svom.vhf.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;


import java.io.File;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VhfApi {
    private ApiClient apiClient;

    public VhfApi() {
        this(Configuration.getDefaultApiClient());
    }

    public VhfApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /* Build call for findVhfTransferFrames */
    private com.squareup.okhttp.Call findVhfTransferFramesCall(Long frame, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/vhf/{frame}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "frame" + "\\}", apiClient.escapeString(frame.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "application/xml"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call findVhfTransferFramesValidateBeforeCall(Long frame, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'frame' is set
        if (frame == null) {
            throw new ApiException("Missing the required parameter 'frame' when calling findVhfTransferFrames(Async)");
        }
        
        
        com.squareup.okhttp.Call call = findVhfTransferFramesCall(frame, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Finds vhf transfer frame by ID.
     * This method will search for transfer frame.
     * @param frame id of the frame (required)
     * @return VhfTransferFrameDto
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public VhfTransferFrameDto findVhfTransferFrames(Long frame) throws ApiException {
        ApiResponse<VhfTransferFrameDto> resp = findVhfTransferFramesWithHttpInfo(frame);
        return resp.getData();
    }

    /**
     * Finds vhf transfer frame by ID.
     * This method will search for transfer frame.
     * @param frame id of the frame (required)
     * @return ApiResponse&lt;VhfTransferFrameDto&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<VhfTransferFrameDto> findVhfTransferFramesWithHttpInfo(Long frame) throws ApiException {
        com.squareup.okhttp.Call call = findVhfTransferFramesValidateBeforeCall(frame, null, null);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Finds vhf transfer frame by ID. (asynchronously)
     * This method will search for transfer frame.
     * @param frame id of the frame (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call findVhfTransferFramesAsync(Long frame, final ApiCallback<VhfTransferFrameDto> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = findVhfTransferFramesValidateBeforeCall(frame, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for listVhfTransferFrames */
    private com.squareup.okhttp.Call listVhfTransferFramesCall(String by, Integer page, Integer size, String sort, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/vhf".replaceAll("\\{format\\}","json");

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (by != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "by", by));
        if (page != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "page", page));
        if (size != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "size", size));
        if (sort != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "sort", sort));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "application/xml"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call listVhfTransferFramesValidateBeforeCall(String by, Integer page, Integer size, String sort, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = listVhfTransferFramesCall(by, page, size, sort, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Finds a VhfTransferFrameDtos lists.
     * This method allows to perform search and sorting.Arguments: by&#x3D;&lt;pattern&gt;, page&#x3D;{ipage}, size&#x3D;{isize}, sort&#x3D;&lt;sortpattern&gt;. The pattern &lt;pattern&gt; is in the form &lt;param-name&gt;&lt;operation&gt;&lt;param-value&gt;       &lt;param-name&gt; is the name of one of the fields in the dto       &lt;operation&gt; can be [&lt; : &gt;] ; for string use only [:]        &lt;param-value&gt; depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for &lt;pattern&gt;.      The pattern &lt;sortpattern&gt; is &lt;field&gt;:[DESC|ASC]
     * @param by by: the search pattern {none} (optional, default to none)
     * @param page page: the page number {0} (optional, default to 0)
     * @param size size: the page size {1000} (optional, default to 1000)
     * @param sort sort: the sort pattern {name:ASC} (optional, default to name:ASC)
     * @return List&lt;VhfTransferFrameDto&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<VhfTransferFrameDto> listVhfTransferFrames(String by, Integer page, Integer size, String sort) throws ApiException {
        ApiResponse<List<VhfTransferFrameDto>> resp = listVhfTransferFramesWithHttpInfo(by, page, size, sort);
        return resp.getData();
    }

    /**
     * Finds a VhfTransferFrameDtos lists.
     * This method allows to perform search and sorting.Arguments: by&#x3D;&lt;pattern&gt;, page&#x3D;{ipage}, size&#x3D;{isize}, sort&#x3D;&lt;sortpattern&gt;. The pattern &lt;pattern&gt; is in the form &lt;param-name&gt;&lt;operation&gt;&lt;param-value&gt;       &lt;param-name&gt; is the name of one of the fields in the dto       &lt;operation&gt; can be [&lt; : &gt;] ; for string use only [:]        &lt;param-value&gt; depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for &lt;pattern&gt;.      The pattern &lt;sortpattern&gt; is &lt;field&gt;:[DESC|ASC]
     * @param by by: the search pattern {none} (optional, default to none)
     * @param page page: the page number {0} (optional, default to 0)
     * @param size size: the page size {1000} (optional, default to 1000)
     * @param sort sort: the sort pattern {name:ASC} (optional, default to name:ASC)
     * @return ApiResponse&lt;List&lt;VhfTransferFrameDto&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<VhfTransferFrameDto>> listVhfTransferFramesWithHttpInfo(String by, Integer page, Integer size, String sort) throws ApiException {
        com.squareup.okhttp.Call call = listVhfTransferFramesValidateBeforeCall(by, page, size, sort, null, null);
        Type localVarReturnType = new TypeToken<List<VhfTransferFrameDto>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Finds a VhfTransferFrameDtos lists. (asynchronously)
     * This method allows to perform search and sorting.Arguments: by&#x3D;&lt;pattern&gt;, page&#x3D;{ipage}, size&#x3D;{isize}, sort&#x3D;&lt;sortpattern&gt;. The pattern &lt;pattern&gt; is in the form &lt;param-name&gt;&lt;operation&gt;&lt;param-value&gt;       &lt;param-name&gt; is the name of one of the fields in the dto       &lt;operation&gt; can be [&lt; : &gt;] ; for string use only [:]        &lt;param-value&gt; depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for &lt;pattern&gt;.      The pattern &lt;sortpattern&gt; is &lt;field&gt;:[DESC|ASC]
     * @param by by: the search pattern {none} (optional, default to none)
     * @param page page: the page number {0} (optional, default to 0)
     * @param size size: the page size {1000} (optional, default to 1000)
     * @param sort sort: the sort pattern {name:ASC} (optional, default to name:ASC)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call listVhfTransferFramesAsync(String by, Integer page, Integer size, String sort, final ApiCallback<List<VhfTransferFrameDto>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = listVhfTransferFramesValidateBeforeCall(by, page, size, sort, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<VhfTransferFrameDto>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for saveHexaVhfTransferFrames */
    private com.squareup.okhttp.Call saveHexaVhfTransferFramesCall(String packet, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/vhf/hexa".replaceAll("\\{format\\}","json");

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (packet != null)
        localVarFormParams.put("packet", packet);

        final String[] localVarAccepts = {
            "application/json", "application/xml"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/x-www-form-urlencoded"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call saveHexaVhfTransferFramesValidateBeforeCall(String packet, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'packet' is set
        if (packet == null) {
            throw new ApiException("Missing the required parameter 'packet' when calling saveHexaVhfTransferFrames(Async)");
        }
        
        
        com.squareup.okhttp.Call call = saveHexaVhfTransferFramesCall(packet, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Saves VhfTransferFrame from hexa string
     * Upload a file containing binary packet in hexa string format
     * @param packet packet: the packet in hexadecimal format (required)
     * @return VhfTransferFrameDto
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public VhfTransferFrameDto saveHexaVhfTransferFrames(String packet) throws ApiException {
        ApiResponse<VhfTransferFrameDto> resp = saveHexaVhfTransferFramesWithHttpInfo(packet);
        return resp.getData();
    }

    /**
     * Saves VhfTransferFrame from hexa string
     * Upload a file containing binary packet in hexa string format
     * @param packet packet: the packet in hexadecimal format (required)
     * @return ApiResponse&lt;VhfTransferFrameDto&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<VhfTransferFrameDto> saveHexaVhfTransferFramesWithHttpInfo(String packet) throws ApiException {
        com.squareup.okhttp.Call call = saveHexaVhfTransferFramesValidateBeforeCall(packet, null, null);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Saves VhfTransferFrame from hexa string (asynchronously)
     * Upload a file containing binary packet in hexa string format
     * @param packet packet: the packet in hexadecimal format (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call saveHexaVhfTransferFramesAsync(String packet, final ApiCallback<VhfTransferFrameDto> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = saveHexaVhfTransferFramesValidateBeforeCall(packet, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for savePacketFromStream */
    private com.squareup.okhttp.Call savePacketFromStreamCall(File stream, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/vhf/upload".replaceAll("\\{format\\}","json");

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (stream != null)
        localVarFormParams.put("stream", stream);

        final String[] localVarAccepts = {
            "application/json", "application/xml"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call savePacketFromStreamValidateBeforeCall(File stream, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = savePacketFromStreamCall(stream, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Saves VhfTransferFrame from binary stream
     * Upload a binary packet
     * @param stream  (optional)
     * @return VhfTransferFrameDto
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public VhfTransferFrameDto savePacketFromStream(File stream) throws ApiException {
        ApiResponse<VhfTransferFrameDto> resp = savePacketFromStreamWithHttpInfo(stream);
        return resp.getData();
    }

    /**
     * Saves VhfTransferFrame from binary stream
     * Upload a binary packet
     * @param stream  (optional)
     * @return ApiResponse&lt;VhfTransferFrameDto&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<VhfTransferFrameDto> savePacketFromStreamWithHttpInfo(File stream) throws ApiException {
        com.squareup.okhttp.Call call = savePacketFromStreamValidateBeforeCall(stream, null, null);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Saves VhfTransferFrame from binary stream (asynchronously)
     * Upload a binary packet
     * @param stream  (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call savePacketFromStreamAsync(File stream, final ApiCallback<VhfTransferFrameDto> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = savePacketFromStreamValidateBeforeCall(stream, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for saveVhfTransferFrames */
    private com.squareup.okhttp.Call saveVhfTransferFramesCall(File file, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/vhf".replaceAll("\\{format\\}","json");

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (file != null)
        localVarFormParams.put("file", file);

        final String[] localVarAccepts = {
            "application/json", "application/xml"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call saveVhfTransferFramesValidateBeforeCall(File file, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = saveVhfTransferFramesCall(file, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Saves VhfTransferFrame from file
     * Upload a file containing binary packet
     * @param file  (optional)
     * @return VhfTransferFrameDto
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public VhfTransferFrameDto saveVhfTransferFrames(File file) throws ApiException {
        ApiResponse<VhfTransferFrameDto> resp = saveVhfTransferFramesWithHttpInfo(file);
        return resp.getData();
    }

    /**
     * Saves VhfTransferFrame from file
     * Upload a file containing binary packet
     * @param file  (optional)
     * @return ApiResponse&lt;VhfTransferFrameDto&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<VhfTransferFrameDto> saveVhfTransferFramesWithHttpInfo(File file) throws ApiException {
        com.squareup.okhttp.Call call = saveVhfTransferFramesValidateBeforeCall(file, null, null);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Saves VhfTransferFrame from file (asynchronously)
     * Upload a file containing binary packet
     * @param file  (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call saveVhfTransferFramesAsync(File file, final ApiCallback<VhfTransferFrameDto> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = saveVhfTransferFramesValidateBeforeCall(file, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<VhfTransferFrameDto>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
