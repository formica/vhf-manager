/*
 * VHF REST API setting in JAX-RS
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: andrea.formica@cern.ch
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package fr.svom.vhf.swagger.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * FrameHeaderPacketDto
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-31T17:00:55.002+02:00")
public class FrameHeaderPacketDto {
  @SerializedName("fId")
  private Long fId = null;

  @SerializedName("satId")
  private Integer satId = null;

  @SerializedName("vcId")
  private Integer vcId = null;

  @SerializedName("counters")
  private Integer counters = null;

  @SerializedName("dfStatus")
  private Integer dfStatus = null;

  public FrameHeaderPacketDto fId(Long fId) {
    this.fId = fId;
    return this;
  }

   /**
   * Get fId
   * @return fId
  **/
  @ApiModelProperty(example = "null", value = "")
  public Long getFId() {
    return fId;
  }

  public void setFId(Long fId) {
    this.fId = fId;
  }

  public FrameHeaderPacketDto satId(Integer satId) {
    this.satId = satId;
    return this;
  }

   /**
   * Get satId
   * @return satId
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getSatId() {
    return satId;
  }

  public void setSatId(Integer satId) {
    this.satId = satId;
  }

  public FrameHeaderPacketDto vcId(Integer vcId) {
    this.vcId = vcId;
    return this;
  }

   /**
   * Get vcId
   * @return vcId
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getVcId() {
    return vcId;
  }

  public void setVcId(Integer vcId) {
    this.vcId = vcId;
  }

  public FrameHeaderPacketDto counters(Integer counters) {
    this.counters = counters;
    return this;
  }

   /**
   * Get counters
   * @return counters
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getCounters() {
    return counters;
  }

  public void setCounters(Integer counters) {
    this.counters = counters;
  }

  public FrameHeaderPacketDto dfStatus(Integer dfStatus) {
    this.dfStatus = dfStatus;
    return this;
  }

   /**
   * Get dfStatus
   * @return dfStatus
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getDfStatus() {
    return dfStatus;
  }

  public void setDfStatus(Integer dfStatus) {
    this.dfStatus = dfStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FrameHeaderPacketDto frameHeaderPacketDto = (FrameHeaderPacketDto) o;
    return Objects.equals(this.fId, frameHeaderPacketDto.fId) &&
        Objects.equals(this.satId, frameHeaderPacketDto.satId) &&
        Objects.equals(this.vcId, frameHeaderPacketDto.vcId) &&
        Objects.equals(this.counters, frameHeaderPacketDto.counters) &&
        Objects.equals(this.dfStatus, frameHeaderPacketDto.dfStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fId, satId, vcId, counters, dfStatus);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FrameHeaderPacketDto {\n");
    
    sb.append("    fId: ").append(toIndentedString(fId)).append("\n");
    sb.append("    satId: ").append(toIndentedString(satId)).append("\n");
    sb.append("    vcId: ").append(toIndentedString(vcId)).append("\n");
    sb.append("    counters: ").append(toIndentedString(counters)).append("\n");
    sb.append("    dfStatus: ").append(toIndentedString(dfStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

