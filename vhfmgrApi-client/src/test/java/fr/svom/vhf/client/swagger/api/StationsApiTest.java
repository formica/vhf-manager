/*
 * VHF REST API setting in JAX-RS
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: andrea.formica@cern.ch
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package fr.svom.vhf.client.swagger.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.svom.vhf.client.ApiException;
import fr.svom.vhf.swagger.model.VhfGroundStationDto;

/**
 * API tests for StationsApi
 */
// @Ignore
public class StationsApiTest {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	private final StationsApi api = new StationsApi();

	/**
	 * Saves VhfGroundStation
	 *
	 * Create new station
	 *
	 * @throws ApiException
	 *             if the Api call fails
	 */
	@Test
	public void createVhfGroundStationsTest() throws ApiException {
		api.getApiClient().setBasePath("http://localhost:8090/vhf");
		VhfGroundStationDto body = new VhfGroundStationDto();
		body.setStationId(10);
		body.setIpAddress("1.0.0.1");
		body.setLocation("London");
		body.setThread("london-ip1");
		VhfGroundStationDto response = api.createVhfGroundStations(body);
		assertThat(response.getLocation()).isEqualTo("London");

		// TODO: test validations
	}

	/**
	 * Finds VhfGroundStation by id
	 *
	 * Search for a specific resource
	 *
	 * @throws ApiException
	 *             if the Api call fails
	 */
	@Test
	public void findVhfGroundStationsTest() throws ApiException {
		Integer id = 10;
		VhfGroundStationDto response = api.findVhfGroundStations(id);
		assertThat(response.getLocation()).isEqualTo("London");
	}

	/**
	 * Finds a VhfGroundStationDtos lists.
	 *
	 * This method allows to perform search and sorting.Arguments:
	 * by&#x3D;&lt;pattern&gt;, page&#x3D;{ipage}, size&#x3D;{isize},
	 * sort&#x3D;&lt;sortpattern&gt;. The pattern &lt;pattern&gt; is in the form
	 * &lt;param-name&gt;&lt;operation&gt;&lt;param-value&gt; &lt;param-name&gt;
	 * is the name of one of the fields in the dto &lt;operation&gt; can be
	 * [&lt; : &gt;] ; for string use only [:] &lt;param-value&gt; depends on
	 * the chosen parameter. A list of this criteria can be provided using comma
	 * separated strings for &lt;pattern&gt;. The pattern &lt;sortpattern&gt; is
	 * &lt;field&gt;:[DESC|ASC]
	 *
	 * @throws ApiException
	 *             if the Api call fails
	 */
	@Test
	public void listVhfGroundStationsTest() throws ApiException {
		String by = "location:Pa";
		Integer page = null;
		Integer size = null;
		String sort = null;
		VhfGroundStationDto body = new VhfGroundStationDto();
		body.setStationId(20);
		body.setIpAddress("1.0.0.2");
		body.setLocation("Paris");
		body.setThread("paris-ip2");
		VhfGroundStationDto resp = api.createVhfGroundStations(body);
		body = new VhfGroundStationDto();
		body.setStationId(30);
		body.setIpAddress("1.1.0.3");
		body.setLocation("Dubai");
		body.setThread("dubai-ip3");
		resp = api.createVhfGroundStations(body);
		body = new VhfGroundStationDto();
		body.setStationId(40);
		body.setIpAddress("1.1.0.1");
		body.setLocation("Paname");
		body.setThread("paname-ip4");
		resp = api.createVhfGroundStations(body);

		List<VhfGroundStationDto> response = api.listVhfGroundStations(by, page, size, sort);
		assertThat(response.size()).isEqualTo(2);

	}

	@Test
	public void findVhfGroundStationsErrorTest() throws ApiException {
		VhfGroundStationDto body = new VhfGroundStationDto();
		body.setStationId(20);
		body.setIpAddress("1.0.0.2");
		body.setLocation("Paris");
		body.setThread("paris-ip2");
		VhfGroundStationDto resp = api.createVhfGroundStations(body);
		try {
			VhfGroundStationDto response = api.findVhfGroundStations(21);
			log.debug("Received response " + response);
		} catch (ApiException e) {
			String jsonString = e.getResponseBody().toString();
			log.error("Method got exception: " + jsonString);
		}

	}

}
