import java.nio.file.{Files, Paths}
import java.math.BigInteger
import java.io.{FileOutputStream, BufferedOutputStream};
import scala.collection.mutable.ListBuffer

object GenerateVhfBin {
  
  def main(args: Array[String]) {
    var bytearr = GenerateVhfBin.readfile(openfile)
    GenerateVhfBin.dumpBytes("/tmp/data/cnes.scala.bin",bytearr)
  }
  
  def openfile(filename: String): Iterator[String] = {
      println("Reading file name "+filename)
      val source = timed(scala.io.Source.fromFile(filename)) 
      // took 0ms
      val lines = timed(source.getLines)
      // took 0ms
      lines
  }
  
  // measures time taken by enclosed code
  def timed[A](block: => A) = {
    val t0 = System.currentTimeMillis
    val result = block
    println("took " + (System.currentTimeMillis - t0) + "ms")
    result
  }
  
  def hexToBin(a: String): BigInteger = {
    val bi = new BigInteger(a,16)
    bi
  }
  
  def readfile(f: String => Iterator[String]): List[Array[Byte]] = {
    var lines = new ListBuffer[Array[Byte]]()
    for (l <- f("/tmp/data/cnes.hexa")){
      println(l)
      val pkt=l.drop(12)
      println(hexToBin(pkt).toString(2))
      val a:Array[Byte] = hexToBin(pkt).toByteArray();
      println("writing into array of size "+a.size)
      lines += a
    }    
    lines.toList
  }
  
  def dumpBytes(a: String, b: List[Array[Byte]]) {
    val bos = new BufferedOutputStream(new FileOutputStream(a))
    for (l <- b) {
      bos.write(l)
      println("writing array of "+l.size)
    }
    bos.close()  
  }
    
}
