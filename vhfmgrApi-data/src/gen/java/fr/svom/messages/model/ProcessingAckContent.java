/*
 * Svom Message Model
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0-oas3
 * Contact: andrea.formica@cern.ch
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package fr.svom.messages.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.svom.messages.model.ProcessingDescriptor;
import fr.svom.messages.model.ServiceDescriptor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * ProcessingAckContent
 */
@JsonPropertyOrder({
  ProcessingAckContent.JSON_PROPERTY_SERVICE_DESCRIPTOR,
  ProcessingAckContent.JSON_PROPERTY_PROCESSING_DESCRIPTOR
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class ProcessingAckContent   {
  public static final String JSON_PROPERTY_SERVICE_DESCRIPTOR = "service_descriptor";
  @JsonProperty(JSON_PROPERTY_SERVICE_DESCRIPTOR)
  private ServiceDescriptor serviceDescriptor;

  public static final String JSON_PROPERTY_PROCESSING_DESCRIPTOR = "processing_descriptor";
  @JsonProperty(JSON_PROPERTY_PROCESSING_DESCRIPTOR)
  private ProcessingDescriptor processingDescriptor;

  public ProcessingAckContent serviceDescriptor(ServiceDescriptor serviceDescriptor) {
    this.serviceDescriptor = serviceDescriptor;
    return this;
  }

  /**
   * Get serviceDescriptor
   * @return serviceDescriptor
   **/
  @JsonProperty("service_descriptor")
  @ApiModelProperty(required = true, value = "")
  @NotNull @Valid 
  public ServiceDescriptor getServiceDescriptor() {
    return serviceDescriptor;
  }

  public void setServiceDescriptor(ServiceDescriptor serviceDescriptor) {
    this.serviceDescriptor = serviceDescriptor;
  }

  public ProcessingAckContent processingDescriptor(ProcessingDescriptor processingDescriptor) {
    this.processingDescriptor = processingDescriptor;
    return this;
  }

  /**
   * Get processingDescriptor
   * @return processingDescriptor
   **/
  @JsonProperty("processing_descriptor")
  @ApiModelProperty(required = true, value = "")
  @NotNull @Valid 
  public ProcessingDescriptor getProcessingDescriptor() {
    return processingDescriptor;
  }

  public void setProcessingDescriptor(ProcessingDescriptor processingDescriptor) {
    this.processingDescriptor = processingDescriptor;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProcessingAckContent processingAckContent = (ProcessingAckContent) o;
    return Objects.equals(this.serviceDescriptor, processingAckContent.serviceDescriptor) &&
        Objects.equals(this.processingDescriptor, processingAckContent.processingDescriptor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceDescriptor, processingDescriptor);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProcessingAckContent {\n");
    
    sb.append("    serviceDescriptor: ").append(toIndentedString(serviceDescriptor)).append("\n");
    sb.append("    processingDescriptor: ").append(toIndentedString(processingDescriptor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

