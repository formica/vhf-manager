/*
 * VHF REST API setting in JAX-RS
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: andrea.formica@cern.ch
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package fr.svom.vhf.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * VhfGroundStationDto
 */
@JsonPropertyOrder({
  VhfGroundStationDto.JSON_PROPERTY_STATION_ID,
  VhfGroundStationDto.JSON_PROPERTY_NAME,
  VhfGroundStationDto.JSON_PROPERTY_ID,
  VhfGroundStationDto.JSON_PROPERTY_LOCATION,
  VhfGroundStationDto.JSON_PROPERTY_COUNTRY,
  VhfGroundStationDto.JSON_PROPERTY_DESCRIPTION,
  VhfGroundStationDto.JSON_PROPERTY_LONGITUDE,
  VhfGroundStationDto.JSON_PROPERTY_LATITUDE,
  VhfGroundStationDto.JSON_PROPERTY_ALTITUDE,
  VhfGroundStationDto.JSON_PROPERTY_MIN_ELEVATION_ANGLE,
  VhfGroundStationDto.JSON_PROPERTY_MACADDRESS
})
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class VhfGroundStationDto   {
  public static final String JSON_PROPERTY_STATION_ID = "stationId";
  @JsonProperty(JSON_PROPERTY_STATION_ID)
  private Integer stationId;

  public static final String JSON_PROPERTY_NAME = "name";
  @JsonProperty(JSON_PROPERTY_NAME)
  private String name;

  public static final String JSON_PROPERTY_ID = "id";
  @JsonProperty(JSON_PROPERTY_ID)
  private String id;

  public static final String JSON_PROPERTY_LOCATION = "location";
  @JsonProperty(JSON_PROPERTY_LOCATION)
  private String location;

  public static final String JSON_PROPERTY_COUNTRY = "country";
  @JsonProperty(JSON_PROPERTY_COUNTRY)
  private String country;

  public static final String JSON_PROPERTY_DESCRIPTION = "description";
  @JsonProperty(JSON_PROPERTY_DESCRIPTION)
  private String description;

  public static final String JSON_PROPERTY_LONGITUDE = "longitude";
  @JsonProperty(JSON_PROPERTY_LONGITUDE)
  private Double longitude;

  public static final String JSON_PROPERTY_LATITUDE = "latitude";
  @JsonProperty(JSON_PROPERTY_LATITUDE)
  private Double latitude;

  public static final String JSON_PROPERTY_ALTITUDE = "altitude";
  @JsonProperty(JSON_PROPERTY_ALTITUDE)
  private Double altitude;

  public static final String JSON_PROPERTY_MIN_ELEVATION_ANGLE = "minElevationAngle";
  @JsonProperty(JSON_PROPERTY_MIN_ELEVATION_ANGLE)
  private Double minElevationAngle;

  public static final String JSON_PROPERTY_MACADDRESS = "macaddress";
  @JsonProperty(JSON_PROPERTY_MACADDRESS)
  private String macaddress;

  public VhfGroundStationDto stationId(Integer stationId) {
    this.stationId = stationId;
    return this;
  }

  /**
   * Get stationId
   * @return stationId
   **/
  @JsonProperty("stationId")
  @ApiModelProperty(value = "")
  
  public Integer getStationId() {
    return stationId;
  }

  public void setStationId(Integer stationId) {
    this.stationId = stationId;
  }

  public VhfGroundStationDto name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @JsonProperty("name")
  @ApiModelProperty(value = "")
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public VhfGroundStationDto id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @JsonProperty("id")
  @ApiModelProperty(value = "")
  
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public VhfGroundStationDto location(String location) {
    this.location = location;
    return this;
  }

  /**
   * Get location
   * @return location
   **/
  @JsonProperty("location")
  @ApiModelProperty(value = "")
  
  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public VhfGroundStationDto country(String country) {
    this.country = country;
    return this;
  }

  /**
   * Get country
   * @return country
   **/
  @JsonProperty("country")
  @ApiModelProperty(value = "")
  
  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public VhfGroundStationDto description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
   **/
  @JsonProperty("description")
  @ApiModelProperty(value = "")
  
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public VhfGroundStationDto longitude(Double longitude) {
    this.longitude = longitude;
    return this;
  }

  /**
   * Get longitude
   * @return longitude
   **/
  @JsonProperty("longitude")
  @ApiModelProperty(value = "")
  
  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public VhfGroundStationDto latitude(Double latitude) {
    this.latitude = latitude;
    return this;
  }

  /**
   * Get latitude
   * @return latitude
   **/
  @JsonProperty("latitude")
  @ApiModelProperty(value = "")
  
  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public VhfGroundStationDto altitude(Double altitude) {
    this.altitude = altitude;
    return this;
  }

  /**
   * Get altitude
   * @return altitude
   **/
  @JsonProperty("altitude")
  @ApiModelProperty(value = "")
  
  public Double getAltitude() {
    return altitude;
  }

  public void setAltitude(Double altitude) {
    this.altitude = altitude;
  }

  public VhfGroundStationDto minElevationAngle(Double minElevationAngle) {
    this.minElevationAngle = minElevationAngle;
    return this;
  }

  /**
   * Get minElevationAngle
   * @return minElevationAngle
   **/
  @JsonProperty("minElevationAngle")
  @ApiModelProperty(value = "")
  
  public Double getMinElevationAngle() {
    return minElevationAngle;
  }

  public void setMinElevationAngle(Double minElevationAngle) {
    this.minElevationAngle = minElevationAngle;
  }

  public VhfGroundStationDto macaddress(String macaddress) {
    this.macaddress = macaddress;
    return this;
  }

  /**
   * Get macaddress
   * @return macaddress
   **/
  @JsonProperty("macaddress")
  @ApiModelProperty(value = "")
  
  public String getMacaddress() {
    return macaddress;
  }

  public void setMacaddress(String macaddress) {
    this.macaddress = macaddress;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VhfGroundStationDto vhfGroundStationDto = (VhfGroundStationDto) o;
    return Objects.equals(this.stationId, vhfGroundStationDto.stationId) &&
        Objects.equals(this.name, vhfGroundStationDto.name) &&
        Objects.equals(this.id, vhfGroundStationDto.id) &&
        Objects.equals(this.location, vhfGroundStationDto.location) &&
        Objects.equals(this.country, vhfGroundStationDto.country) &&
        Objects.equals(this.description, vhfGroundStationDto.description) &&
        Objects.equals(this.longitude, vhfGroundStationDto.longitude) &&
        Objects.equals(this.latitude, vhfGroundStationDto.latitude) &&
        Objects.equals(this.altitude, vhfGroundStationDto.altitude) &&
        Objects.equals(this.minElevationAngle, vhfGroundStationDto.minElevationAngle) &&
        Objects.equals(this.macaddress, vhfGroundStationDto.macaddress);
  }

  @Override
  public int hashCode() {
    return Objects.hash(stationId, name, id, location, country, description, longitude, latitude, altitude, minElevationAngle, macaddress);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VhfGroundStationDto {\n");
    
    sb.append("    stationId: ").append(toIndentedString(stationId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    longitude: ").append(toIndentedString(longitude)).append("\n");
    sb.append("    latitude: ").append(toIndentedString(latitude)).append("\n");
    sb.append("    altitude: ").append(toIndentedString(altitude)).append("\n");
    sb.append("    minElevationAngle: ").append(toIndentedString(minElevationAngle)).append("\n");
    sb.append("    macaddress: ").append(toIndentedString(macaddress)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

