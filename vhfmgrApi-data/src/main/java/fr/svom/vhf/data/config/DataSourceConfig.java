package fr.svom.vhf.data.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Configuration for datasource.
 *
 * @author formica
 *
 */
@Configuration
@ComponentScan("fr.svom.vhf.data.repositories")
public class DataSourceConfig {

    /**
     * @param ds
     *            the DataSource
     * @return JdbcTemplate
     */
    @Bean(name = "jdbcMain")
    public JdbcTemplate getMainTemplate(@Autowired DataSource ds) {
        return new JdbcTemplate(ds);
    }
}
