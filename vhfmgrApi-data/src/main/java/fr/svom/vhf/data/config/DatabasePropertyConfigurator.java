/**
 * 
 */
package fr.svom.vhf.data.config;

/**
 * Configuration properties.
 *
 * @author aformic
 *
 */
public final class DatabasePropertyConfigurator {

    /**
     * SCHEMA_NAME.
     */
    public static final String SCHEMA_NAME = "";

    /**
     * Default Ctor.
     */
    private DatabasePropertyConfigurator() {
    }
}
