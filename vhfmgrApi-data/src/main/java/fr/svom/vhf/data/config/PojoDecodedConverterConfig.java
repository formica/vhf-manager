package fr.svom.vhf.data.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfStationStatus;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * Configuration for decoded objects.
 *
 * @author formica
 *
 */
@Configuration
public class PojoDecodedConverterConfig {

    /**
     * FRAME.
     */
    private static final String FRAME = "frame";

    /**
     * @return MapperFactory
     */
    @Bean(name = "mapperDecodedFactory")
    public MapperFactory createOrikaMapperFactory() {
        final MapperFactory mapperDecodedFactory = new DefaultMapperFactory.Builder().build();
        this.initCcsdsPacketMap(mapperDecodedFactory);
        this.initFrameHeaderPacketMap(mapperDecodedFactory);
        this.initApidPacketMap(mapperDecodedFactory);
        this.initFrameHeaderPacketMap(mapperDecodedFactory);
        this.initPrimaryHeaderPacketMap(mapperDecodedFactory);
        return mapperDecodedFactory;
    }

    /**
     * @param mapperFactory
     *            the MapperFactory
     * @return
     */
    protected void initStationStatusMap(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(VhfStationStatus.class, fr.svom.frame.data.model.VhfStationStatus.class)
                .exclude(FRAME).exclude("sstatusId").byDefault().register();
    }

    /**
     * @param mapperFactory
     *            the MapperFactory
     * @return
     */
    protected void initCcsdsPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(CcsdsPacket.class, fr.svom.frame.data.model.CcsdsPacket.class)
                .exclude(FRAME).exclude("ccsdsId").exclude("ccsdsCrc").byDefault().register();
    }

    /**
     * @param mapperFactory
     *            the MapperFactory
     * @return
     */
    protected void initFrameHeaderPacketMap(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(FrameHeaderPacket.class,
                        fr.svom.frame.data.model.VhfFrameHeaderPacket.class)
                .exclude(FRAME).exclude("fId").byDefault().register();
    }

    /**
     * @param mapperFactory
     *            the MapperFactory
     * @return
     */
    protected void initPrimaryHeaderPacketMap(MapperFactory mapperFactory) {
        mapperFactory
                .classMap(PrimaryHeaderPacket.class,
                        fr.svom.frame.data.model.VhfDataHeaderPacket.class)
                .exclude(FRAME).exclude("phId").byDefault().register();
    }

    /**
     * @param mapperFactory
     *            the MapperFactory
     * @return
     */
    protected void initApidPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(PacketApid.class, fr.svom.frame.data.model.ApidPacket.class)
                .field("apid", "apidValue").exclude("packetName").exclude("packetDescription")
                .exclude("className").exclude("instrument").exclude("category").byDefault()
                .register();
    }

    /**
     * @param mapperFactory
     *            the MapperFactory
     * @return MapperFacade
     */
    @Bean(name = "mapperDecoded")
    @Autowired
    public MapperFacade createMapperFacade(
            @Qualifier("mapperDecodedFactory") MapperFactory mapperFactory) {
        return mapperFactory.getMapperFacade();
    }
}
