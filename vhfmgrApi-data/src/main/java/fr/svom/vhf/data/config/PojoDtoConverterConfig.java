package fr.svom.vhf.data.config;

import fr.svom.vhf.data.config.converters.DateToOffDateTimeConverter;
import fr.svom.vhf.data.config.converters.TimestampToOffDateTimeConverter;
import fr.svom.vhf.data.mappers.PrimaryHeaderConverter;
import fr.svom.vhf.data.mappers.VhfBurstIdConverter;
import fr.svom.vhf.data.mappers.VhfDecodedBaseCustom;
import fr.svom.vhf.data.mappers.VhfDecodedCustom;
import fr.svom.vhf.data.mappers.VhfTransferToBaseConverter;
import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.swagger.model.CcsdsPacketDto;
import fr.svom.vhf.swagger.model.FrameHeaderPacketDto;
import fr.svom.vhf.swagger.model.PacketApidDto;
import fr.svom.vhf.swagger.model.RawPacketDto;
import fr.svom.vhf.swagger.model.VhfBasePacket;
import fr.svom.vhf.swagger.model.VhfBinaryPacketDto;
import fr.svom.vhf.swagger.model.VhfDecodedPacketDto;
import fr.svom.vhf.swagger.model.VhfGroundStationDto;
import fr.svom.vhf.swagger.model.VhfNotificationDto;
import fr.svom.vhf.swagger.model.VhfSatAttitudeDto;
import fr.svom.vhf.swagger.model.VhfSatPositionDto;
import fr.svom.vhf.swagger.model.VhfStationStatusDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for mapping.
 *
 * @author formica
 */
@Configuration
public class PojoDtoConverterConfig {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PojoDtoConverterConfig.class);

    /**
     * Create the mapper factory to generate DTOs from POJOs and vice versa.
     *
     * @return the mapper factory.
     */
    @Bean(name = "mapperFactory")
    public MapperFactory createOrikaMapperFactory() {
        log.info("Creating Orika mappers....");
        final MapperFactory mapperFactory = new DefaultMapperFactory.Builder()
                .propertyResolverStrategy(new BuilderPropertyResolver())
                .build();
        // Create factory.
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();
        // Register converters in factory.
        converterFactory.registerConverter(new DateToOffDateTimeConverter());
        converterFactory.registerConverter(new TimestampToOffDateTimeConverter());
        // Register converters in factory.
        PrimaryHeaderConverter phconv = new PrimaryHeaderConverter();
        converterFactory.registerConverter(phconv);
        VhfTransferToBaseConverter tfb = new VhfTransferToBaseConverter();
        converterFactory.registerConverter(tfb);
        VhfBurstIdConverter bidconv = new VhfBurstIdConverter();
        converterFactory.registerConverter(bidconv);
        // Init mappings.
        this.initVhfGrounStationMap(mapperFactory);
        this.initCcsdsPacketMap(mapperFactory);
        this.initFrameHeaderPacketMap(mapperFactory);
        this.initVhfStationStatusMap(mapperFactory);
        this.initPacketApidMap(mapperFactory);
        this.initVhfTransferFrameMap(mapperFactory);
        this.initVhfBinaryPacketMap(mapperFactory);
        this.initVhfDecodedPacketMap(mapperFactory);
        this.initVhfRawPacketMap(mapperFactory);
        this.initVhfBinaryToBasePacketMap(mapperFactory);
        this.initVhfDecodedToBasePacketMap(mapperFactory);
        this.initVhfSatAttitudeMap(mapperFactory);
        this.initVhfSatPositionMap(mapperFactory);
        this.initVhfNotificationMap(mapperFactory);
        return mapperFactory;
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfGrounStationMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfGroundStation.class, VhfGroundStationDto.class).byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfSatAttitudeMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfSatAttitude.class, VhfSatAttitudeDto.class).byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfSatPositionMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfSatPosition.class, VhfSatPositionDto.class).byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initCcsdsPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(CcsdsPacket.class, CcsdsPacketDto.class).byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfStationStatusMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfStationStatus.class, VhfStationStatusDto.class).exclude("insertionTime")
                .exclude("updateTime").byDefault().register();
    }


    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initFrameHeaderPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(FrameHeaderPacket.class, FrameHeaderPacketDto.class).byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initPacketApidMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(PacketApid.class, PacketApidDto.class).exclude("tid").exclude("pid").exclude("pcat")
                .byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfBinaryPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfBinaryPacket.class, VhfBinaryPacketDto.class).exclude("frames")
                .exclude("binaryPacket").byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfBinaryToBasePacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfBinaryPacket.class, VhfBasePacket.class).exclude("frames").exclude("binaryPacket")
                .byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfDecodedPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfDecodedPacket.class, VhfDecodedPacketDto.class).customize(new VhfDecodedCustom())
                .register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfDecodedToBasePacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfDecodedPacket.class, VhfBasePacket.class).customize(new VhfDecodedBaseCustom())
                .register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfTransferFrameMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfTransferFrame.class, VhfTransferFrameDto.class).field("binaryHash",
                "hashId").exclude("packet")
                .exclude("packetJson").byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfNotificationMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfNotification.class, VhfNotificationDto.class)
                .exclude("frame")
                .byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return
     */
    protected void initVhfRawPacketMap(MapperFactory mapperFactory) {
        mapperFactory.classMap(VhfRawPacket.class, RawPacketDto.class).byDefault().register();
    }

    /**
     * @param mapperFactory the MapperFactory
     * @return MapperFacade
     */
    @Bean(name = "mapper")
    @Autowired
    public MapperFacade createMapperFacade(@Qualifier("mapperFactory") MapperFactory mapperFactory) {
        return mapperFactory.getMapperFacade();
    }
}
