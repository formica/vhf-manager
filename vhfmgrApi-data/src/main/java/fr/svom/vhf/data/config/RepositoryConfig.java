package fr.svom.vhf.data.config;

import javax.sql.DataSource;

import fr.svom.vhf.data.mappers.PacketDateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.svom.vhf.data.repositories.IJdbcRepository;
import fr.svom.vhf.data.repositories.JdbcRepositoryImpl;

/**
 * Configuration for repositories.
 *
 * @author formica
 *
 */
@Configuration
@ComponentScan("fr.svom.vhf.data.repositories")
public class RepositoryConfig {

    /**
     * Configuration properties.
     */
    @Autowired
    private VhfProperties cprops;

    /**
     * @param mainDataSource
     *            the DataSource
     * @return IJdbcRepository
     */
    @Bean(name = "jdbcrepo")
    public IJdbcRepository payloadDefaultRepository(
            @Qualifier("dataSource") DataSource mainDataSource) {
        return new JdbcRepositoryImpl(mainDataSource);
    }

    /**
     * Create a utility bean for date formats.
     *
     * @return PacketDateFormat
     */
    @Bean
    public PacketDateFormat packetDateFormat() {
        PacketDateFormat pdf = new PacketDateFormat();
        pdf.setOffset(cprops.getTimeOffset());
        return pdf;
    }
}
