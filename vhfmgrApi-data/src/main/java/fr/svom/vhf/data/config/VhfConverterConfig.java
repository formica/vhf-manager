package fr.svom.vhf.data.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.svom.frame.data.service.VhfDecoding;

/**
 * Configuration for decoder bean.
 *
 * @author formica
 *
 */
@Configuration
public class VhfConverterConfig {

    /**
     * @return VhfDecoding
     */
    @Bean(name = "vhfDecoderBean")
    public VhfDecoding createDecoder() {
        return new VhfDecoding();
    }
}
