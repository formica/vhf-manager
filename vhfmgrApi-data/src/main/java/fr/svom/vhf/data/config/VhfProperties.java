package fr.svom.vhf.data.config;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration properties.
 *
 * @author formica
 */
@Getter
@Setter
@Accessors(fluent = false)
@Component
@ConfigurationProperties("vhfmgr")
public class VhfProperties {

    /**
     * The directory to dump files.
     */
    private String dumpdir = "/tmp";
    /**
     * The schema name in the DB.
     */
    private String schemaname = "";
    /**
     * The schema name in the DB.
     */
    private String version = "";
    /**
     * The static web directory to serve.
     */
    private String webstaticdir = "/tmp";
    /**
     * The security settings.
     */
    private String security = "none";
    /**
     * The authentication type.
     */
    private String authenticationtype = "memory";
    /**
     * The upload directory.
     */
    private String uploaddir = "/tmp";
    /**
     * The upload directory.
     */
    private Long timeOffset = 0L;


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VhfProperties [dumpdir=" + dumpdir + ", schemaname=" + schemaname
               + ", version=" + version + ", webstaticdir=" + webstaticdir + ", security=" + security
               + ", authenticationtype=" + authenticationtype + ", uploaddir=" + uploaddir + ", timeOffset="
               + timeOffset + "]";
    }

}
