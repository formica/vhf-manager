/**
 * This file is part of PhysCondDB.
 * <p>
 * PhysCondDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * PhysCondDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with PhysCondDB.  If not, see <http://www.gnu.org/licenses/>.
 **/
package fr.svom.vhf.data.exceptions;

import fr.svom.vhf.data.packets.VhfDecodedPacket;

import javax.ws.rs.core.Response;

/**
 * Exception for decoding.
 *
 * @author formica
 *
 */
public class VhfDecodingException extends AbstractCdbServiceException {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = 1851049463533897275L;

    /**
     * Decoding error.
     */
    private VhfDecodedPacket errordecoded = null;

    /**
     * @param string
     *            the String
     */
    public VhfDecodingException(String string) {
        super(string);
    }

    /**
     * Default Ctor.
     *
     * @param msg
     *            the String
     * @param saved
     *            the VhfDecodedPacket
     */
    public VhfDecodingException(String msg, VhfDecodedPacket saved) {
        super(msg);
        this.errordecoded = saved;
    }

    /**
     * Add context information.
     * @param message
     * @param err
     */
    public VhfDecodingException(String message, Throwable err) {
        super(message, err);
    }

    /**
     * Add context information.
     * @param err
     */
    public VhfDecodingException(Throwable err) {
        super(err);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return "Decoding error " + super.getMessage();
    }

    /**
     * Associate an HTTP response code, in case this error needs to be sent to the client.
     *
     * @return the response status
     */
    @Override
    public Response.StatusType getResponseStatus() {
        return Response.Status.BAD_REQUEST;
    }


    /**
     * @return VhfDecodedPacket
     */
    public VhfDecodedPacket getErrordecoded() {
        return errordecoded;
    }
}
