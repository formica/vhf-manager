/**
 * 
 */
package fr.svom.vhf.data.exceptions;

import javax.ws.rs.core.Response;

/**
 * Generic service Exception.
 *
 * @author aformic
 *
 */
public class VhfServiceException extends AbstractCdbServiceException {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = -7770200527960758980L;

    /**
     * @param string
     *            the String
     */
    public VhfServiceException(String string) {
        super(string);
    }

    /**
     * Add context information.
     * @param message
     * @param err
     */
    public VhfServiceException(String message, Throwable err) {
        super(message, err);
    }

    /**
     * Add context information.
     * @param err
     */
    public VhfServiceException(Throwable err) {
        super(err);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Throwable#getMessage()
     */
    @Override
    public String getMessage() {
        return "Service error " + super.getMessage();
    }

    /**
     * Associate an HTTP response code, in case this error needs to be sent to the client.
     *
     * @return the response status
     */
    @Override
    public Response.StatusType getResponseStatus() {
        return Response.Status.INTERNAL_SERVER_ERROR;
    }

}
