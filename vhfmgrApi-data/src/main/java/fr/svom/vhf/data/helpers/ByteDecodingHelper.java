/**
 * 
 */
package fr.svom.vhf.data.helpers;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for decoding.
 *
 * @author formica
 *
 */
public final class ByteDecodingHelper {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ByteDecodingHelper.class);

    /**
     * Default Ctor.
     */
    private ByteDecodingHelper() {

    }

    /**
     * @param bi
     *            the BigInteger
     * @param byteorder
     *            the ByteOrder
     * @param destinationclass
     *            the String
     * @return Object
     */
    public static Object getObject(BigInteger bi, ByteOrder byteorder, String destinationclass) {
        try {
            Object data = null;
            final byte[] mb = bi.toByteArray();
            // Store the length of the received byte array.
            final int len = mb.length;
            // This variable will keep track of the difference in the number of bytes.
            int diff = 0;
            // Check destination class.
            switch (destinationclass) {
            case "java.lang.Integer":
                diff = 4 - len;
                break;
            case "java.lang.Long":
                diff = 8 - len;
                break;
            case "java.lang.Short":
                diff = 2 - len;
                break;
            default:
                diff = 0;
                break;
            }
            log.debug("Parsing byte array of size {} using {} and diff {}", len, destinationclass,
                    diff);
            // Dimension the byte buffer.
            final ByteBuffer bybuf = ByteBuffer.allocate(len + diff);
            // Check endianness.
            if (byteorder == ByteOrder.BIG_ENDIAN) {
                // The order is BIG_ENDIAN
                for (int ib = diff; ib > 0; ib--) {
                    log.debug("add bytes at the end...but remember that this IS BIG endian ??");
                    bybuf.put((byte) 0x00);
                }
                for (final byte b : mb) {
                    bybuf.put(b);
                }
            }
            else {
                // The order is LITTLE_ENDIAN
                for (final byte b : mb) {
                    bybuf.put(b);
                }
                for (int ib = diff; ib > 0; ib--) {
                    log.debug("add bytes at the end...but remember that this IS little endian ??");
                    bybuf.put((byte) 0x00);
                }
            }
            // Reorder and extract object.
            switch (destinationclass) {
            case "java.lang.Integer":
                data = bybuf.order(byteorder).getInt(0);
                break;
            case "java.lang.Long":
                data = bybuf.order(byteorder).getLong(0);
                break;
            case "java.lang.Short":
                data = bybuf.order(byteorder).getShort(0);
                break;
            default:
                break;
            }
            log.debug("Parsed data {}", data);
            return data;
        }
        catch (final RuntimeException e) {
            log.error("Exception in byte decoding: {}", e);
        }
        return null;
    }

    /**
     * @param bi
     *            the BigInteger
     * @param byteorder
     *            the ByteOrder
     * @param destinationclass
     *            the String
     * @return Object
     */
    public static Object getFloating(BigInteger bi, ByteOrder byteorder, String destinationclass) {
        try {
            Object data = null;
            byte[] barr = null;
            // Check the floating type.
            switch (destinationclass) {
            case "java.lang.Float":
                // the destination is a float.
                barr = ByteBuffer.allocate(4).putInt(bi.intValue()).array();
                final ByteBuffer fwrapped = ByteBuffer.wrap(barr);
                final Float ff = fwrapped.order(byteorder).getFloat();
                data = ff;
                break;
            case "java.lang.Double":
                // the destination is a double.
                barr = ByteBuffer.allocate(8).putLong(bi.longValue()).array();
                final ByteBuffer dwrapped = ByteBuffer.wrap(barr);
                final Double dd = dwrapped.order(byteorder).getDouble();
                data = dd;
                break;
            default:
                break;
            }
            log.debug("Parsed data {}", data);
            return data;
        }
        catch (final RuntimeException e) {
            log.error("Exception in byte decoding: {}", e);
        }
        return null;
    }
}
