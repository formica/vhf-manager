package fr.svom.vhf.data.helpers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.Map;

/**
 * A service for managing VHF transfer frames.
 *
 * @author formica
 */
public final class DataModelsHelper {

    /**
     * The Constant ALERT_TIME_TB0_ABS_SECONDS.
     */
    private static final String ALERT_TIME_TB0_ABS_SECONDS = "AlertTimeTb0AbsSeconds";
    /**
     * The Constant ALERT_TIME_TB0_SECONDS.
     */
    private static final String ALERT_TIME_TB0_SECONDS = "AlertTimeTb0Seconds";
    /**
     * The Constant ALERT_TIME_TB.
     */
    private static final String ALERT_TIME_TB = "AlertTimeTb";

    /**
     * The Constant INIT_NOPOS. Used when no available position.
     */
    private static final double INIT_NOPOS = -1.;

    /**
     * The Constant BIN_16INT. Represent 2^16-1
     */
    private static final double BIN_16INT = 65535.;

    /**
     * The Constant ANG_2PI. Represent the angle
     */
    private static final double ANG_2PI = 360.;

    /**
     * The Constant NONE.
     */
    private static final String NONE = "none";

    /**
     * The Constant INSTRUMENT_MODE.
     */
    private static final String INSTRUMENT_MODE = "InstrumentMode";

    /**
     * The Constant SAT_POSITION_LAT.
     */
    private static final String SAT_POSITION_LAT = "SatPositionLat";

    /**
     * The Constant SAT_POSITION_LON.
     */
    private static final String SAT_POSITION_LON = "SatPositionLon";

    /**
     * The Constant SAT_POSITION_Z.
     */
    private static final String SAT_POSITION_Z = "SatPositionZ";

    /**
     * The Constant SAT_POSITION_Y.
     */
    private static final String SAT_POSITION_Y = "SatPositionY";

    /**
     * The Constant SAT_POSITION_X.
     */
    private static final String SAT_POSITION_X = "SatPositionX";

    /**
     * The Constant SAT_ATTITUDE_Q3.
     */
    private static final String SAT_ATTITUDE_Q3 = "SatAttitudeQ3";

    /**
     * The Constant SAT_ATTITUDE_Q2.
     */
    private static final String SAT_ATTITUDE_Q2 = "SatAttitudeQ2";

    /**
     * The Constant SAT_ATTITUDE_Q1.
     */
    private static final String SAT_ATTITUDE_Q1 = "SatAttitudeQ1";

    /**
     * The Constant SAT_ATTITUDE_Q0.
     */
    private static final String SAT_ATTITUDE_Q0 = "SatAttitudeQ0";

    /**
     * The Constant representing seconds in a day.
     */
    private static final long SECOND_IN_DAY = 86400;
    /**
     * The Constant representing the fraction we want to obtain. 100 will be a number with 2 digits.
     */
    private static final int NORM_FACTOR = 100;
    /**
     * The Constant ALERT_TB0 for GRM Alert.
     */
    private static Map<String, String> tb0map = null;

    /**
     * Initialize map.
     */
    static {
        tb0map = new HashMap<String, String>();
        tb0map.put("TmVhfEclairsAlert", "AlertTimeTbAbsSeconds");
        tb0map.put("TmVhfEclairsHighPriorityLightCurve", ALERT_TIME_TB0_ABS_SECONDS);
        tb0map.put("TmVhfEclairsLowPriorityLightCurve", ALERT_TIME_TB0_ABS_SECONDS);
        tb0map.put("TmVhfGrmLPLcurve", ALERT_TIME_TB0_SECONDS);
        tb0map.put("TmVhfGrmHPLcurve", ALERT_TIME_TB0_SECONDS);
        tb0map.put("TmVhfGrmTrigger", ALERT_TIME_TB0_SECONDS);
    }

    /**
     * The Constant CURRENT_OBS_ID.
     */
    private static final String CURRENT_OBS_ID = "CurrentObsId";
    /**
     * The constant VHF_IOBSERV_ID.
     */
    private static final String VHF_IOBSERV_ID = "VhfIObservId";

    /**
     * The name extensions.
     */
    private static final String[] EXT_QI = {"Q0", "Q1", "Q2", "Q3"};

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DataModelsHelper.class);

    /**
     * ERROR message.
     */
    private static final String ERROR_NO_JSON = "No JSON content";
    /**
     * ERROR no info extraction message.
     */
    private static final String ERROR_NO_INFO = "Cannot extract info from decoded pkt {} : {}";

    /**
     * The jackson mapper.
     */
    private static final ObjectMapper JACKSON_MAPPER = new com.fasterxml.jackson.databind.ObjectMapper();

    /**
     * The time offset of the satellite: taken from properties.
     */
    private static Long timeOffset = 0L;

    /**
     * Hide default ctor.
     */
    private DataModelsHelper() {
    }

    public static void setTimeOffset(Long timeOffset) {
        DataModelsHelper.timeOffset = timeOffset;
    }

    /**
     * Creates the decoded packet.
     *
     * @param pkt     the String
     * @param format  the String
     * @param hashid  the String
     * @param pkttype the String
     * @return VhfDecodedPacket
     */
    public static VhfDecodedPacket buildDecodedPacket(String pkt, String format, String hashid, String pkttype) {
        final VhfDecodedPacket decpkt = new VhfDecodedPacket();
        decpkt.setPacket(pkt);
        decpkt.setPktformat(format);
        final long length = pkt == null ? 0 : (long) pkt.length();
        decpkt.setSize(length);
        decpkt.setHashId(hashid);
        decpkt.setPkttype(pkttype);

        return decpkt;
    }

    /**
     * Extract info from ccsds json. Instrument mode and obsid.
     *
     * @param json the String in json to be parsed
     * @return the map containing type and message content
     */
    public static Map<String, String> parseCcsds(String json) {
        final Map<String, String> jmap = new HashMap<>();
        try {
            log.debug("Extract InstrumentMode and Obsid from decoded packet string: {}", json);
            if (NONE.equals(json)) {
                log.error("No input json found");
                throw new IOException(ERROR_NO_JSON);
            }
            // Retrieve json node from input string
            final JsonNode jsonNode = JACKSON_MAPPER.readTree(json);
            // Search for InstrumentMode: as a consequence set the OBSID.
            if (jsonNode.has(INSTRUMENT_MODE)) {
                log.debug("Found instrument mode field {}", INSTRUMENT_MODE);
                jmap.put(INSTRUMENT_MODE, jsonNode.get(INSTRUMENT_MODE).asText());
            }
            JsonNode obsidNode = null;
            if (jsonNode.has(CURRENT_OBS_ID)) {
                log.debug("Found current obsid field {}", CURRENT_OBS_ID);
                obsidNode = jsonNode.get(CURRENT_OBS_ID);
            }
            if (jsonNode.has(VHF_IOBSERV_ID)) {
                log.debug("Found vhf obsid field {}", VHF_IOBSERV_ID);
                obsidNode = jsonNode.get(VHF_IOBSERV_ID);
            }
            // If JSON node is set, it should contain the obsid section.
            if (obsidNode != null) {
                jmap.put("VhfIObsType", obsidNode.get("VhfIObsType").asText());
                jmap.put("VhfNObsNumber", obsidNode.get("VhfNObsNumber").asText());
            }
            log.debug("Extracted obsid type and num map from decoded packet: {}", jmap);
            return jmap;
        }
        catch (final IOException e) {
            log.error(ERROR_NO_INFO, json, e.getMessage());
        }
        jmap.put(INSTRUMENT_MODE, NONE);
        return jmap;
    }

    /**
     * Extract attitude info from json.
     *
     * @param json the String in json to be parsed
     * @return the attitude object VhfSatAttitude
     */
    public static VhfSatAttitude parseAttitude(String json) {
        final Map<String, Double> jmap = new HashMap<>();
        try {
            log.trace("Parsing Attitude from decoded packet string");
            if (NONE.equals(json)) {
                throw new IOException(ERROR_NO_JSON);
            }
            // Retrieve json node from input string
            final JsonNode jsonNode = JACKSON_MAPPER.readTree(json);

            // Search for Attitude fields
            if (jsonNode.get(SAT_ATTITUDE_Q0) == null && jsonNode.get("SatAttitudeLCQ0") == null) {
                log.warn("Cannot find attitude in this packet {}", json);
            }
            else {
                // Get the attitude from decoded packet.
                String satatt = "SatAttitude";
                if (jsonNode.get("SatAttitudeLCQ0") != null) {
                    satatt = "SatAttitudeLC";
                }
                for (String ext : EXT_QI) {
                    String valstr = jsonNode.get(satatt + ext).asText();
                    Double val = null;
                    if (satatt.contains("LC")) {
                        // Stephan: Donc pour faire la conversion
                        // je prends le float (qui est entre 0 et 1),
                        // je multiplie par 65535 et je tronque sur des int.
                        // int16 = int(att*65535) => att = int16/65535
                        // Henri: int_quat = round(((float_quat + 1)/2.)*(2**16 - 1)) - 2**15
                        //        float_quat = ((2*int_quat + 2**16)/(2**16 - 1)) - 1
                        val = ((2 * Double.parseDouble(valstr) + BIN_16INT + 1.) / BIN_16INT) - 1.;
                        //val = Double.valueOf(valstr) / BIN_16INT;
                        Double timealert = Double.valueOf(jsonNode.get(ALERT_TIME_TB0_ABS_SECONDS).asText());
                        jmap.put(ALERT_TIME_TB0_ABS_SECONDS, timealert);
                        log.info("Use in attitude the time of alert : {}", timealert);
                    }
                    else {
                        val = Double.valueOf(valstr);
                    }
                    jmap.put("SatAttitude" + ext, val);
                }
            }
            // Exctract attitude if jmap not empty
            if (!jmap.isEmpty()) {
                // Create an attitude object.
                Long talert = (jmap.containsKey(ALERT_TIME_TB0_ABS_SECONDS)
                        ? jmap.get(ALERT_TIME_TB0_ABS_SECONDS).longValue()
                        : 0L);
                VhfSatAttitude attitude = new VhfSatAttitude();
                attitude.setAttId(-1L);
                attitude.setPacketTime(talert);
                attitude.setApidName(NONE);
                attitude.setSatAttitudeq0(jmap.get(SAT_ATTITUDE_Q0));
                attitude.setSatAttitudeq1(jmap.get(SAT_ATTITUDE_Q1));
                attitude.setSatAttitudeq2(jmap.get(SAT_ATTITUDE_Q2));
                attitude.setSatAttitudeq3(jmap.get(SAT_ATTITUDE_Q3));
                log.debug("Extracted attitude from decoded packet: {}", attitude);
                return attitude;
            }

        }
        catch (final IOException e) {
            log.error(ERROR_NO_INFO, json, e.getMessage());
        }
        return null;
    }

    /**
     * Extract position info from json.
     *
     * @param json the String in json to be parsed
     * @return the position object VhfSatPosition
     */
    public static VhfSatPosition parsePosition(String json) {
        final Map<String, Double> jmap = new HashMap<>();
        try {
            log.trace("Parsing Position from the decoded packet string");
            if (NONE.equals(json)) {
                throw new IOException(ERROR_NO_JSON);
            }
            // Retrieve json node from input string
            final JsonNode jsonNode = JACKSON_MAPPER.readTree(json);

            // Search for Position fields
            if ((jsonNode.get(SAT_POSITION_X) == null) && (jsonNode.get(SAT_POSITION_LAT) == null)) {
                log.warn("Cannot find position in this packet {}", json);
            }
            else {
                // Get the position from decoded packet.
                String satpos = "SatPosition";
                jmap.put(SAT_POSITION_X, INIT_NOPOS);
                jmap.put(SAT_POSITION_Y, INIT_NOPOS);
                jmap.put(SAT_POSITION_Z, INIT_NOPOS);
                jmap.put(SAT_POSITION_LON, INIT_NOPOS);
                jmap.put(SAT_POSITION_LAT, INIT_NOPOS);
                if (jsonNode.get(SAT_POSITION_X) != null) {
                    jmap.put(SAT_POSITION_X, Double.valueOf(jsonNode.get(satpos + "X").asText()));
                    jmap.put(SAT_POSITION_Y, Double.valueOf(jsonNode.get(satpos + "Y").asText()));
                    jmap.put(SAT_POSITION_Z, Double.valueOf(jsonNode.get(satpos + "Z").asText()));
                }
                if (jsonNode.get(SAT_POSITION_LON) != null) {
                    // Donc on prend l’angle (0-360°),
                    // on divise par 360, on multiplie par 65535 et on tronque.
                    // int16 = int((angle/360)*65535) => angle = (int16/65535)*360
                    Double lon = ((Double.parseDouble(jsonNode.get(satpos + "Lon").asText())) / BIN_16INT) * ANG_2PI;
                    Double lat = ((Double.parseDouble(jsonNode.get(satpos + "Lat").asText())) / BIN_16INT) * ANG_2PI;
                    jmap.put(SAT_POSITION_LON, lon);
                    jmap.put(SAT_POSITION_LAT, lat);
                }
            }
            // Extract position if jmap not empty.
            if (!jmap.isEmpty()) {
                // Create a position object.
                VhfSatPosition position = new VhfSatPosition();
                position.setPosId(-1L);
                position.setPacketTime(0L);
                position.setApidName(NONE);
                position.setSatPositionX(jmap.get(SAT_POSITION_X));
                position.setSatPositionY(jmap.get(SAT_POSITION_Y));
                position.setSatPositionZ(jmap.get(SAT_POSITION_Z));
                position.setSatPositionLat(jmap.get(SAT_POSITION_LAT));
                position.setSatPositionLon(jmap.get(SAT_POSITION_LON));
                log.debug("Extracted position from decoded packet: {}", position);
                return position;
            }
        }
        catch (final IOException e) {
            log.error(ERROR_NO_INFO, json, e.getMessage());
        }
        return null;
    }

    /**
     * Extract PacketNumber for "multi-packets" APIDs.
     *
     * @param jsonNode the json node.
     * @param key the String.
     * @return the Map<String, Integer>
     */
    public static Map<String, Integer> getPacketNumber(final JsonNode jsonNode, String key) {
        final Map<String, Integer> jmap = new HashMap<>();
        try {
            log.trace("Parsing Packet Number from the decoded packet string");
            if (jsonNode == null) {
                throw new IOException(ERROR_NO_JSON);
            }
            // Search for KEY field
            int packetNumber = -1;
            if (jsonNode.get(key) == null) {
                log.warn("Cannot find Packet ID in this packet for key {}", key);
            }
            else {
                packetNumber = Integer.parseInt(jsonNode.get(key).asText());
                // Get the PacketNumber from decoded packet.
                jmap.put(key, packetNumber);
            }
        }
        catch (final IOException e) {
            log.error(ERROR_NO_INFO, key, e.getMessage());
        }
        log.debug("Extracted packet number from decoded packet: {}", jmap);
        return jmap;
    }

    /**
     * Extract attitude info from json.
     *
     * @param json     the String in json to be parsed
     * @param apidName the apid name
     * @return the burstID object VhfBurstId
     */
    public static VhfBurstId parseAlertT0(String json, String apidName) {
        final Map<String, Object> jmap = new HashMap<>();
        try {
            log.trace("Parsing BurstID from decoded packet string");
            if (NONE.equals(json)) {
                throw new IOException(ERROR_NO_JSON);
            }
            // Retrieve json node from input string
            final JsonNode jsonNode = JACKSON_MAPPER.readTree(json);

            // Search for Alert time in fields
            log.debug("Search for alerttime in {}", json);
            Long alerttime;
            for (String key : tb0map.keySet()) {
                if (key.equals(apidName)) {
                    log.trace("Extract information from type {}", key);
                    if (jsonNode.get(tb0map.get(key)) != null) {
                        String jmapkey = tb0map.get(key);
                        alerttime = Long.valueOf(jsonNode.get(jmapkey).asText());
                        // We take the time from fields which are named differently in different packets,
                        // but then use store the value only in this key which will be used as source for the pojo.
                        jmap.put(ALERT_TIME_TB0_ABS_SECONDS, alerttime);
                        if ("TmVhfEclairsAlert".equals(key)) {
                            Long ugtstime = Long.valueOf(jsonNode.get(ALERT_TIME_TB).asText());
                            jmap.put(ALERT_TIME_TB, ugtstime);
                        }
                        else {
                            jmap.put(ALERT_TIME_TB, alerttime * 1000L);
                        }
                        // Convert to date: here we should convert the Tb0 to the correct number of seconds
                        // Get the instant to have a correct formatter for the burstId: sbYYMMDDff
                        Instant t0 = Instant.ofEpochMilli(alerttime * 1000L + (timeOffset * 1000L));
                        ZonedDateTime zdt = ZonedDateTime.ofInstant(t0, ZoneId.of("UTC"));
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
                        String svomId = zdt.format(formatter);
                        // Compute the fraction ff that shall be used in the burstId name.
                        int ff = (int) (NORM_FACTOR * ((double) zdt.get(ChronoField.SECOND_OF_DAY) / SECOND_IN_DAY));
                        jmap.put("burstId", String.format("sb%s%02d", svomId, ff));
                        // Set the default end time of the burst. This is the t0 + an interval corresponding to
                        // the resolution of our burstId : in case of a fraction expressed as ff it will be 864 seconds.
                        jmap.put("burstEndtime", alerttime + (SECOND_IN_DAY / NORM_FACTOR));
                        // Default burst start time is alerttime - 5 minutes.
                        jmap.put("burstStartime", alerttime - (300L));
                    }
                }
            }
            // Exctract burstid if jmap not empty
            if (!jmap.isEmpty()) {
                // Create an VhfBurstId object.
                Long tb0 = 0L;
                if (jmap.containsKey(ALERT_TIME_TB0_ABS_SECONDS)) {
                    tb0 = (Long) jmap.get(ALERT_TIME_TB0_ABS_SECONDS);
                }
                VhfBurstId bid = new VhfBurstId();
                bid.setBurstId((String) jmap.get("burstId"));
                bid.setAlertTimeTb((Long) jmap.get(ALERT_TIME_TB));
                bid.setAlertTimeTb0AbsSeconds(tb0);
                bid.setBurstEndtime((Long) jmap.get("burstEndtime"));
                bid.setBurstStartime((Long) jmap.get("burstStartime"));
                log.debug("Extracted burstid from decoded packet: {}", bid);
                return bid;
            }
        }
        catch (final IOException e) {
            log.error(ERROR_NO_INFO, json, e.getMessage());
        }
        return null;
    }

}
