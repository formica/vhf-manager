package fr.svom.vhf.data.helpers;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;

import fr.svom.vhf.data.exceptions.PayloadEncodingException;

/**
 * Utility class for hash generation.
 *
 * @author formica
 *
 */
public final class HashGenerator {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(HashGenerator.class);
    /**
     * SHA_TYPE.
     */
    private static final String SHA_TYPE = "SHA-256";

    /**
     * Default Ctor.
     */
    private HashGenerator() {
    }

    /**
     * @param message
     *            The message byte[] from which to generate md5.
     * @return The MD5 representation of the message string.
     * @throws PayloadEncodingException
     *             Exception encoding the message.
     */
    public static String shaJava(byte[] message) throws PayloadEncodingException {
        String digest = null;
        try {
            final MessageDigest md = MessageDigest.getInstance(SHA_TYPE);
            final byte[] hash = md.digest(message);
            // converting byte array to Hexadecimal String
            final StringBuilder sb = new StringBuilder(2 * hash.length);
            for (final byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }
            digest = sb.toString();
        }
        catch (final NoSuchAlgorithmException ex) {
            log.error(ex.getMessage());
            throw new PayloadEncodingException(ex);
        }
        return digest;
    }

    /**
     * @param text
     *            The text byte[] from which to generate md5 using DigestUtils.
     * @return The M5 representation of text.
     */
    public static String md5Spring(byte[] text) {
        return DigestUtils.md5DigestAsHex(text);
    }

    /**
     * @param in
     *            Inputstream.
     * @return The hash.
     * @throws IOException
     *             Exception writing files.
     * @throws NoSuchAlgorithmException
     *             Exception looking for a given algorithm instance.
     */
    public static String hash(BufferedInputStream in) throws IOException, NoSuchAlgorithmException {

        String digestHash;
        final MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
        final byte[] buffer = new byte[1024];
        int sizeRead = -1;
        while ((sizeRead = in.read(buffer)) != -1) {
            digest.update(buffer, 0, sizeRead);
        }
        in.close();

        byte[] hash = null;
        hash = digest.digest();

        // converting byte array to Hexadecimal String
        final StringBuilder sb = new StringBuilder(2 * hash.length);
        for (final byte b : hash) {
            sb.append(String.format("%02x", b & 0xff));
        }
        digestHash = sb.toString();
        return digestHash;
    }

    /**
     * @param in
     *            Inputstream.
     * @param os
     *            Outputstream.
     * @return The hash.
     * @throws IOException
     *             Exception writing files.
     * @throws NoSuchAlgorithmException
     *             Exception looking for a given algorithm instance.
     */
    public static String hashoutstream(InputStream in, OutputStream os)
            throws IOException, NoSuchAlgorithmException {

        String digestHash;
        final MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
        final byte[] buffer = new byte[1024];
        int sizeRead = -1;
        int loop = 0;
        while ((sizeRead = in.read(buffer)) != -1) {
            digest.update(buffer, 0, sizeRead);
            os.write(buffer, 0, sizeRead);
            loop++;
            if (loop % 100 == 0) {
                os.flush();
            }
        }
        os.flush();
        os.close();
        in.close();

        byte[] hash = null;
        hash = digest.digest();
        // converting byte array to Hexadecimal String
        final StringBuilder sb = new StringBuilder(2 * hash.length);
        for (final byte b : hash) {
            sb.append(String.format("%02x", b & 0xff));
        }
        digestHash = sb.toString();
        return digestHash;
    }
}
