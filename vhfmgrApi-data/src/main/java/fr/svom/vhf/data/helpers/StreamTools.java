/**
 * 
 */
package fr.svom.vhf.data.helpers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author formica
 *
 */
public final class StreamTools {

    /**
     * Private ctor.
     */
    private StreamTools() {

    }

    /**
     * Function that can be used to filter a list of objects on the base of a field
     * which is considered to be a key. Duplicates of that key will be filtered.
     *
     * @param keyExtractor
     *            The method to extract the key.
     * @param <T>
     *            The template class.
     * @return Predicate<T>
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        final Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
