package fr.svom.vhf.data.mappers;

/**
 * Enumeration for notification status.
 *
 * @author formica
 *
 */
public enum NotifStatus {
    /**
     * AWAIT.
     */
    AWAIT("AWAIT"),
    /**
     * FAILED.
     */
    FAILED("FAILED"),
    /**
     * COMPLETED.
     */
    SENT("SENT"),
    /**
     * IGNORED. This is the case for duplicates.
     */
    IGNORED("IGNORED"),
    /**
     * PROCESSED.
     */
    PROCESSED("PROCESSED");

    /**
     * The status.
     */
    private String status;

    /**
     * Default Ctor.
     *
     * @param status
     *            the String
     */
    NotifStatus(String status) {
        this.status = status;
    }

    /**
     * @return String
     */
    public String status() {
        return status;
    }
}
