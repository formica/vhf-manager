package fr.svom.vhf.data.mappers;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class PacketDateFormat {

    /**
     * time offset.
     */
    private static long offset = 0L;

    /**
     * Set the offset due to different satellite time.
     *
     * @param offset
     */
    public void setOffset(long offset) {
        PacketDateFormat.offset = offset;
    }

    /**
     * Get the offset due to different satellite time.
     *
     * @return offset the long.
     */
    public long getOffset() {
        return PacketDateFormat.offset;
    }

    /**
     * @param epochMilli the Instant
     * @return String
     */
    public static String format(Instant epochMilli) {
        // Use UTC zone.
        final ZonedDateTime zdt = epochMilli.atZone(ZoneId.of("Z"));
        // Return the millisec as a string formatted in ISO.
        return zdt.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    /**
     * The input is typically the packet time in seconds. Returns the instant after offset shift.
     *
     * @param seconds
     * @return Instant
     */
    public static Instant getInstant(long seconds) {
        return Instant.ofEpochMilli((seconds + offset) * 1000L);
    }
}
