package fr.svom.vhf.data.mappers;

import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.swagger.model.PrimaryHeaderPacketDto;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

/**
 * A bidirectional converter for the PrimaryHeader.
 * From Pojo to DTO and vice-versa.
 */
public class PrimaryHeaderConverter extends BidirectionalConverter<PrimaryHeaderPacket, PrimaryHeaderPacketDto> {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PrimaryHeaderConverter.class);

    @Override
    public PrimaryHeaderPacketDto convertTo(PrimaryHeaderPacket source, Type<PrimaryHeaderPacketDto> destinationType,
                                            MappingContext mappingContext) {
        // Create the DTO.
        PrimaryHeaderPacketDto dto = new PrimaryHeaderPacketDto();
        log.debug("convert source {} to dto", source);
        // Set fields.
        dto.packetObsid(source.getPacketObsid()).packetTime(source.getPacketTime());
        // Check for PacketObsidNum and PacketObsidType to be non null.
        if (source.getPacketObsidNum() != null && source.getPacketObsidType() != null) {
            dto.packetObsidNum((long) source.getPacketObsidNum())
                    .packetObsidType((long) source.getPacketObsidType());
        }
        // Check for InstrumentMode to be non null.
        if (source.getInstrumentMode() != null) {
            dto.instrumentMode(source.getInstrumentMode().longValue());
        }
        // Transform packettime in UTC time in milliseconds, using the offset.
        Instant pt = PacketDateFormat.getInstant(source.getPacketTime());
        // Format as ISO.
        dto.packetTimeIso(PacketDateFormat.format(pt));
        return dto;
    }

    @Override
    public PrimaryHeaderPacket convertFrom(PrimaryHeaderPacketDto source, Type<PrimaryHeaderPacket> destinationType,
                                           MappingContext mappingContext) {
        // Create the Pojo.
        PrimaryHeaderPacket entity = new PrimaryHeaderPacket();
        // Check InstrumentMode.
        if (source.getInstrumentMode() != null) {
            entity.setInstrumentMode(source.getInstrumentMode().intValue());
        }
        // Set other fields.
        entity.setPacketObsid(source.getPacketObsid());
        entity.setPacketObsidNum(source.getPacketObsidNum().intValue());
        entity.setPacketObsidType(source.getPacketObsidType().intValue());
        entity.setPacketTime(source.getPacketTime());
        return entity;
    }
}
