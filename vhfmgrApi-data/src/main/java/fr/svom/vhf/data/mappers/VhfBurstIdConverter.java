package fr.svom.vhf.data.mappers;

import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.swagger.model.VhfBurstIdDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

import java.time.Instant;

/**
 * A bidirectional converter for the VhfBurstId.
 * From Pojo to DTO and vice-versa.
 */
public class VhfBurstIdConverter extends BidirectionalConverter<VhfBurstId, VhfBurstIdDto> {

    @Override
    public VhfBurstIdDto convertTo(VhfBurstId source, Type<VhfBurstIdDto> destinationType,
                                   MappingContext mappingContext) {
        VhfBurstIdDto dto = new VhfBurstIdDto();
        dto.burstId(source.getBurstId()).packetTime(source.getPacketTime())
                .alertTimeTb0AbsSeconds(source.getAlertTimeTb0AbsSeconds())
                .alertTimeTb(source.getAlertTimeTb())
                .burstStartime(source.getBurstStartime())
                .burstEndtime(source.getBurstEndtime())
                .eclairObsid(source.getEclairObsid());
        if (source.getInsertionTime() != null) {
            // Set insertion time.
            dto.insertionTime(source.getInsertionTime().getTime());
            // Set the Insertion time ISO string.
            Instant pti = Instant.ofEpochMilli(source.getInsertionTime().getTime());
            dto.insertionTimeIso(PacketDateFormat.format(pti));
        }
        // Create the DTO.
        VhfTransferFrameDto framedto = mapperFacade.map(source.getFrame(), VhfTransferFrameDto.class);
        dto.frame(framedto);
        // Get the instant using the satellite offset.
        Instant pt = PacketDateFormat.getInstant(source.getAlertTimeTb0AbsSeconds());
        // Set the Alert Tb0.
        dto.alertTimeTb0AbsIso(PacketDateFormat.format(pt));
        return dto;
    }

    @Override
    public VhfBurstId convertFrom(VhfBurstIdDto source, Type<VhfBurstId> destinationType,
                                  MappingContext mappingContext) {
        // Create the POJO.
        VhfBurstId entity = new VhfBurstId();
        // Set fields.
        entity.setBurstId(source.getBurstId());
        entity.setBurstStartime(source.getBurstStartime());
        entity.setBurstEndtime(source.getBurstEndtime());
        // Set obsid ECL.
        entity.setEclairObsid(source.getEclairObsid());
        // Set Tb.
        entity.setAlertTimeTb(source.getAlertTimeTb());
        // Set Tb0.
        entity.setAlertTimeTb0AbsSeconds(source.getAlertTimeTb0AbsSeconds());
        // Set packet time.
        entity.setPacketTime(source.getPacketTime());
        return entity;
    }
}
