/**
 * 
 */
package fr.svom.vhf.data.mappers;

import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.swagger.model.VhfDecodedPacketDto;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

/**
 * @author formica
 *
 */
public class VhfDecodedCustom extends CustomMapper<VhfDecodedPacket, VhfDecodedPacketDto> {

    /*
     * (non-Javadoc)
     *
     * @see ma.glasnost.orika.CustomMapper#mapAtoB(java.lang.Object,
     * java.lang.Object, ma.glasnost.orika.MappingContext)
     */
    @Override
    public void mapAtoB(VhfDecodedPacket a, VhfDecodedPacketDto b, MappingContext context) {
        // Set fields for VhfDecodedPacketDto.
        b.setHashId(a.getHashId());
        b.setInsertionTime(a.getInsertionTime().getTime());
        b.setPacket(a.getPacket());
        b.setPktsize((long) a.getPacket().length());
        b.setPkttype(a.getPkttype());
        b.setPktformat("JSON");
        super.mapAtoB(a, b, context);
    }
}
