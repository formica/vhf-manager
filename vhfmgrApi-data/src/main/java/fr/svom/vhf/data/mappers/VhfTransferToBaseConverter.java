package fr.svom.vhf.data.mappers;

import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.swagger.model.VhfBasePacket;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;


/**
 * A bidirectional converter for the VhfTransferFrame.
 * From Pojo to DTO and vice-versa.
 */
public class VhfTransferToBaseConverter extends BidirectionalConverter<VhfTransferFrame, VhfBasePacket> {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfTransferToBaseConverter.class);

    @Override
    public VhfBasePacket convertTo(VhfTransferFrame source, Type<VhfBasePacket> destinationType,
                                            MappingContext mappingContext) {

        // Create the base packet.
        log.debug("convert transfer frame source {} to dto", source);
        VhfBasePacket b = new VhfBasePacket();
        // Set hash.
        b.setHashId(source.getBinaryHash());
        // Set reception time.
        if (source.getInsertionTime() != null) {
            b.setInsertionTime(source.getInsertionTime().getTime());
        }
        else {
            // Set reception time to now.
            log.warn("Null reception time, put the time of Instant.now");
            b.setInsertionTime(Instant.now().toEpochMilli());
        }
        // Set raw packet.
        b.setPacket(source.getRawPacket());
        // Set raw packet size.
        if (source.getRawPacket() != null) {
            b.setPktsize((long) source.getRawPacket().length());
        }
        else {
            // Set raw packet size to -1.
            b.setPktsize(-1L);
        }
        // Set Apid.
        if (source.getApid() != null) {
            b.setPkttype(source.getApid().getName());
        }
        else {
            b.setPkttype("Unknown");
        }
        b.setPktformat("HEXA");
        return b;
    }

    /**
     * Create a transfer frame by using only the hash field.
     *
     * @param source the VhfBasePacket.
     * @param destinationType
     * @param mappingContext
     * @return VhfTransferFrame
     */
    @Override
    public VhfTransferFrame convertFrom(VhfBasePacket source, Type<VhfTransferFrame> destinationType,
                                           MappingContext mappingContext) {
        log.info("convert source {} to transfer frame...should never happen", source.getHashId());
        // Create the POJO.
        VhfTransferFrame entity = new VhfTransferFrame();
        // Set the hash.
        entity.setBinaryHash(source.getHashId());
        return entity;
    }
}
