/**
 * 
 */
package fr.svom.vhf.data.packets;

import java.io.Serializable;

/**
 * This class represents an abstract packet.
 *
 * @author formica
 *
 */
public interface AbstractPacket extends Serializable {

    /**
     * @param frame
     *            the VhfTransferFrame to set
     * @return
     */
    void setFrame(VhfTransferFrame frame);
}
