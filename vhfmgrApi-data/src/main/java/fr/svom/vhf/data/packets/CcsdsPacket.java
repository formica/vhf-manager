/**
 * 
 */
package fr.svom.vhf.data.packets;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;

/**
 * CcsdsPacket POJO. This object contains data related to CCSDS packet. It is included in the VhfTransferFrame POJO.
 *
 * @author formica
 *
 */
@Data
@Accessors(fluent = false)
@Embeddable
public class CcsdsPacket {

    /**
     * The version number.
     */
    private Integer ccsdsVersionNum = null; // 3 bits

    /**
     * The type.
     */
    private Integer ccsdsType = null; // 1 bit

    /**
     * The secondary header flag.
     */
    private Integer ccsdsSecHeadFlag = null; // 1 bit

    /**
     * The apid.
     */
    private Integer ccsdsApid = null;

    /**
     * The flag.
     */
    private Integer ccsdsGFlag = null;

    /**
     * The counter.
     */
    private Integer ccsdsCounter = null;

    /**
     * The packet length.
     */
    private Integer ccsdsPlength = null;

    /**
     * The Crc.
     */
    private Integer ccsdsCrc = null;

}
