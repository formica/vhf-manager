package fr.svom.vhf.data.packets;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;

/**
 * FrameHeaderPacket POJO. It is included in the VhfTransferFrame POJO.
 *
 * @author formica
 *
 */
@Data
@Accessors(fluent = false)
@Embeddable
public class FrameHeaderPacket {

    /**
     * TM frame version.
     */
    private Integer tframeVersion;

    /**
     * The satellite ID.
     */
    private Integer spaceCraftId;

    /**
     * Virtual Channel ID.
     */
    private Integer vcId;

    /**
     * The OCF flag.
     */
    private Integer ocFlag;

    /**
     * The Master Channel Frame count.
     */
    private Integer mcfCount;
    /**
     * The Virtual Channel Frame count.
     */
    private Integer vcfCount;

    /**
     * The transfer frame data field status.
     */
    private Integer dfStatus;

}
