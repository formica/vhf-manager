/**
 * $Id$
 */
package fr.svom.vhf.data.packets;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * The PacketApid POJO. It represents an APID description.
 *
 * @author formica
 */
@Entity
@Table(name = "VHF_PACKET_APID", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class PacketApid implements Serializable {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = -5130733730326044527L;

    /**
     * Primary key : the APID.
     */
    @Id
    @Column(name = "APID")
    private Integer apid;

    /**
     * Packet name.
     */
    @Column(name = "NAME", unique = true, nullable = false, length = 100)
    private String name;

    /**
     * Packet description.
     */
    @Column(name = "DESCR", length = 100)
    private String description;

    /**
     * Class type to be used for parsing the byte stream.
     */
    @Column(name = "CLASS", length = 200)
    private String className;

    /**
     * Class type to be used for parsing the byte stream.
     */
    @Column(name = "DESTINATION", length = 200)
    private String destination;

    /**
     * The tid.
     */
    @Column(name = "TID")
    private Integer tid; // 1 bit

    /**
     * The pid.
     */
    @Column(name = "PID")
    private Integer pid; // 5 bits

    /**
     * The pcat.
     */
    @Column(name = "PCAT")
    private Integer pcat; // 5 bits

    /**
     * The instrument.
     */
    @Column(name = "INSTRUMENT", length = 100)
    private String instrument;

    /**
     * The category.
     */
    @Column(name = "CATEGORY", length = 50)
    private String category;

    /**
     * The details. Comma separated list mentioning details on relevant content.
     * It can be for example: Attitude,Position,
     */
    @Column(name = "DETAILS", length = 4000)
    private String details;

    /**
     * Alternative name found in some documentation.
     * Ex: for apid name TmVhfVtFindChartR1 the alias is VT_VHF_FINDCHART_R_1
     */
    @Column(name = "ALIAS", length = 100)
    private String alias;

    /**
     * The number of expected packets.
     */
    @Column(name = "EXPECTED_PACKETS")
    private Integer expectedPackets;

    /**
     * The priority.
     */
    @Column(name = "PRIORITY")
    private Integer priority;

}
