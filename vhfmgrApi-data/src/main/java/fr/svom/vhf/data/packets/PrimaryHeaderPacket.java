package fr.svom.vhf.data.packets;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;

/**
 * PrimaryHeaderPacket POJO.  It is included in the VhfTransferFrame POJO.
 *
 * @author formica
 */
@Data
@Accessors(fluent = false)
@Embeddable
public class PrimaryHeaderPacket {

    /**
     * The packet time in seconds.
     */
    private Long packetTime = null;
    /**
     * The packet Instrument Mode. This is present only for some Recurrent packets.
     */
    private Integer instrumentMode = null;
    /**
     * The packet Obsid.
     */
    private Long packetObsid = null;
    /**
     * The obsid type.
     */
    private Integer packetObsidType = null; // 1 byte
    /**
     * The obsid num.
     */
    private Integer packetObsidNum = null; // 3 bytes

}
