package fr.svom.vhf.data.packets;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.codec.binary.Hex;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;

/**
 * VhfBinaryPacket POJO.
 *
 * @author formica
 *
 */
@Entity
@Table(name = "VHF_BINARY_PACKET", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfBinaryPacket implements Serializable {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = 3773059870927496021L;

    /**
     * The primary header ID.
     */
    @Id
    @Column(name = "HASH_ID")
    private String hashId;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime;

    /**
     * The size.
     */
    @Column(name = "PKT_SIZE")
    private Integer size = 0;

    /**
     * The packet data.
     */
    @Column(name = "PKT_DATA", length = 512)
    private String packet;

    /**
     * The packet format.
     */
    @Column(name = "PKT_FORMAT", length = 50)
    private String pktformat = "HEX";

    /**
     * List of associated frames.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "binaryHash")
    private Set<VhfTransferFrame> frames = new HashSet<>(0);

    /**
     * The packet byte array.
     */
    @Transient
    private byte[] binaryPacket;

    /**
     * The packet location.
     */
    @Transient
    private String uri;

    /**
     * @param pkt the byte[]
     */
    public void setBinaryPacket(byte[] pkt) {
        final String hexaEncoded = Hex.encodeHexString(pkt);
        this.binaryPacket = pkt;
        this.packet = hexaEncoded;
    }
    /**
     * Check times.
     * 
     * @return
     */
    @PrePersist
    public void prePersist() {
        final Instant now = Instant.now();
        this.insertionTime = new Timestamp(now.toEpochMilli());
        setSize(this.packet.length());
    }
    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VhfBinaryPacket [hashId=" + hashId + ", insertionTime=" + insertionTime + ", size="
                + size + ", packet=" + packet + ", binaryPacket=" + Arrays.toString(binaryPacket)
                + ", uri=" + uri + "]";
    }
}
