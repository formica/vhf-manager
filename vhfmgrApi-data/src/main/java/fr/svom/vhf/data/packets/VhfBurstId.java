package fr.svom.vhf.data.packets;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * VhfSatAttitude POJO.
 *
 * @author formica
 */
@Entity
@Table(name = "SVOM_BURST_ID", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfBurstId implements Serializable {

    /**
     * The burst window length from the T0 or TB0, in seconds.
     */
    private static Long maxResolution = 800L;
    /**
     * The burst window length before the TB0, in seconds.
     */
    private static Long beforeBurst = 300L;
    /**
     * The burst ID. The string is generated in the method parseAlertT0 of DataModelsHelper.
     */
    @Id
    @Column(name = "BURST_ID", length = 50)
    private String burstId;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime;

    /**
     * The packet time in seconds.
     */
    @Column(name = "PACKET_TIME", nullable = false)
    private Long packetTime = null;

    /**
     * The Eclairs OBSID. This is set when ECLAIR packets are seen.
     */
    @Column(name = "ECL_OBSID", nullable = false)
    private Long eclairObsid = 0L;

    /**
     * The Tb0.
     */
    @Column(name = "ALERT_TIME_TB0_ABS_SECONDS", nullable = false)
    private Long alertTimeTb0AbsSeconds = 0L;

    /**
     * The Time of burst by UGTS.
     */
    @Column(name = "ALERT_TIME_TB", nullable = false)
    private Long alertTimeTb = 0L;

    /**
     * The original frame ID which triggered this burst_id.
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FRAME_ID")
    private VhfTransferFrame frame;

    /**
     * The endtime in seconds.
     */
    @Column(name = "burst_end_time")
    private Long burstEndtime = this.alertTimeTb0AbsSeconds + maxResolution;

    /**
     * The endtime in seconds.
     */
    @Column(name = "burst_start_time")
    private Long burstStartime = this.alertTimeTb0AbsSeconds - beforeBurst;

    /**
     * Set insertion time.
     */
    @PrePersist
    public void prePersist() {
        final Instant now = Instant.now();
        this.insertionTime = new Timestamp(now.toEpochMilli());
    }

    @Override
    public String toString() {
        return "VhfBurstId{"
               + "burstId='" + burstId + '\'' + ", insertionTime=" + insertionTime
               + ", packetTime=" + packetTime + ", eclairObsid=" + eclairObsid
               + ", alertTimeTb0AbsSeconds=" + alertTimeTb0AbsSeconds
               + ", alertTimeTb=" + alertTimeTb
               + ", burstEndtime=" + burstEndtime
               + ", burstStartime=" + burstStartime + '}';
    }
}
