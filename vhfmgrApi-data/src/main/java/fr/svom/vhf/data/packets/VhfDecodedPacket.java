package fr.svom.vhf.data.packets;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * VhfDecodedPacket POJO.
 *
 * @author formica
 *
 */
@Entity
@Table(name = "VHF_DECODED_PACKET", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfDecodedPacket implements Serializable {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = 3773059870927496021L;

    /**
     * The primary header ID.
     */
    @Id
    @Column(name = "HASH_ID")
    private String hashId;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime;

    /**
     * The packet size.
     */
    @Column(name = "PKT_SIZE")
    private Long size = 0L;

    /**
     * The packet format.
     */
    @Column(name = "PKT_FORMAT", length = 50)
    private String pktformat = "JSON";

    /**
     * The packet type.
     */
    @Column(name = "PKT_TYPE", length = 50)
    private String pkttype = "none";

    /**
     * The packet.
     */
    @Column(name = "PKT_DATA", nullable = false, length = 4000)
    private String packet;

    /**
     * The locator.
     */
    @Transient
    private String uri;

    /**
     * The set of associated frames.
     */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "binaryHash")
    private Set<VhfTransferFrame> frames;
    /**
     * Set times.
     *
     * @return
     */
    @PrePersist
    public void prePersist() {
        final Instant now = Instant.now();
        this.insertionTime = new Timestamp(now.toEpochMilli());
        setSize((long) this.packet.length());
    }
    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VhfDecodedPacket [hashId=" + hashId + ", insertionTime=" + insertionTime + ", size="
                + size + ", pktformat=" + pktformat + ", packet=" + packet + ", uri=" + uri + "]";
    }

}
