package fr.svom.vhf.data.packets;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Entity representing a ground station. station_id integer NOT NULL,
 * description character varying(500), identifier character varying(20) NOT
 * NULL, name character varying(30), location character varying(255) NOT NULL,
 * country character varying(255) NOT NULL, longitude numeric(12,5), latitude
 * numeric(12,5), altitude numeric(12,5), min_elevation_angle numeric(12,6),
 * mac_address character varying(30) NOT NULL
 *
 * @author formica
 */
@Entity
@Table(name = "VHF_GROUND_STATION", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfGroundStation implements Serializable {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = 1847289460494224092L;

    /**
     * The station Id.
     */
    @Id
    @Column(name = "STATION_ID", precision = 10)
    private Integer stationId;

    /**
     * The station name.
     */
    @Column(name = "NAME", unique = true, nullable = false, length = 30)
    private String name;

    /**
     * The station description.
     */
    @Column(name = "DESCRIPTION", length = 500)
    private String description;

    /**
     * The station location.
     */
    @Column(name = "LOCATION", nullable = false, length = 255)
    private String location;

    /**
     * The station country.
     */
    @Column(name = "COUNTRY", length = 255)
    private String country;

    /**
     * The id of the station as a String.
     */
    @Column(name = "IDENTIFIER", unique = true, nullable = false, length = 20)
    private String id;

    /** The longitude. */
    @Column(name = "longitude", columnDefinition = "NUMERIC")
    private Double longitude = 0.;

    /** The latitude. */
    @Column(name = "latitude", columnDefinition = "NUMERIC")
    private Double latitude = 0.;

    /** The altitude. */
    @Column(name = "altitude", columnDefinition = "NUMERIC")
    private Double altitude = 0.;

    /** The min elevation angle. */
    @Column(name = "min_elevation_angle", columnDefinition = "NUMERIC")
    private Double minElevationAngle = 0.;

    /**
     * The macaddress of the station. Not needed.
     */
    @Column(name = "MAC_ADDRESS", unique = true, nullable = false, length = 30)
    private String macaddress;
    @Override
    public String toString() {
        return "VhfGroundStation [stationId=" + stationId + ", name=" + name + ", description=" + description
                + ", location=" + location + ", country=" + country + ", id=" + id + ", longitude=" + longitude
                + ", latitude=" + latitude + ", altitude=" + altitude + ", minElevationAngle=" + minElevationAngle
                + ", macaddress=" + macaddress + "]";
    }
}
