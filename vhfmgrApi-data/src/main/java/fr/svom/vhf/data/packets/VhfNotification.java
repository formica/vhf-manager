package fr.svom.vhf.data.packets;

import java.sql.Timestamp;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * VHF Notification POJO. Used to log notifications that are sent via NATs.
 *
 * @author formica
 * 
 */
@Entity
@Table(name = "VHF_NOTIFICATION", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfNotification {

    /**
     * The ID.
     */
    @Id
    @SequenceGenerator(name = "notif_seq",
            sequenceName = DatabasePropertyConfigurator.SCHEMA_NAME + ".NOTIF_SEQUENCE")
    @GeneratedValue(generator = "notif_seq")
    @Column(name = "NOTIF_ID")
    private Long notifId;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime;

    /**
     * The size.
     */
    @Column(name = "NOTIF_SIZE")
    private Integer size = 0;

    /**
     * The message.
     */
    @Column(name = "NOTIF_MESSAGE", length = 1024)
    private String notifMessage;

    /**
     * The format.
     */
    @Column(name = "NOTIF_FORMAT", length = 50)
    private String notifFormat = "ASCII";

    /**
     * The flag.
     */
    @Column(name = "PKT_FLAG", length = 50)
    private String pktflag = "NOT_DECODED";

    /**
     * The related frame id.
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FRAME_ID")
    private VhfTransferFrame frame;
    /**
     * Set time when inserting.
     *
     * @return
     */
    @PrePersist
    public void prePersist() {
        final Instant now = Instant.now();
        final Timestamp nowts = new Timestamp(now.toEpochMilli());
        this.insertionTime = nowts;
    }
    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VhfNotification [notifId=" + notifId + ", insertionTime=" + insertionTime
                + ", size=" + size + ", notifMessage=" + notifMessage + ", notifFormat="
                + notifFormat + ", pktflag=" + pktflag + "]";
    }
}
