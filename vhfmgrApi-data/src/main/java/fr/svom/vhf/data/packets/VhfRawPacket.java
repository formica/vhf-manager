package fr.svom.vhf.data.packets;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * RawPacket POJO.
 *
 * @author formica
 * 
 */
@Entity
@Table(name = "VHF_RAW_PACKET", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfRawPacket implements Serializable {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = -6995436458039679019L;

    /**
     * The primary header ID.
     */
    @Id
    @SequenceGenerator(name = "rawpkt_seq",
            sequenceName = DatabasePropertyConfigurator.SCHEMA_NAME + ".RAWPKT_SEQUENCE")
    @GeneratedValue(generator = "rawpkt_seq")
    @Column(name = "RAW_ID")
    private Long rawId;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime;

    /**
     * The size.
     */
    @Column(name = "PKT_SIZE")
    private Integer size = 0;

    /**
     * The packet string.
     */
    @Column(name = "PKT_DATA", length = 512, unique = true)
    private String packet;

    /**
     * The packet string format.
     */
    @Column(name = "PKT_FORMAT", length = 50)
    private String pktformat = "HEX";

    /**
     * The packet flag.
     */
    @Column(name = "PKT_FLAG", length = 50)
    private String pktflag = "NOT_DECODED";
    /**
     * Set insertion time.
     *
     * @return
     */
    @PrePersist
    public void prePersist() {
        final Instant now = Instant.now();
        final Timestamp nowts = new Timestamp(now.toEpochMilli());
        this.insertionTime = nowts;
    }
    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VhfRawPacket [rawId=" + rawId + ", insertionTime=" + insertionTime + ", size="
                + size + ", packet=" + packet + ", pktformat=" + pktformat + ", pktflag=" + pktflag
                + "]";
    }
}
