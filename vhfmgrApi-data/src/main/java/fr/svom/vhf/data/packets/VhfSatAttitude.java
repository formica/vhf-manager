package fr.svom.vhf.data.packets;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * VhfSatAttitude POJO.
 *
 * @author formica
 */
@Entity
@Table(name = "VHF_SAT_ATTITUDE", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfSatAttitude implements Serializable {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = -6995436458039679019L;

    /**
     * The attitude ID. It is the same as the frame ID. This is why it is not
     * generated but assigned.
     */
    @Id
    @Column(name = "ATT_ID")
    private Long attId;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime;

    /**
     * The packet time.
     */
    @Column(name = "PACKET_TIME", nullable = false)
    private Long packetTime = null;

    /**
     * The Q0.
     */
    @Column(name = "sat_attitudeq0", columnDefinition = "NUMERIC")
    private Double satAttitudeq0 = 0.0;

    /**
     * The Q1.
     */
    @Column(name = "sat_attitudeq1", columnDefinition = "NUMERIC")
    private Double satAttitudeq1 = 0.0;

    /**
     * The Q2.
     */
    @Column(name = "sat_attitudeq2", columnDefinition = "NUMERIC")
    private Double satAttitudeq2 = 0.0;

    /**
     * The Q3.
     */
    @Column(name = "sat_attitudeq3", columnDefinition = "NUMERIC")
    private Double satAttitudeq3 = 0.0;

    /**
     * Apid name.
     */
    @Column(name = "APID_NAME", nullable = false, length = 100)
    private String apidName;
    /**
     * Set insertion time.
     */
    @PrePersist
    public void prePersist() {
        final Instant now = Instant.now();
        this.insertionTime = new Timestamp(now.toEpochMilli());
    }
    /*
     * (non-javadoc) toString: @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VhfSatAttitude [attId=" + attId + ", insertionTime=" + insertionTime + ", packetTime=" + packetTime
                + ", SatAttitudeQ0=" + satAttitudeq0 + ", SatAttitudeQ1=" + satAttitudeq1 + ", SatAttitudeQ2="
                + satAttitudeq2 + ", SatAttitudeQ3=" + satAttitudeq3 + ", apidName=" + apidName + "]";
    }
}
