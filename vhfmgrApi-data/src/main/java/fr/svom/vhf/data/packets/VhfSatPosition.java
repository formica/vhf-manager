package fr.svom.vhf.data.packets;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * VhfSatAttitude POJO.
 *
 * @author formica
 */
@Entity
@Table(name = "VHF_SAT_POSITION", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfSatPosition implements Serializable {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = -6995436458039679019L;

    /**
     * The attitude ID. It is the same as the frame ID. This is why it is not
     * generated but assigned.
     */
    @Id
    @Column(name = "POS_ID")
    private Long posId;

    /**
     * The insertion time.
     */
    @Column(name = "INSERTION_TIME", nullable = true, updatable = false)
    private Timestamp insertionTime;

    /**
     * The packet time.
     */
    @Column(name = "PACKET_TIME", nullable = false)
    private Long packetTime = null;

    /**
     * The X pos.
     */
    @Column(name = "sat_position_x", columnDefinition = "NUMERIC")
    private Double satPositionX = 0.0;

    /**
     * The Y pos.
     */
    @Column(name = "sat_position_y", columnDefinition = "NUMERIC")
    private Double satPositionY = 0.0;

    /**
     * The Z pos.
     */
    @Column(name = "sat_position_z", columnDefinition = "NUMERIC")
    private Double satPositionZ = 0.0;

    /**
     * The Latitude.
     */
    @Column(name = "sat_position_lat", columnDefinition = "NUMERIC")
    private Double satPositionLat = 0.0;
    /**
     * The Longitude.
     */
    @Column(name = "sat_position_lon", columnDefinition = "NUMERIC")
    private Double satPositionLon = 0.0;

    /**
     * Apid name.
     */
    @Column(name = "APID_NAME", nullable = false, length = 100)
    private String apidName;
    /**
     * Set insertion time.
     */
    @PrePersist
    public void prePersist() {
        final Instant now = Instant.now();
        this.insertionTime = new Timestamp(now.toEpochMilli());
    }
}
