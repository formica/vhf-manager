/**
 * 
 */
package fr.svom.vhf.data.packets;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The station status POJO. It is filled using information from VHF stream.
 * Specifically the trailer part of the TCP/IP packet.
 *
 * @author formica
 *
 */
@Entity
@Table(name = "VHF_STATION_STATUS", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfStationStatus implements AbstractPacket {

    /**
     * Serializer.
     */
    private static final long serialVersionUID = 6791225887386942852L;

    /**
     * The station status id.
     */
    @Id
    @SequenceGenerator(name = "station_seq",
            sequenceName = DatabasePropertyConfigurator.SCHEMA_NAME + ".STATION_SEQUENCE")
    @GeneratedValue(generator = "station_seq")
    private Long sstatusId;

    /**
     * The correspondent frame. There is a one to one connection with
     * VhfTransferFrame.
     */
    @OneToOne(fetch = FetchType.LAZY,
            cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
    @JoinColumn(name = "FRAME_ID")
    private VhfTransferFrame frame;

    /**
     * The Id of the station.
     */
    private Integer idStation;

    /**
     * The channel.
     */
    private Integer channel;

    /**
     * The padding.
     */
    private Integer padding1;

    /**
     * The station time.
     */
    private Long stationTime;

    /**
     * The correction power.
     */
    private Integer corrPower;

    /**
     * The padding.
     */
    private Integer padding2;
    /**
     * The doppler.
     */
    private Float doppler;
    /**
     * The doppler drift.
     */
    private Float dopDrift;
    /**
     * The error.
     */
    private Float srerror;

    /**
     * The reed Solomon correction.
     */
    private Integer reedSolomonCorr;

    /**
     * The check sum.
     */
    private Integer ckSum;

    /**
     * The padding.
     */
    private Integer padding3;

    @Override
    public String toString() {
        return "VhfStationStatus [sstatusId=" + sstatusId + ", idStation=" + idStation
                + ", channel=" + channel + ", padding1=" + padding1 + ", stationTime=" + stationTime
                + ", corrPower=" + corrPower + ", padding2=" + padding2 + ", doppler=" + doppler
                + ", dopDrift=" + dopDrift + ", srerror=" + srerror + ", reedSolomonCorr="
                + reedSolomonCorr + ", ckSum=" + ckSum + ", padding3=" + padding3 + "]";
    }
}
