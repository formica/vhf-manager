/**
 * 
 */
package fr.svom.vhf.data.packets;

import fr.svom.vhf.data.config.DatabasePropertyConfigurator;
import fr.svom.vhf.data.mappers.NotifStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * The VhfTransferFrame POJO. It contains generic information that the VHF
 * software chain can collect when a packet arrives: reception time and other
 * parameters are evaluated. The frameId is stored together with every other
 * POJO of the unpacked packet. Hibernate / JPA mappings: Remember that all
 * mapping xxxToOne (@ManyToOne and @OneToOne) are EAGER by default.
 *
 * @author formica
 */
@Entity
@Table(name = "VHF_TRANSFER_FRAME", schema = DatabasePropertyConfigurator.SCHEMA_NAME)
@Data
@Accessors(fluent = false)
public class VhfTransferFrame implements Serializable {

    /**
     * Logger.
     */
    private static final long serialVersionUID = -7399116339129740039L;

    /**
     * The frame id , a generated long as PK.
     */
    @SequenceGenerator(name = "frame_seq", sequenceName = DatabasePropertyConfigurator.SCHEMA_NAME + ".FRAME_SEQUENCE")
    @Id
    @GeneratedValue(generator = "frame_seq")
    @Column(name = "FRAME_ID", precision = 20)
    private Long frameId;

    /**
     * The time of the packet reception by the fsc-asm. Units are milliseconds since
     * epoch.
     */
    @Column(name = "INSERTION_TIME", length = 6)
    @EqualsAndHashCode.Exclude
    private Timestamp insertionTime;

    /**
     * Validation flag for the tcp/ip frame received.
     */
    @Column(name = "IS_VALID", nullable = false)
    private Boolean isFrameValid;

    /**
     * Duplicate flag for the tcp/ip frame received.
     */
    @Column(name = "IS_DUPLICATE", nullable = false)
    @EqualsAndHashCode.Exclude
    private Boolean isFrameDuplicate;

    /**
     * The duplicate frame id, or null if the frame has no duplicate.
     */
    @Column(name = "DUP_FRAME_ID", precision = 20, nullable = true)
    private Long dupFrameId;

    /**
     * The notification status for this frame: AWAIT, SENT, FAILED, PROCESSED or IGNORED.
     */
    @Column(name = "NOTIF_STATUS", length = 10)
    private String notifStatus = NotifStatus.AWAIT.status();

    /**
     * The status of this frame processing.
     */
    @Column(name = "FRAME_STATUS", length = 520)
    private String frameStatus = "START";

    /**
     * The apid id, a foreign key pointing to another table.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APID_ID", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private PacketApid apid;

    /**
     * The station id, an foreign key pointing to another table.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "STATION_ID", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private VhfGroundStation station;

    /**
     * The ccsds packet words.
     */
    private FrameHeaderPacket frameHeader;
    /**
     * The ccsds packet words.
     */
    private CcsdsPacket ccsds;

    /**
     * The ccsds packet words.
     */
    private PrimaryHeaderPacket header;

    /**
     * The station status trailer word.
     */
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "frame", cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private VhfStationStatus stationStatus;

    /**
     * Here we just need the HASH.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HASH_ID", referencedColumnName = "HASH_ID", insertable = false, updatable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private VhfBinaryPacket packet;

    /**
     * Here we just need the HASH.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HASH_ID", referencedColumnName = "HASH_ID", insertable = false, updatable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private VhfDecodedPacket packetJson;

    /**
     * The raw packet.
     */
    @Column(name = "RAW_PACKET", length = 520)
    private String rawPacket;

    /**
     * The binary Hash.
     */
    @Column(name = "HASH_ID", length = 512)
    private String binaryHash;

    /**
     * The burst id.
     */
    @Column(name = "BURST_ID", length = 50)
    private String burstId = "none";
    /**
     * Check time before persistence.
     *
     * @return
     */
    @PrePersist
    public void checktime() {
        if (insertionTime == null) {
            final Instant now = Instant.now();
            insertionTime = new Timestamp(now.toEpochMilli());
        }
    }
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "VhfTransferFrame [frameId=" + frameId + ", receptionTime=" + insertionTime + ", isFrameValid="
               + isFrameValid + ", isFrameDuplicate=" + isFrameDuplicate + ", dupFrameId=" + dupFrameId
               + ", frameStatus=" + frameStatus + ", apid=" + apid + ", station=" + station + ", frameHeader="
               + frameHeader + ", ccsds=" + ccsds + ", header=" + header + ", stationStatus=" + stationStatus
               + ", rawPacket=" + rawPacket + ", binaryHash=" + binaryHash + "]";
    }

}
