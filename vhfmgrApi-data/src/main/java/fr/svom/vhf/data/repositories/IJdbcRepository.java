package fr.svom.vhf.data.repositories;

import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.swagger.model.ObsIdApidCountDto;
import fr.svom.vhf.swagger.model.VhfStationStatusCountDto;

import java.util.List;
import java.util.Map;

/**
 * Interface for special jdbc methods.
 *
 * @author formica
 */
public interface IJdbcRepository {

    /**
     * @param apidname the String
     * @param product  the String
     * @param obsid    the Long
     * @param from     the Timestamp
     * @param to       the Timestamp
     * @return List<ObsIdApidCountDto>
     */
    List<ObsIdApidCountDto> getCountObsIdSummaryInfo(String apidname, String product, Long obsid, Long from, Long to);

    /**
     * @param from the Long
     * @param to   the Long
     * @return List<VhfStationStatusCountDto>
     */
    List<VhfStationStatusCountDto> getCountVhfStationStatus(Long from, Long to);

    /**
     * @param from the Long
     * @param to   the Long
     * @return List<String>
     */
    List<String> getDuplicateHashes(Long from, Long to);

    /**
     * @param from the Long
     * @param to   the Long
     * @return Long
     */
    Long getPacketCountInTimeRange(Long from, Long to);

    /**
     * Gets the obsid list in time range.
     *
     * @param from the from
     * @param to   the to
     * @return the obsid list in time range
     */
    List<Map<String, Object>> getObsidListInTimeRange(Long from, Long to);

    /**
     * Gets the obsid list in time range.
     *
     * @param id the obsid
     * @return the number of deleted packets
     * @throws AbstractCdbServiceException
     */
    Integer deletePacketsByObsid(Long id) throws AbstractCdbServiceException;

}
