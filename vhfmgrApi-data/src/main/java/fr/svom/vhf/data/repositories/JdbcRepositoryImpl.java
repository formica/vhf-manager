/**
 * This file is part of PhysCondDB.
 * <p>
 * PhysCondDB is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * PhysCondDB is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with PhysCondDB.  If not, see <http://www.gnu.org/licenses/>.
 **/
package fr.svom.vhf.data.repositories;

import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.VhfSQLException;
import fr.svom.vhf.swagger.model.ObsIdApidCountDto;
import fr.svom.vhf.swagger.model.VhfStationStatusCountDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A repository for JDBC queries.
 *
 * @author formica
 */
public class JdbcRepositoryImpl implements IJdbcRepository {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(JdbcRepositoryImpl.class);

    /**
     * Datasource.
     */
    private final DataSource ds;

    /**
     * time offset for packet time.
     */
    private static final Long PKT_TIME_OFFSET = 1483228800L;

    /**
     * Default Ctor.
     *
     * @param ds the DataSource
     */
    public JdbcRepositoryImpl(DataSource ds) {
        super();
        this.ds = ds;
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.svom.vhf.data.repositories.IJdbcRepository#getCountObsIdSummaryInfo(java.
     * lang.String, java.lang.Long, java.lang.Long, java.lang.Long)
     */
    @Override
    public List<ObsIdApidCountDto> getCountObsIdSummaryInfo(String apidname, String destination, Long obsid, Long from,
                                                            Long to) {
        // Create the jdbc template from the Datasource ds.
        log.info("Select count of packets by obsid and apid using JDBCTEMPLATE");
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);

        // Create the SQL query.
        // This is a native query.
        final String sql = "select vhftf.PACKET_OBSID as packetObsId, vpa.APID as apid, vpa.NAME as packetName, "
                           + " vpa.DESTINATION as destination, vpa.EXPECTED_PACKETS as expectedPackets, "
                           + " COUNT(*) as countedPackets,  min(vhftf.PACKET_TIME) as packetMinTime, "
                           + " max(vhftf.PACKET_TIME) as packetMaxTime, "
                           + " avg(extract(epoch from vhftf.INSERTION_TIME)-vhftf.PACKET_TIME) as avgTimeAvailability, "
                           + " min(extract(epoch from vhftf.INSERTION_TIME)-vhftf.PACKET_TIME) as minTimeAvailability, "
                           + " max(extract(epoch from vhftf.INSERTION_TIME)-vhftf.PACKET_TIME) as maxTimeAvailability "
                           + " FROM VHF_TRANSFER_FRAME vhftf, VHF_PACKET_APID vpa "
                           + " where vhftf.PACKET_TIME>=? and vhftf.PACKET_TIME<? and vhftf.IS_DUPLICATE is False "
                           + " and vpa.NAME like ? and vpa.DESTINATION like ? "
                           + " and (vhftf.PACKET_OBSID=? or ?<=0) and vpa.APID=vhftf.APID_ID ";

        // Add group by expression and order by.
        String groupby = " GROUP BY vpa.APID, vpa.DESTINATION, vpa.EXPECTED_PACKETS, vhftf.PACKET_OBSID";
        final String orderby = " order by vhftf.PACKET_OBSID ASC, vpa.APID ASC";
        log.info("Using arguments from={} to={} apid={} obsid={} destination={}", from, to, apidname, obsid,
                destination);
        /**
         * Tested request: select vhfhp.PACKET_OBSID as packetObsId, vpa.APID as apid,
         * vpa.NAME as packetName, COUNT(*) as countedPackets from VHF_TRANSFER_FRAME
         * vhftf, VHF_HEADER_PACKET vhfhp, VHF_PACKET_APID vpa where
         * vhfhp.FRAME_ID=vhftf.FRAME_ID and vpa.APID=vhftf.APID_ID GROUP BY
         * vpa.NAME,vpa.APID,vhfhp.PACKET_OBSID;
         */
        return jdbcTemplate.query(sql + groupby + orderby,
                (rs, num) -> {
                    final ObsIdApidCountDto entity = new ObsIdApidCountDto();
                    entity.setApid(rs.getInt("apid"));
                    entity.setDestination(rs.getString("destination"));
                    entity.setPacketName(rs.getString("packetName"));
                    entity.setPacketMinTime(rs.getLong("packetMinTime"));
                    entity.setPacketMaxTime(rs.getLong("packetMaxTime"));
                    entity.setPacketObsid(rs.getLong("packetObsId"));
                    entity.setCountedPackets(rs.getInt("countedPackets"));
                    entity.setExpectedPackets(rs.getInt("expectedPackets"));
                    entity.avgTimeAvailability(rs.getFloat("avgTimeAvailability") - PKT_TIME_OFFSET);
                    entity.maxTimeAvailability(rs.getFloat("maxTimeAvailability") - PKT_TIME_OFFSET);
                    entity.minTimeAvailability(rs.getFloat("minTimeAvailability") - PKT_TIME_OFFSET);
                    return entity;
                }, from, to, apidname, destination, obsid, obsid);
    }

    /*
     * (non-Javadoc)
     * @see
     * fr.svom.vhf.data.repositories.IJdbcRepository#getCountVhfStationStatus(java.
     * lang.Long, java.lang.Long)
     */
    @Override
    public List<VhfStationStatusCountDto> getCountVhfStationStatus(Long from, Long to) {

        // Create the jdbc template from the Datasource ds.
        log.info("Select count station status using JDBCTEMPLATE");
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);

        // Create the SQL query.
        // This is a native query.
        final String sql = "select vhfss.ID_STATION as idStation, count(vhfss.ID_STATION) as npackets, "
                           + " min(vhfss.STATION_TIME) as minTime, " + " max(vhfss.STATION_TIME) as maxTime, "
                           + " avg(vhfss.CORR_POWER) as avgCorrPower, stddev(vhfss.CORR_POWER) as stdCorrPower, "
                           + " avg(vhfss.DOPPLER) as avgDoppler, stddev(vhfss.DOPPLER) as stdDoppler "
                           + " FROM VHF_STATION_STATUS vhfss "
                           + " where vhfss.STATION_TIME>=? and vhfss.STATION_TIME<? "
                           + " GROUP BY vhfss.ID_STATION ";

        log.info("Using arguments from={} to={} ", from, to);
        /**
         * Tested request: select vhfhp.PACKET_OBSID as packetObsId, vpa.APID as apid,
         * vpa.NAME as packetName, COUNT(*) as countedPackets from VHF_TRANSFER_FRAME
         * vhftf, VHF_HEADER_PACKET vhfhp, VHF_PACKET_APID vpa where
         * vhfhp.FRAME_ID=vhftf.FRAME_ID and vpa.APID=vhftf.APID_ID GROUP BY
         * vpa.NAME,vpa.APID,vhfhp.PACKET_OBSID;
         */
        return jdbcTemplate.query(sql, new Object[]{from, to}, (rs, num) -> {
            final VhfStationStatusCountDto entity = new VhfStationStatusCountDto();
            entity.setIdStation(rs.getInt("idStation"));
            entity.setNpackets(rs.getInt("npackets"));
            entity.setMaxTime(rs.getInt("maxTime"));
            entity.setMinTime(rs.getInt("minTime"));
            entity.setAvgCorrPower(rs.getFloat("avgCorrPower"));
            entity.setStdCorrPower(rs.getFloat("stdCorrPower"));
            entity.setAvgDoppler(rs.getFloat("avgDoppler"));
            entity.setStdDoppler(rs.getFloat("stdDoppler"));
            return entity;
        });
    }

    /*
     * (non-javadoc) getDuplicateHashes: @see
     * fr.svom.vhf.data.repositories.IJdbcRepository#getDuplicateHashes(java.lang.
     * Long, java.lang.Long)
     */
    @Override
    public List<String> getDuplicateHashes(Long from, Long to) {

        // Create the jdbc template from the Datasource ds.
        log.debug("Select duplicate hashes using JDBCTEMPLATE " + " using arguments time in ms from={} to={}", from,
                to);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
        Timestamp since = new Timestamp(from);
        Timestamp until = new Timestamp(to);

        // Create the SQL query.
        // This is a native query.
        final String sql = "select hash_id from" + "(select hash_id, count(hash_id) as nhash from VHF_TRANSFER_FRAME "
                           + " where (IS_DUPLICATE is False OR (IS_DUPLICATE is True and DUP_FRAME_ID<0)) "
                           + " and INSERTION_TIME >= ? " + " and INSERTION_TIME < ? group by hash_id) as duplicates "
                           + " where nhash>1";

        return jdbcTemplate.query(sql, new Object[]{since, until}, (rs, num) -> {
            return rs.getString("hash_id");
        });
    }

    /*
     * (non-javadoc) getPacketCountInTimeRange: @see
     * fr.svom.vhf.data.repositories.IJdbcRepository#getPacketCountInTimeRange(java.
     * lang.Long, java.lang.Long)
     */
    @Override
    public Long getPacketCountInTimeRange(Long from, Long to) {

        // Create the jdbc template from the Datasource ds.
        log.debug("Select vhf packet count using JDBCTEMPLATE time in range in ms from={} to={}", from, to);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
        Timestamp since = new Timestamp(from);
        Timestamp until = new Timestamp(to);

        // Create the SQL query.
        // This is a native query.
        final String sql = "select count(*) as npkt from VHF_TRANSFER_FRAME "
                           + " where INSERTION_TIME >= ? and INSERTION_TIME < ? ";

        return jdbcTemplate.queryForObject(sql, (rs, num) -> {
            return Long.valueOf(rs.getLong("npkt"));
        }, since, until);
    }

    /*
     * (non-javadoc) getObsidListInTimeRange: @see
     * fr.svom.vhf.data.repositories.IJdbcRepository#getObsidListInTimeRange(java.
     * lang.Long, java.lang.Long)
     */
    @Override
    public List<Map<String, Object>> getObsidListInTimeRange(Long from, Long to) {

        // Create the jdbc template from the Datasource ds.
        log.debug("Select obsid list using JDBCTEMPLATE time in range in ms from={} to={}", from, to);
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
        Timestamp since = new Timestamp(from);
        Timestamp until = new Timestamp(to);

        // Create the SQL query.
        // This is a native query.
        final String sql = "select vhftf.PACKET_OBSID as obsid, min(vhftf.PACKET_OBSID_NUM) as obsidnum, "
                           + " min(vhftf.PACKET_OBSID_TYPE) as obsidtype, "
                           + " min(vhftf.INSERTION_TIME) as insertionTime"
                           + " from VHF_TRANSFER_FRAME vhftf " + " where  "
                           + " vhftf.INSERTION_TIME >= ? and vhftf.INSERTION_TIME < ? "
                           + " group by vhftf.PACKET_OBSID "
                           + " order by min(vhftf.INSERTION_TIME) DESC limit 10000";

        return jdbcTemplate.query(sql, (rs, num) -> {
            Map<String, Object> map = new HashMap<>();
            map.put("obsid", rs.getLong("obsid"));
            map.put("obsidnum", rs.getInt("obsidnum"));
            map.put("obsidtype", rs.getInt("obsidtype"));
            map.put("receptionTime", rs.getTimestamp("insertionTime"));
            return map;
        }, since, until);
    }

    /**
     * Remove the packets related to the obsid provided.
     *
     * @param id the obsid
     * @return the number of deleted packets
     * @throws AbstractCdbServiceException
     */
    @Override
    public Integer deletePacketsByObsid(Long id) throws AbstractCdbServiceException {
        Integer res = 0;
        log.info("Removing packets with obsnum {} via a postgres function", id);
        try (Connection conn = ds.getConnection();
             CallableStatement properCase = conn.prepareCall("{ ? = call public.delete_packetsbyobsid( ? ) }")) {
            properCase.registerOutParameter(1, Types.INTEGER);
            properCase.setInt(2, id.intValue());
            properCase.execute();
            return properCase.getInt(1);
        }
        catch (RuntimeException | SQLException e) {
            log.error("Error in executing function delete_packetsbyobsid with arg {}: {}", id, e.getMessage());
            throw new VhfSQLException("Error in deleting packet by obsid " + id, e);
        }
    }
}
