/**
 * 
 */
package fr.svom.vhf.data.repositories;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.PacketApid;

/**
 * @author formica
 *
 */
@Repository
public interface PacketApidRepository extends PagingAndSortingRepository<PacketApid, Integer>,
                                              QuerydslPredicateExecutor<PacketApid> {

    /**
     * @param name
     *            the String
     * @return PacketApid
     */
    PacketApid findByName(@Param("name") String name);
}
