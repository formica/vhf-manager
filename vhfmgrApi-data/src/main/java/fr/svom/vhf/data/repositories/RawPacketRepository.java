/**
 * 
 */
package fr.svom.vhf.data.repositories;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.VhfRawPacket;

/**
 * Raw packet management.
 *
 * @author formica
 *
 */
@Repository
public interface RawPacketRepository extends PagingAndSortingRepository<VhfRawPacket, Long>,
                                             QuerydslPredicateExecutor<VhfRawPacket> {
// No special mthods.
    
    /**
     * @param flag
     *            the String
     * @param fromt
     *            the Timestamp
     * @param tot
     *            the Timestamp
     * @return List<VhfRawPacket>
     */
    @Query("SELECT distinct p FROM VhfRawPacket p WHERE p.pktflag = (:flag) "
            + " and p.insertionTime >= (:fromt) and p.insertionTime < (:tot)")
    List<VhfRawPacket> findByFlagRawPackets(@Param("flag") String flag,
            @Param("fromt") Timestamp fromt, @Param("tot") Timestamp tot);

    /**
     *
     * @param packet
     * @return Optional VhfRawPacket
     */
    Optional<VhfRawPacket> findByPacket(@Param("packet") String packet);

}
