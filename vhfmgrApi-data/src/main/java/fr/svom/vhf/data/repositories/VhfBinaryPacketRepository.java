package fr.svom.vhf.data.repositories;

import fr.svom.vhf.data.packets.VhfBinaryPacket;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Binary packets management.
 *
 * @author formica
 *
 */
@Repository
public interface VhfBinaryPacketRepository
        extends PagingAndSortingRepository<VhfBinaryPacket, String>, QuerydslPredicateExecutor<VhfBinaryPacket> {
// No method implemented here.
}
