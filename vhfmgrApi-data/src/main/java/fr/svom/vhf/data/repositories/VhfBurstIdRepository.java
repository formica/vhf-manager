/**
 * 
 */
package fr.svom.vhf.data.repositories;

import fr.svom.vhf.data.packets.VhfBurstId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The base repository.
 *
 * @author formica
 *
 */
@Repository
public interface VhfBurstIdRepository
        extends PagingAndSortingRepository<VhfBurstId, String>,
        QuerydslPredicateExecutor<VhfBurstId> {

    /**
     * Find a list of burstId containing this packettime and matching the obsid in input.
     * The packettime is requested to be higher than the burst start time and the different of the packettime
     * and the burst end time in seconds should be less than 24 hours.
     * @param packettime time in seconds since CUC.
     * @param obsid
     * @return  List<VhfBurstId>
     */
    @Query("SELECT t " + " FROM VhfBurstId t " + " LEFT JOIN t.frame f WHERE "
           + " (t.burstStartime <= (:packettime) and abs(t.burstEndtime - (:packettime)) < 3600*24)"
           + " and (f.header.packetObsid = (:obsid) or t.eclairObsid = (:obsid) or (:obsid) < 0) "
           + " order by t.insertionTime DESC")
    //// A condition to exclude the obsid is present here....+ " and (f.header.packetObsid = (:obsid) or (:obsid)<0)")
    List<VhfBurstId> findByTimeAndObsid(Long packettime, Long obsid);

    /**
     * Find a list of burstId containing this packettime in input using a short time window.
     * The packettime is requested to be higher than the burst start time and the different of the packettime
     * and the burst end time in seconds should be less than 15 minutes.
     * @param packettime time in seconds since CUC.
     * @param nsec
     * @return  List<VhfBurstId>
     */
    @Query("SELECT t " + " FROM VhfBurstId t " + " LEFT JOIN t.frame f WHERE "
           + " (t.burstStartime <= (:packettime) and abs(t.burstEndtime - (:packettime)) < (:nsec)) "
           + "order by t.insertionTime DESC")
    List<VhfBurstId> findByTimeWindow(Long packettime, Long nsec);

    /**
     * Find a list of burstId containing this packettime in input using a short time window.
     * The packettime is requested to be higher than the burst start time and the different of the packettime
     * and the burst end time in seconds should be less than 15 minutes, AND the obsid provided is equal to
     * the one of the frame or to the one of Eclairs or the one stored for Eclairs is 0.
     * This should allow to: select only burst in the correct time window, and for a GRM first still
     * we should find the burst if an ECL alert arrives, because the eclairObsid would be 0.
     * @param packettime time in seconds since CUC.
     * @param nsec length in seconds.
     * @param obsid the obsid.
     * @return  List<VhfBurstId>
     */
    @Query("SELECT t " + " FROM VhfBurstId t " + " LEFT JOIN t.frame f WHERE "
           + " (t.burstStartime <= (:packettime) and "
           + " abs(t.burstEndtime - (:packettime)) < 3600*24"
           + " and abs(t.burstEndtime - (:packettime)) < (:nsec) ) "
           + " and (f.header.packetObsid = (:obsid) or t.eclairObsid = (:obsid) "
           + " or (:obsid) < 0 or t.eclairObsid = 0)"
           + " order by t.insertionTime DESC")
    List<VhfBurstId> findByTimeWindowAndObsid(Long packettime, Long nsec, Long obsid);


    /**
     * Find last burstId ordering by packettime.
     * @param bursttime
     * @param page
     * @return  List<VhfBurstId>
     */
    @Query("SELECT t " + " FROM VhfBurstId t " + " LEFT JOIN t.frame f "
           + " where t.alertTimeTb0AbsSeconds > (:bursttime) ORDER BY t.packetTime DESC")
    List<VhfBurstId> findByTimeGreaterThan(Long bursttime, Pageable page);

}
