package fr.svom.vhf.data.repositories;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.VhfDecodedPacket;

/**
 * Base repository.
 *
 * @author formica
 *
 */
@Repository
public interface VhfDecodedPacketRepository
        extends PagingAndSortingRepository<VhfDecodedPacket, String>,
        QuerydslPredicateExecutor<VhfDecodedPacket> {

    /**
     * @param format
     *            the String
     * @param hashid
     *            the String
     * @return Optional<VhfDecodedPacket>
     */
    @NonNull
    Optional<VhfDecodedPacket> findByPktformatAndHashId(@Param("format") String format,
            @Param("hashid") String hashid);

    /**
     * @param fmt
     *            the String
     * @param fromt
     *            the Timestamp
     * @param tot
     *            the Timestamp
     * @return List<VhfDecodedPacket>
     */
    @Query("SELECT distinct p FROM VhfDecodedPacket p WHERE p.pktformat = (:fmt) "
            + " and p.insertionTime >= (:fromt) and p.insertionTime < (:tot)")
    List<VhfDecodedPacket> findByFormatDecodedPackets(@Param("fmt") String fmt,
            @Param("fromt") Timestamp fromt, @Param("tot") Timestamp tot);

    /**
     * @param apid
     *            the Integer
     * @param since
     *            the Timestamp
     * @param until
     *            the Timestamp
     * @param obsid
     *            the Integer
     * @return List<VhfDecodedPacket>
     */
    @Query("SELECT t " + " FROM VhfDecodedPacket t " + " LEFT JOIN t.frames f "
            + " WHERE f.isFrameDuplicate=FALSE and f.apid.apid = (:apid) and "
            + " (f.insertionTime >= (:since) and f.insertionTime < (:until))"
            + " and f.header.packetObsid = (:obsid)")
    List<VhfDecodedPacket> findByApidAndObsIdDecodedPackets(@Param("apid") Integer apid,
            @Param("since") Timestamp since, @Param("until") Timestamp until,
            @Param("obsid") Integer obsid);

    /**
     * @param apid
     *            the Integer
     * @param since
     *            the Timestamp
     * @param until
     *            the Timestamp
     * @param obsid
     *            the Integer
     * @return List<VhfDecodedPacket>
     */
    @Query("SELECT t " + " FROM VhfDecodedPacket t " + " LEFT JOIN t.frames f "
            + " WHERE f.isFrameDuplicate=FALSE and f.apid.name like (:apid) "
            + " and (f.insertionTime >= (:since) and f.insertionTime < (:until)) "
            + " and (f.header.packetObsid = (:obsid) OR  (:obsid) < 0) ORDER BY f.header.packetTime asc")
    List<VhfDecodedPacket> findByApidnameAndObsIdDecodedPackets(@Param("apid") String apid,
            @Param("since") Timestamp since, @Param("until") Timestamp until,
            @Param("obsid") Integer obsid);

    /**
     * @param apid
     *            the Integer
     * @param tstart
     *            the Timestamp
     * @param tend
     *            the Timestamp
     * @return List<VhfDecodedPacket>
     */
    @Query("SELECT distinct p FROM VhfDecodedPacket p " + " LEFT JOIN p.frames tf "
            + " WHERE tf.isFrameDuplicate = FALSE AND tf.apid.apid = (:apid) "
            + " AND tf.insertionTime BETWEEN :tstart AND :tend "
            + " ")
    List<VhfDecodedPacket> findByApidAndTimeRange(@Param("apid") Integer apid,
            @Param("tstart") Timestamp tstart, @Param("tend") Timestamp tend);

    /**
     * @param stationid
     *            the Integer
     * @param tstart
     *            the Timestamp
     * @param tend
     *            the Timestamp
     * @return List<VhfDecodedPacket>
     */
    @Query("SELECT distinct p FROM VhfDecodedPacket p " + " LEFT JOIN p.frames tf "
            + " WHERE tf.isFrameDuplicate = FALSE AND tf.station.stationId = (:stationid) "
            + " AND tf.insertionTime BETWEEN :tstart AND :tend ")
    List<VhfDecodedPacket> findByStationIdAndTimeRange(@Param("stationid") Integer stationid,
            @Param("tstart") Timestamp tstart, @Param("tend") Timestamp tend);

}
