package fr.svom.vhf.data.repositories;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.VhfGroundStation;

/**
 * Base Repository.
 *
 * @author formica
 *
 */
@Repository
public interface VhfGroundStationRepository
        extends PagingAndSortingRepository<VhfGroundStation, Integer>,
        QuerydslPredicateExecutor<VhfGroundStation> {
 // No method implemented here.
}
