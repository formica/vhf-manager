/**
 * 
 */
package fr.svom.vhf.data.repositories;

import fr.svom.vhf.data.packets.VhfTransferFrame;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.VhfNotification;

/**
 * Notifications managements.
 *
 * @author formica
 *
 */
@Repository
public interface VhfNotificationRepository
        extends PagingAndSortingRepository<VhfNotification, Long>,
        QuerydslPredicateExecutor<VhfNotification> {
// No methods implemented here.

    Long deleteByFrame(VhfTransferFrame frame);
}
