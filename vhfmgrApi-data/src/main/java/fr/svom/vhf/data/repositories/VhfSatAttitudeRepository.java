/**
 * 
 */
package fr.svom.vhf.data.repositories;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.VhfSatAttitude;

/**
 * Attitude management.
 *
 * @author formica
 *
 */
@Repository
public interface VhfSatAttitudeRepository extends PagingAndSortingRepository<VhfSatAttitude, Long>,
                                                  QuerydslPredicateExecutor<VhfSatAttitude> {
 // No methods implemented here.
}
