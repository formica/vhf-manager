/**
 * 
 */
package fr.svom.vhf.data.repositories;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.VhfSatPosition;


/**
 * Positions management.
 *
 * @author formica
 *
 */
@Repository
public interface VhfSatPositionRepository extends PagingAndSortingRepository<VhfSatPosition, Long>,
                                                  QuerydslPredicateExecutor<VhfSatPosition> {
 // No methods implemented here.
}
