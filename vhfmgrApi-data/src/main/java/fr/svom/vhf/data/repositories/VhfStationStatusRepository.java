/**
 * 
 */
package fr.svom.vhf.data.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.svom.vhf.data.packets.VhfStationStatus;

/**
 * @author formica
 *
 */
@Repository
public interface VhfStationStatusRepository
        extends PagingAndSortingRepository<VhfStationStatus, Long>,
        QuerydslPredicateExecutor<VhfStationStatus> {

    /**
     * @param fid
     *            the Long
     * @return VhfStationStatus
     */
    @Query("SELECT distinct p FROM VhfStationStatus p JOIN FETCH p.frame f WHERE f.frameId = (:frameid)")
    VhfStationStatus findByFrameId(@Param("frameid") Long fid);
}
