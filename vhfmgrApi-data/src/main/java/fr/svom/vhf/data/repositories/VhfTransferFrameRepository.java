/**
 * 
 */
package fr.svom.vhf.data.repositories;

import fr.svom.vhf.data.packets.VhfTransferFrame;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * The base repository.
 *
 * @author formica
 *
 */
@Repository
public interface VhfTransferFrameRepository
        extends PagingAndSortingRepository<VhfTransferFrame, Long>,
        QuerydslPredicateExecutor<VhfTransferFrame> {

    /**
     * @param receptionTimeStart
     *            the Timestamp
     * @param receptionTimeEnd
     *            the Timestamp
     * @return List<VhfTransferFrame>
     */
    List<VhfTransferFrame> findByInsertionTimeBetween(
            @Param("receptionTimeStart") Timestamp receptionTimeStart,
            @Param("receptionTimeEnd") Timestamp receptionTimeEnd);

    /**
     * @param hash
     *            the String
     * @return List<VhfTransferFrame>
     */
    @Query("SELECT distinct p FROM VhfTransferFrame p WHERE p.binaryHash = (:hash) ORDER BY p.insertionTime ASC")
    List<VhfTransferFrame> findByHashId(@Param("hash") String hash);

    /**
     * Load a frame based on raw packet, fetching dependencies.
     *
     * @param rawpacket
     *            the String
     * @return List<VhfTransferFrame>
     */
    @Query(value = "SELECT distinct p FROM VhfTransferFrame p JOIN FETCH p.stationStatus ss "
                   + " JOIN FETCH p.apid ap  JOIN FETCH p.packet binp JOIN FETCH p.packetJson jsonp "
                   + "WHERE p.rawPacket =  (:rawpkt) ORDER BY p.insertionTime ASC")
    List<VhfTransferFrame> findByRawPacket(@Param("rawpkt") String rawpacket);

    /**
     * Be careful: this does not seem to work.
     *
     * @param hash
     *            the String
     * @param isdup
     *            the Boolean
     * @return VhfTransferFrame
     */
    @Query("SELECT distinct p FROM VhfTransferFrame p "
            + "WHERE p.binaryHash = (:hash) and p.isFrameDuplicate = :isdup ORDER BY p.insertionTime ASC")
    List<VhfTransferFrame> findByBinaryHashAndisFrameDuplicate(@Param("hash") String hash,
            @Param("isdup") Boolean isdup);

}
