/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.util.List;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.exceptions.VhfServiceException;

/**
 * @author aformic
 *
 */
public interface IFilteringCriteria {

    /**
     * @param criteria
     *            the List of SearchCriteria
     * @throws VhfServiceException
     *             If an Exception occurred
     * @return List<BooleanExpression>
     */
    List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException;
}
