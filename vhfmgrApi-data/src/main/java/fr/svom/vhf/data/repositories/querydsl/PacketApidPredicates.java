/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.packets.QPacketApid;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class PacketApidPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PacketApidPredicates.class);

    /**
     * Default Ctor.
     */
    private PacketApidPredicates() {

    }

    /**
     * @param id
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasId(Integer id) {
        log.debug("hasId: argument {}", id);
        return QPacketApid.packetApid.apid.eq(id);
    }

    /**
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasNameLike(String name) {
        log.debug("hasNameLike: argument {}", name);
        return QPacketApid.packetApid.name.like("%" + name + "%");
    }

    /**
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasClassLike(String name) {
        log.debug("hasClassLike: argument {}", name);
        return QPacketApid.packetApid.className.like("%" + name + "%");
    }

    /**
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasCategoryLike(String name) {
        log.debug("hasCategoryLike: argument {}", name);
        return QPacketApid.packetApid.category.like("%" + name + "%");
    }

    /**
     * @param desc
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasDescriptionLike(String desc) {
        log.debug("hasDescriptionLike: argument {}", desc);
        return QPacketApid.packetApid.description.like("%" + desc + "%");
    }

}
