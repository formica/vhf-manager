/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.exceptions.VhfServiceException;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("vhfBinaryPacketFiltering")
public class VhfBinaryPacketFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfBinaryPacketFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria#
     * createFilteringConditions(java .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria.
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria {} {} {}", searchCriteria.getKey(),
                    searchCriteria.getOperation(), searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("insertiontime".equals(key)) {
                // Filter based on reception time.
                final BooleanExpression receptionTimexthan = VhfBinaryPacketPredicates
                        .isInsertionTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(receptionTimexthan);
            }
            else if ("packettime".equals(key)) {
                // Filter based on packet time.
                final BooleanExpression packetTimexthan = VhfBinaryPacketPredicates
                        .isPacketTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(packetTimexthan);
            }
            else if ("isframevalid".equals(key)) {
                // Filter based on frame validity.
                final BooleanExpression isFrameValid = VhfBinaryPacketPredicates
                        .isFrameValid(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameValid);
            }
            else if ("isframeduplicate".equals(key)) {
                // Filter based on frame is duplicate.
                final BooleanExpression isFrameDuplicate = VhfBinaryPacketPredicates
                        .isFrameDuplicate(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameDuplicate);
            }
            else if ("stationid".equals(key)) {
                // Filter based on station ID.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasStationId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("hashid".equals(key)) {
                // Filter based on packet HASH.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasHashid(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("apid".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasPacketApid(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidtype".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasObsidType(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidnum".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasObsidNum(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsid".equals(key)) {
                // Filter based on OBSID.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasObsId(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apidname".equals(key)) {
                // Filter based on APID NAME.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasPacketApidName(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("burstid".equals(key)) {
                // Filter based on frame ID.
                final BooleanExpression exp = VhfBinaryPacketPredicates
                        .hasBurstId(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
        }
        // Return the expression
        return expressions;
    }
}
