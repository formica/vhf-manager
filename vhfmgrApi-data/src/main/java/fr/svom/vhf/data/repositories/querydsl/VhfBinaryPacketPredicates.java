/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.packets.QVhfBinaryPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class VhfBinaryPacketPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfBinaryPacketPredicates.class);

    /**
     * Default Ctor.
     */
    private VhfBinaryPacketPredicates() {

    }

    /**
     * @param id
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasHashid(String id) {
        log.debug("hasHashid: argument {}", id);
        return QVhfBinaryPacket.vhfBinaryPacket.hashId.eq(id);
    }

    /**
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameValid(Boolean status) {
        log.debug("isFrameValid: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().isFrameValid.isTrue();
        }
        else {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().isFrameValid.isFalse();
        }
        return pred;
    }

    /**
     * @param apid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApid(Integer apid) {
        log.debug("hasPacketApid: argument {}", apid);
        return QVhfBinaryPacket.vhfBinaryPacket.frames.any().apid.apid.eq(apid);
    }

    /**
     * Where condition on obsid type.
     *
     * @param obsidtype
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidType(Integer obsidtype) {
        log.debug("hasObsidType: argument {}", obsidtype);
        return QVhfBinaryPacket.vhfBinaryPacket.frames.any().header.packetObsidType.eq(obsidtype);
    }

    /**
     * Where condition on obsid.
     *
     * @param obsidnum
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidNum(Integer obsidnum) {
        log.debug("hasObsidNum: argument {}", obsidnum);
        return QVhfBinaryPacket.vhfBinaryPacket.frames.any().header.packetObsidNum.eq(obsidnum);
    }

    /**
     * Where on obsid.
     *
     * @param obsid
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsId(Long obsid) {
        log.debug("hasObsId: argument {}", obsid);
        return QVhfBinaryPacket.vhfBinaryPacket.frames.any().header.packetObsid.eq(obsid);
    }

    /**
     * Where on station.
     *
     * @param stationid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasStationId(Integer stationid) {
        log.debug("hasStationId: argument {}", stationid);
        return QVhfBinaryPacket.vhfBinaryPacket.frames.any().station.stationId.eq(stationid);
    }

    /**
     * @param apidName
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApidName(String apidName) {
        log.debug("hasPacketApidName: argument {}", apidName);
        return QVhfBinaryPacket.vhfBinaryPacket.frames.any().apid.name
                .like(apidName);
    }

    /**
     * @param burstId
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasBurstId(String burstId) {
        log.debug("hasBurstId: argument {}", burstId);
        return QVhfBinaryPacket.vhfBinaryPacket.frames.any().burstId
                .like(burstId);
    }

    /**
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameDuplicate(Boolean status) {
        log.debug("isFrameDuplicate: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().isFrameDuplicate.isTrue();
        }
        else {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().isFrameDuplicate.isFalse();
        }
        return pred;
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isInsertionTimeXThan(String oper, String num) {
        log.debug("isReceptionTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;

        if ("<".equals(oper)) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().insertionTime
                    .lt(new Timestamp(new Long(num)));
        }
        else if (">".equals(oper)) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().insertionTime
                    .gt(new Timestamp(new Long(num)));
        }
        else if (":".equals(oper)) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().insertionTime
                    .eq(new Timestamp(new Long(num)));
        }
        return pred;
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketTimeXThan(String oper, String num) {
        log.debug("isPacketTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = new Long(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }
        if ("<".equals(oper)) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().header.packetTime.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().header.packetTime.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QVhfBinaryPacket.vhfBinaryPacket.frames.any().header.packetTime.eq(tsec);
        }
        return pred;
    }
}
