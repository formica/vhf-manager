/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("vhfBurstIdFiltering")
public class VhfBurstIdFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfBurstIdFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria#
     * createFilteringConditions(java .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria.
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria {} {} {}", searchCriteria.getKey(),
                    searchCriteria.getOperation(), searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            switch (key) {
                // Search by insertion time.
                case "insertiontime":
                    // Filter based on reception time.
                    final BooleanExpression insertionTimexthan = VhfBurstIdPredicates
                            .isInsertionTimeXThan(searchCriteria.getOperation(),
                                    searchCriteria.getValue().toString());
                    expressions.add(insertionTimexthan);
                    break;
                // Search by packet time.
                case "packettime":
                    // Filter based on packet time.
                    final BooleanExpression packetTimexthan = VhfBurstIdPredicates
                            .isPacketTimeXThan(searchCriteria.getOperation(),
                                    searchCriteria.getValue().toString());
                    expressions.add(packetTimexthan);
                    break;
                // Search by alerttimetb time.
                case "alerttimetb":
                    // Filter based on burstId t0.
                    final BooleanExpression satt0Xthan = VhfBurstIdPredicates
                            .isT0XThan(searchCriteria.getOperation(),
                                    searchCriteria.getValue().toString());
                    expressions.add(satt0Xthan);
                    break;
                // Search by alerttimetb time.
                case "alerttimetb0absseconds":
                    // Filter based on burstId t0.
                    final BooleanExpression sattb0Xthan = VhfBurstIdPredicates
                            .isTb0XThan(searchCriteria.getOperation(),
                                    searchCriteria.getValue().toString());
                    expressions.add(sattb0Xthan);
                    break;
                // Search by burstid.
                case "burstid":
                    // Filter based on burstid.
                    final BooleanExpression bid = VhfBurstIdPredicates
                            .hasBurstidLike(searchCriteria.getValue().toString());
                    expressions.add(bid);
                    break;
                // Search by apid name.
                case "apidname":
                    // Filter based on APID name.
                    final BooleanExpression apidname = VhfBurstIdPredicates
                            .hasPacketApidNameLike(searchCriteria.getValue().toString());
                    expressions.add(apidname);
                    break;
                case "obsidtype":
                    // Filter based on OBSTYPE.
                    final BooleanExpression obstype = VhfBurstIdPredicates
                            .hasObsidType(Integer.valueOf(searchCriteria.getValue().toString()));
                    expressions.add(obstype);
                    break;
                case "obsidnum":
                    // Filter based on OBSNUM.
                    final BooleanExpression obsnum = VhfBurstIdPredicates
                            .hasObsidNum(Integer.valueOf(searchCriteria.getValue().toString()));
                    expressions.add(obsnum);
                    break;
                case "obsid":
                    // Filter based on OBSID.
                    final BooleanExpression obsid = VhfBurstIdPredicates
                            .hasObsId(Long.valueOf(searchCriteria.getValue().toString()));
                    expressions.add(obsid);
                    break;
                case "eclobsid":
                    // Filter based on ECL OBSID.
                    final BooleanExpression eclobsid = VhfBurstIdPredicates
                            .hasEclObsId(Long.valueOf(searchCriteria.getValue().toString()));
                    expressions.add(eclobsid);
                    break;
                default:
                    // Default.
                    log.warn("Wrong key provided in input : {}", key);
            }
        }
        // Return the expression
        return expressions;
    }
}
