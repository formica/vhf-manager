/**
 *
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.packets.QVhfBurstId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class VhfBurstIdPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfBurstIdPredicates.class);

    /**
     * Default Ctor.
     */
    private VhfBurstIdPredicates() {

    }

    /**
     * @param id
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasBurstidLike(String id) {
        log.debug("hasBurstid: argument {}", id);
        return QVhfBurstId.vhfBurstId.burstId.like("%" + id + "%");
    }

    /**
     * Where condition on obsid type.
     *
     * @param obsidtype
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidType(Integer obsidtype) {
        log.debug("hasObsidType: argument {}", obsidtype);
        return QVhfBurstId.vhfBurstId.frame.header.packetObsidType.eq(obsidtype);
    }

    /**
     * Where condition on obsid.
     *
     * @param obsidnum
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidNum(Integer obsidnum) {
        log.debug("hasObsidNum: argument {}", obsidnum);
        return QVhfBurstId.vhfBurstId.frame.header.packetObsidNum.eq(obsidnum);
    }

    /**
     * Where on obsid.
     *
     * @param obsid
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsId(Long obsid) {
        log.debug("hasObsId: argument {}", obsid);
        return QVhfBurstId.vhfBurstId.frame.header.packetObsid.eq(obsid);
    }
    /**
     * Where on eclobsid.
     *
     * @param obsid
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasEclObsId(Long obsid) {
        log.debug("hasEclObsId: argument {}", obsid);
        return QVhfBurstId.vhfBurstId.eclairObsid.eq(obsid);
    }

    /**
     * @param apidName
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApidNameLike(String apidName) {
        log.debug("hasPacketApidName: argument {}", apidName);
        return QVhfBurstId.vhfBurstId.frame.apid.name
                .like("%" + apidName + "%");
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isInsertionTimeXThan(String oper, String num) {
        log.debug("isInsertionTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;

        if ("<".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.insertionTime
                    .lt(new Timestamp(Long.valueOf(num)));
        }
        else if (">".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.insertionTime
                    .gt(new Timestamp(Long.valueOf(num)));
        }
        else if (":".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.insertionTime
                    .eq(new Timestamp(Long.valueOf(num)));
        }
        return pred;
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketTimeXThan(String oper, String num) {
        log.debug("isPacketTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = Long.valueOf(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }
        if ("<".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.packetTime.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.packetTime.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.packetTime.eq(tsec);
        }
        return pred;
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isT0XThan(String oper, String num) {
        log.debug("isT0XThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = Long.valueOf(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }
        if ("<".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.alertTimeTb.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.alertTimeTb.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.alertTimeTb.eq(tsec);
        }
        return pred;
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isTb0XThan(String oper, String num) {
        log.debug("isTb0XThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = Long.valueOf(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }
        if ("<".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.alertTimeTb0AbsSeconds.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.alertTimeTb0AbsSeconds.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QVhfBurstId.vhfBurstId.alertTimeTb0AbsSeconds.eq(tsec);
        }
        return pred;
    }

}
