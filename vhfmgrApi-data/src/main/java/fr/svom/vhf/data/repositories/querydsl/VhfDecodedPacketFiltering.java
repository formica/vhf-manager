/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 * Use criteria like reception time, apid, obsid and others.
 *
 * @author aformic
 *
 */
@Component("vhfDecodedPacketFiltering")
public class VhfDecodedPacketFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfDecodedPacketFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria#
     * createFilteringConditions(java .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria.
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria " + searchCriteria.getKey() + " "
                    + searchCriteria.getOperation() + " " + searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("insertiontime".equals(key)) {
                // Filter based on reception time
                final BooleanExpression insertionTimexthan = VhfDecodedPacketPredicates
                        .isInsertionTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(insertionTimexthan);
            }
            else if ("packettime".equals(key)) {
                // Filter based on packet time
                final BooleanExpression packetTimexthan = VhfDecodedPacketPredicates
                        .isPacketTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(packetTimexthan);
            }
            else if ("burstid".equals(key)) {
                // Filter based on frame ID.
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasBurstId(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("isframevalid".equals(key)) {
                // Filter based on valid frame
                final BooleanExpression isFrameValid = VhfDecodedPacketPredicates
                        .isFrameValid(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameValid);
            }
            else if ("isframeduplicate".equals(key)) {
                // Filter based on duplicate frame
                final BooleanExpression isFrameDuplicate = VhfDecodedPacketPredicates
                        .isFrameDuplicate(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameDuplicate);
            }
            else if ("stationid".equals(key)) {
                // Filter based on station id
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasStationId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("hashid".equals(key)) {
                // Filter based on hash
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasHashid(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("obsidtype".equals(key)) {
                // Filter based on obsid type.
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasObsidType(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidnum".equals(key)) {
                // Filter based on obsid num.
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasObsidNum(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsid".equals(key)) {
                // Filter based on obsid
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasObsId(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apid".equals(key)) {
                // Filter based on apid
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasPacketApid(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apidname".equals(key)) {
                // Filter based on apid name
                final BooleanExpression exp = VhfDecodedPacketPredicates
                        .hasPacketApidName(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
        }
        // Return the full expressions.
        return expressions;
    }
}
