/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.exceptions.VhfServiceException;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("vhfGroundStationFiltering")
public class VhfGroundStationFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfGroundStationFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria {} {} {}", searchCriteria.getKey(),
                    searchCriteria.getOperation(), searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("stationid".equals(key)) {
                // Filter based on station ID
                final BooleanExpression exp = VhfGroundStationPredicates
                        .hasId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("name".equals(key)) {
                // Filter based on station NAME
                final BooleanExpression exp = VhfGroundStationPredicates
                        .hasStationNameLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("id".equals(key)) {
                // Filter based on ID station
                final BooleanExpression exp = VhfGroundStationPredicates
                        .hasIdstationLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("country".equals(key)) {
                // Filter based on station short location
                final BooleanExpression exp = VhfGroundStationPredicates
                        .hasCountryLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("location".equals(key)) {
                // Filter based on station location
                final BooleanExpression exp = VhfGroundStationPredicates
                        .hasLocationLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
        }
        return expressions;
    }
}
