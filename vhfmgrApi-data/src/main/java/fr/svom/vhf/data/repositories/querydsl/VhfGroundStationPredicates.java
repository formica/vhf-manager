/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.packets.QVhfGroundStation;
import fr.svom.vhf.data.packets.VhfGroundStation;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class VhfGroundStationPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfGroundStation.class);

    /**
     * Default Ctor.
     */
    private VhfGroundStationPredicates() {

    }

    /**
     * @param id
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasId(Integer id) {
        log.debug("hasId: argument {}", id);
        return QVhfGroundStation.vhfGroundStation.stationId.eq(id);
    }

    /**
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasStationNameLike(String name) {
        log.debug("hasStationNameLike: argument {}", name);
        return QVhfGroundStation.vhfGroundStation.name.like("%" + name + "%");
    }

    /**
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasIdstationLike(String name) {
        log.debug("hasIdstationLike: argument {}", name);
        return QVhfGroundStation.vhfGroundStation.id.like("%" + name + "%");
    }

    /**
     * @param loc
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasLocationLike(String loc) {
        log.debug("hasLocationLike: argument {}", loc);
        return QVhfGroundStation.vhfGroundStation.location.like("%" + loc + "%");
    }

    /**
     * @param country
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasCountryLike(String country) {
        log.debug("hasCountryLike: argument {}", country);
        return QVhfGroundStation.vhfGroundStation.country.like("%" + country + "%");
    }
}
