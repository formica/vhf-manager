/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("vhfNotificationFiltering")
public class VhfNotificationFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfNotificationFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria {} {} {}", searchCriteria.getKey(),
                    searchCriteria.getOperation(), searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("pktflag".equals(key)) {
                // Filter based on station ID
                final BooleanExpression exp = VhfNotificationPredicates
                        .hasPktflagLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("insertiontime".equals(key)) {
                // Filter based on insertion time.
                final BooleanExpression insertionTimexthan = VhfNotificationPredicates
                        .isInsertionTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(insertionTimexthan);
            }
            else if ("notifmessage".equals(key)) {
                // Filter based on ID station
                final BooleanExpression exp = VhfNotificationPredicates
                        .hasMessageLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
        }
        return expressions;
    }
}
