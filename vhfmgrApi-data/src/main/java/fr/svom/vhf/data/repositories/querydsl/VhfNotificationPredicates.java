/**
 *
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.packets.QVhfNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class VhfNotificationPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfNotificationPredicates.class);

    /**
     * Default Ctor.
     */
    private VhfNotificationPredicates() {

    }

    /**
     * @param value
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPktflagLike(String value) {
        log.debug("hasPktflag: argument {}", value);
        return QVhfNotification.vhfNotification.pktflag.like("%" + value + "%");
    }

    /**
     * @param value
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasMessageLike(String value) {
        log.debug("hasPktflag: argument {}", value);
        return QVhfNotification.vhfNotification.notifMessage.like("%" + value + "%");
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isInsertionTimeXThan(String oper, String num) {
        log.debug("isInsertionTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        if ("<".equals(oper)) {
            pred = QVhfNotification.vhfNotification.insertionTime.lt(new Timestamp(new Long(num)));
        }
        else if (">".equals(oper)) {
            pred = QVhfNotification.vhfNotification.insertionTime.gt(new Timestamp(new Long(num)));
        }
        else if (":".equals(oper)) {
            pred = QVhfNotification.vhfNotification.insertionTime.eq(new Timestamp(new Long(num)));
        }
        return pred;
    }
}
