/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.exceptions.VhfServiceException;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("vhfRawPacketFiltering")
public class VhfRawPacketFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfRawPacketFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria {} {} {}", searchCriteria.getKey(),
                    searchCriteria.getOperation(), searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("flag".equals(key)) {
                // Filter based on flag.
                final BooleanExpression exp = VhfRawPacketPredicates
                        .hasPktflagLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("insertiontime".equals(key)) {
                // Filter based on insertion time.
                final BooleanExpression insertionTimexthan = VhfRawPacketPredicates
                        .isInsertionTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(insertionTimexthan);
            }
            else {
                log.warn("Ignore condition {} for raw packet filtering", key);
            }
        }
        // Return the full expressions.
        return expressions;

    }
}
