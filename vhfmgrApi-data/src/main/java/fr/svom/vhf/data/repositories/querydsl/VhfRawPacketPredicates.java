/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.packets.QVhfRawPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class VhfRawPacketPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfGroundStation.class);

    /**
     * Default Ctor.
     */
    private VhfRawPacketPredicates() {

    }

    /**
     * @param value
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPktflagLike(String value) {
        log.debug("hasPktflag: argument {}", value);
        return QVhfRawPacket.vhfRawPacket.pktflag.like("%" + value + "%");
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isInsertionTimeXThan(String oper, String num) {
        log.debug("isInsertionTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        if ("<".equals(oper)) {
            pred = QVhfRawPacket.vhfRawPacket.insertionTime.lt(new Timestamp(Long.valueOf(num)));
        }
        else if (">".equals(oper)) {
            pred = QVhfRawPacket.vhfRawPacket.insertionTime.gt(new Timestamp(Long.valueOf(num)));
        }
        else if (":".equals(oper)) {
            pred = QVhfRawPacket.vhfRawPacket.insertionTime.eq(new Timestamp(Long.valueOf(num)));
        }
        return pred;
    }
}
