/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.exceptions.VhfServiceException;

/**
 * Filtering methods.
 *
 * @author aformic
 */
@Component("vhfsatattitudeFiltering")
public class VhfSatAttitudeFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfSatAttitudeFiltering.class);

    /*
     * (non-Javadoc)
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria) throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria {} {} {}", searchCriteria.getKey(), searchCriteria.getOperation(),
                    searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("id".equals(key)) {
                // Filter based on ID
                final BooleanExpression exp = VhfSatAttitudePredicates
                        .hasId(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("packettime".equals(key)) {
                // Filter based on description
                final BooleanExpression exp = VhfSatAttitudePredicates.isPacketTimeXThan(searchCriteria.getOperation(),
                        searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("insertiontime".equals(key)) {
                // Filter based on description
                final BooleanExpression exp = VhfSatAttitudePredicates
                        .isInsertionTimeXThan(searchCriteria.getOperation(), searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("apidname".equals(key)) {
                // Filter based on Packet NAME.
                final BooleanExpression exp = VhfSatAttitudePredicates
                        .hasNameLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
        }
        return expressions;
    }
}
