/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.packets.QVhfSatAttitude;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class VhfSatAttitudePredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfSatAttitudePredicates.class);

    /**
     * Default Ctor.
     */
    private VhfSatAttitudePredicates() {

    }

    /**
     * @param id
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasId(Long id) {
        log.debug("hasId: argument {}", id);
        return QVhfSatAttitude.vhfSatAttitude.attId.eq(id);
    }

    /**
     * @param name
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasNameLike(String name) {
        log.debug("hasNameLike: argument {}", name);
        return QVhfSatAttitude.vhfSatAttitude.apidName.like("%" + name + "%");
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketTimeXThan(String oper, String num) {
        log.debug("isPacketTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = new Long(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }

        if ("<".equals(oper)) {
            pred = QVhfSatAttitude.vhfSatAttitude.packetTime.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QVhfSatAttitude.vhfSatAttitude.packetTime.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QVhfSatAttitude.vhfSatAttitude.packetTime.eq(tsec);
        }
        return pred;
    }
    
    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isInsertionTimeXThan(String oper, String num) {
        log.debug("isInsertionTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;

        if ("<".equals(oper)) {
            pred = QVhfSatAttitude.vhfSatAttitude.insertionTime
                    .lt(new Timestamp(new Long(num)));
        }
        else if (">".equals(oper)) {
            pred = QVhfSatAttitude.vhfSatAttitude.insertionTime
                    .gt(new Timestamp(new Long(num)));
        }
        else if (":".equals(oper)) {
            pred = QVhfSatAttitude.vhfSatAttitude.insertionTime
                    .eq(new Timestamp(new Long(num)));
        }
        return pred;
    }


}
