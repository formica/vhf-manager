/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.exceptions.VhfServiceException;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("vhfStationStatusFiltering")
public class VhfStationStatusFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfStationStatusFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        final List<BooleanExpression> expressions = new ArrayList<>();
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria {} {} {}", searchCriteria.getKey(),
                    searchCriteria.getOperation(), searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("stationtime".equals(key)) {
                // Filter based on station time.
                final BooleanExpression stationTimexthan = VhfStationStatusPredicates
                        .isStationTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(stationTimexthan);
            }
            else if ("idstation".equals(key)) {
                // Filter based on ID station.
                final BooleanExpression hasIdStation = VhfStationStatusPredicates
                        .hasIdStation(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(hasIdStation);
            }
            else if ("doppler".equals(key)) {
                // Filter based on doppler.
                final BooleanExpression dopplerXThan = VhfStationStatusPredicates.isDopplerXThan(
                        searchCriteria.getOperation(), searchCriteria.getValue().toString());
                expressions.add(dopplerXThan);
            }
        }
        return expressions;
    }
}
