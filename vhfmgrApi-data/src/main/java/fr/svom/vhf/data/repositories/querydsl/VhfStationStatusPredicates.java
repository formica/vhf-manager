/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.querydsl.core.types.dsl.BooleanExpression;

import fr.svom.vhf.data.packets.QVhfStationStatus;

/**
 * Predicates.
 *
 * @author aformic
 *
 */
public final class VhfStationStatusPredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfStationStatusPredicates.class);

    /**
     * Default Ctor.
     */
    private VhfStationStatusPredicates() {

    }

    /**
     * @param id
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasIdStation(Integer id) {
        log.debug("hasIdStation: argument {}", id);
        return QVhfStationStatus.vhfStationStatus.idStation.eq(id);
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isStationTimeXThan(String oper, String num) {
        log.debug("isStationTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;

        if ("<".equals(oper)) {
            pred = QVhfStationStatus.vhfStationStatus.stationTime.lt(new Long(num));
        }
        else if (">".equals(oper)) {
            pred = QVhfStationStatus.vhfStationStatus.stationTime.gt(new Long(num));
        }
        else if (":".equals(oper)) {
            pred = QVhfStationStatus.vhfStationStatus.stationTime.eq(new Long(num));
        }
        return pred;
    }

    /**
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isDopplerXThan(String oper, String num) {
        log.debug("isDopplerXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;

        if ("<".equals(oper)) {
            pred = QVhfStationStatus.vhfStationStatus.doppler.lt(new Float(num));
        }
        else if (">".equals(oper)) {
            pred = QVhfStationStatus.vhfStationStatus.doppler.gt(new Float(num));
        }
        else if (":".equals(oper)) {
            pred = QVhfStationStatus.vhfStationStatus.doppler.eq(new Float(num));
        }
        return pred;
    }
}
