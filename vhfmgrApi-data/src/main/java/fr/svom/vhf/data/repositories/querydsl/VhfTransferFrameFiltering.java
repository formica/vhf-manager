/**
 * 
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Filtering methods.
 *
 * @author aformic
 *
 */
@Component("vhfTransferFrameFiltering")
public class VhfTransferFrameFiltering implements IFilteringCriteria {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfTransferFrameFiltering.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * hep.phycdb.svc.querydsl.IFilteringCriteria#createFilteringConditions(java
     * .util.List, java.lang.Object)
     */
    @Override
    public List<BooleanExpression> createFilteringConditions(List<SearchCriteria> criteria)
            throws VhfServiceException {
        // Initialize.
        final List<BooleanExpression> expressions = new ArrayList<>();
        // Loop over criteria
        for (final SearchCriteria searchCriteria : criteria) {
            log.debug("search criteria " + searchCriteria.getKey() + " "
                    + searchCriteria.getOperation() + " " + searchCriteria.getValue());
            // Get the key in lower case.
            final String key = searchCriteria.getKey().toLowerCase(Locale.ENGLISH);
            // Build expressions.
            if ("id".equals(key)) {
                // Filter based on frame ID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasId(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("burstid".equals(key)) {
                // Filter based on frame ID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasBurstId(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("insertiontime".equals(key)) {
                // Filter based on reception time.
                final BooleanExpression insertionTimexthan = VhfTransferFramePredicates
                        .isInsertionTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(insertionTimexthan);
            }
            else if ("packettime".equals(key)) {
                // Filter based on packet time.
                final BooleanExpression packetTimexthan = VhfTransferFramePredicates
                        .isPacketTimeXThan(searchCriteria.getOperation(),
                                searchCriteria.getValue().toString());
                expressions.add(packetTimexthan);
            }
            else if ("isframevalid".equals(key)) {
                // Filter based on frame validity.
                final BooleanExpression isFrameValid = VhfTransferFramePredicates
                        .isFrameValid(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameValid);
            }
            else if ("isframeduplicate".equals(key)) {
                // Filter based on frame is duplicate.
                final BooleanExpression isFrameDuplicate = VhfTransferFramePredicates
                        .isFrameDuplicate(Boolean.valueOf(searchCriteria.getValue().toString()));
                expressions.add(isFrameDuplicate);
            }
            else if ("notifstatus".equals(key)) {
                // Filter based on notification status.
                final BooleanExpression hasNotifStatus = VhfTransferFramePredicates.hasNotifStatus(
                        searchCriteria.getOperation(), searchCriteria.getValue().toString());
                expressions.add(hasNotifStatus);
            }
            else if ("stationid".equals(key)) {
                // Filter based on station ID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasStationId(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apid".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasPacketApid(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidtype".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasObsidType(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsidnum".equals(key)) {
                // Filter based on APID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasObsidNum(Integer.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("obsid".equals(key)) {
                // Filter based on OBSID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasObsid(Long.valueOf(searchCriteria.getValue().toString()));
                expressions.add(exp);
            }
            else if ("apidname".equals(key)) {
                // Filter based on APID NAME.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasPacketApidName(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("pktformat".equals(key)) {
                // Filter based on PKT format.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasPacketDecodedFormatLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("hashid".equals(key)) {
                // Filter based on Hash of the binary packet.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasHashIdLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("category".equals(key)) {
                // Filter based on Category of the APID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasCategoryLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else if ("classname".equals(key)) {
                // Filter based on Classname of the APID.
                final BooleanExpression exp = VhfTransferFramePredicates
                        .hasClassNameLike(searchCriteria.getValue().toString());
                expressions.add(exp);
            }
            else {
                // Default
                log.warn("Wrong key provided in input : {}", key);
            }
        }
        // Return expression.
        return expressions;
    }
}
