/**
 *
 */
package fr.svom.vhf.data.repositories.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.packets.QVhfTransferFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;

/**
 * Predicates for searches.
 *
 * @author aformic
 *
 */
public final class VhfTransferFramePredicates {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfTransferFramePredicates.class);

    /**
     * Default Ctor.
     */
    private VhfTransferFramePredicates() {

    }

    /**
     * Where condition on frame ID.
     *
     * @param id
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasId(Long id) {
        log.debug("hasId: argument {}", id);
        return QVhfTransferFrame.vhfTransferFrame.frameId.eq(id);
    }

    /**
     * Where condition on burst ID.
     *
     * @param burstid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasBurstId(String burstid) {
        log.debug("hasBurstId: argument {}", burstid);
        return QVhfTransferFrame.vhfTransferFrame.burstId.like("%" + burstid + "%");
    }

    /**
     * Where condition on frame valid.
     *
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameValid(Boolean status) {
        log.debug("isFrameValid: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QVhfTransferFrame.vhfTransferFrame.isFrameValid.isTrue();
        }
        else {
            pred = QVhfTransferFrame.vhfTransferFrame.isFrameValid.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on frame valid.
     *
     * @param pktformat
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketDecodedFormatLike(String pktformat) {
        log.debug("hasPacketDecodedFormatLike: argument {}", pktformat);
        return QVhfTransferFrame.vhfTransferFrame.packetJson.pktformat.like("%" + pktformat + "%");
    }

    /**
     * Where condition on apid.
     *
     * @param apid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApid(Integer apid) {
        log.debug("hasPacketApid: argument {}", apid);
        return QVhfTransferFrame.vhfTransferFrame.apid.apid.eq(apid);
    }

    /**
     * Where condition on category.
     *
     * @param category
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasCategoryLike(String category) {
        log.debug("hasCategoryLike: argument {}", category);
        return QVhfTransferFrame.vhfTransferFrame.apid.category.like(category);
    }

    /**
     * Where condition on class.
     *
     * @param classname
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasClassNameLike(String classname) {
        log.debug("hasClassNameLike: argument {}", classname);
        return QVhfTransferFrame.vhfTransferFrame.apid.className.like(classname);
    }


    /**
     * Where condition on obsid.
     *
     * @param obsid
     *            the Long
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsid(Long obsid) {
        log.debug("hasObsid: argument {}", obsid);
        return QVhfTransferFrame.vhfTransferFrame.header.packetObsid.eq(obsid);
    }

    /**
     * Where condition on obsid type.
     *
     * @param obsidtype
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidType(Integer obsidtype) {
        log.debug("hasObsidType: argument {}", obsidtype);
        return QVhfTransferFrame.vhfTransferFrame.header.packetObsidType.eq(obsidtype);
    }

    /**
     * Where condition on obsid.
     *
     * @param obsidnum
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasObsidNum(Integer obsidnum) {
        log.debug("hasObsidNum: argument {}", obsidnum);
        return QVhfTransferFrame.vhfTransferFrame.header.packetObsidNum.eq(obsidnum);
    }

    /**
     * Where condition on stationid.
     *
     * @param stationid
     *            the Integer
     * @return BooleanExpression
     */
    public static BooleanExpression hasStationId(Integer stationid) {
        log.debug("hasStationId: argument {}", stationid);
        return QVhfTransferFrame.vhfTransferFrame.station.stationId.eq(stationid);
    }

    /**
     * @param apidName
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasPacketApidName(String apidName) {
        log.debug("hasPacketApidName: argument {}", apidName);
        return QVhfTransferFrame.vhfTransferFrame.apid.name.like(apidName);
    }

    /**
     * @param op
     *            the String
     * @param notifStatus
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasNotifStatus(String op, String notifStatus) {
        log.debug("hasNotifStatus: argument {}", notifStatus);
        if ("~".equals(op)) {
            return QVhfTransferFrame.vhfTransferFrame.notifStatus.notLike("%" + notifStatus + "%");
        }
        return QVhfTransferFrame.vhfTransferFrame.notifStatus.like("%" + notifStatus + "%");
    }

    /**
     * @param status
     *            the Boolean
     * @return BooleanExpression
     */
    public static BooleanExpression isFrameDuplicate(Boolean status) {
        log.debug("isFrameDuplicate: argument {}", status);
        BooleanExpression pred = null;
        if (status) {
            pred = QVhfTransferFrame.vhfTransferFrame.isFrameDuplicate.isTrue();
        }
        else {
            pred = QVhfTransferFrame.vhfTransferFrame.isFrameDuplicate.isFalse();
        }
        return pred;
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isInsertionTimeXThan(String oper, String num) {
        log.debug("isReceptionTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;

        if ("<".equals(oper)) {
            pred = QVhfTransferFrame.vhfTransferFrame.insertionTime
                    .lt(new Timestamp(new Long(num)));
        }
        else if (">".equals(oper)) {
            pred = QVhfTransferFrame.vhfTransferFrame.insertionTime
                    .gt(new Timestamp(new Long(num)));
        }
        else if (":".equals(oper)) {
            pred = QVhfTransferFrame.vhfTransferFrame.insertionTime
                    .eq(new Timestamp(new Long(num)));
        }
        return pred;
    }

    /**
     * Where condition on time range.
     *
     * @param oper
     *            the String
     * @param num
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression isPacketTimeXThan(String oper, String num) {
        log.debug("isPacketTimeXThan: argument {} operation {}", num, oper);
        BooleanExpression pred = null;
        Long tsec = new Long(num);
        if (tsec > 9952054817L) {
            tsec = tsec / 1000L; // packetTime is coded in seconds
        }

        if ("<".equals(oper)) {
            pred = QVhfTransferFrame.vhfTransferFrame.header.packetTime.lt(tsec);
        }
        else if (">".equals(oper)) {
            pred = QVhfTransferFrame.vhfTransferFrame.header.packetTime.gt(tsec);
        }
        else if (":".equals(oper)) {
            pred = QVhfTransferFrame.vhfTransferFrame.header.packetTime.eq(tsec);
        }
        return pred;
    }

    /**
     * @param hashid
     *            the String
     * @return BooleanExpression
     */
    public static BooleanExpression hasHashIdLike(String hashid) {
        log.debug("hasHashIdLike: argument {}", hashid);
        return QVhfTransferFrame.vhfTransferFrame.binaryHash.like("%" + hashid + "%");
    }


}
