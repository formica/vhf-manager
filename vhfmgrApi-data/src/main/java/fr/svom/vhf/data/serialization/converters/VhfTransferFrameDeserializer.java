/**
 *
 */
package fr.svom.vhf.data.serialization.converters;

import fr.svom.frame.data.model.VhfDataSourcePacket;
import fr.svom.frame.data.model.VhfPacket;
import fr.svom.frame.data.service.VhfDecoding;
import fr.svom.frame.exceptions.ParsingException;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Deserializer for VHF packets. Underlying decoding library is in a separate package.
 *
 * @author aformic
 */
@Service
public class VhfTransferFrameDeserializer {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfTransferFrameDeserializer.class);

    /**
     * Decoder.
     */
    @Autowired
    private VhfDecoding vhfDecoderBean;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapperDecoded")
    private MapperFacade mapper;

    /**
     * VHF total length.
     */
    private static final int VHF_LEN = 128;
    /**
     * CCSDS length.
     */
    private static final int CCSDS_LEN = 100;
    /**
     * Packet length.
     */
    private static final int PKT_LEN = 94;

    /**
     * @param framedata the byte array
     * @throws AbstractCdbServiceException If a service Exception occurred.
     * @return VhfTransferFrame
     */
    public VhfTransferFrame deserialize(byte[] framedata) throws AbstractCdbServiceException {

        try {
            log.debug("Deserialize byte array of length {}", framedata.length);
            final VhfTransferFrame frame = new VhfTransferFrame();
            // Encode the byte array into an hexadecimal string.
            frame.setRawPacket(Hex.encodeHexString(framedata));
            // Decode the packet.
            final int pktl = framedata.length;
            // Check the length. This help in determine if you are reading a full packet
            // a CCSDS packet or just the internal data packet.
            if (pktl != VHF_LEN && pktl != CCSDS_LEN && pktl != PKT_LEN) {
                // Just log if the length does not seem correct. May be we should send an
                // exception.
                log.warn("ATTENTION: Received packet length is unknown {}; parsing is not predictable....", pktl);
                throw new VhfDecodingException("Wrong VHF packet length " + pktl);
            }
            // Decode the data.
            final VhfPacket pkt = vhfDecoderBean.binaryDataDecoder(framedata);
            // Reading the full VHF packet.
            if (framedata.length > CCSDS_LEN) {
                // Get the station status.
                final VhfStationStatus stationStatus = mapper.map(pkt.getStationStatus(), VhfStationStatus.class);
                log.trace("station status extracted: {}", stationStatus);
                frame.setStationStatus(stationStatus);
            }
            // Reading the CCSDS packet.
            if (framedata.length > PKT_LEN) {
                // Get the frame header.
                final FrameHeaderPacket fhp = mapper.map(pkt.getVhfFrameHeaderPacket(), FrameHeaderPacket.class);
                log.trace("frame header extracted: {}", fhp);
                frame.setFrameHeader(fhp);
            }
            // Use CCSDS information to get PacketApid and checksum.
            final CcsdsPacket ccsds = mapper.map(pkt.getCcsdsPacket(), CcsdsPacket.class);
            log.trace("ccsds extracted: {}", ccsds);
            ccsds.setCcsdsCrc(pkt.getChecksum());
            frame.setCcsds(ccsds);
            final PacketApid apid = mapper.map(pkt.getApidPacket(), PacketApid.class);
            log.trace("apid extracted: {}", apid);
            frame.setApid(apid);
            // Read primary header.
            final PrimaryHeaderPacket php = mapper.map(pkt.getVhfDataHeaderPacket(), PrimaryHeaderPacket.class);
            log.trace("primary header packet extracted: {}", php);
            frame.setHeader(php);
            // Read data.
            final VhfDataSourcePacket vhfds = pkt.getVhfDataSourcePacket();
            final int pktlength = vhfds.getBinaryData().length;
            final String encodehexa = Hex.encodeHexString(vhfds.getBinaryData());
            // Create a binary packet with a given hash.
            final VhfBinaryPacket vhfbin = new VhfBinaryPacket();
            vhfbin.setBinaryPacket(vhfds.getBinaryData());
            vhfbin.setHashId(vhfds.getHashId());
            vhfbin.setSize(pktlength);
            vhfbin.setPacket(encodehexa);
            // Discrepancy: something went wrong in parsing.
            if (!encodehexa.equals(vhfds.getData())) {
                throw new VhfDecodingException("Different encoding from deserialize library and local transformation: "
                                              + encodehexa + " != " + vhfds.getData());
            }
            frame.setPacket(vhfbin);
            frame.setBinaryHash(vhfbin.getHashId());
            // Return the VHF frame.
            log.debug("deserialize(): raw packet deserialized into frame with binary hash {}", vhfbin.getHashId());
            return frame;
        }
        catch (final RuntimeException | ParsingException e) {
            log.error("Cannot deserialize byte array to generate VhfTransferFrame, return null: {}", e.getMessage());
        }
        return null;
    }
}
