package fr.svom.vhf.data.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.mappers.PacketDateFormat;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DataHelperTests {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Test
    public void testA_getPacketNumber() {
        ObjectMapper jacksonMapper = new ObjectMapper();
        String json = "{ \"packetId\": 111 }";
        try {
            final JsonNode jsonNode = jacksonMapper.readTree(json);
            Map<String, Integer> map = DataModelsHelper.getPacketNumber(jsonNode, "packetId");
            Integer val = map.get("packetId");
            assertThat(val).isEqualTo(111);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testA_getPacketNumberFails() {
        ObjectMapper jacksonMapper = new ObjectMapper();
        String json = "{ \"packetId\": 111 }";
        try {
            final JsonNode jsonNode = jacksonMapper.readTree(json);
            Map<String, Integer> map = DataModelsHelper.getPacketNumber(jsonNode, "pkt");
            assertThat(map.containsKey("pkt")).isFalse();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testB_dateFormatter() {
        PacketDateFormat pdf = new PacketDateFormat();
        pdf.setOffset(12345);
        Instant it = PacketDateFormat.getInstant(12345L);
        Long msec = it.toEpochMilli();
        log.debug("Millisecond time is {}", msec);
        assertThat(msec).isEqualTo((12345L + 12345) * 1000L);
    }

    @Test
    public void testC_parseAlert() {
        String lc = "{\"CcsdsTmHeadr\":{\"CcsdsApid\":577,\"CcsdsGFlag\":1,\"CcsdsCounter\":0,\"CcsdsPLength\":87},"
                    + "\"VhfTmHeader\":{\"PacketTimeS\":1592721093},\"VhfIObservId\":{\"VhfIObsType\":0,"
                    + "\"VhfNObsNumber\":100},\"AlertTimeTb0AbsSeconds\":1592721091,"
                    + "\"AlertTimeTb0AbsMilliseconds\":69,\"LightCurvePacketNumber\":1,\"SatAttitudeLCQ0\":14058,"
                    + "\"SatAttitudeLCQ1\":21521,\"SatAttitudeLCQ2\":9840,\"SatAttitudeLCQ3\":17781,"
                    + "\"SatPositionLon\":69,\"SatPositionLat\":17228,\"Sample0Eband0IntCount\":8537,"
                    + "\"Sample0Eband1IntCount\":2722,\"Sample0Eband2IntCount\":2462,\"Sample0Eband3IntCount\":2818,"
                    + "\"Sample0EsatIntCount\":0,\"Sample0MultipleIntCount\":0,\"Sample1Eband0DiffCount\":2374,"
                    + "\"Sample1Eband1DiffCount\":801,\"Sample1Eband2DiffCount\":775,\"Sample1Eband3DiffCount\":895,"
                    + "\"Sample1EsatDiffCount\":0,\"Sample1MultipleDiffCount\":0,\"Sample2Eband0DiffCount\":2139,"
                    + "\"Sample2Eband1DiffCount\":647,\"Sample2Eband2DiffCount\":616,\"Sample2Eband3DiffCount\":687,"
                    + "\"Sample2EsatDiffCount\":0,\"Sample2MultipleDiffCount\":0,\"Sample3Eband0DiffCount\":2064,"
                    + "\"Sample3Eband1DiffCount\":615,\"Sample3Eband2DiffCount\":477,\"Sample3Eband3DiffCount\":586,"
                    + "\"Sample3EsatDiffCount\":0,\"Sample3MultipleDiffCount\":0,\"VhfCrc\":9995}";
        DataModelsHelper.setTimeOffset(10000L);
        VhfBurstId burstId = DataModelsHelper.parseAlertT0(lc, "TmVhfEclairsLowPriorityLightCurve");
        log.info("Create burst id {}", burstId);
        assertThat(burstId).isNotNull();
    }

}
