/**
 *
 */
package fr.svom.vhf.data.test;

import static org.assertj.core.api.Assertions.assertThat;

import fr.svom.vhf.data.exceptions.AlreadyExistsPojoException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.PayloadEncodingException;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.data.exceptions.VhfDeserializeException;
import fr.svom.vhf.data.exceptions.VhfRequestFormatException;
import fr.svom.vhf.data.exceptions.VhfSQLException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.core.Response;

/**
 * @author formica
 *
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ExceptionsTests {

    private static final Logger log = LoggerFactory.getLogger(ExceptionsTests.class);

    @Test
    public void testExceptions() throws Exception {
        VhfSQLException sql = new VhfSQLException("Error in sql request");
        assertThat(sql.getResponseStatus()).isEqualTo(Response.Status.NOT_MODIFIED);
        assertThat(sql.getMessage()).contains("SQL");

        NotExistsPojoException notfound = new NotExistsPojoException("Entity not found");
        assertThat(notfound.getResponseStatus()).isEqualTo(Response.Status.NOT_FOUND);
        assertThat(notfound.getMessage()).contains("Not Found");

        VhfCriteriaException criteria = new VhfCriteriaException("Criteria not correct");
        assertThat(criteria.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(criteria.getMessage()).contains("Criteria");

        VhfRequestFormatException rf = new VhfRequestFormatException("Service request error");
        assertThat(rf.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(rf.getMessage()).contains("Bad");

        VhfDecodingException badr = new VhfDecodingException("Some bad request");
        assertThat(badr.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(badr.getMessage()).contains("Decoding");

        AlreadyExistsPojoException conf = new AlreadyExistsPojoException("Some conflict");
        assertThat(conf.getResponseStatus()).isEqualTo(Response.Status.CONFLICT);
        assertThat(conf.getMessage()).contains("Conflict");

        VhfDeserializeException cint = new VhfDeserializeException("Some deserialize error");
        assertThat(cint.getResponseStatus()).isEqualTo(Response.Status.NOT_ACCEPTABLE);
        assertThat(cint.getMessage()).contains("Deserialize");

        VhfServiceException serv = new VhfServiceException("Some service error");
        assertThat(serv.getResponseStatus()).isEqualTo(Response.Status.INTERNAL_SERVER_ERROR);
        assertThat(serv.getMessage()).contains("Service");

        PayloadEncodingException pyld = new PayloadEncodingException("Some error in payload");
        assertThat(pyld.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(pyld.getMessage()).contains("Encoding");
    }

    @Test
    public void testExceptionsWithReThrow() throws Exception {
        RuntimeException e = new RuntimeException("some runtime");
        VhfSQLException sql = new VhfSQLException("Error in sql request", e);
        assertThat(sql.getResponseStatus()).isEqualTo(Response.Status.NOT_MODIFIED);
        assertThat(sql.getMessage()).contains("SQL");

        NotExistsPojoException notfound = new NotExistsPojoException("Entity not found", e);
        assertThat(notfound.getResponseStatus()).isEqualTo(Response.Status.NOT_FOUND);
        assertThat(notfound.getMessage()).contains("Not Found");

        VhfCriteriaException criteria = new VhfCriteriaException("Criteria not correct", e);
        assertThat(criteria.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(criteria.getMessage()).contains("Criteria");

        VhfServiceException serv = new VhfServiceException("Some service error", e);
        assertThat(serv.getResponseStatus()).isEqualTo(Response.Status.INTERNAL_SERVER_ERROR);
        assertThat(serv.getMessage()).contains("Service");

        VhfRequestFormatException rf = new VhfRequestFormatException("Service request error", e);
        assertThat(rf.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(rf.getMessage()).contains("Bad");

        VhfDecodingException badr = new VhfDecodingException("Some bad request", e);
        assertThat(badr.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(badr.getMessage()).contains("Decoding");

        AlreadyExistsPojoException conf = new AlreadyExistsPojoException("Some conflict", e);
        assertThat(conf.getResponseStatus()).isEqualTo(Response.Status.CONFLICT);
        assertThat(conf.getMessage()).contains("Conflict");

        VhfDeserializeException cint = new VhfDeserializeException("Some deserialize error", e);
        assertThat(cint.getResponseStatus()).isEqualTo(Response.Status.NOT_ACCEPTABLE);
        assertThat(cint.getMessage()).contains("Deserialize");

        PayloadEncodingException pyld = new PayloadEncodingException("Some error in payload", e);
        assertThat(pyld.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(pyld.getMessage()).contains("Encoding");
    }

    @Test
    public void testExceptionsWithReThrowOnly() throws Exception {
        RuntimeException e = new RuntimeException("some runtime");
        VhfSQLException sql = new VhfSQLException(e);
        assertThat(sql.getResponseStatus()).isEqualTo(Response.Status.NOT_MODIFIED);
        assertThat(sql.getMessage()).contains("SQL");

        NotExistsPojoException notfound = new NotExistsPojoException(e);
        assertThat(notfound.getResponseStatus()).isEqualTo(Response.Status.NOT_FOUND);
        assertThat(notfound.getMessage()).contains("Not Found");

        VhfServiceException serv = new VhfServiceException(e);
        assertThat(serv.getResponseStatus()).isEqualTo(Response.Status.INTERNAL_SERVER_ERROR);
        assertThat(serv.getMessage()).contains("Service");

        VhfRequestFormatException rf = new VhfRequestFormatException(e);
        assertThat(rf.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(rf.getMessage()).contains("Bad");

        VhfCriteriaException criteria = new VhfCriteriaException(e);
        assertThat(criteria.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(criteria.getMessage()).contains("Criteria");

        VhfDecodingException badr = new VhfDecodingException(e);
        assertThat(badr.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(badr.getMessage()).contains("Decoding");

        AlreadyExistsPojoException conf = new AlreadyExistsPojoException(e);
        assertThat(conf.getResponseStatus()).isEqualTo(Response.Status.CONFLICT);
        assertThat(conf.getMessage()).contains("Conflict");

        VhfDeserializeException cint = new VhfDeserializeException(e);
        assertThat(cint.getResponseStatus()).isEqualTo(Response.Status.NOT_ACCEPTABLE);
        assertThat(cint.getMessage()).contains("Deserialize");

        PayloadEncodingException pyld = new PayloadEncodingException(e);
        assertThat(pyld.getResponseStatus()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(pyld.getMessage()).contains("Encoding");
    }

}
