package fr.svom.vhf.data.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.nio.ByteOrder;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.vhf.data.helpers.ByteDecodingHelper;
import fr.svom.vhf.data.helpers.HashGenerator;
import fr.svom.vhf.data.test.generators.DataGenerator;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FunctionsTests {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Test
	public void testByteHelper() throws Exception {
		final BigInteger bi = new BigInteger("0000803e", 16);
		final BigInteger bd = new BigInteger("000000000000d03f", 16);
		final Float f = (Float) ByteDecodingHelper.getFloating(bi, ByteOrder.LITTLE_ENDIAN, "java.lang.Float");
		final Double d = (Double) ByteDecodingHelper.getFloating(bd, ByteOrder.LITTLE_ENDIAN, "java.lang.Double");
		final Float ff = new Float(0.25);
		final Double dd = new Double(0.25);
		final int bits = Float.floatToIntBits(ff);
		byte[] bytes = new byte[4];
		bytes[0] = (byte)(bits & 0xff);
		bytes[1] = (byte)(bits >> 8 & 0xff);
		bytes[2] = (byte)(bits >> 16 & 0xff);
		bytes[3] = (byte)(bits >> 24 & 0xff);

		log.info( Hex.encodeHexString( bytes ) );

		final long dbits = Double.doubleToLongBits(dd);
		bytes = new byte[8];
		bytes[0] = (byte)(dbits & 0xff);
		bytes[1] = (byte)(dbits >> 8 & 0xff);
		bytes[2] = (byte)(dbits >> 16 & 0xff);
		bytes[3] = (byte)(dbits >> 24 & 0xff);
		bytes[4] = (byte)(dbits >> 32 & 0xff);
		bytes[5] = (byte)(dbits >> 40 & 0xff);
		bytes[6] = (byte)(dbits >> 48 & 0xff);
		bytes[7] = (byte)(dbits >> 56 & 0xff);

		assertThat(f).isEqualTo(ff);
		assertThat(d).isEqualTo(dd);
		
		final Long ii = 32830L;
	    final Long i = (Long) ByteDecodingHelper.getObject(bi, ByteOrder.BIG_ENDIAN, "java.lang.Long");
	    log.info("parsed object {} from bi={}",i,bi.longValue());
	    assertThat(i).isEqualTo(ii);

	}

    @Test
    public void testHashGenerator() throws Exception {
        final byte[] dataarr = DataGenerator.generatePacket();
        final String hash = HashGenerator.shaJava(dataarr);
        assertThat(hash.length()).isGreaterThan(0);
        
        final BufferedInputStream istream = new BufferedInputStream(new ByteArrayInputStream(dataarr));
        final String hash1 = HashGenerator.hash(istream);
        assertThat(hash1).isEqualTo(hash);
        
        final BufferedInputStream istream2 = new BufferedInputStream(new ByteArrayInputStream(dataarr));
        final BufferedOutputStream os2 = new BufferedOutputStream(new FileOutputStream(new File("/tmp/teststream.out")));
        final String hash2 = HashGenerator.hashoutstream(istream2, os2);
        assertThat(hash2).isEqualTo(hash);
        
        final BufferedInputStream istream3 = new BufferedInputStream(new ByteArrayInputStream(dataarr));
        final BufferedOutputStream os3 = new BufferedOutputStream(new FileOutputStream(new File("/tmp/teststream3.out")));
        final String hash3 = HashGenerator.hashoutstream(istream3, os3);
        assertThat(hash3).isEqualTo(hash);
    }
	
}
