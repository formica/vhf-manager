package fr.svom.vhf.data.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.svom.vhf.data.serialization.converters.CustomTimeDeserializer;
import fr.svom.vhf.data.serialization.converters.CustomTimeSerializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.serialization.converters.VhfTransferFrameDeserializer;
import fr.svom.vhf.data.test.generators.DataGenerator;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MessageConverterTests {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    VhfTransferFrameDeserializer vhfTransferFrameDeserializer;

    /**
     * Test message deserialization.
     */
    @Test
    public void deserializeBinary() throws Exception {
        final String hexadata = DataGenerator.generateAlertFramePacket();
        final byte[] data = DatatypeConverter.parseHexBinary(hexadata);
        final VhfStationStatus sstatus = DataGenerator.generateVhfStationStatus();
        final VhfTransferFrame vhfframe = vhfTransferFrameDeserializer.deserialize(data);
        assertThat(vhfframe.getCcsds().getCcsdsApid() >= 1).isTrue();
        log.info("deserializeBinary: Created frame {}", vhfframe);
    }
    
    @Test
    public void dataDecodingHelper() throws Exception {
        String newfmt = "{\"CcsdsTmHeadr\": {\"CcsdsApid\": 0, \"CcsdsGFlag\": 0, \"CcsdsCounter\": 0, \"CcsdsPLength\": 0}, \"VhfTmHeader\": {\"DPacketTimeS\": 1565687742}, \"InstrumentMode\":255,\"CurrentObsId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":1280}}";
        Map<String, String> map = DataModelsHelper.parseCcsds(newfmt);
        assertThat(map).isNotEmpty();
        String lc = "{\"CcsdsTmHeadr\":{\"CcsdsApid\":577,\"CcsdsGFlag\":1,\"CcsdsCounter\":0,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592721093},\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":100},\"AlertTimeTb0AbsSeconds\":1592721091,\"AlertTimeTb0AbsMilliseconds\":69,\"LightCurvePacketNumber\":1,\"SatAttitudeLCQ0\":14058,\"SatAttitudeLCQ1\":21521,\"SatAttitudeLCQ2\":9840,\"SatAttitudeLCQ3\":17781,\"SatPositionLon\":69,\"SatPositionLat\":17228,\"Sample0Eband0IntCount\":8537,\"Sample0Eband1IntCount\":2722,\"Sample0Eband2IntCount\":2462,\"Sample0Eband3IntCount\":2818,\"Sample0EsatIntCount\":0,\"Sample0MultipleIntCount\":0,\"Sample1Eband0DiffCount\":2374,\"Sample1Eband1DiffCount\":801,\"Sample1Eband2DiffCount\":775,\"Sample1Eband3DiffCount\":895,\"Sample1EsatDiffCount\":0,\"Sample1MultipleDiffCount\":0,\"Sample2Eband0DiffCount\":2139,\"Sample2Eband1DiffCount\":647,\"Sample2Eband2DiffCount\":616,\"Sample2Eband3DiffCount\":687,\"Sample2EsatDiffCount\":0,\"Sample2MultipleDiffCount\":0,\"Sample3Eband0DiffCount\":2064,\"Sample3Eband1DiffCount\":615,\"Sample3Eband2DiffCount\":477,\"Sample3Eband3DiffCount\":586,\"Sample3EsatDiffCount\":0,\"Sample3MultipleDiffCount\":0,\"VhfCrc\":9995}";
        VhfSatAttitude lcatt = DataModelsHelper.parseAttitude(lc);
        VhfSatPosition lcpos = DataModelsHelper.parsePosition(lc);
        assertThat(lcatt).isNotNull();
        assertThat(lcpos).isNotNull();
    }

    @Test
    public void customDateSerializer() {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .appendPattern("yyyy-MM-dd HH:mm:ss")
                .optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd()
                .appendPattern("x")
                .toFormatter();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        JavaTimeModule module = new JavaTimeModule();
        module.addSerializer(OffsetDateTime.class, new CustomTimeSerializer(formatter));
        module.addDeserializer(OffsetDateTime.class, new CustomTimeDeserializer(formatter));
        mapper.registerModule(module);

        String model = "{\"timestamp\": \"2017-09-17 13:45:42.710576+02\"}";
        try {
            Model mm = mapper.readValue(model, new TypeReference<Model>(){});
            log.info("Parsed short model " + mm.toString() + " " + mm.getTimestamp());
            String json = mapper.writeValueAsString(mm);
            assertThat(json).contains("timestamp");
        }
        catch (JsonProcessingException e) {
            log.error("Error in parsing model....");
            e.printStackTrace();
        }
    }
}
