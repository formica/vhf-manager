package fr.svom.vhf.data.test;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

public class Model {
    @JsonProperty("timestamp")
    private OffsetDateTime timestamp;

    public Model() {}

    public void setTimestamp(OffsetDateTime odt) { this.timestamp = odt;}
    public OffsetDateTime getTimestamp() { return this.timestamp;}
}
