package fr.svom.vhf.data.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.util.List;

import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.swagger.model.*;
import org.joda.time.Instant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.test.generators.DataGenerator;
import ma.glasnost.orika.MapperFacade;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PojoDtoConverterTests {

    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    @Test
    public void testPojos() throws Exception {
        final VhfGroundStation entity = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        String entitydump = entity.toString();
        assertThat(entitydump.length()).isGreaterThan(0);

        final PacketApid entity1 = DataGenerator.generatePacketApid(1, "TmVhfMxtSome", "category", "class", "MXT");
        entitydump = entity1.toString();
        assertThat(entitydump.length()).isGreaterThan(0);
    }

    @Test
    public void testPacketApidConverter() throws Exception {
        final PacketApid entity = DataGenerator.generatePacketApid(578, "TmVhfMxtSome","svom", "lc", "ECLAIRS");

        final PacketApidDto dto = mapper.map(entity, PacketApidDto.class);
        assertThat(entity.getCategory()).isEqualTo(dto.getCategory());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);

        final PacketApid entitydto = mapper.map(dto, PacketApid.class);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(entitydto.hashCode())).isGreaterThan(0);
        assertThat(entitydto.getCategory()).isEqualTo(entity.getCategory());
    }

    @Test
    public void testObsIdApidCount() throws Exception {
        final ObsIdApidCountDto dto = new ObsIdApidCountDto();
        dto.apid(1).countedPackets(10).expectedPackets(10).packetMaxTime(1000000L).packetMinTime(1000L);
        assertThat(dto.toString()).isNotEmpty();
        dto.setPacketName("somename");
        dto.setPacketObsid(1L);
        dto.setStatus("RUNNING");
        dto.setUpdateTime(1000000L);
        
        final ObsIdApidCountDto dto1 = new ObsIdApidCountDto();
        dto1.packetName("somename").packetObsid(1L).status("RUNNING").updateTime(1000L);
        dto1.setApid(1);
        dto1.setCountedPackets(10);
        dto1.setPacketMaxTime(10L);
        dto1.setPacketMinTime(1L);
        dto1.setExpectedPackets(1);
        assertThat(dto1.toString()).isNotEmpty();
        assertThat(dto1.equals(dto)).isFalse();
    }

    @Test
    public void testBurstId() throws Exception {
        final VhfBurstIdDto dto = new VhfBurstIdDto();
        dto.burstId("sb20050510").packetTime(1000000L).burstEndtime(1000L);
        assertThat(dto.toString()).isNotEmpty();
        dto.setBurstId("sb20050511");
        dto.setAlertTimeTb0AbsSeconds(10000L);
        dto.setAlertTimeTb(10001L);
        dto.setInsertionTime(Instant.now().getMillis());
        final VhfBurstIdDto dto1 = new VhfBurstIdDto();
        dto.burstId("sb20050610").packetTime(1000000L).burstEndtime(1000L);
        dto1.setBurstId("sb20050511");
        dto1.setAlertTimeTb(10000L);
        dto1.setAlertTimeTb0AbsSeconds(10001L);
        dto1.setInsertionTime(Instant.now().getMillis());
        assertThat(dto1.toString()).isNotEmpty();
        assertThat(dto1.equals(dto)).isFalse(); // insertion time is different.

        VhfBurstId entity = mapper.map(dto, VhfBurstId.class);
        VhfBurstIdDto dtofromentity = mapper.map(entity, VhfBurstIdDto.class);
        assertThat(dtofromentity.getBurstId()).isEqualTo(entity.getBurstId());
    }

    @Test
    public void testPacketSearch() throws Exception {
        final VhfPacketSearchDto dto = new VhfPacketSearchDto();
        dto.criteria("apid:1").pkttype("VhfRawPacket").size(1);
        final VhfBasePacket b = new VhfBasePacket();
        b.setHashId("anhash");
        b.setPacket("some packet");
        b.setPkttype("test");
        dto.addPacketsItem(b);
        assertThat(dto.toString()).isNotEmpty();
        final VhfBasePacket bb = new VhfBasePacket();
        bb.hashId("anhash");
        bb.packet("some packet");
        bb.pkttype("test");
        assertThat(b.equals(bb)).isTrue();
    }

    @Test
    public void testCcsdsConverter() throws Exception {
        final CcsdsPacket entity = DataGenerator.generateCcsdsPacket();

        final CcsdsPacketDto dto = mapper.map(entity, CcsdsPacketDto.class);
        assertThat(entity.getCcsdsGFlag()).isEqualTo(dto.getCcsdsGFlag());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);

        final CcsdsPacket entitydto = mapper.map(dto, CcsdsPacket.class);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(entitydto.hashCode())).isGreaterThan(0);
        assertThat(entitydto.getCcsdsType()).isEqualTo(entity.getCcsdsType());
 
        final CcsdsPacket entity2 = DataGenerator.generateCcsdsPacket();
        entity2.setCcsdsApid(577);
        final CcsdsPacketDto dto2 = mapper.map(entity2, CcsdsPacketDto.class);
        assertThat(dto2.equals(dto)).isFalse();   
    }

    @Test
    public void testVhfGroundStationConverter() throws Exception {
        final VhfGroundStation entity = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");

        final VhfGroundStationDto dto = mapper.map(entity, VhfGroundStationDto.class);
        assertThat(entity.getId()).isEqualTo(dto.getId());
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);

        final VhfGroundStation entitydto = mapper.map(dto, VhfGroundStation.class);
        assertThat(entity.getId()).isEqualTo(entitydto.getId());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(entitydto.hashCode())).isGreaterThan(0);
        assertThat(entitydto.getLocation()).isEqualTo(entity.getLocation());
    }

    @Test
    public void testPrimaryHeaderConverter() throws Exception {
        final PrimaryHeaderPacket entity = DataGenerator.generatePrimaryHeader();

        final PrimaryHeaderPacketDto dto = mapper.map(entity, PrimaryHeaderPacketDto.class);
        assertThat(entity.getPacketObsid()).isEqualTo(dto.getPacketObsid());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);

        final PrimaryHeaderPacket entitydto = mapper.map(dto, PrimaryHeaderPacket.class);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(entitydto.hashCode())).isGreaterThan(0);
        assertThat(entitydto.getPacketObsid()).isEqualTo(entity.getPacketObsid());
    }

    @Test
    public void testVhfStationStatusConverter() throws Exception {
        final VhfStationStatus entity = DataGenerator.generateVhfStationStatus();

        final VhfStationStatusDto dto = mapper.map(entity, VhfStationStatusDto.class);
        assertThat(entity.getIdStation()).isEqualTo(dto.getIdStation());
        assertThat(entity.getDoppler()).isEqualTo(dto.getDoppler());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);

        
        final VhfStationStatus entitydto = mapper.map(dto, VhfStationStatus.class);
        assertThat(entitydto.getIdStation()).isEqualTo(entity.getIdStation());
        assertThat(Math.abs(entitydto.hashCode())).isGreaterThan(0);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        
        final StationStatusSetDto setdto = new StationStatusSetDto();
        GenericStringMap criteria = new GenericStringMap();
        criteria.put("frameid", "1");
        setdto.filter(criteria).size(1L);
        setdto.addResourcesItem(dto);
        assertThat(setdto.toString()).isNotEmpty();
      
    }

    @Test
    public void testVhfStationStatusCountConverter() throws Exception {
        final VhfStationStatusCountDto dto = new VhfStationStatusCountDto();
        dto.avgCorrPower(1.2F).avgDoppler(1.2F).idStation(7).maxTime(10).minTime(1).npackets(9);
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);
        assertThat(dto.getAvgCorrPower()).isEqualTo(1.2F);
        assertThat(dto.getAvgDoppler()).isEqualTo(1.2F);
        assertThat(dto.getIdStation()).isEqualTo(7);
        assertThat(dto.getMinTime()).isEqualTo(1);
        assertThat(dto.getMaxTime()).isEqualTo(10);
        assertThat(dto.getNpackets()).isEqualTo(9);
    }

    @Test
    public void testVhfAttitudeConverter() throws Exception {
        final VhfSatAttitude entity = DataGenerator.generateVhfSatAttitude();

        final VhfSatAttitudeDto dto = mapper.map(entity, VhfSatAttitudeDto.class);
        assertThat(entity.getSatAttitudeq0()).isEqualTo(dto.getSatAttitudeq0());
        assertThat(entity.getSatAttitudeq1()).isEqualTo(dto.getSatAttitudeq1());
        assertThat(entity.getSatAttitudeq2()).isEqualTo(dto.getSatAttitudeq2());
        assertThat(entity.getSatAttitudeq3()).isEqualTo(dto.getSatAttitudeq3());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);

        
        final VhfSatAttitude entitydto = mapper.map(dto, VhfSatAttitude.class);
        assertThat(entitydto.getSatAttitudeq0()).isEqualTo(entity.getSatAttitudeq0());
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        
        final VhfSatAttitudeSetDto setdto = new VhfSatAttitudeSetDto();
        GenericStringMap criteria = new GenericStringMap();
        criteria.put("frameid", "1");
        setdto.filter(criteria).size(1L);
        setdto.addResourcesItem(dto);
        assertThat(setdto.toString()).isNotEmpty();
    }

    @Test
    public void testVhfPositionConverter() throws Exception {
        final VhfSatPosition entity = DataGenerator.generateVhfSatPosition();

        final VhfSatPositionDto dto = mapper.map(entity, VhfSatPositionDto.class);
        assertThat(entity.getSatPositionLat()).isEqualTo(dto.getSatPositionLat());
        assertThat(entity.getSatPositionLon()).isEqualTo(dto.getSatPositionLon());
        assertThat(entity.getSatPositionX()).isEqualTo(dto.getSatPositionX());
        assertThat(entity.getSatPositionY()).isEqualTo(dto.getSatPositionY());
        assertThat(entity.getSatPositionZ()).isEqualTo(dto.getSatPositionZ());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(Math.abs(dto.hashCode())).isGreaterThan(0);

        
        final VhfSatPosition entitydto = mapper.map(dto, VhfSatPosition.class);
        assertThat(entitydto.getSatPositionX()).isEqualTo(entity.getSatPositionX());
        assertThat(entitydto.toString().length()).isGreaterThan(0);
        GenericStringMap criteria = new GenericStringMap();
        criteria.put("frameid", "1");
        final VhfSatPositionSetDto setdto = new VhfSatPositionSetDto();
        setdto.filter(criteria).size(1L);
        setdto.addResourcesItem(dto);
        assertThat(setdto.toString()).isNotEmpty();
    }

    @Test
    public void testVhfRawPacketConverter() throws Exception {
        final VhfRawPacket entity = DataGenerator.generateVhfRawPacket("HEXA", "OK", "7CAFE");
        entity.setSize(entity.getPacket().length());
        final RawPacketDto dto = mapper.map(entity, RawPacketDto.class);
        assertThat(entity.getPacket()).isEqualTo(dto.getPacket());
        assertThat(entity.getPktformat()).isEqualTo(dto.getPktformat());
        assertThat(entity.getSize()).isEqualTo(dto.getSize());
        assertThat(dto.toString().length()).isGreaterThan(0);
        final VhfRawPacket entitydto = mapper.map(dto, VhfRawPacket.class);
        assertThat(entitydto.getPacket()).isEqualTo(entity.getPacket());
        assertThat(Math.abs(entitydto.hashCode())).isGreaterThan(0);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
    }
    
    @Test
    public void testVhfDecodedPacketConverter() throws Exception {
        final Instant now = Instant.now();
        final VhfDecodedPacket entity = DataGenerator.generateVhfDecodedPacket("ANHASH", "JSON", "7CAFE");
        entity.setInsertionTime(new Timestamp(now.getMillis()));
        final VhfDecodedPacketDto dto = mapper.map(entity, VhfDecodedPacketDto.class);
        assertThat(entity.getPacket()).isEqualTo(dto.getPacket());
        assertThat(entity.getPktformat()).isEqualTo(dto.getPktformat());
        assertThat(dto.toString().length()).isGreaterThan(0);
        final VhfBasePacket base = mapper.map(entity,VhfBasePacket.class);
        assertThat(base.getPktformat()).isEqualTo(entity.getPktformat());
    }

    @Test
    public void testVhfBinaryPacketConverter() throws Exception {
        final VhfBinaryPacket entity = DataGenerator.generateVhfBinaryPacket("ANHASH", "HEXA", "7CAFE");

        final VhfBinaryPacketDto dto = mapper.map(entity, VhfBinaryPacketDto.class);
        assertThat(entity.getPacket()).isEqualTo(dto.getPacket());
        assertThat(entity.getPktformat()).isEqualTo(dto.getPktformat());
        assertThat(dto.toString().length()).isGreaterThan(0);
        final VhfBinaryPacket entitydto = mapper.map(dto, VhfBinaryPacket.class);
        assertThat(entitydto.getPacket()).isEqualTo(entity.getPacket());
        assertThat(Math.abs(entitydto.hashCode())).isGreaterThan(0);
        assertThat(entitydto.toString().length()).isGreaterThan(0);
    }



    @Test
    public void testApiDtos() throws Exception {
        final ApiHttpError httper = new ApiHttpError();
        httper.code(1).message("Error test");
        assertThat(httper.toString().length()).isGreaterThan(0);

        final HTTPResponse httpresp = new HTTPResponse();
        httpresp.code(1).message("Error test");
        assertThat(httpresp.toString().length()).isGreaterThan(0);
    }

    @Test
    public void testVhfTransferFrameConverter() throws Exception {
        final VhfTransferFrame entity = new VhfTransferFrame();
        final PacketApid apid = DataGenerator.generatePacketApid(578, "TmVhfEclSome","svom", "lc", "ECLAIRS");
        final CcsdsPacket ccsds = DataGenerator.generateCcsdsPacket();
        final FrameHeaderPacket fhp = DataGenerator.generateFrameHeader();
        assertThat(fhp.toString().length()).isGreaterThan(0);
        final VhfGroundStation vhfgs = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final PrimaryHeaderPacket php = DataGenerator.generatePrimaryHeader();
        final VhfStationStatus sst = DataGenerator.generateStationStatus();
        final VhfBinaryPacket binarypkt = DataGenerator.generateVhfBinaryPacket("somehash", "ascii", "somepacket");
        entity.setApid(apid);
        entity.setCcsds(ccsds);
        entity.setFrameHeader(fhp);
        entity.setIsFrameDuplicate(false);
        entity.setStation(vhfgs);
        entity.setIsFrameValid(true);
        entity.setHeader(php);
        entity.setStationStatus(sst);
        final VhfTransferFrameDto dto = mapper.map(entity, VhfTransferFrameDto.class);
        final FrameHeaderPacketDto fhdto = mapper.map(fhp, FrameHeaderPacketDto.class);
        final FrameHeaderPacketDto fhdto1 = new FrameHeaderPacketDto();
        fhdto1.dfStatus(fhdto.getDfStatus()).mcfCount(fhdto.getMcfCount()).ocFlag(fhdto.getOcFlag());
        fhdto1.vcfCount(fhdto.getVcfCount()).vcId(fhdto.getVcId()).tframeVersion(fhdto.getTframeVersion()).spaceCraftId(fhdto.getSpaceCraftId());

        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(fhdto.toString().length()).isGreaterThan(0);
        assertThat(fhdto.equals(fhdto1)).isTrue();
        entity.setDupFrameId(10L);
        entity.setRawPacket("ARAWPACKET");
        final Long now = Instant.now().getMillis();
        entity.setInsertionTime(new Timestamp(now));
        assertThat(entity.toString().length()).isGreaterThan(0);
        
        final VhfTransferFrameDto dto1 = new VhfTransferFrameDto();
        final PacketApidDto apiddto = mapper.map(apid, PacketApidDto.class);
        dto1.apid(apiddto);
        final VhfGroundStation station = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final VhfGroundStationDto stationdto = mapper.map(station, VhfGroundStationDto.class);
        dto1.station(stationdto);
        dto1.hashId("somehash");
        dto1.dupFrameId(1L);
        final CcsdsPacketDto ccsdsdto = mapper.map(entity, CcsdsPacketDto.class);
        dto1.ccsds(ccsdsdto);
        final VhfBinaryPacketDto bindto = mapper.map(binarypkt, VhfBinaryPacketDto.class);
        dto1.packet(bindto);
        assertThat(dto1.hashCode()).isNotNull();
        assertThat(dto1.equals(dto)).isFalse();
        
        final VhfTransferFrameSetDto setdto = new VhfTransferFrameSetDto();
        GenericStringMap criteria = new GenericStringMap();
        criteria.put("frameid", "1");
        setdto.filter(criteria).size(1L);
        setdto.addResourcesItem(dto1);
        assertThat(setdto.toString()).isNotEmpty();

        final VhfBasePacket bpkt = mapper.map(entity, VhfBasePacket.class);
        assertThat(bpkt.getHashId()).isEqualTo(entity.getBinaryHash());
    }
    
    @Test
    public void testSatellite() throws Exception {
        VhfSatAttitude att = DataGenerator.generateAttitude();
        VhfSatAttitudeDto attdto = mapper.map(att, VhfSatAttitudeDto.class);
        assertThat(attdto.getApidName()).isEqualTo(att.getApidName());
        assertThat(attdto.getPacketTime()).isEqualTo(att.getPacketTime());
        VhfSatPosition pos = DataGenerator.generatePosition();
        VhfSatPositionDto posdto = mapper.map(pos, VhfSatPositionDto.class);
        assertThat(posdto.getApidName()).isEqualTo(pos.getApidName());
        assertThat(posdto.getPacketTime()).isEqualTo(pos.getPacketTime());
    }

    @Test
    public void testSetDtos() throws Exception {
        final PacketApidSetDto setdto = new PacketApidSetDto();
        final PacketApid apid = DataGenerator.generatePacketApid(578, "TmVhfEclElse","svom", "lc", "ECLAIRS");
        final PacketApidDto apiddto = mapper.map(apid, PacketApidDto.class);

        GenericStringMap criteria = new GenericStringMap();
        criteria.put("apid", "578");
        setdto.format("PacketApid").filter(criteria).size(1L);
        setdto.addResourcesItem(apiddto);
        assertThat(setdto.getSize()).isGreaterThan(0);
        
        final List<PacketApidDto> resources = setdto.getResources();
        assertThat(resources.size()).isGreaterThan(0);
        assertThat(setdto.toString().length()).isGreaterThan(0);

        final StationSetDto setdto1 = new StationSetDto();
        GenericStringMap criteria1 = new GenericStringMap();
        criteria1.put("stationid", "1");
        setdto1.filter(criteria1);
        setdto1.format("VhfGroundStationDto");
        setdto1.size(1L);
        final VhfGroundStation entity = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final VhfGroundStationDto stationdto = mapper.map(entity, VhfGroundStationDto.class);
        setdto1.addResourcesItem(stationdto);
        final List<VhfGroundStationDto> resources1 = setdto1.getResources();
        assertThat(resources1.size()).isGreaterThan(0);
        assertThat(setdto1.toString().length()).isGreaterThan(0);
    }

    @Test
    public void testNotificationDtos() throws Exception {
        VhfNotification entity = DataGenerator.generateVhfNotification();
        Instant now = Instant.now();
        entity.setInsertionTime(new Timestamp(now.getMillis()));
        final VhfNotificationDto dto = mapper.map(entity, VhfNotificationDto.class);
        assertThat(dto.getPktflag()).isEqualTo(entity.getPktflag());
        assertThat(dto.toString().length()).isGreaterThan(0);
        assertThat(entity.toString().length()).isGreaterThan(0);
    }
}
