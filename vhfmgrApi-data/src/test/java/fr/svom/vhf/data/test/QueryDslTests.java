package fr.svom.vhf.data.test;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.RawPacketRepository;
import fr.svom.vhf.data.repositories.VhfBinaryPacketRepository;
import fr.svom.vhf.data.repositories.VhfBurstIdRepository;
import fr.svom.vhf.data.repositories.VhfDecodedPacketRepository;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import fr.svom.vhf.data.repositories.VhfNotificationRepository;
import fr.svom.vhf.data.repositories.VhfSatAttitudeRepository;
import fr.svom.vhf.data.repositories.VhfSatPositionRepository;
import fr.svom.vhf.data.repositories.VhfStationStatusRepository;
import fr.svom.vhf.data.repositories.VhfTransferFrameRepository;
import fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria;
import fr.svom.vhf.data.repositories.querydsl.PacketApidFiltering;
import fr.svom.vhf.data.repositories.querydsl.SearchCriteria;
import fr.svom.vhf.data.repositories.querydsl.VhfBinaryPacketFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfBurstIdFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfDecodedPacketFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfGroundStationFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfNotificationFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfRawPacketFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfSatAttitudeFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfSatPositionFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfStationStatusFiltering;
import fr.svom.vhf.data.repositories.querydsl.VhfTransferFrameFiltering;
import fr.svom.vhf.data.test.generators.ApidGenerator;
import fr.svom.vhf.data.test.generators.DataGenerator;
import org.apache.commons.codec.binary.Hex;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QueryDslTests {

    @Autowired
    private VhfGroundStationRepository repository;

    @Autowired
    private PacketApidRepository apidrepository;

    @Autowired
    private RawPacketRepository vhfrawrepository;

    @Autowired
    private VhfNotificationRepository vhfNotificationRepository;

    @Autowired
    private VhfSatAttitudeRepository vhfSatAttitudeRepository;

    @Autowired
    private VhfSatPositionRepository vhfSatPositionRepository;

    @Autowired
    private VhfTransferFrameRepository vhfTransferFrameRepository;

    @Autowired
    private VhfBinaryPacketRepository vhfBinaryPacketRepository;

    @Autowired
    private VhfDecodedPacketRepository vhfDecodedPacketRepository;

    @Autowired
    private VhfStationStatusRepository vhfStationStatusRepository;

    @Autowired
    private VhfBurstIdRepository vhfBurstIdRepository;

    private static final String SORT_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:)([ASC|DESC]+?),";
    private static final String QRY_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:|<|>)([a-zA-Z0-9_\\-\\/\\.\\*\\%]+?),";

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Test
    public void testVhfGroundStation() throws Exception {
        final VhfGroundStation entity = DataGenerator.generateVhfGroundStation(0x10, "stvhf10",
                "00:01:02:10");
        repository.save(entity);
        final IFilteringCriteria filter = new VhfGroundStationFiltering();
        final PageRequest preq = createPageRequest(0, 10, "id:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "stationId:9,country:%,id:%,location:%,name:st");
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfGroundStation> dtolist = repository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

    }

    @Test
    public void testPacketApid() throws Exception {
        final ApidGenerator agen = new ApidGenerator();
        final List<PacketApid> apidlist = agen.getApidList();
        for (final PacketApid packetApid : apidlist) {
            apidrepository.save(packetApid);
        }
        final IFilteringCriteria filter = new PacketApidFiltering();
        final PageRequest preq = createPageRequest(0, 10, "apid:ASC");

        final List<SearchCriteria> params = createMatcherCriteria("id:576,name:%,description:%,classname:%,"
                                                                  + "category:VHF");
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);

        final Page<PacketApid> dtolist = apidrepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);
    }

    @Test
    public void testVhfFrame() throws Exception {
        final VhfTransferFrame entity = new VhfTransferFrame();
        final PacketApid apid = DataGenerator.generatePacketApid(578, "TmVhfEclSome", "svom", "lc", "ECLAIRS");
        PacketApid savedapid = apidrepository.save(apid);
        final CcsdsPacket ccsds = DataGenerator.generateCcsdsPacket();
        final FrameHeaderPacket fhp = DataGenerator.generateFrameHeader();
        final VhfGroundStation vhfgs = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final VhfGroundStation savedst = repository.save(vhfgs);
        final PrimaryHeaderPacket php = DataGenerator.generatePrimaryHeader();
        php.setPacketObsid(123L);
        php.setPacketObsidNum(123);
        php.setPacketObsidType(0);
        final VhfStationStatus sst = DataGenerator.generateStationStatus();
        final VhfStationStatus savedsst = vhfStationStatusRepository.save(sst);
        entity.setApid(savedapid);
        entity.setCcsds(ccsds);
        entity.setFrameHeader(fhp);
        entity.setIsFrameDuplicate(false);
        entity.setStation(savedst);
        entity.setIsFrameValid(true);
        entity.setNotifStatus("FAILED");
        entity.setHeader(php);
        entity.setStationStatus(savedsst);

        final Long now = Instant.now().toEpochMilli();
        final VhfTransferFrame saved = vhfTransferFrameRepository.save(entity);
        assertThat(entity.getApid()).isEqualTo(apid);

        final IFilteringCriteria filter = new VhfTransferFrameFiltering();
        final PageRequest preq = createPageRequest(0, 10, "insertionTime:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "apid:578,insertionTime>0,insertiontime<" + (now + 3600000L) + ",packettime>-1");
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfTransferFrame> dtolist = vhfTransferFrameRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params2 = createMatcherCriteria(
                "apidname:svom,obsid:123,isframevalid:True,isframeduplicate:False,packetTime<" + now
                + ",notifstatus:FAILED");
        final List<BooleanExpression> expressions2 = filter.createFilteringConditions(params2);
        final BooleanExpression wherepred2 = buildWhere(expressions2);
        final Page<VhfTransferFrame> dtolist2 = vhfTransferFrameRepository.findAll(wherepred2,
                preq);
        assertThat(dtolist2.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params3 = createMatcherCriteria(
                "id:2,isframevalid:False,isframeduplicate:True,packetTime>0,insertiontime>0,stationid:1,pktformat:JSON"
                + ",notifstatus:FAILED");
        final List<BooleanExpression> expressions3 = filter.createFilteringConditions(params3);
        final BooleanExpression wherepred3 = buildWhere(expressions3);
        final Page<VhfTransferFrame> dtolist3 = vhfTransferFrameRepository.findAll(wherepred3,
                preq);
        assertThat(dtolist3.getSize()).isGreaterThanOrEqualTo(0);

        final List<SearchCriteria> params4 = createMatcherCriteria(
                "obsid:123,obsidnum:123,obsidtype:0,classname:%,category:%");
        final List<BooleanExpression> expressions4 = filter.createFilteringConditions(params4);
        final BooleanExpression wherepred4 = buildWhere(expressions4);
        final Page<VhfTransferFrame> dtolist4 = vhfTransferFrameRepository.findAll(wherepred4,
                preq);
        assertThat(dtolist4.getSize()).isGreaterThanOrEqualTo(0);

        final IFilteringCriteria filterss = new VhfStationStatusFiltering();
        final PageRequest preqss = createPageRequest(0, 10, "stationTime:ASC");
        final List<SearchCriteria> paramsss = createMatcherCriteria(
                "stationtime>0,stationtime<" + now + ",doppler>-1,doppler<100.4");
        final List<BooleanExpression> expressionsss = filterss.createFilteringConditions(paramsss);
        final BooleanExpression wherepredss = buildWhere(expressionsss);
        final Page<VhfStationStatus> dtolistss = vhfStationStatusRepository.findAll(wherepredss,
                preqss);
        assertThat(dtolistss.getSize()).isGreaterThan(0);
        final List<SearchCriteria> paramsss1 = createMatcherCriteria(
                "idstation:1,stationtime:1,doppler:10.");
        final List<BooleanExpression> expressionsss1 = filterss.createFilteringConditions(paramsss1);
        final BooleanExpression wherepredss1 = buildWhere(expressionsss1);
        final Page<VhfStationStatus> dtolistss1 = vhfStationStatusRepository.findAll(wherepredss1,
                preqss);
        assertThat(dtolistss1.getSize()).isGreaterThanOrEqualTo(0);

    }

    @Test
    public void testVhfRawPacket() throws Exception {
        final VhfRawPacket vhftf = DataGenerator.generateVhfRawPacket("ASCII", "TEST",
                "ALONGHEXASTRING");
        final VhfRawPacket saved = vhfrawrepository.save(vhftf);
        log.info("Saved frame {}", saved);
        assertThat(saved.getPacket()).isEqualTo("ALONGHEXASTRING");
        final IFilteringCriteria filter = new VhfRawPacketFiltering();
        final PageRequest preq = createPageRequest(0, 10, "insertionTime:ASC");

        final List<SearchCriteria> params = createMatcherCriteria("flag:%,insertionTime>0");
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfRawPacket> dtolist = vhfrawrepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params1 = createMatcherCriteria(
                "insertionTime:1,insertionTime<999999999999");
        final List<BooleanExpression> expressions1 = filter.createFilteringConditions(params1);
        final BooleanExpression wherepred1 = buildWhere(expressions1);
        final Page<VhfRawPacket> dtolist1 = vhfrawrepository.findAll(wherepred1, preq);
        assertThat(dtolist1.getSize()).isGreaterThan(0);
    }

    @Test
    public void testVhfBinaryPacket() throws Exception {
        final VhfTransferFrame entity = new VhfTransferFrame();
        final PacketApid apid = DataGenerator.generatePacketApid(578, "TmVhfEclSome", "svom", "lc", "ECLAIRS");
        PacketApid savedapid = apidrepository.save(apid);
        final CcsdsPacket ccsds = DataGenerator.generateCcsdsPacket();
        final FrameHeaderPacket fhp = DataGenerator.generateFrameHeader();
        final VhfGroundStation vhfgs = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final VhfGroundStation savedst = repository.save(vhfgs);
        final PrimaryHeaderPacket php = DataGenerator.generatePrimaryHeader();
        php.setPacketObsid(1234L);
        php.setPacketObsidNum(1234);
        php.setPacketObsidType(0);
        final VhfStationStatus sst = DataGenerator.generateStationStatus();
        final VhfStationStatus savedsst = vhfStationStatusRepository.save(sst);
        entity.setApid(savedapid);
        entity.setCcsds(ccsds);
        entity.setFrameHeader(fhp);
        entity.setIsFrameDuplicate(false);
        entity.setStation(savedst);
        entity.setIsFrameValid(true);
        entity.setNotifStatus("FAILED");
        entity.setHeader(php);
        entity.setStationStatus(savedsst);
        entity.setBurstId("sb2021123");
        final Long now = Instant.now().toEpochMilli();
        final VhfBinaryPacket pkt = DataGenerator.generateVhfBinaryPacket("anhashid", "binary",
                "ABINARYPKT");
        entity.setBinaryHash("anhashid");
        final VhfBinaryPacket saved = vhfBinaryPacketRepository.save(pkt);
        log.info("Saved binary packet {}", saved);
        assertThat(saved.getPacket()).isEqualTo(Hex.encodeHexString("ABINARYPKT".getBytes()));
        assertThat(saved.getHashId()).isEqualTo("anhashid");
        final IFilteringCriteria filter = new VhfBinaryPacketFiltering();
        final PageRequest preq = createPageRequest(0, 10, "insertionTime:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "apid:578,obsid:1,apidname:%,insertiontime>0,packettime>-1");
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfBinaryPacket> dtolist = vhfBinaryPacketRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params2 = createMatcherCriteria(
                "apidname:svom,obsid:123,isframevalid:True,isframeduplicate:False,packettime<" + now
                + ",stationid:1");
        final List<BooleanExpression> expressions2 = filter.createFilteringConditions(params2);
        final BooleanExpression wherepred2 = buildWhere(expressions2);
        final Page<VhfBinaryPacket> dtolist2 = vhfBinaryPacketRepository.findAll(wherepred2, preq);
        assertThat(dtolist2.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params3 = createMatcherCriteria(
                "isframevalid:Frue,isframeduplicate:True,packettime>0,insertiontime>0,insertiontime:1"
                + ",stationid:1");
        final List<BooleanExpression> expressions3 = filter.createFilteringConditions(params3);
        final BooleanExpression wherepred3 = buildWhere(expressions3);
        final Page<VhfBinaryPacket> dtolist3 = vhfBinaryPacketRepository.findAll(wherepred3, preq);
        assertThat(dtolist3.getSize()).isGreaterThanOrEqualTo(0);

        final List<SearchCriteria> params4 = createMatcherCriteria(
                "hashid:anhashid,obsid:1234,obsidnum:1234,obsidtype:0");
        final List<BooleanExpression> expressions4 = filter.createFilteringConditions(params4);
        final BooleanExpression wherepred4 = buildWhere(expressions4);
        final Page<VhfBinaryPacket> dtolist4 = vhfBinaryPacketRepository.findAll(wherepred4, preq);
        assertThat(dtolist4.getSize()).isGreaterThanOrEqualTo(0);

    }

    @Test
    public void testVhfDecodedPacket() throws Exception {
//        final VhfTransferFrame entity = new VhfTransferFrame();
//        final PacketApid apid = DataGenerator.generatePacketApid(578, "TmVhfEclSome", "svom", "lc", "ECLAIRS");
//        PacketApid savedapid = apidrepository.save(apid);
//        final CcsdsPacket ccsds = DataGenerator.generateCcsdsPacket();
//        final FrameHeaderPacket fhp = DataGenerator.generateFrameHeader();
//        final VhfGroundStation vhfgs = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
//                "00:01:02:03");
//        final VhfGroundStation savedst = repository.save(vhfgs);
//        final PrimaryHeaderPacket php = DataGenerator.generatePrimaryHeader();
//        php.setPacketObsid(1234L);
//        php.setPacketObsidNum(1234);
//        php.setPacketObsidType(0);
//        final VhfStationStatus sst = DataGenerator.generateStationStatus();
//        final VhfStationStatus savedsst = vhfStationStatusRepository.save(sst);
//        entity.setApid(savedapid);
//        entity.setCcsds(ccsds);
//        entity.setFrameHeader(fhp);
//        entity.setIsFrameDuplicate(false);
//        entity.setStation(savedst);
//        entity.setIsFrameValid(true);
//        entity.setNotifStatus("FAILED");
//        entity.setHeader(php);
//        entity.setStationStatus(savedsst);
//        entity.setBurstId("sb2021123");
        final Long now = Instant.now().toEpochMilli();
        final VhfDecodedPacket pkt = DataGenerator.generateVhfDecodedPacket("adecodedhashid", "json",
                "ADECODEDPKT");
        final VhfDecodedPacket saved = vhfDecodedPacketRepository.save(pkt);
        //final VhfTransferFrame framesaved = vhfTransferFrameRepository.save(entity);
        log.info("Saved decoded packet {}", saved);
        assertThat(saved.getPacket()).isEqualTo("ADECODEDPKT");
        assertThat(saved.getHashId()).isEqualTo("adecodedhashid");
        final IFilteringCriteria filter = new VhfDecodedPacketFiltering();
        final PageRequest preq = createPageRequest(0, 10, "insertionTime:ASC");

        log.info("Filtering 1");
        final List<SearchCriteria> params = createMatcherCriteria(
                "apid:578,obsid:1,apidname:%,insertionTime>0,insertionTime:1,packetTime>-1");
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfDecodedPacket> dtolist = vhfDecodedPacketRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

        log.info("Filtering 2");
        final List<SearchCriteria> params1 = createMatcherCriteria(
                "isframevalid:False,isframeduplicate:True,insertionTime<999999999999,packetTime<999999999999");
        final List<BooleanExpression> expressions1 = filter.createFilteringConditions(params1);
        final BooleanExpression wherepred1 = buildWhere(expressions1);
        final Page<VhfDecodedPacket> dtolist1 = vhfDecodedPacketRepository.findAll(wherepred1,
                preq);
        assertThat(dtolist1.getSize()).isGreaterThanOrEqualTo(0);

        log.info("Filtering 3");
        final List<SearchCriteria> params2 = createMatcherCriteria(
                "isframevalid:True,isframeduplicate:False,hashid:anhash,stationid:1");
        final List<BooleanExpression> expressions2 = filter.createFilteringConditions(params2);
        final BooleanExpression wherepred2 = buildWhere(expressions2);
        final Page<VhfDecodedPacket> dtolist2 = vhfDecodedPacketRepository.findAll(wherepred2,
                preq);
        assertThat(dtolist2.getSize()).isGreaterThanOrEqualTo(0);

        log.info("Filtering 4");
        final List<SearchCriteria> params3 = createMatcherCriteria(
                "obsid:1234,obsidnum:1234,obsidtype:0,burstid:sb2021123");
        final List<BooleanExpression> expressions3 = filter.createFilteringConditions(params3);
        final BooleanExpression wherepred3 = buildWhere(expressions3);
        final Page<VhfDecodedPacket> dtolist3 = vhfDecodedPacketRepository.findAll(wherepred3,
                preq);
        assertThat(dtolist3.getSize()).isGreaterThanOrEqualTo(0);
    }

    @Test
    public void testVhfBurstId() throws Exception {

        final VhfTransferFrame entity = new VhfTransferFrame();
        final PacketApid apid = DataGenerator.generatePacketApid(576, "TmVhfEclAlert", "VHF", "alert", "ECLAIRS");
        PacketApid savedapid = apidrepository.save(apid);
        final CcsdsPacket ccsds = DataGenerator.generateCcsdsPacket();
        final FrameHeaderPacket fhp = DataGenerator.generateFrameHeader();
        final VhfGroundStation vhfgs = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final VhfGroundStation savedst = repository.save(vhfgs);
        final PrimaryHeaderPacket php = DataGenerator.generatePrimaryHeader();
        php.setPacketObsidNum(123);
        php.setPacketObsidType(0);
        php.setPacketObsid(123L);
        final VhfStationStatus sst = DataGenerator.generateStationStatus();
        final VhfStationStatus savedsst = vhfStationStatusRepository.save(sst);
        entity.setApid(savedapid);
        entity.setCcsds(ccsds);
        entity.setFrameHeader(fhp);
        entity.setIsFrameDuplicate(false);
        entity.setStation(savedst);
        entity.setIsFrameValid(true);
        entity.setNotifStatus("FAILED");
        entity.setHeader(php);
        entity.setStationStatus(savedsst);

        long tnow = Instant.now().toEpochMilli();
        final VhfTransferFrame framesaved = vhfTransferFrameRepository.save(entity);
        assertThat(entity.getApid()).isEqualTo(apid);

        final VhfBurstId pkt = DataGenerator.generateBurst("sb2021001", tnow, 99);
        pkt.setFrame(framesaved);
        final VhfBurstId saved = vhfBurstIdRepository.save(pkt);
        log.info("Saved burstid {}", saved);
        assertThat(saved.getBurstId()).isEqualTo("sb2021001");
        assertThat(saved.getPacketTime()).isGreaterThan(tnow - 10);
        final IFilteringCriteria filter = new VhfBurstIdFiltering();
        final PageRequest preq = createPageRequest(0, 10, "packetTime:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "burstid:%,packetTime>0,insertionTime>1,id:1");
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfBurstId> dtolist = vhfBurstIdRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params1 = createMatcherCriteria(
                "insertionTime<999999999999,packettime<999999999999,alerttimetb<999999999999,"
                + "alerttimetb0absseconds<999999999999");
        final List<BooleanExpression> expressions1 = filter.createFilteringConditions(params1);
        final BooleanExpression wherepred1 = buildWhere(expressions1);
        final Page<VhfBurstId> dtolist1 = vhfBurstIdRepository.findAll(wherepred1,
                preq);
        assertThat(dtolist1.getSize()).isGreaterThanOrEqualTo(0);

        final List<SearchCriteria> params2 = createMatcherCriteria(
                "obsidnum:123,obsidtype:0,obsid:123,packettime:" + tnow + ",alerttimetb>0,alerttimetb0absseconds>0,"
                + "apidname:TmVhf,eclobsid:123");
        final List<BooleanExpression> expressions2 = filter.createFilteringConditions(params2);
        final BooleanExpression wherepred2 = buildWhere(expressions1);
        final Page<VhfBurstId> dtolist2 = vhfBurstIdRepository.findAll(wherepred2,
                preq);
        assertThat(dtolist2.getSize()).isGreaterThanOrEqualTo(0);

    }

    @Test
    public void testVhfSatAttitudePacket() throws Exception {
        final VhfSatAttitude pkt = DataGenerator.generateAttitude();
        final VhfSatAttitude saved = vhfSatAttitudeRepository.save(pkt);
        log.info("Saved attitude {}", saved);
        assertThat(saved.getApidName()).isEqualTo("someapid");
        assertThat(saved.getPacketTime()).isEqualTo(1000L);
        final IFilteringCriteria filter = new VhfSatAttitudeFiltering();
        final PageRequest preq = createPageRequest(0, 10, "packetTime:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "apidname:%,packetTime>0,insertionTime>1,id:1,packettime:" + saved.getPacketTime());
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfSatAttitude> dtolist = vhfSatAttitudeRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params1 = createMatcherCriteria(
                "insertionTime<999999999999,packetTime<999999999999,insertiontime:"
                + saved.getInsertionTime().getTime());
        final List<BooleanExpression> expressions1 = filter.createFilteringConditions(params1);
        final BooleanExpression wherepred1 = buildWhere(expressions1);
        final Page<VhfSatAttitude> dtolist1 = vhfSatAttitudeRepository.findAll(wherepred1,
                preq);
        assertThat(dtolist1.getSize()).isGreaterThanOrEqualTo(0);

    }

    @Test
    public void testVhfSatPositionPacket() throws Exception {
        final VhfSatPosition pkt = DataGenerator.generatePosition();
        final VhfSatPosition saved = vhfSatPositionRepository.save(pkt);
        log.info("Saved position {}", saved);
        assertThat(saved.getApidName()).isEqualTo("someapid");
        assertThat(saved.getPacketTime()).isEqualTo(1000L);
        final IFilteringCriteria filter = new VhfSatPositionFiltering();
        final PageRequest preq = createPageRequest(0, 10, "packetTime:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "apidname:%,packetTime>0,insertionTime>1,id:1,packettime:" + saved.getPacketTime());
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfSatPosition> dtolist = vhfSatPositionRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);

        final List<SearchCriteria> params1 = createMatcherCriteria(
                "insertionTime<999999999999,packetTime<999999999999,insertionTime:"
                + saved.getInsertionTime().getTime());
        final List<BooleanExpression> expressions1 = filter.createFilteringConditions(params1);
        final BooleanExpression wherepred1 = buildWhere(expressions1);
        final Page<VhfSatPosition> dtolist1 = vhfSatPositionRepository.findAll(wherepred1,
                preq);
        assertThat(dtolist1.getSize()).isGreaterThanOrEqualTo(0);

    }

    @Test
    public void testVhfNotification() throws Exception {
        final VhfNotification entity = DataGenerator.generateVhfNotification();
        final VhfNotification saved = vhfNotificationRepository.save(entity);
        log.info("Saved notification {}", saved);
        assertThat(saved.getPktflag()).isEqualTo("ERROR");
        final IFilteringCriteria filter = new VhfNotificationFiltering();
        final PageRequest preq = createPageRequest(0, 10, "insertionTime:ASC");

        final List<SearchCriteria> params = createMatcherCriteria(
                "notifmessage:test,insertiontime>0,insertiontime<99999999999999,pktflag:ERROR,insertiontime:"
                + saved.getInsertionTime().getTime());
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        final BooleanExpression wherepred = buildWhere(expressions);
        final Page<VhfNotification> dtolist = vhfNotificationRepository.findAll(wherepred, preq);
        assertThat(dtolist.getSize()).isGreaterThan(0);
    }

    protected PageRequest createPageRequest(Integer page, Integer size, String sort) {

        final Pattern sortpattern = Pattern.compile(SORT_PATTERN);
        final Matcher sortmatcher = sortpattern.matcher(sort + ",");
        final List<Order> orderlist = new ArrayList<>();
        while (sortmatcher.find()) {
            Direction direc = Direction.ASC;
            if (sortmatcher.group(3).equals("DESC")) {
                direc = Direction.DESC;
            }
            final String field = sortmatcher.group(1);
            log.debug("Creating new order: {} {} ", direc, field);
            orderlist.add(new Order(direc, field));
        }
        log.debug("Created list of sorting orders of size {}", orderlist.size());
        final Order[] orders = new Order[orderlist.size()];
        int i = 0;
        for (final Order order : orderlist) {
            log.debug("Order @ {} = {}", i, order);
            orders[i++] = order;
        }
        final Sort msort = Sort.by(orders);
        return PageRequest.of(page, size, msort);
    }

    protected List<SearchCriteria> createMatcherCriteria(String by) {

        final Pattern pattern = Pattern.compile(QRY_PATTERN);
        final Matcher matcher = pattern.matcher(by + ",");
        log.debug("Pattern is {}", pattern);
        log.debug("Matcher is {}", matcher);
        final List<SearchCriteria> params = new ArrayList<>();
        while (matcher.find()) {
            String val = matcher.group(3);
            val = val.replaceAll("\\*", "\\%");
            params.add(new SearchCriteria(matcher.group(1), matcher.group(2), val));
        }
        log.debug("List of search criteria: {}", params.size());
        return params;
    }

    protected BooleanExpression buildWhere(List<BooleanExpression> expressions) {
        BooleanExpression wherepred = null;

        for (final BooleanExpression exp : expressions) {
            if (wherepred == null) {
                wherepred = exp;
            }
            else {
                wherepred = wherepred.and(exp);
            }
        }
        return wherepred;
    }
}
