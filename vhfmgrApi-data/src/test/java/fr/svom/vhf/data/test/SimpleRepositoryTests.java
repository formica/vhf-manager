package fr.svom.vhf.data.test;

import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.IJdbcRepository;
import fr.svom.vhf.data.repositories.JdbcRepositoryImpl;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.RawPacketRepository;
import fr.svom.vhf.data.repositories.VhfBinaryPacketRepository;
import fr.svom.vhf.data.repositories.VhfDecodedPacketRepository;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import fr.svom.vhf.data.repositories.VhfNotificationRepository;
import fr.svom.vhf.data.repositories.VhfSatAttitudeRepository;
import fr.svom.vhf.data.repositories.VhfSatPositionRepository;
import fr.svom.vhf.data.repositories.VhfStationStatusRepository;
import fr.svom.vhf.data.repositories.VhfTransferFrameRepository;
import fr.svom.vhf.data.test.generators.APIDS;
import fr.svom.vhf.data.test.generators.ApidGenerator;
import fr.svom.vhf.data.test.generators.DataGenerator;
import fr.svom.vhf.swagger.model.ObsIdApidCountDto;
import fr.svom.vhf.swagger.model.VhfStationStatusCountDto;
import org.apache.commons.codec.binary.Hex;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SimpleRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VhfGroundStationRepository repository;

    @Autowired
    private PacketApidRepository apidrepository;

    @Autowired
    private RawPacketRepository vhfrawrepository;

    @Autowired
    private VhfNotificationRepository vhfNotificationRepository;

    @Autowired
    private VhfTransferFrameRepository vhfTransferFrameRepository;

    @Autowired
    private VhfStationStatusRepository vhfStationStatusRepository;

    @Autowired
    private VhfBinaryPacketRepository vhfBinaryPacketRepository;

    @Autowired
    private VhfDecodedPacketRepository vhfDecodedPacketRepository;

    @Autowired
    private VhfSatAttitudeRepository vhfSatAttitudeRepository;

    @Autowired
    private VhfSatPositionRepository vhfSatPositionRepository;

    @Autowired
    @Qualifier("dataSource")
    private DataSource mainDataSource;


    private final Logger log = LoggerFactory.getLogger(this.getClass());


    @Test
    public void testVhfSatellite() throws Exception {
        VhfSatAttitude att = DataGenerator.generateAttitude();

        this.entityManager.persist(att);
        final Optional<VhfSatAttitude> attst = vhfSatAttitudeRepository.findById(1L);
        if (attst.isPresent()) {
            log.info("Found attitude " + attst.get());
        }
        else {
            log.error("cannot find attitude....something is wrong with the storage");
        }
        final VhfSatAttitude saved = attst.get();
        if (saved.equals(att)) {
            log.info("The original and saved entities are identical");
        }
        assertThat(attst.get().getSatAttitudeq0()).isGreaterThan(-1);
        assertThat(attst.get().getSatAttitudeq1()).isGreaterThan(-1);
        assertThat(attst.get().getSatAttitudeq2()).isGreaterThan(-1);
        assertThat(attst.get().getSatAttitudeq3()).isGreaterThan(-1);

        VhfSatPosition pos = DataGenerator.generatePosition();
        this.entityManager.persist(pos);
        final Optional<VhfSatPosition> posst = vhfSatPositionRepository.findById(1L);
        if (posst.isPresent()) {
            log.info("Found position " + posst.get());
        }
        else {
            log.error("cannot find position....something is wrong with the storage");
        }
        final VhfSatPosition savedp = posst.get();
        if (savedp.equals(pos)) {
            log.info("The original and saved entities are identical");
        }
        assertThat(posst.get().getSatPositionX()).isGreaterThan(-1);
        assertThat(posst.get().getSatPositionY()).isGreaterThan(-1);
        assertThat(posst.get().getSatPositionZ()).isGreaterThan(-1);

    }


    @Test
    public void testVhfGroundStation() throws Exception {
        final VhfGroundStation entity = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        this.entityManager.persist(entity);
        final Optional<VhfGroundStation> station = this.repository.findById(0x01);
        if (station.isPresent()) {
            log.info("Found ground station " + station.get());
        }
        else {
            log.error("cannot find station....something is wrong with the storage");
        }
        final VhfGroundStation saved = station.get();
        if (saved.equals(entity)) {
            log.info("The original and saved entities are identical");
        }
        assertThat(station.get().getId()).isEqualTo("stvhf1");
        assertThat(station.get().getLocation()).isEqualTo("PARIS");
        assertThat(saved).isEqualTo(entity);

        final VhfGroundStation another = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        assertThat(saved.toString()).isEqualTo(another.toString());
    }

    @Test
    public void testPacketApid() throws Exception {
        final ApidGenerator agen = new ApidGenerator();
        final List<PacketApid> apidlist = agen.getApidList();
        for (final PacketApid packetApid : apidlist) {
            this.entityManager.persist(packetApid);
        }
        final Optional<PacketApid> apid = this.apidrepository.findById(576);
        if (apid.isPresent()) {
            log.info("Found apid " + apid.get());
        }
        else {
            log.error("cannot find apid....something is wrong with the storage");
            assertThat(apid.get()).isNotNull();
        }
        assertThat(apid.get().getClassName()).isEqualTo("TmVhfEclairsAlert");
        assertThat(apid.get().getName()).isEqualTo("ECLALERTL1");
    }

    @Test
    public void test1_VhfFrame() throws Exception {
        log.info(">>>>>>> test1_VhfFrame");
        final VhfTransferFrame entity = new VhfTransferFrame();
        final CcsdsPacket ccsds = DataGenerator.generateCcsdsPacket();
        final PacketApid apid = DataGenerator.generatePacketApid(ccsds.getCcsdsApid(), "TmVhfEclSome", "TestSvom",
                "lc", "ECLAIRS");
        final PacketApid savedapid = apidrepository.save(apid);
        final FrameHeaderPacket fhp = DataGenerator.generateFrameHeader();
        final VhfGroundStation vhfgs = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final VhfGroundStation savedst = repository.save(vhfgs);
        final PrimaryHeaderPacket php = DataGenerator.generatePrimaryHeader();
        final VhfStationStatus sst = DataGenerator.generateStationStatus();
        final VhfStationStatus savedsst = vhfStationStatusRepository.save(sst);
        log.info("Saved station status {}", savedsst);
        entity.setApid(savedapid);
        entity.setCcsds(ccsds);
        entity.setFrameHeader(fhp);
        entity.setIsFrameDuplicate(false);
        entity.setStation(savedst);
        entity.setIsFrameValid(true);
        entity.setHeader(php);
        entity.setStationStatus(savedsst);

        final VhfTransferFrame saved = vhfTransferFrameRepository.save(entity);
        assertThat(entity.getApid()).isEqualTo(apid);
        assertThat(saved.getApid().getName()).isEqualTo(entity.getApid().getName());
        log.info("Saved vhfframe {}", saved);
        log.info(">>>>>>> END test1_VhfFrame");
    }

    @Test
    public void testFrameHeader() throws Exception {
        final FrameHeaderPacket entity = new FrameHeaderPacket();
        entity.setDfStatus(1);
        entity.setMcfCount(10);
        entity.setOcFlag(0);
        entity.setSpaceCraftId(100);
        entity.setTframeVersion(9);
        entity.setVcfCount(99);
        entity.setVcId(1);
        assertThat(entity.toString().length()).isGreaterThan(0);
    }

    @Test
    public void testCcsdsHeader() throws Exception {
        final CcsdsPacket entity = new CcsdsPacket();
        entity.setCcsdsApid(576);
        entity.setCcsdsCounter(1);
        entity.setCcsdsCrc(9);
        entity.setCcsdsGFlag(0);
        entity.setCcsdsPlength(100);
        entity.setCcsdsSecHeadFlag(1);
        entity.setCcsdsType(2);
        entity.setCcsdsVersionNum(10);
        assertThat(entity.toString().length()).isGreaterThan(0);
    }

    @Test
    public void testVhfNotification() throws Exception {
        final VhfNotification entity = new VhfNotification();
        entity.setNotifFormat("JSON");
        entity.setNotifMessage("{ \"message\" : \"Some message\"}");
        entity.setPktflag("ERROR");
        entity.setSize(entity.getNotifMessage().length());
        log.info("Created VhfNotification entity: {}", entity);
        final VhfNotification saved = vhfNotificationRepository.save(entity);
        assertThat(saved).isEqualTo(entity);
    }

    @Test
    public void testVhfRawPacket() throws Exception {
        final VhfRawPacket vhftf = DataGenerator.generateVhfRawPacket("ASCII", "TEST",
                "ALONGHEXASTRING");
        final VhfRawPacket saved = vhfrawrepository.save(vhftf);
        log.info("Saved frame {}", saved);
        assertThat(saved.getPacket()).isEqualTo("ALONGHEXASTRING");
    }

    @Test
    public void testVhfBinaryPacket() throws Exception {
        final VhfBinaryPacket pkt = DataGenerator.generateVhfBinaryPacket("anhashid", "binary",
                "ABINARYPKT");
        final VhfBinaryPacket saved = vhfBinaryPacketRepository.save(pkt);
        log.info("Saved binary packet {}", saved);
        assertThat(saved.getPacket()).isEqualTo(Hex.encodeHexString("ABINARYPKT".getBytes()));
        assertThat(saved.getHashId()).isEqualTo("anhashid");
    }

    @Test
    public void testVhfDecodedPacket() throws Exception {
        final VhfDecodedPacket pkt = DataGenerator.generateVhfDecodedPacket("anhashid", "json",
                "ADECODEDPKT");
        final VhfDecodedPacket saved = vhfDecodedPacketRepository.save(pkt);
        log.info("Saved decoded packet {}", saved);
        assertThat(saved.getPacket()).isEqualTo("ADECODEDPKT");
        assertThat(saved.getHashId()).isEqualTo("anhashid");
    }


    @Test
    public void test9_jdbcRepo() throws Exception {
        log.info(">>>>>>> test9_jdbcRepo");
        final VhfTransferFrame saved = createFrame();
        assertThat(saved).isNotNull();
        Iterable<VhfTransferFrame> framelist = vhfTransferFrameRepository.findAll();
        for (final VhfTransferFrame f : framelist) {
            log.info("Found frame {}", f);
        }
        final VhfTransferFrame saved1 = createFrame();
        assertThat(saved1).isNotNull();
        framelist = vhfTransferFrameRepository.findAll();
        for (final VhfTransferFrame f : framelist) {
            log.info("Found frame {}", f);
        }

        final IJdbcRepository jdbcRepository = new JdbcRepositoryImpl(mainDataSource);
        final List<ObsIdApidCountDto> counters = jdbcRepository.getCountObsIdSummaryInfo("TmVhfEclLc", "all", 1L, 0L,
                9999999999999L);
        log.info("Obsid Apid counters {}", counters);
        assertThat(counters).isNotNull();

        final List<VhfStationStatusCountDto> sscounters = jdbcRepository.getCountVhfStationStatus(0L, 1600000000L);
        log.info("Station status counters {}", sscounters);
        assertThat(sscounters).isNotNull();

        final Long vhfcount = jdbcRepository.getPacketCountInTimeRange(0L, 1590709019000L);
        log.info("Packet counter {}", vhfcount);
        assertThat(vhfcount).isNotNull();

        log.info("=============== Obsid counts test ================");
        final String apidname = "TmVhfEclLc";
        final List<ObsIdApidCountDto> obl = jdbcRepository.getCountObsIdSummaryInfo(apidname, "all",
                1L, 0L, 999999999999L);
        assertThat(obl).isNotNull();
        final List<ObsIdApidCountDto> obl2 = jdbcRepository.getCountObsIdSummaryInfo("TmVhfEclLc", "OBLC_ECL",
                1L, 0L, 999999999999L);
        assertThat(obl2).isNotNull();
        List<Map<String, Object>> oblist = jdbcRepository.getObsidListInTimeRange(0L, 999999999999L);
        assertThat(oblist).isNotNull();
        Long obscount = jdbcRepository.getPacketCountInTimeRange(0L, 999999999999L);
        assertThat(obscount).isNotNull();


        log.info(">>>>>> END test9_jdbcRepo");
    }

    protected VhfTransferFrame createFrame() {
        final VhfTransferFrame entity = new VhfTransferFrame();

        final CcsdsPacket ccsds = DataGenerator.generateCcsdsPacket();
        final PacketApid apid = DataGenerator.generatePacketApid(ccsds.getCcsdsApid(), "TmVhfEclLc", "TestSvom",
                "lc", "ECLAIRS");
        final PacketApid savedapid = apidrepository.save(apid);
        final FrameHeaderPacket fhp = DataGenerator.generateFrameHeader();
        final VhfGroundStation vhfgs = DataGenerator.generateVhfGroundStation(0x01, "stvhf1",
                "00:01:02:03");
        final VhfGroundStation savedst = repository.save(vhfgs);
        final PrimaryHeaderPacket php = DataGenerator.generatePrimaryHeader();
        php.setPacketObsidNum(1);
        php.setPacketObsidType(0);
        php.setPacketObsid(1L);
        final VhfStationStatus sst = DataGenerator.generateStationStatus();
        final VhfStationStatus savedsst = vhfStationStatusRepository.save(sst);
        log.info("Saved station status {}", savedsst);
        entity.setApid(savedapid);
        entity.setCcsds(ccsds);
        entity.setFrameHeader(fhp);
        entity.setIsFrameDuplicate(false);
        entity.setStation(savedst);
        entity.setIsFrameValid(true);
        entity.setHeader(php);
        entity.setStationStatus(savedsst);
        final VhfTransferFrame saved = vhfTransferFrameRepository.save(entity);
        return saved;
    }
}
