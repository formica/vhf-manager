package fr.svom.vhf.data.test;

import fr.svom.messages.model.DataGroup;
import fr.svom.messages.model.DataGroupClass;
import fr.svom.messages.model.DataNotification;
import fr.svom.messages.model.DataNotificationContent;
import fr.svom.messages.model.DataProvider;
import fr.svom.messages.model.DataSource;
import fr.svom.messages.model.DataStream;
import fr.svom.messages.model.ErrorLevel;
import fr.svom.messages.model.ErrorMessage;
import fr.svom.messages.model.ErrorMessageContent;
import fr.svom.messages.model.ProcStatus;
import fr.svom.messages.model.ProcessingDescriptor;
import fr.svom.messages.model.Program;
import fr.svom.messages.model.ServiceDescriptor;
import fr.svom.messages.model.ServiceStatus;
import fr.svom.messages.model.ServiceStatusContent;
import fr.svom.messages.model.Target;
import fr.svom.messages.model.VhfLogMessage;
import fr.svom.vhf.data.config.VhfProperties;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.PayloadEncodingException;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.helpers.ByteDecodingHelper;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.helpers.StreamTools;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.repositories.querydsl.SearchCriteria;
import fr.svom.vhf.data.test.generators.DataGenerator;
import ma.glasnost.orika.MapperFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigInteger;
import java.nio.ByteOrder;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SvomMessageTests {

    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    @Test
    public void testSvomMessageDtos() throws Exception {
        final DataNotification dn = new DataNotification();
        final DataNotificationContent dnc = new DataNotificationContent();
        dnc.dataProvider(DataProvider.VHFDB)
                .dataStream(DataStream.FPOC).message("some message")
                .resourceLocator("anurl");
        assertThat(dn.toString().length()).isGreaterThan(0);
        assertThat(DataSource.BANDX.name()).isEqualTo("BANDX");
        assertThat(DataSource.VHF.name()).isEqualTo("VHF");
        assertThat(DataProvider.VHFDB.name()).isEqualTo("VHFDB");
        DataGroup group = new DataGroup();
        group.identifier("AN_OBSID");
        group.propertyClass(DataGroupClass.OBS_ID);
        final Target tg = new Target().collectedData(group);
        final Target tg2 = new Target().collectedData(group);
        assertThat(tg.toString().length()).isGreaterThan(0);
        assertThat(tg).isEqualTo(tg2);
        final VhfLogMessage vl = new VhfLogMessage().apid(576).channel(1).corrPower(9).dopdrift(1.0F).doppler(9.0F).errmsg("AnError").isdup(0).obsid(1L);
        final VhfLogMessage vl2 = new VhfLogMessage().apid(576).channel(1).corrPower(9).dopdrift(1.0F).doppler(9.0F).errmsg("AnError").isdup(0).obsid(1L);
        assertThat(vl.toString().length()).isGreaterThan(0);
        assertThat(vl.getApid()).isEqualTo(576);
        assertThat(vl.getChannel()).isEqualTo(1);
        assertThat(vl.getObsid()).isEqualTo(1L);
        assertThat(vl).isEqualTo(vl2);
        final ProcessingDescriptor pd = new ProcessingDescriptor();
        pd.message("A descriptor").target(tg).processName("some-process").processId("ABC");
        final ErrorMessage em = new ErrorMessage();
        final ErrorMessageContent emc = new ErrorMessageContent().httpErrorCode(404).info("An Error").level(ErrorLevel.ERROR).message("An error").processingDescriptor(pd);
        em.content(emc);
        em.setMessageClass(ErrorMessage.MessageClassEnum.ERRORMESSAGE);
        assertThat(em.toString().length()).isGreaterThan(0);
        assertThat(pd.toString().length()).isGreaterThan(0);
        
        final ServiceDescriptor sd = new ServiceDescriptor();
        sd.name("vhfmgr").uri("someuri").version("v0").program(Program.COREPROGRAM).creationDate(Instant.now().atOffset(ZoneOffset.UTC));
        assertThat(sd.toString().length()).isGreaterThan(0);

        final ServiceStatus ss = new ServiceStatus();
        final ServiceStatusContent ssc = new ServiceStatusContent().serviceDescriptor(sd).activity(ProcStatus.IDLE).info("some info");
        ss.setContent(ssc);
        ss.setMessageClass(ServiceStatus.MessageClassEnum.SERVICESTATUS);
        assertThat(ss.toString().length()).isGreaterThan(0);
    }

    @Test
    public void propertiesTest() {
        final VhfProperties props = new VhfProperties();
        props.setAuthenticationtype("none");
        props.setDumpdir("/tmp");
        props.setSchemaname("VHF");
        props.setWebstaticdir("/tmp/web");
        props.setSecurity("none");
        props.setUploaddir("somedir");
        assertThat(props.toString().length()).isGreaterThan(0);

        assertThat(props.getAuthenticationtype()).isEqualTo("none");
        assertThat(props.getDumpdir()).isEqualTo("/tmp");
        assertThat(props.getWebstaticdir()).isEqualTo("/tmp/web");
        assertThat(props.getSchemaname()).isEqualTo("VHF");
        assertThat(props.getSecurity()).isEqualTo("none");
        assertThat(props.getUploaddir()).isEqualTo("somedir");

    }

    @Test
    public void helperTest() {
        final VhfDecodedPacket dp = DataModelsHelper.buildDecodedPacket("some content", "someformat", "thehash", "anapid");
        assertThat(dp.toString().length()).isGreaterThan(0);
        final VhfDecodingException e = new VhfDecodingException("some error", dp);
        assertThat(e.getMessage()).contains("error");
        
        final BigInteger bi = BigInteger.valueOf(8171518);
        final Integer vi = (Integer) ByteDecodingHelper.getObject(bi, ByteOrder.BIG_ENDIAN, "java.lang.Integer");

        final BigInteger bsi = BigInteger.valueOf(1621);
        final Short si = (Short) ByteDecodingHelper.getObject(bsi, ByteOrder.BIG_ENDIAN, "java.lang.Short");
        assertThat(vi).isEqualTo(8171518);
        assertThat(si).isEqualTo((short)1621);

        final BigInteger bsile = BigInteger.valueOf(1621);
        final Short sile = (Short) ByteDecodingHelper.getObject(bsile, ByteOrder.LITTLE_ENDIAN, "java.lang.Short");
        final BigInteger bsilenull = null;
        final Short silenull = (Short) ByteDecodingHelper.getObject(bsilenull, ByteOrder.LITTLE_ENDIAN, "java.lang.Short");
        assertThat(sile).isNotNull();
        assertThat(silenull).isNull();

        final VhfDecodedPacket dp1 = DataModelsHelper.buildDecodedPacket("pkt1", "ascii", "hash1", "anapid1");
        final VhfDecodedPacket dp2 = DataModelsHelper.buildDecodedPacket("pkt2", "ascii", "hash1", "anapid1");
        final List<VhfDecodedPacket> dplist = new ArrayList<>();
        dplist.add(dp);
        dplist.add(dp1);
        dplist.add(dp2);
        final List<VhfDecodedPacket> dplistclean = StreamSupport.stream(dplist.spliterator(), false)
        .filter(StreamTools.distinctByKey(VhfDecodedPacket::getHashId))
        .collect(Collectors.toList());    
        assertThat(dplistclean).isNotEmpty();
    }

    @Test
    public void exceptionTest() {
        final NullPointerException np = new NullPointerException("null");
        final VhfServiceException es = new VhfServiceException("message");
        assertThat(es.getMessage()).contains("message");
        final VhfServiceException ees = new VhfServiceException("message", np);
        assertThat(ees.getCause()).isNotNull();
        final NotExistsPojoException e = new NotExistsPojoException("message");
        assertThat(e.getMessage()).contains("message");
        final NotExistsPojoException ee = new NotExistsPojoException("message", np);
        assertThat(ee.getMessage()).contains("message");
        
/*
        final StreamingException e1 = new StreamingException("message");
        assertThat(e1.getMessage()).contains("message");
        final StreamingException ee1 = new StreamingException(np);
        assertThat(ee1.getCause()).isNotNull();
*/
        final PayloadEncodingException e2 = new PayloadEncodingException("message");
        assertThat(e2.getMessage()).contains("message");
        final PayloadEncodingException ee2 = new PayloadEncodingException(np);
        assertThat(ee2.getCause()).isNotNull();
        final VhfServiceException e3 = new VhfServiceException("message");
        assertThat(e3.getMessage()).contains("message");
        final VhfServiceException ee3 = new VhfServiceException(np);
        assertThat(ee3.getCause()).isNotNull();
        final VhfCriteriaException e4 = new VhfCriteriaException("message");
        assertThat(e4.getMessage()).contains("message");
        final VhfCriteriaException ee4 = new VhfCriteriaException(np);
        assertThat(ee4.getCause()).isNotNull();
        final VhfDecodedPacket pkt = DataGenerator.generateVhfDecodedPacket("anhash", "txt", "the packet content");
        final VhfDecodingException e5 = new VhfDecodingException("message", pkt);
        assertThat(e5.getErrordecoded()).isNotNull();
        
    }

    @Test
    public void criteriaTest() {
        final SearchCriteria sc = new SearchCriteria("key",":","val");
        sc.setKey("key1");
        sc.setOperation(">");
        sc.setValue("v1");
        assertThat(sc.dump().length()).isGreaterThan(0);
        assertThat(sc.toString().length()).isGreaterThan(0);
    }
    //    @Test
//    public void testSvomMessage() throws Exception {
//        final SvomMessage msg = new SvomMessage();
//        msg.messageDate("2019-01-01T10:00:23");
//        msg.messageId("anId");
//        msg.messageClass("SvomType");
//        assertThat(msg.toString().length()).isGreaterThan(0);
//    }    
}
