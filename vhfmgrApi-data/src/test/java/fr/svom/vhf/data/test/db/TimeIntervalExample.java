package fr.svom.vhf.data.test.db;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class TimeIntervalExample {


    /**
     * Parse the time string.
     *
     * @param atimestr
     *            the String
     * @param hoffset
     *            the int
     * @return Long
     */
    protected Long timeStringParser(String atimestr, int hoffset) {
        System.out.println("Trying to extract time information from "+ atimestr);

        // Init since to now - hoffset hours.
        Long since = Instant.now().minus(hoffset, ChronoUnit.HOURS).toEpochMilli();
        if (atimestr == null) {
            return since;
        }
        try {
            // If the time is a long and is greater than 0.
            final Long t = new Long(atimestr);
            if (t >= 0) {
                since = t;
            }
        }
        catch (final NumberFormatException e) {
            // The time string should have the format "now-n<x>" : x can be "m" or "h" for
            // minutes and hours.
            if (atimestr.startsWith("now")) {
                // Interpret string.
                since = Instant.now().toEpochMilli();
                final String[] fargs = atimestr.split("-");
                if (fargs.length > 1) {
                    final String duration = "PT" + fargs[1];
                    final java.time.Duration d = java.time.Duration.parse(duration);
                    since = since - d.get(java.time.temporal.ChronoUnit.SECONDS) * 1000L;
                }
            }
        }
        catch (final java.time.format.DateTimeParseException e) {
            System.err.println("Error in parsing time string : " + e);
        }
        return since;
    }

    public static void main(String args[]) {
        int a = (int)(1.99);
        System.out.println("cast to int "+ a);
    }
}
