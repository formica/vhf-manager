package fr.svom.vhf.data.test.generators;

import fr.svom.vhf.data.packets.PacketApid;

/**
 * Utility enumeration for APIDs.
 *
 * @author formica
 *
 */
public enum APIDS {
    /**
     * ECLRECURR2.
     */
    ECLRECURR2("ECLRECURR2", 1, 23, "ECL", "TmVhfEclairsRecurrent2", "InstrumentMode,Attitude,Position,", 587),
    /**
     * ECLALERTL1.
     */
    ECLALERTL1("ECLALERTL1", 1, 1, "ECL", "TmVhfEclairsAlert", "Attitude,Position",576),
    /**
     * ECLLCURHP1.
     */
    ECLLCURHP1("ECLLCURHP1", 22, 10, "ECL", "TmVhfEclairsHighPriorityLightCurve", "Attitude,Position", 577),
    /**
     * ECLLCURLP1.
     */
    ECLLCURLP1("ECLLCURLP1", 42, 18, "ECL", "TmVhfEclairsLowPriorityLightCurve", "Attitude,Position", 578);
//    MXTRECURR1("MXTRECURR1", 1, 25, "none", "none", 612),
//    VTRECURR1("VTRECURR1", 1, 0, "none", "none", 519),
//    VTRECURR2("VTRECURR2", 1, 0, "none", "none", 520),
//    MXTPHOTDATA("MXTPHOTDATA", 1, 28, "none", "none", 610),
//    IDLEFRAME("IDLEFRAME", 1, 0, "none", "none", 0),
//    ECLRECURR1("ECLRECURR1", 1, 23, "none", "none", 586),
//    GRMRECURR1("GRMRECURR1", 1, 0, "none", "none", 550),
//    ECLALERTL1("ECLALERTL1", 1, 1, "none", "TmVhfAlert",576),
//    ECLLCURHP1("ECLLCURHP1", 22, 10, "none", "none", 577),
//    GRMLCURHIP("GRMLCURHIP", 1, 0, "none", "none", 546),
//    MXTPOSITIO("MXTPOSITIO", 1, 2, "none", "none", 608),
//    MXTPHOTONL("MXTPHOTONL",10, 8, "none", "none", 609),
//    ECLALDESC1("ECLALDESC1", 1,13, "none", "none", 579),
//    ECLALDESC2("ECLALDESC2",1, 13,"none","none", 580),
//    ECLALDESC3("ECLALDESC3",1, 13,"none","none", 581),
//    ECLALDESC4("ECLALDESC4",1,13,"none","none",582),
//    ECLALDESC5("ECLALDESC5", 1, 13, "none","none",583),
//    ECLLCURLP1("ECLLCURLP1", 42, 18, "none","none",578),
//    VTATTCHART("VTATTCHART",1, 0, "none","none", 513),
//    GRMLCURLOP("GRMLCURLOP",1,0,"none","none", 548),
//    ECLSUBIMAG("ECLSUBIMAG",49,22,"none","none",584),
//    VTFCHARTR("VTFCHARTR",1,0,"none","none",514),
//    VTFCHARTV("VTFCHARTV",1,0,"none","none",515),
//    VT1SUBIMAR1("VT1SUBIMAR1",1,0,"none","none",517),
//    VT1SUBIMAR2("VT1SUBIMAR2",1,0,"none","none",521),
//    VTFCHARTVR("VTFCHARTVR",1,0,"none","none",516),
//    VT2SUBIMAGR1("VT2SUBIMAGR1",1, 0,"none","none",518);

    /**
     * The apid.
     */
    private final int apid;
    /**
     * Packet name.
     */
    private final String name;
    /**
     * The description.
     */
    private final String desc;
    /**
     * Instrument mode.
     */
    private final String instrument;
    /**
     * Number of expected packets.
     */
    private final Integer expected;
    /**
     * The priority.
     */
    private final Integer priority;
    /**
     * The class name.
     */
    private final String classname;
    /**
     * The class name.
     */
    private final String details;

    /**
     * Default Ctor.
     * @param name the String
     * @param expected the Integer
     * @param priority the Integer
     * @param instrument the String
     * @param classname the String
     * @param details the String
     * @param apid the Integer
     */
    APIDS(String name, Integer expected, Integer priority, String instrument, String classname, String details,
            int apid) {
        this.apid = apid;
        this.name = name;
        this.desc = "none";
        this.instrument = instrument;
        this.expected = expected;
        this.priority = priority;
        this.details = details;
        this.classname = classname;
    }

    /**
     * Get the apid value.
     *
     * @return int
     */
    public int apid() {
        return this.apid;
    }

    /**
     * Get the PacketApid generated from this enum.
     * @return PacketApid
     */
    public PacketApid getPacketApid() {
        PacketApid papid = new PacketApid();
        papid.setApid(this.apid);
        papid.setName(this.name);
        papid.setCategory("VHF");
        papid.setExpectedPackets(this.expected);
        papid.setPriority(this.priority);
        papid.setClassName(this.classname);
        papid.setDescription(this.desc);
        papid.setInstrument(this.instrument);
        papid.setDetails(this.details);
        return papid;
    }
}
