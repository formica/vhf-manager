/**
 * 
 */
package fr.svom.vhf.data.test.generators;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.swagger.model.PacketApidDto;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * @author formica
 *
 */
public class ApidGenerator {

	private List<PacketApid> apidList = new ArrayList<>();
	
	public ApidGenerator() {
		init();
	}
	

	protected void init() {
		APIDS[] lapids = APIDS.values();
		for (APIDS apids : lapids) {
			PacketApid pa = apids.getPacketApid();
			apidList.add(pa);
		}
	}

	public List<PacketApid> getApidList() {
		return apidList;
	}

	public PacketApid getAlertApid() {
		return APIDS.ECLALERTL1.getPacketApid();
	}

	public static void main(String[] args) {
		ApidGenerator agen = new ApidGenerator();
		MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();

		mapperFactory.classMap(PacketApid.class, PacketApidDto.class).exclude("tid").exclude("pid").exclude("pcat")
				.byDefault().register();

		File output = new File("/tmp/apids.json");
		MapperFacade mapper = mapperFactory.getMapperFacade();
		ObjectMapper jm = new com.fasterxml.jackson.databind.ObjectMapper();
		try (PrintWriter fout = new PrintWriter(
				new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(output, true))));) {

			List<PacketApid> apidlist = agen.getApidList();
			for (PacketApid packetApid : apidlist) {
				System.out.println(packetApid);
				PacketApidDto dto = mapper.map(packetApid, PacketApidDto.class);
				try {
					String json = jm.writeValueAsString(dto);
//					System.out.println("JSON: " + json);
					fout.println(json);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}
}
