package fr.svom.vhf.data.test.generators;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class Conversions {

	public Conversions() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Float f = new Float("2.50f");
		   
	      /* returns the floating-point value with the same bit pattern */
	      System.out.println(f.intBitsToFloat(123));
	      System.out.println(f.intBitsToFloat(0x7f800000));  
	      System.out.println(f.intBitsToFloat(0xff800000)); 	
	      
	      long bits = Double.doubleToLongBits(f);
	      System.out.println(Long.toHexString(bits));
	      
	      System.out.println("=============");
	      Integer mi = new Integer(1311301);
	      System.out.println(Integer.toHexString(mi));

	      Float data = Float.intBitsToFloat(mi);
	      System.out.println(data);

	      int ibits = Float.floatToIntBits(data);
	      System.out.println(Integer.toHexString(ibits));

	      Integer mi2 = new Integer(1157764096);
	      System.out.println(Integer.toHexString(mi2));
	      Float data2 = Float.intBitsToFloat(mi2);
	      System.out.println(data2);

	      System.out.println("=============");
	      ByteBuffer byteBuffer = ByteBuffer.allocate(4);
	      byteBuffer.order(ByteOrder.BIG_ENDIAN);
	      byteBuffer.putInt(88);
	      byte[] result = byteBuffer.array();
	      System.out.println(Arrays.toString(result));
	      
	      ByteBuffer byteBuffer1 = ByteBuffer.allocate(4);
	      byteBuffer1.order(ByteOrder.LITTLE_ENDIAN);
	      byteBuffer1.putInt(88);
	      byte[] result1 = byteBuffer1.array();
	      System.out.println(Arrays.toString(result1));
	      System.out.println("=============");

	      Integer ih = 0x140245;
	      byte[] mb = ByteBuffer.allocate(4).putInt(ih).array();
	      BigInteger word = new BigInteger(mb);
	      System.out.println("ih="+ih+" and word="+word);
	      ByteBuffer fw = ByteBuffer.allocate(4).wrap(mb);
	      Float fd = fw.order(ByteOrder.LITTLE_ENDIAN).getFloat();
	      System.out.println("fd="+fd);
	      
	      BigInteger shBits = new BigInteger(1,new byte[] {0x25,0x00});
	      byte[] mbsb = shBits.toByteArray();
	      for (byte b : mbsb) {
	    	   System.out.format("0x%x ", b);
	    	  }
	      System.out.println();
	      ByteBuffer fwsb = ByteBuffer.wrap(mbsb);
	      byte[] bufby = fwsb.array();
	      for (byte b : bufby) {
	    	   System.out.format("0x%x ", b);
	    	  }
	      Object sdata = fwsb.order(ByteOrder.LITTLE_ENDIAN).getShort();
	      System.out.println("sdata="+sdata);

	      BigInteger intBits = new BigInteger(1,new byte[] {(byte) 0x11, (byte) 0xBB, 0x00});
	      byte[] mbib = intBits.toByteArray();
	      ByteBuffer fwib = ByteBuffer.allocate((mbib.length%2 == 0) ? mbib.length : mbib.length+1);
	      fwib.order(ByteOrder.LITTLE_ENDIAN);
  	  	  System.out.println("fill buffer with "+intBits);
	      for (byte b: mbib) {
	    	  	System.out.format("0x%x ", b);
	    	  	fwib.put(b);
	      }
	      if (fwib.position() < 4) {
	    	  	System.out.println("Add another byte");
	    	  	byte b = 0x00;
	    	  	fwib.put(b);
	      }
	      System.out.println("buffer position is "+fwib.position());
	      Object idata = fwib.order(ByteOrder.LITTLE_ENDIAN).getInt(0);
	      System.out.println("idata="+idata);

	      System.out.println("==========================");
	      byte[] bytes = ByteBuffer.allocate(4).putInt(mi).array();
	      Float nd = ByteBuffer.wrap(bytes).getFloat(); 
	      System.out.println(nd);

	      byte[] xbytes = ByteBuffer.allocate(4).putFloat((float)2081.25).array();
	      for (byte b : xbytes) {
	    	   System.out.format("0x%x ", b);
	    	  }
	      System.out.println();
	      ByteBuffer wrapped = ByteBuffer.allocate(4).wrap(xbytes);
	      Integer idoppler = wrapped.order(ByteOrder.BIG_ENDIAN).getInt();
	      System.out.println(idoppler);
	      System.out.println(Integer.toHexString(idoppler));

	      ByteBuffer fwrapped = ByteBuffer.allocate(4).wrap(xbytes);
	      Float fdoppler = fwrapped.order(ByteOrder.BIG_ENDIAN).getFloat();
	      System.out.println(fdoppler);
	      System.out.println(Float.toHexString(fdoppler));

	      Integer dopdrift = 0xabaaa6c1;
	      byte[] ddbytes = ByteBuffer.allocate(4).putInt(dopdrift).array();
	      ByteBuffer ddwrapped = ByteBuffer.allocate(4).wrap(ddbytes);
	      Float dd = ddwrapped.order(ByteOrder.LITTLE_ENDIAN).getFloat();
	      System.out.println(dd);
	      System.out.println(Float.toHexString(dd));
	      

	}
	
}
