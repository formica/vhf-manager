/**
 * 
 */
package fr.svom.vhf.data.test.generators;

import fr.svom.vhf.data.mappers.NotifStatus;
import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;

/**
 * @author aformic
 *
 */
public final class DataGenerator {

    // / Size of a CCSDS frame [Bytes]
    private static final int FRAME_HEADER_SIZE = 6;
    private static final int CCSDS_FRAME_HEADER_SIZE = 6;
    private static final int CCSDS_FRAME_DATA_SIZE = 88;
    private static final int CCSDS_FRAME_SIZE = CCSDS_FRAME_HEADER_SIZE + CCSDS_FRAME_DATA_SIZE;
    private static final int FRAME_SIZE = FRAME_HEADER_SIZE + CCSDS_FRAME_SIZE;

    private static final int STATION_HEADER_SIZE = 28;

    private static Logger log = LoggerFactory.getLogger(DataGenerator.class);

    protected static Random apidRandom = new Random();

    public static byte[] generatePacket() {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream(
                FRAME_SIZE + STATION_HEADER_SIZE);
        final DataOutputStream dos = new DataOutputStream(baos);
        byte[] frame = null;
        try {
            // The station header (28 bits)
            final byte[] datastation = new byte[STATION_HEADER_SIZE];
            apidRandom.nextBytes(datastation);
            dos.write(datastation);

            final byte[] frameHeader = new byte[FRAME_HEADER_SIZE];
            apidRandom.nextBytes(frameHeader);
            dos.write(frameHeader);

            final byte[] ccsdsframeHeader = new byte[CCSDS_FRAME_HEADER_SIZE];
            apidRandom.nextBytes(ccsdsframeHeader);
            dos.write(ccsdsframeHeader);

            System.out.println(
                    ">>>>> Frame header value: 0x" + new BigInteger(frameHeader).toString(16));

            // The main frame (88 bits)
            final byte[] data = new byte[CCSDS_FRAME_DATA_SIZE];
            apidRandom.nextBytes(data);
            dos.write(data);

        }
        catch (final IOException e) {
            e.printStackTrace();
        }
        frame = baos.toByteArray();
        log.debug(">>>>> Frame size: " + frame.length);
        log.debug(">>>>> Frame value: 0x" + new BigInteger(frame).toString(16));
        return frame;
    }

    public static String generateAlertFramePacket() {
        final String pktnewhexa = "070000005DB055BA000000000000000000000000000000000001000018761E0018000240C00100575DB055BA000000960005480EFE434C414C4552544C0000010000014C4552544C310045434C414C4552544C000000004C414C4552544C310045434C414C4552544C310045434C414C4552544C31000000000000000000CDD0";
//      final String pkthexa = "070000005BF41527000000000000000000000000000000000001000018761E0018000240C00100570000003D000000010000000145434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C31000000000000000000CDD0";
        return pktnewhexa;
    }

    public static VhfBurstId generateBurst(String id, long pt, long obsid) {

        final VhfBurstId bid = new VhfBurstId();
        bid.setBurstId(id);
        bid.setPacketTime(pt);
        bid.setBurstEndtime(pt+3600L);
        bid.setBurstStartime(pt-60L);
        bid.setEclairObsid(obsid);
        bid.setAlertTimeTb(pt-5L);
        bid.setAlertTimeTb0AbsSeconds(pt/1000);
        return bid;
    }

    public static PrimaryHeaderPacket generatePrimaryHeader() {

        final PrimaryHeaderPacket phpkt = new PrimaryHeaderPacket();
        phpkt.setPacketObsid(new Long(123));
        phpkt.setPacketTime(new Long(427870798));
        phpkt.setPacketObsidType(0);
        phpkt.setPacketObsidNum(123);
        phpkt.setInstrumentMode(255);

        return phpkt;
    }

    public static VhfGroundStation generateVhfGroundStation(Integer id, String name,
            String macadd) {

        final VhfGroundStation entity = new VhfGroundStation();
        entity.setStationId(id);
        entity.setId(name);
        entity.setCountry("France");
        entity.setLocation("PARIS");
        entity.setAltitude(10.);
        entity.setLongitude(20.);
        entity.setLatitude(30.);
        entity.setName(name);
        entity.setMacaddress(macadd);
        return entity;
    }

    public static VhfRawPacket generateVhfRawPacket(String format, String flag, String packet) {

        final VhfRawPacket entity = new VhfRawPacket();
        entity.setPacket(packet);
        entity.setPktflag(flag);
        entity.setPktformat(format);
        entity.setSize(packet.length());
        return entity;
    }

    public static VhfBinaryPacket generateVhfBinaryPacket(String hash, String format, String packet) {

        final VhfBinaryPacket entity = new VhfBinaryPacket();
        entity.setPacket(packet);
        entity.setBinaryPacket(packet.getBytes());
        entity.setHashId(hash);
        entity.setPktformat(format);
        entity.setSize(packet.length());
        return entity;
    }
    
    public static VhfDecodedPacket generateVhfDecodedPacket(String hash, String format, String packet) {

        final VhfDecodedPacket entity = new VhfDecodedPacket();
        entity.setPacket(packet);
        entity.setHashId(hash);
        entity.setPktformat(format);
        entity.setSize((long)packet.length());
        return entity;
    }

    public static PacketApid generatePacketApid(Integer apid, String name, String category, String classname,
                                                String instrument) {

        final PacketApid entity = new PacketApid();
        entity.setDescription("some packet apid");
        entity.setName(name);
        entity.setApid(apid);
        entity.setCategory(category);
        entity.setClassName(classname);
        entity.setExpectedPackets(1);
        entity.setInstrument(instrument);
        entity.setDetails("Attitude");
        entity.setPcat(1);
        entity.setPid(2);
        entity.setTid(3);
        return entity;
    }

    public static VhfStationStatus generateVhfStationStatus() {

        final VhfStationStatus vhfstatus = new VhfStationStatus();
        vhfstatus.setIdStation(0x01);
        vhfstatus.setChannel(1);
        vhfstatus.setPadding1(0);
        vhfstatus.setPadding2(0);
        vhfstatus.setPadding3(0);
        vhfstatus.setStationTime(1500000000L);
        vhfstatus.setCorrPower(1000);
        vhfstatus.setDoppler((float) 20.3);
        vhfstatus.setDopDrift((float) 2.3);
        vhfstatus.setSrerror((float) 0.3);
        vhfstatus.setReedSolomonCorr(3);
        vhfstatus.setCkSum(1);
        return vhfstatus;
    }

    public static FrameHeaderPacket generateFrameHeader() {
        final FrameHeaderPacket entity = new FrameHeaderPacket();
        entity.setDfStatus(1);
        entity.setMcfCount(10);
        entity.setOcFlag(0);
        entity.setSpaceCraftId(100);
        entity.setTframeVersion(9);
        entity.setVcfCount(99);
        entity.setVcId(1);
        return entity;
    }
    
    public static VhfStationStatus generateStationStatus() {
        final VhfStationStatus vhfstatus = new VhfStationStatus();
        vhfstatus.setIdStation(0x01);
        vhfstatus.setChannel(1);
        vhfstatus.setPadding1(0);
        vhfstatus.setPadding2(0);
        vhfstatus.setPadding3(0);
        vhfstatus.setStationTime(1500000000L);
        vhfstatus.setCorrPower(1000);
        vhfstatus.setDoppler((float) 20.3);
        vhfstatus.setDopDrift((float) 2.3);
        vhfstatus.setSrerror((float) 0.3);
        vhfstatus.setReedSolomonCorr(3);
        vhfstatus.setCkSum(1);
        return vhfstatus;
    }
    
    public static CcsdsPacket generateCcsdsPacket() {
        final CcsdsPacket entity = new CcsdsPacket();
        entity.setCcsdsApid(576);
        entity.setCcsdsCounter(1);
        entity.setCcsdsCrc(9);
        entity.setCcsdsGFlag(0);
        entity.setCcsdsPlength(100);
        entity.setCcsdsSecHeadFlag(1);
        entity.setCcsdsType(2);
        entity.setCcsdsVersionNum(10);
        return entity;
    }
    
    public static VhfSatAttitude generateAttitude() {
        final VhfSatAttitude vhfatt = new VhfSatAttitude();
        vhfatt.setAttId(1L);
        vhfatt.setApidName("someapid");
        vhfatt.setPacketTime(1000L);
        vhfatt.setSatAttitudeq0(1.0);
        vhfatt.setSatAttitudeq1(2.0);
        vhfatt.setSatAttitudeq2(3.0);
        vhfatt.setSatAttitudeq3(4.0);
        return vhfatt;
    }
    
    public static VhfSatPosition generatePosition() {
        final VhfSatPosition vhfpos = new VhfSatPosition();
        vhfpos.setPosId(1L);
        vhfpos.setApidName("someapid");
        vhfpos.setPacketTime(1000L);
        vhfpos.setSatPositionX(1.0);
        vhfpos.setSatPositionY(1.0);
        vhfpos.setSatPositionZ(1.0);
        vhfpos.setSatPositionLat(1.0);
        vhfpos.setSatPositionLon(1.0);
        return vhfpos;
    }

    public static VhfTransferFrame generateFrame(final String qual) {
        final Timestamp receptionTime = new Timestamp(new Date().getTime());
        final Boolean isFrameValid = true;
        final VhfTransferFrame locvhfframe = new VhfTransferFrame();
        locvhfframe.setInsertionTime(receptionTime);
        locvhfframe.setIsFrameValid(isFrameValid);

        final VhfStationStatus vhfstatus = new VhfStationStatus();
        vhfstatus.setIdStation(0x01);
        vhfstatus.setChannel(1);
        vhfstatus.setPadding1(0);
        vhfstatus.setPadding2(0);
        vhfstatus.setPadding3(0);
        vhfstatus.setStationTime(1500000000L);
        vhfstatus.setCorrPower(1000);
        vhfstatus.setDoppler((float) 20.3);
        vhfstatus.setDopDrift((float) 2.3);
        vhfstatus.setSrerror((float) 0.3);
        vhfstatus.setReedSolomonCorr(3);
        vhfstatus.setCkSum(1);

        final FrameHeaderPacket fhpkt = new FrameHeaderPacket();
        fhpkt.setSpaceCraftId(10);
        fhpkt.setVcId(8);
        fhpkt.setMcfCount(90);
        fhpkt.setVcfCount(80);
        fhpkt.setDfStatus(1);

        final PrimaryHeaderPacket phpkt = new PrimaryHeaderPacket();
        phpkt.setPacketObsid(new Long(123));
        phpkt.setPacketTime(new Long(427870798));

        final CcsdsPacket cspkt = new CcsdsPacket();
        cspkt.setCcsdsApid(1455);
        cspkt.setCcsdsCounter(400);
        cspkt.setCcsdsGFlag(1);
        cspkt.setCcsdsPlength(86);
        cspkt.setCcsdsCrc(2);

        // UtsAlertPacket uapkt = new UtsAlertPacket();
        // uapkt.setRepetitionCounter(new Short((short) 2));
        // uapkt.setTriggerId(3);
        // uapkt.setFitQuality(new BigInteger(qual));

        // Quaternion quat = new Quaternion();
        // quat.setQ0((float) 1.0);
        // quat.setQ1((float) 1.1);
        // quat.setQ2((float) 1.2);
        // quat.setQ3((float) 1.3);
        //
        // uapkt.setAttitude(quat);

        final PacketApid apid = new PacketApid();
        apid.setApid(cspkt.getCcsdsApid());
        apid.setName("UtsAlertPacket");
        // apid.setClassName(uapkt.getClass().getName());
        apid.setDescription("Contains the description of the Alert send by UTS");

        locvhfframe.setFrameHeader(fhpkt);
        locvhfframe.setCcsds(cspkt);
        locvhfframe.setHeader(phpkt);
        locvhfframe.setStationStatus(vhfstatus);
        // locvhfframe.setDetectorPacket(uapkt);
        locvhfframe.setApid(apid);
        return locvhfframe;
    }

    public static VhfSatAttitude generateVhfSatAttitude() {
        VhfSatAttitude entity = new VhfSatAttitude();
        entity.setApidName("TmVhfEclairsAlert");
        entity.setAttId(1L);
        entity.setPacketTime(1000L);
        entity.setSatAttitudeq0(0.0);
        entity.setSatAttitudeq1(1.0);
        entity.setSatAttitudeq2(2.0);
        entity.setSatAttitudeq3(3.0);
        return entity;
    }

    public static VhfSatPosition generateVhfSatPosition() {
        VhfSatPosition entity = new VhfSatPosition();
        entity.setApidName("TmVhfEclairsAlert");
        entity.setPosId(1L);
        entity.setPacketTime(1000L);
        entity.setSatPositionLat(1.0);
        entity.setSatPositionLon(3.0);
        entity.setSatPositionX(5.0);
        entity.setSatPositionY(7.0);
        entity.setSatPositionZ(8.0);
        return entity;
    }

    public static VhfNotification generateVhfNotification() {
        VhfNotification notif = new VhfNotification();
        notif.setNotifFormat(NotifStatus.SENT.name());
        notif.setNotifMessage("test notification");
        notif.setPktflag("ERROR");
        notif.setSize(notif.getNotifMessage().length());
        return notif;
    }
}
