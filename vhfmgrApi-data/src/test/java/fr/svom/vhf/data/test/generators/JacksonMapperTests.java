package fr.svom.vhf.data.test.generators;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.csv.CsvSchema.Builder;

import fr.svom.messages.model.DataNotification;
import fr.svom.messages.model.DataNotificationContent;
import fr.svom.messages.model.DataProvider;
import fr.svom.messages.model.DataStream;

public class JacksonMapperTests {

	public static final void main(String[] args) {
		
		ObjectMapper jacksonMapper;
		
		final Map<String, DataNotification> amapnot= new HashMap<String, DataNotification>();
		try {
			jacksonMapper = new ObjectMapper();
			final DataNotification dn = new DataNotification();
			final DataNotificationContent dnc = new DataNotificationContent();
			final Instant now = Instant.now();
			dnc.setDate(now.atOffset(ZoneOffset.UTC));
			dnc.setDataStream(DataStream.VHF);
			dnc.setDataProvider(DataProvider.VHFDB);
			dnc.setResourceLocator("anurl");
			dnc.setMessage("hello");
			dn.content(dnc);
			amapnot.put("DataNotification", dn);
			final String retval = jacksonMapper.writeValueAsString(amapnot);
			System.out.println("dump message "+retval);
			
            final String jsonstring = jacksonMapper.writeValueAsString(dn);
            final JsonNode node = jacksonMapper.readTree(jsonstring);
            final Builder csvSchemaBuilder = CsvSchema.builder();
            node.fieldNames().forEachRemaining(
                    csvSchemaBuilder::addColumn
                );
            csvSchemaBuilder.setColumnSeparator(';');
            final CsvSchema csvSchema = csvSchemaBuilder.build();
            final CsvMapper csvMapper = new CsvMapper();
            final String csvstring = csvMapper.writerFor(JsonNode.class).with(csvSchema).writeValueAsString(node);
            System.out.println("dump CSV message "+csvstring);

		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
