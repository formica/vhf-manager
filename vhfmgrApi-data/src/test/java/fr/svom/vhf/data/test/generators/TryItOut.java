package fr.svom.vhf.data.test.generators;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.swagger.model.PrimaryHeaderPacketDto;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public class TryItOut {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        final Integer testint = new Integer("332693091");
        System.out.println("testint = " + testint);
        final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(PrimaryHeaderPacket.class, PrimaryHeaderPacketDto.class)
                .exclude("frame").byDefault().register();
        final MapperFacade mapper = mapperFactory.getMapperFacade();

        final PrimaryHeaderPacket entity = DataGenerator.generatePrimaryHeader();
        final PrimaryHeaderPacketDto dto = mapper.map(entity, PrimaryHeaderPacketDto.class);
        assertThat(entity.getPacketObsid()).isEqualTo(dto.getPacketObsid());

        // Path path = Paths.get("/tmp/data/tm_vhf_alert.bin");
        // System.out.println(" Create path "+path.toString());
        // try {
        // byte[] binarypacket = Files.readAllBytes(path);
        // System.out.println(" Read array of bytes "+binarypacket.length+" content
        // "+binarypacket);
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        final String pktcnes = "0996010018000240C001005759194F2B71E09D4247820BB1E6210BBC2D82459F2D509388F3F0B268554515A552ED9CC72C6FE41711D74BC5079F5E87AE48C33674CAFA6E4BC110E5C5640A8562AC97051FBEB6541D2854DBCA17DC2C97F464A10B75DCAD";
        final BigInteger mpkt = new BigInteger(pktcnes, 16);
        final byte[] binarypacket = mpkt.toByteArray();
        System.out.println("Dump packet from cnes in binary " + binarypacket + " with length "
                + binarypacket.length);
        byte[] iceframe = null;
        iceframe = Arrays.copyOf(binarypacket, 115);

        final String pktcnes2 = "0244C007005759194F3555A98A5A26BC30F2F0F15B7276CD5DA39EBFA0E21626CF7D2511DFD9E66F83D9C55A6B95AA778F58110A90853DC41201A0A28DE53AD330B28ADC0ADD5D73107738BE60DE61423ED16D5648304DEF219C3A36D988";
        // String pktcnes2 =
        // "0240C001005759194F2B71E09D4247820BB1E6210BBC2D82459F2D509388F3F0B268554515A552ED9CC72C6FE41711D74BC5079F5E87AE48C33674CAFA6E4BC110E5C5640A8562AC97051FBEB6541D2854DBCA17DC2C97F464A10B75DCAD";
        final BigInteger mpkt2 = new BigInteger(pktcnes2, 16);
        final byte[] binarypacket2 = mpkt2.toByteArray();
        System.out.println("Dump packet2 from cnes in binary " + binarypacket2 + " with length "
                + binarypacket2.length);
        byte[] iceframe2 = null;
        iceframe2 = Arrays.copyOf(binarypacket2, 115);

        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream("/tmp/data/cnes.bin");
            stream.write(binarypacket2);
        }
        catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            try {
                stream.close();
            }
            catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        Pattern pattern = Pattern.compile("H:(\\d+)\\sE:(\\d+)\\sP:(\\d+)");
        Matcher matcher = pattern.matcher("H:7 E:7 P:10");
        if (!matcher.matches()) {
            System.out.println("Does not match....");
        }
        else {
            final String hValue = matcher.group(1);
            final String eValue = matcher.group(2);
            final String pValue = matcher.group(3);
            System.out.println("found...." + hValue + " " + eValue + " " + pValue);
        }

        pattern = Pattern.compile("SVOM_([\\p{Lower}\\p{Upper}]+)_(\\d+)_([\\p{Digit}\\p{Alpha}]+).dat");
        matcher = pattern.matcher("SVOM_TEST_123_20200130T150001.dat");
        if (!matcher.matches()) {
            System.out.println("Does not match....");
        }
        else {
            final String hValue = matcher.group(1);
            final String eValue = matcher.group(2);
            final String pValue = matcher.group(3);
            System.out.println("found...." + hValue + " " + eValue + " " + pValue);
        }

    }

}
