package fr.svom.vhf.server.swagger.api;

import fr.svom.vhf.swagger.model.*;
import fr.svom.vhf.server.swagger.api.ApidsApiService;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import fr.svom.vhf.swagger.model.ApiHttpError;
import fr.svom.vhf.swagger.model.PacketApidDto;
import fr.svom.vhf.swagger.model.PacketApidSetDto;

import java.util.Map;
import java.util.List;
import fr.svom.vhf.server.swagger.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/apids")


@io.swagger.annotations.Api(description = "the apids API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class ApidsApi  {
   @Autowired
   private ApidsApiService delegate;

    @POST
    
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Saves PacketApid", notes = "Create a new packet APID description entry in the DB.", response = PacketApidDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = PacketApidDto.class),
        @io.swagger.annotations.ApiResponse(code = 500, message = "internal server error", response = ApiHttpError.class)
    })
    public Response createPacketApid(@ApiParam(value = "A json string that is used to construct a packetapiddto object: { name: xxx, ... }", required = true) @NotNull @Valid  PacketApidDto body,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.createPacketApid(body, securityContext, info);
    }
    @GET
    @Path("/{id}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds PacketApid by id", notes = "Search for a resource by ID", response = PacketApidDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketApidDto.class)
    })
    public Response findPacketApids(@ApiParam(value = "packet apid", required = true) @PathParam("id") @NotNull  Integer id,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.findPacketApids(id, securityContext, info);
    }
    @GET
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a packet APID list.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = PacketApidSetDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketApidSetDto.class)
    })
    public Response listPacketApids(@ApiParam(value = "by: the search pattern {none}; searchable keys [id, description, name]", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {apid:DESC}", defaultValue = "name:ASC") @DefaultValue("name:ASC") @QueryParam("sort")  String sort,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listPacketApids(by, page, size, sort, securityContext, info);
    }
    @PUT
    @Path("/{id}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Update PacketApid by id", notes = "Search for a resource by ID and update fields", response = PacketApidDto.class, tags={ "apids", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketApidDto.class)
    })
    public Response updatePacketApids(@ApiParam(value = "packet apid", required = true) @PathParam("id") @NotNull  Integer id,@ApiParam(value = "A json string that is used to construct a packetapiddto object: { name: xxx, ... }", required = true) @NotNull @Valid  PacketApidDto body,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.updatePacketApids(id, body, securityContext, info);
    }
}
