package fr.svom.vhf.server.swagger.api;

import fr.svom.vhf.server.swagger.api.*;
import fr.svom.vhf.swagger.model.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import fr.svom.vhf.swagger.model.ApiHttpError;
import fr.svom.vhf.swagger.model.PacketApidDto;
import fr.svom.vhf.swagger.model.PacketApidSetDto;

import java.util.List;
import fr.svom.vhf.server.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class ApidsApiService {
    public abstract Response createPacketApid(PacketApidDto body,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response findPacketApids(Integer id,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listPacketApids(String by,Integer page,Integer size,String sort,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response updatePacketApids(Integer id,PacketApidDto body,SecurityContext securityContext, UriInfo info) throws NotFoundException;
}
