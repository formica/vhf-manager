package fr.svom.vhf.server.swagger.api;

import fr.svom.vhf.swagger.model.*;
import fr.svom.vhf.server.swagger.api.StationsApiService;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import fr.svom.vhf.swagger.model.StationSetDto;
import fr.svom.vhf.swagger.model.VhfGroundStationDto;

import java.util.Map;
import java.util.List;
import fr.svom.vhf.server.swagger.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/stations")


@io.swagger.annotations.Api(description = "the stations API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class StationsApi  {
   @Autowired
   private StationsApiService delegate;

    @POST
    
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Saves VhfGroundStation", notes = "Create new station", response = VhfGroundStationDto.class, tags={ "stations", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = VhfGroundStationDto.class)
    })
    public Response createVhfGroundStations(@ApiParam(value = "A json string that is used to construct a vhfgroundstationdto object: { name: xxx, ... }", required = true) @NotNull @Valid  VhfGroundStationDto body,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.createVhfGroundStations(body, securityContext, info);
    }
    @GET
    @Path("/{id}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds VhfGroundStationDto by id", notes = "Search for a specific resource", response = VhfGroundStationDto.class, tags={ "stations", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfGroundStationDto.class)
    })
    public Response findVhfGroundStations(@ApiParam(value = "station id", required = true) @PathParam("id") @NotNull  Integer id,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.findVhfGroundStations(id, securityContext, info);
    }
    @GET
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a VhfGroundStationDtos list.", notes = "This method allows to perform search and sorting.Arguments: by=<pattern>, page={ipage}, size={isize}, sort=<sortpattern>. The pattern <pattern> is in the form <param-name><operation><param-value>       <param-name> is the name of one of the fields in the dto       <operation> can be [< : >] ; for string use only [:]        <param-value> depends on the chosen parameter. A list of this criteria can be provided       using comma separated strings for <pattern>.      The pattern <sortpattern> is <field>:[DESC|ASC]", response = StationSetDto.class, tags={ "stations", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = StationSetDto.class)
    })
    public Response listVhfGroundStations(@ApiParam(value = "by: the search pattern {none}", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {stationId:ASC}", defaultValue = "stationId:ASC") @DefaultValue("stationId:ASC") @QueryParam("sort")  String sort,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfGroundStations(by, page, size, sort, securityContext, info);
    }
    @PUT
    @Path("/{id}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Update Station by id", notes = "Search for a resource by ID and update fields", response = VhfGroundStationDto.class, tags={ "stations", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfGroundStationDto.class)
    })
    public Response updateVhfGroundStation(@ApiParam(value = "station id", required = true) @PathParam("id") @NotNull  Integer id,@ApiParam(value = "A json string that is used to construct a VhfGroundStationDto object: { name: xxx, ... }", required = true) @NotNull @Valid  VhfGroundStationDto body,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.updateVhfGroundStation(id, body, securityContext, info);
    }
}
