package fr.svom.vhf.server.swagger.api;

import fr.svom.vhf.server.swagger.api.*;
import fr.svom.vhf.swagger.model.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import fr.svom.vhf.swagger.model.StationSetDto;
import fr.svom.vhf.swagger.model.VhfGroundStationDto;

import java.util.List;
import fr.svom.vhf.server.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class StationsApiService {
    public abstract Response createVhfGroundStations(VhfGroundStationDto body,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response findVhfGroundStations(Integer id,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfGroundStations(String by,Integer page,Integer size,String sort,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response updateVhfGroundStation(Integer id,VhfGroundStationDto body,SecurityContext securityContext, UriInfo info) throws NotFoundException;
}
