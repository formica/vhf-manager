package fr.svom.vhf.server.swagger.api;

import fr.svom.vhf.swagger.model.*;
import fr.svom.vhf.server.swagger.api.VhfApiService;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import fr.svom.vhf.swagger.model.ApiHttpError;
import java.io.File;
import fr.svom.vhf.swagger.model.GenericMap;
import fr.svom.vhf.swagger.model.HTTPResponse;
import java.util.Map;
import fr.svom.vhf.swagger.model.ObsidSetDto;
import fr.svom.vhf.swagger.model.PacketCounterSetDto;
import fr.svom.vhf.swagger.model.RawPacketSetDto;
import fr.svom.vhf.swagger.model.StationStatusSetDto;
import fr.svom.vhf.swagger.model.VhfBasePacket;
import fr.svom.vhf.swagger.model.VhfBurstIdDto;
import fr.svom.vhf.swagger.model.VhfBurstIdSetDto;
import fr.svom.vhf.swagger.model.VhfNotificationSetDto;
import fr.svom.vhf.swagger.model.VhfPacketSearchDto;
import fr.svom.vhf.swagger.model.VhfSatAttitudeSetDto;
import fr.svom.vhf.swagger.model.VhfSatPositionSetDto;
import fr.svom.vhf.swagger.model.VhfStationStatusCountDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameSetDto;

import java.util.Map;
import java.util.List;
import fr.svom.vhf.server.swagger.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.ws.rs.*;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/vhf")


@io.swagger.annotations.Api(description = "the vhf API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public class VhfApi  {
   @Autowired
   private VhfApiService delegate;

    @GET
    @Path("/packets/decode")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Decode a VHF packet.", notes = "This method utilize an hexa string to test decoding at server level.", response = HTTPResponse.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = HTTPResponse.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "Request does not have the correct format or cannot be processed", response = ApiHttpError.class)
    })
    public Response decodePacket(@ApiParam(value = "The format of the input data" , defaultValue="swig")@HeaderParam("format") String format,@ApiParam(value = "pkt: the packet in hexa {none}.", defaultValue = "none") @DefaultValue("none") @QueryParam("pkt")  String pkt,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.decodePacket(format, pkt, securityContext, info);
    }
    @POST
    @Path("/packets/decode")
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Decode a VHF packet list.", notes = "This method utilize a json body which is like the one in the search method.", response = HTTPResponse.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = HTTPResponse.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "Request does not have the correct format or cannot be processed", response = ApiHttpError.class)
    })
    public Response decodePacketList(@ApiParam(value = "A json string that is used to construct a RawPacketSetDto object", required = true) @NotNull @Valid  RawPacketSetDto rawPacketSetDto,@ApiParam(value = "The format of the input data" , defaultValue="raw")@HeaderParam("format") String format,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.decodePacketList(rawPacketSetDto, format, securityContext, info);
    }
    @DELETE
    @Path("/obsids/{id}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Delete vhf packets by obsid.", notes = "This method will delete all packets having the provided obsid.", response = HTTPResponse.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation, return httpresponse", response = HTTPResponse.class)
    })
    public Response deleteObsids(@ApiParam(value = "obsid id", required = true) @PathParam("id") @NotNull  String id,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.deleteObsids(id, securityContext, info);
    }
    @GET
    @Path("/{frame}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds vhf transfer frame by ID.", notes = "This method will search for transfer frame.", response = VhfTransferFrameDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfTransferFrameDto.class)
    })
    public Response findVhfTransferFrames(@ApiParam(value = "id of the frame", required = true) @PathParam("frame") @NotNull  Long frame,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.findVhfTransferFrames(frame, securityContext, info);
    }
    @POST
    @Path("/packets/duplicates")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Flag duplicates frames.", notes = "This method utilize an interval in input to search for duplicates.", response = HTTPResponse.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = HTTPResponse.class),
        @io.swagger.annotations.ApiResponse(code = 400, message = "Request does not have the correct format or cannot be processed", response = ApiHttpError.class)
    })
    public Response flagDuplicateFrames(@ApiParam(value = "A json string that is used to search duplicates frames: { tfrom: xxx, tto: yyyy }", required = true) @NotNull @Valid  Map<String, Long> requestBody,@ApiParam(value = "The format of the input data" , defaultValue="raw")@HeaderParam("format") String format,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.flagDuplicateFrames(requestBody, format, securityContext, info);
    }
    @GET
    @Path("/packets/{hash}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds vhf packet by HASH.", notes = "This method will search for a VHF packet, in binary format.", response = VhfBasePacket.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation, return a packet in the desired format (binary, decoded,...)", response = VhfBasePacket.class)
    })
    public Response getVhfPackets(@ApiParam(value = "hash of the packet", required = true) @PathParam("hash") @NotNull  String hash,@ApiParam(value = "header parameter containing the requested output format: binary, decoded, ..." , defaultValue="binary")@HeaderParam("X-VHF-PktFormat") String xVHFPktFormat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.getVhfPackets(hash, xVHFPktFormat, securityContext, info);
    }
    @GET
    @Path("/bursts")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a VhfBurstId lists.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = VhfBurstIdSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfBurstIdSetDto.class)
    })
    public Response listVhfBurstIds(@ApiParam(value = "by: the search pattern {none}.  List of accepted fields: packetTime, tb0, t0, burstid, obsid, apidname, insertionTime.", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {insertionTime:DESC}", defaultValue = "insertionTime:DESC") @DefaultValue("insertionTime:DESC") @QueryParam("sort")  String sort,@ApiParam(value = "The format of the input time fields" , defaultValue="yyyyMMdd'T'HHmmssz")@HeaderParam("dateformat") String dateformat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfBurstIds(by, page, size, sort, dateformat, securityContext, info);
    }
    @GET
    @Path("/count")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a list of summary data (ObsIdApidCountDto).", notes = "This method allows to perform filtering on apids and obsid, also a time period can be selected", response = PacketCounterSetDto.class, responseContainer = "List", tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = PacketCounterSetDto.class, responseContainer = "List")
    })
    public Response listVhfCounters(@ApiParam(value = "apid: the apid name {all means *}.", defaultValue = "all") @DefaultValue("all") @QueryParam("apidname")  String apidname,@ApiParam(value = "productname: the name of the product {all means *}.", defaultValue = "all") @DefaultValue("all") @QueryParam("productname")  String productname,@ApiParam(value = "obsid: the observation id number {0}; 0 means ignore it", defaultValue = "0") @DefaultValue("0") @QueryParam("obsid")  Long obsid,@ApiParam(value = "from: the beginning of the selected time interval {now-1h}", defaultValue = "{now-1h}") @DefaultValue("{now-1h}") @QueryParam("pktmintime")  String pktmintime,@ApiParam(value = "to: the end of the selected time interval {now}", defaultValue = "{now+1h}") @DefaultValue("{now+1h}") @QueryParam("pktmaxtime")  String pktmaxtime,@ApiParam(value = "timeformat: the format to digest previous arguments [iso], [number]. Time(iso) = yyyyMMddTHHmmssz, Time(number) = seconds", defaultValue = "iso") @DefaultValue("iso") @QueryParam("timeformat")  String timeformat,@ApiParam(value = "mode: the mode for counter [detailed | total], default to detailed", defaultValue = "detailed") @DefaultValue("detailed") @QueryParam("mode")  String mode,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfCounters(apidname, productname, obsid, pktmintime, pktmaxtime, timeformat, mode, securityContext, info);
    }
    @GET
    @Path("/notifications")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a VhfNotification lists.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = VhfNotificationSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfNotificationSetDto.class)
    })
    public Response listVhfNotifications(@ApiParam(value = "by: the search pattern {none}.  List of accepted fields: packetTime, tb0, t0, burstid, obsid, apidname, insertionTime.", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {insertionTime:DESC}", defaultValue = "insertionTime:DESC") @DefaultValue("insertionTime:DESC") @QueryParam("sort")  String sort,@ApiParam(value = "The format of the input time fields" , defaultValue="yyyyMMdd'T'HHmmssz")@HeaderParam("dateformat") String dateformat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfNotifications(by, page, size, sort, dateformat, securityContext, info);
    }
    @GET
    @Path("/rawpackets")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a RawPacketDtos lists.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = RawPacketSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = RawPacketSetDto.class)
    })
    public Response listVhfRawPackets(@ApiParam(value = "by: the search pattern {none}; searchable keys [flag, insertionTime].", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {insertionTime:DESC}", defaultValue = "insertionTime:DESC") @DefaultValue("insertionTime:DESC") @QueryParam("sort")  String sort,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfRawPackets(by, page, size, sort, securityContext, info);
    }
    @GET
    @Path("/stationstatus")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a VhfStationStatusDtos lists.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = StationStatusSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = StationStatusSetDto.class)
    })
    public Response listVhfStationStatus(@ApiParam(value = "by: the search pattern {none}.  List of accepted fields: stationTime, idStation, doppler].", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {idStation:ASC}", defaultValue = "idStation:ASC") @DefaultValue("idStation:ASC") @QueryParam("sort")  String sort,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfStationStatus(by, page, size, sort, securityContext, info);
    }
    @GET
    @Path("/stationstatus/count")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a VhfStationStatusCountDtos lists.", notes = "This method allows to perform search and sorting. Arguments: from,to.  The from and to parameters can also be provided as now and now-xh.", response = VhfStationStatusCountDto.class, responseContainer = "List", tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfStationStatusCountDto.class, responseContainer = "List")
    })
    public Response listVhfStationStatusCount(@ApiParam(value = "from: the start time range in which the packets should be selected expressed in yyyyMMdd'T'HHmmssz. If timeformat is set to number then they are seconds.") @QueryParam("from")  String from,@ApiParam(value = "to: the end time range in which the packets should be selected expressed in yyyyMMdd'T'HHmmssz. If timeformat is set to number then they are seconds.") @QueryParam("to")  String to,@ApiParam(value = "timeformat: the format to digest previous arguments [iso], [number]. Time(iso) = yyyyMMddTHHmmssz, Time(number) = seconds", defaultValue = "iso") @DefaultValue("iso") @QueryParam("timeformat")  String timeformat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfStationStatusCount(from, to, timeformat, securityContext, info);
    }
    @GET
    
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds a VhfTransferFrameDtos lists.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = VhfTransferFrameSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfTransferFrameSetDto.class)
    })
    public Response listVhfTransferFrames(@ApiParam(value = "by: the search pattern {none}; searchable keys [id, burstid, receptionTime, packetTime, isFrameValid, isFrameDuplicate, stationId, apid, notifStatus, obsid, obsidType, obsidNum, apidname,  hashid, category, classname, pktformat]", defaultValue = "none") @DefaultValue("none") @QueryParam("by")  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {frameId:ASC}", defaultValue = "frameId:ASC") @DefaultValue("frameId:ASC") @QueryParam("sort")  String sort,@ApiParam(value = "The format of the input time fields: e.g. yyyyMMdd'T'HHmmssz" , defaultValue="ms")@HeaderParam("dateformat") String dateformat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.listVhfTransferFrames(by, page, size, sort, dateformat, securityContext, info);
    }
    @PUT
    @Path("/{frame}/notification")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Update vhf transfer frame by ID.", notes = "This method update the notification status for a frame.", response = GenericMap.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = GenericMap.class)
    })
    public Response notifyVhfTransferFrames(@ApiParam(value = "id of the frame", required = true) @PathParam("frame") @NotNull  Long frame,@ApiParam(value = "A json string that is used to modify fields of a VhfTransferFrame object: { notifStatus: xxx }", required = true) @NotNull @Valid  Map<String, Long> requestBody,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.notifyVhfTransferFrames(frame, requestBody, securityContext, info);
    }
    @POST
    @Path("/upload")
    @Consumes({ "multipart/form-data" })
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Saves VhfTransferFrame from binary stream", notes = "Upload a binary packet", response = HTTPResponse.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 201, message = "successful operation", response = HTTPResponse.class)
    })
    public Response savePacketFromStream(@ApiParam(value = "The format of the input data" , defaultValue="binary")@HeaderParam("format") String format,@ApiParam(value = "The mode for this input data [prod, test]" , defaultValue="prod")@HeaderParam("mode") String mode,
 @FormDataParam("stream") FormDataBodyPart streamBodypart ,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.savePacketFromStream(format, mode, streamBodypart, securityContext, info);
    }
    @GET
    @Path("/obsids")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds vhf packet by query.", notes = "This method will search for list of obsids.", response = ObsidSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation, return a list of obsids", response = ObsidSetDto.class)
    })
    public Response searchObsids(@ApiParam(value = "from: the beginning of the selected time interval: if null {now-1h}") @QueryParam("minreceptiontime")  String minreceptiontime,@ApiParam(value = "to: the end of the selected time interval: if null, {now}") @QueryParam("maxreceptiontime")  String maxreceptiontime,@ApiParam(value = "timeformat: the format to digest previous arguments [iso], [number]. Time(iso) = yyyyMMddTHHmmssz, Time(number) = seconds", defaultValue = "iso") @DefaultValue("iso") @QueryParam("timeformat")  String timeformat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.searchObsids(minreceptiontime, maxreceptiontime, timeformat, securityContext, info);
    }
    @GET
    @Path("/packets")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds vhf packet by query.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC]. Th VHF packet format can be selected via header. Use header [binary, raw, decoded]", response = VhfPacketSearchDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation, return a packet list", response = VhfPacketSearchDto.class)
    })
    public Response searchVhfPackets(@ApiParam(value = "by: the search pattern {none}; searchable keys [burstid, receptionTime, packetTime, isFrameValid, isFrameDuplicate, stationId, apid, notifStatus, obsid, obsidType, obsidNum, apidname,  hashid, category, classname, pktformat]", required = true, defaultValue = "none") @DefaultValue("none") @QueryParam("by") @NotNull  String by,@ApiParam(value = "header parameter containing the requested output format [binary, \\ \\ decoded, raw]." , defaultValue="binary")@HeaderParam("X-VHF-PktFormat") String xVHFPktFormat,@ApiParam(value = "The format of the input time fields" , defaultValue="yyyyMMdd'T'HHmmssz")@HeaderParam("dateformat") String dateformat,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {insertionTime:ASC}", defaultValue = "insertionTime:ASC") @DefaultValue("insertionTime:ASC") @QueryParam("sort")  String sort,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.searchVhfPackets(by, xVHFPktFormat, dateformat, page, size, sort, securityContext, info);
    }
    @GET
    @Path("/attitudes")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds vhf satellite attitudes by query.", notes = "This method will search for Satellite Attitudes.", response = VhfSatAttitudeSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation, return a packet list", response = VhfSatAttitudeSetDto.class)
    })
    public Response searchVhfSatAttitudes(@ApiParam(value = "by: the search pattern {none}; searchable keys [id, insertionTime, packetTime, apidname]", required = true, defaultValue = "none") @DefaultValue("none") @QueryParam("by") @NotNull  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {insertionTime:ASC}", defaultValue = "insertionTime:ASC") @DefaultValue("insertionTime:ASC") @QueryParam("sort")  String sort,@ApiParam(value = "The format of the input time fields" , defaultValue="yyyyMMdd'T'HHmmssz")@HeaderParam("dateformat") String dateformat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.searchVhfSatAttitudes(by, page, size, sort, dateformat, securityContext, info);
    }
    @GET
    @Path("/positions")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Finds vhf satellite positions by query.", notes = "This method will search for Satellite Positions.", response = VhfSatPositionSetDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation, return a packet list", response = VhfSatPositionSetDto.class)
    })
    public Response searchVhfSatPositions(@ApiParam(value = "by: the search pattern {none}; searchable keys [id, insertionTime, packetTime, apidname]", required = true, defaultValue = "none") @DefaultValue("none") @QueryParam("by") @NotNull  String by,@ApiParam(value = "page: the page number {0}", defaultValue = "0") @DefaultValue("0") @QueryParam("page")  Integer page,@ApiParam(value = "size: the page size {1000}", defaultValue = "1000") @DefaultValue("1000") @QueryParam("size")  Integer size,@ApiParam(value = "sort: the sort pattern {insertionTime:ASC}", defaultValue = "insertionTime:ASC") @DefaultValue("insertionTime:ASC") @QueryParam("sort")  String sort,@ApiParam(value = "The format of the input time fields" , defaultValue="yyyyMMdd'T'HHmmssz")@HeaderParam("dateformat") String dateformat,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.searchVhfSatPositions(by, page, size, sort, dateformat, securityContext, info);
    }
    @PUT
    @Path("/bursts/{id}")
    
    @Produces({ "application/json", "application/xml" })
    @io.swagger.annotations.ApiOperation(value = "Update fields in a VhfBurstId.", notes = "This method allows to perform search and sorting. Arguments: by={searchpattern}, page={ipage}, size={isize}, sort={sortpattern}.  The searchpattern is in the form {param-name operation param-value}, where <i>param-name</i> is the name of one of the fields in the dto, <i>operation</i> can be [< : >] (corresponding to lt, eq, gt; for string use only [:]), and <i>param-value</i> depends on the chosen parameter. A list of criteria can be provided using comma separated strings for searchpattern (comma means AND). The sortpattern is {field}:[DESC|ASC].", response = VhfBurstIdDto.class, tags={ "vhf", })
    @io.swagger.annotations.ApiResponses(value = {
        @io.swagger.annotations.ApiResponse(code = 200, message = "successful operation", response = VhfBurstIdDto.class)
    })
    public Response updateVhfBurstId(@ApiParam(value = "burst id", required = true) @PathParam("id") @NotNull  String id,@ApiParam(value = "A json string that is used to modify some fields of a VhfBurstId object: { packetTime: xxx, ... }", required = true) @NotNull @Valid  Map<String, Long> body,@Context SecurityContext securityContext,@Context UriInfo info)
    throws NotFoundException {
        return delegate.updateVhfBurstId(id, body, securityContext, info);
    }
}
