package fr.svom.vhf.server.swagger.api;

import fr.svom.vhf.server.swagger.api.*;
import fr.svom.vhf.swagger.model.*;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;

import fr.svom.vhf.swagger.model.ApiHttpError;
import java.io.File;
import fr.svom.vhf.swagger.model.GenericMap;
import fr.svom.vhf.swagger.model.HTTPResponse;
import java.util.Map;
import fr.svom.vhf.swagger.model.ObsidSetDto;
import fr.svom.vhf.swagger.model.PacketCounterSetDto;
import fr.svom.vhf.swagger.model.RawPacketSetDto;
import fr.svom.vhf.swagger.model.StationStatusSetDto;
import fr.svom.vhf.swagger.model.VhfBasePacket;
import fr.svom.vhf.swagger.model.VhfBurstIdDto;
import fr.svom.vhf.swagger.model.VhfBurstIdSetDto;
import fr.svom.vhf.swagger.model.VhfNotificationSetDto;
import fr.svom.vhf.swagger.model.VhfPacketSearchDto;
import fr.svom.vhf.swagger.model.VhfSatAttitudeSetDto;
import fr.svom.vhf.swagger.model.VhfSatPositionSetDto;
import fr.svom.vhf.swagger.model.VhfStationStatusCountDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameSetDto;

import java.util.List;
import fr.svom.vhf.server.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import javax.validation.constraints.*;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJerseyServerCodegen")
public abstract class VhfApiService {
    public abstract Response decodePacket(String format,String pkt,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response decodePacketList(RawPacketSetDto rawPacketSetDto,String format,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response deleteObsids(String id,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response findVhfTransferFrames(Long frame,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response flagDuplicateFrames(Map<String, Long> requestBody,String format,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response getVhfPackets(String hash,String xVHFPktFormat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfBurstIds(String by,Integer page,Integer size,String sort,String dateformat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfCounters(String apidname,String productname,Long obsid,String pktmintime,String pktmaxtime,String timeformat,String mode,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfNotifications(String by,Integer page,Integer size,String sort,String dateformat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfRawPackets(String by,Integer page,Integer size,String sort,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfStationStatus(String by,Integer page,Integer size,String sort,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfStationStatusCount(String from,String to,String timeformat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response listVhfTransferFrames(String by,Integer page,Integer size,String sort,String dateformat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response notifyVhfTransferFrames(Long frame,Map<String, Long> requestBody,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response savePacketFromStream(String format,String mode,FormDataBodyPart streamBodypart,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response searchObsids(String minreceptiontime,String maxreceptiontime,String timeformat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response searchVhfPackets( @NotNull String by,String xVHFPktFormat,String dateformat,Integer page,Integer size,String sort,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response searchVhfSatAttitudes( @NotNull String by,Integer page,Integer size,String sort,String dateformat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response searchVhfSatPositions( @NotNull String by,Integer page,Integer size,String sort,String dateformat,SecurityContext securityContext, UriInfo info) throws NotFoundException;
    public abstract Response updateVhfBurstId(String id,Map<String, Long> body,SecurityContext securityContext, UriInfo info) throws NotFoundException;
}
