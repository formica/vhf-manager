package fr.svom.cs.authentication;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

/**
 * Utility class for authentication. This is here only as an example.
 *
 * @author formica
 *
 */
public class Authorizer {

    /**
     * The Svom user.
     */
    private FscUser user;
    /**
     * The Svom principal.
     */
    private Principal principal;

    /**
     * Default Ctor.
     *
     * @param user
     *            the FscUser
     */
    public Authorizer(final FscUser user) {
        this.user = user;
        this.principal = () -> user.getUsername();
    }

    /**
     * Get the principal.
     *
     * @return Principal
     */
    public Principal getUserPrincipal() {
        return this.principal;
    }

    /**
     * Check role.
     *
     * @param role the String
     * @return boolean
     */
    public boolean isUserInRole(String role) {
        return (role.equals(user.getRole()));
    }

    /**
     * Is secure default to True. No real check is done here for the moment.
     *
     * @return boolean
     */
    public boolean isSecure() {
        return true;
    }

    /**
     * Get Authentication Scheme: default is BASIC_AUTH.
     * 
     * @return String
     */
    public String getAuthenticationScheme() {
        return SecurityContext.BASIC_AUTH;
    }
}
