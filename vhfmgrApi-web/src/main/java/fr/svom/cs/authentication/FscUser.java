package fr.svom.cs.authentication;

/**
 * Svom representation of a user. Not used for the moment.
 *
 * @author formica
 *
 */
public class FscUser {

    /**
     * The username.
     */
    private String username;
    /**
     * The role.
     */
    private String role;

    /**
     * Default Ctor.
     *
     * @param username
     *            the String
     * @param role
     *            the String
     */
    public FscUser(String username, String role) {
        this.username = username;
        this.role = role;
    }

    /**
     * @return String
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @return String
     */
    public String getRole() {
        return this.role;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "FscUser [username=" + username + ", role=" + role + "]";
    }

}
