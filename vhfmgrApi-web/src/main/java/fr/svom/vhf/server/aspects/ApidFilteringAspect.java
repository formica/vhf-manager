/**
 *
 */
package fr.svom.vhf.server.aspects;

import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.server.services.VhfBurstIdService;
import fr.svom.vhf.server.services.VhfDecodedPacketService;
import fr.svom.vhf.server.services.VhfTransferFrameService;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author formica
 *
 */
@Aspect
@Component
public class ApidFilteringAspect {
    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ApidFilteringAspect.class);

    /**
     * Service.
     */
    @Autowired
    private VhfTransferFrameService vhfTransferFrameService;
    /**
     * Service.
     */
    @Autowired
    private VhfBurstIdService vhfBurstIdService;
    /**
     * Service.
     */
    @Autowired
    private VhfDecodedPacketService vhfDecodedPacketService;

    /**
     * Check synchronization.
     * @param entity
     *            the Iov
     */
    @Before("execution(* fr.svom.vhf.server.services.VhfTransferFrameService.publishNotification(*)) && args(entity)")
    public void checkBurstId(VhfTransferFrame entity) {
        log.debug("Vhf frame insertion should verify the burst ID for APID {}",
                entity.getApid().getName());
        // Get last burstID stored.
        // Get the APID.
        final PacketApid apid = entity.getApid();
        // Get the classname.
        final String classname = apid.getClassName();
        // Get the header.
        final PrimaryHeaderPacket php = entity.getHeader();
        // Search existing burst ids, based on packet time and obsid.
        List<VhfBurstId> burstIdList = vhfBurstIdService.getLastByTimeAndObsid(php.getPacketTime(),
                php.getPacketObsid(), classname);
        // Init the last burst id to null.
        VhfBurstId lastbid = null;
        // Check if there are burst ids or not from the previous query.
        if (!burstIdList.isEmpty()) {
            // Some burstids were found.
            int size = burstIdList.size();
            log.trace("Found list of last burstids of size : {} ; last is {}", size, burstIdList.get(0));
            // A burst ID does already exists...do not create a new
            if (size > 1) {
                // Set a warning on the frame status because too many burst ids were found.
                log.warn("Found more than one burst_id for packettime {} and obsid {}", php.getPacketTime(),
                        php.getPacketObsid());
                vhfTransferFrameService.updateFrameStatus(entity.getFrameId(), ",[BurstId: warn]", Boolean.TRUE);
            }
            // Init the burst id from the list.
            lastbid = burstIdList.get(0);
            // Get the frame.
            VhfTransferFrame lastframe = lastbid.getFrame();
            log.debug("Verify existing burstId frame-apid {} respect to present apid {}", lastframe.getApid(), apid);
            if (lastframe.getApid().getName().startsWith("TmVhfGrm")
                && apid.getName().startsWith("TmVhfEcl")) {
                // Add this obsid to the svom burst id, because the burst id was created from a Grm trigger.
                lastbid.setEclairObsid((long) entity.getHeader().getPacketObsid());
                VhfBurstId bid = vhfDecodedPacketService.createBurstIdFromPacket(entity);
                lastbid.setAlertTimeTb(bid.getAlertTimeTb());
                // Save the burst id after updating the fields.
                log.info("checkBurstId: update existing burstid {}", lastbid);
                vhfBurstIdService.storeOrUpdateVhfBurstId(lastbid);
            }
        }
        else {
            // A burst ID was not found, check the packet ID
            log.debug("No existing burstid found for frame {}: searched using pkt time {} and obsid {}",
                    entity.getFrameId(), php.getPacketTime(),
                    php.getPacketObsid());
            // Create a burst id from the entity.
            VhfBurstId bid = vhfDecodedPacketService.createBurstIdFromPacket(entity);
            if (bid != null) {
                log.info("checkBurstId: creating new burstid {}", bid.getBurstId());
                try {
                    // Store the burst ID.
                    lastbid = vhfBurstIdService.storeOrUpdateVhfBurstId(bid);
                }
                catch (DataIntegrityViolationException e) {
                    // Error in the storage, so set to the one created in memory.
                    log.warn("BurstId {} cannot be stored on DB: {}", bid.getBurstId(), e);
                    lastbid = bid;
                }
            }
            else {
                // Error creating burst id from entity.
                log.warn("Burst id is null when reading information from {}", entity.getFrameId());
            }
        }
        // Check if category is AlertSequence.
        if (lastbid != null && apid.getCategory().matches("(AlertSequence)")) {
            // Link the burst ID to the frame.
            entity.setBurstId(lastbid.getBurstId());
            vhfTransferFrameService.updateFrameBurstId(entity.getFrameId(), lastbid.getBurstId());
            log.info("checkBurstId: associate frame {} with burst_id {}", entity.getFrameId(), lastbid.getBurstId());
        }
        else {
            // In case this is not an AlertSequence do not do anything.
            log.warn("Burst id not found (or apid {} not in AlertSequence) for frame association with : {}",
                    apid.getName(), entity.getFrameId());
        }
    }
}
