/**
 * 
 */
package fr.svom.vhf.server.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Profile aspect.
 *
 * @author formica
 *
 */
@Aspect
@Component
public class ProfileAndLogAspect {
    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ProfileAndLogAspect.class);

    /**
     * Define method for profiling.
     *
     * @param joinPoint
     *            the ProceedingJoinPoint
     * @return Object
     * @throws Throwable
     *             If an exception occurred
     */
    @Around("@annotation(fr.svom.vhf.server.annotations.ProfileAndLog)")
    public Object profileAndLog(ProceedingJoinPoint joinPoint) throws Throwable {
        final long start = System.currentTimeMillis();
        final Object proceed = joinPoint.proceed();
        final long executionTime = System.currentTimeMillis() - start;
        log.debug("{} executed in {} ms", joinPoint.getSignature(), executionTime);
        return proceed;
    }
}
