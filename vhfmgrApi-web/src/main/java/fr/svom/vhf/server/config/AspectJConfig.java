package fr.svom.vhf.server.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * An aspect. The annotation @EnableLoadTimeWeaving is not applied for now.
 *
 * @author formica
 *
 */
@Configuration
@ComponentScan("fr.svom.vhf.server")
@EnableAspectJAutoProxy
public class AspectJConfig {
// No parameters.
}
