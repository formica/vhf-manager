package fr.svom.vhf.server.config;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;

/**
 * An aspect. The annotation @EnableAsync is needed here to configure executors.
 *
 * @author formica
 *
 */
@EnableAsync
@Configuration
public class AsyncConfig implements AsyncConfigurer {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AsyncConfig.class);

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.scheduling.annotation.AsyncConfigurer#getAsyncExecutor()
     */
    @Bean("taskExecutor")
    @Override
    public Executor getAsyncExecutor() {
        return new ConcurrentTaskExecutor(Executors.newFixedThreadPool(3));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.scheduling.annotation.AsyncConfigurer#
     * getAsyncUncaughtExceptionHandler()
     */
    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (throwable, method, objects) -> log.error("-- exception handler -- {}", throwable);
    }

}
