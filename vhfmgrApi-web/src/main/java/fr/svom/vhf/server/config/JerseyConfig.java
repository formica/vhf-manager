package fr.svom.vhf.server.config;

import fr.svom.vhf.server.filters.CORSFilter;
import fr.svom.vhf.server.filters.LoggerFeature;
import fr.svom.vhf.server.swagger.api.ApidsApi;
import fr.svom.vhf.server.swagger.api.StationsApi;
import fr.svom.vhf.server.swagger.api.VhfApi;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Configuration class for Jersey.
 *
 * @author formica
 *
 */
@Component
public class JerseyConfig extends ResourceConfig {

    /**
     * Default Ctor.
     */
    public JerseyConfig() {
        // Add API services.
        super.register(ApidsApi.class);
        super.register(VhfApi.class);
        super.register(StationsApi.class);
        // Exception handler
        super.register(JerseyExceptionHandler.class);
        // Add filter for multipart.
        super.register(MultiPartFeature.class);
        // Add filter for Logger.
        super.register(LoggerFeature.class);
        // Add filter for COARS.
        super.register(CORSFilter.class);
        // Property when URL is not found.
        super.property(ServletProperties.FILTER_FORWARD_ON_404, true);
    }

    /**
     * Initialize the configuration.
     *
     * @return
     */
    @PostConstruct
    public void init() {
        // Register components where DI is needed
        // this.configureSwagger();
    }
}
