package fr.svom.vhf.server.config;

import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.server.swagger.api.ApiResponseMessage;
import fr.svom.vhf.swagger.model.HTTPResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.time.OffsetDateTime;

/**
 * Exception handler.
 * This handler will catch all exceptions thrown and provide a dedicated output response
 * in case they are thrown by server code.
 */
@Provider
public class JerseyExceptionHandler implements ExceptionMapper<Exception> {
    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(JerseyExceptionHandler.class);

    /**
     * Default ctor.
     */
    public JerseyExceptionHandler() {
        // Empty constructor. All initialization in postConstruct().
    }

    @Override
    public Response toResponse(Exception exception) {
        log.warn("Handling exception: {} of type {}", exception.getMessage(), exception.getClass());
        // If exception is a webapplication exception
        if (exception instanceof WebApplicationException) {
            log.debug("Instance of WebApplicationException...get Response from there.");
            // Jersey exceptions: return their standard response
            WebApplicationException e = (WebApplicationException) exception;
            return e.getResponse();
        }
        // If exception is a AbstractCdbServiceException exception
        if (exception instanceof AbstractCdbServiceException) {
            log.debug("Instance of CdbServiceException...generate HTTPResponse");
            // Exceptions thrown by the crest server code
            AbstractCdbServiceException e = (AbstractCdbServiceException) exception;
            HTTPResponse resp = new HTTPResponse().timestamp(OffsetDateTime.now())
                    .code(e.getResponseStatus().getStatusCode())
                    .error(e.getResponseStatus().getReasonPhrase())
                    .message(e.getMessage());
            // Set the response and the cachecontrol.
            return Response.status(e.getResponseStatus()).entity(resp).build();
        }
        // The exception is unhandled, so use INTERNAL_SERVER_ERROR as output code.
        log.error("Unhandled exception of type {}: internal server error: {}", exception.getClass(),
                exception.getMessage());
        String message = exception.getClass() + ";ERRMSG=" + exception.getMessage();
        ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.ERROR, message);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(resp).build();
    }
}
