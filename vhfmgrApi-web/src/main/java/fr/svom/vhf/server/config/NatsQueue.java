package fr.svom.vhf.server.config;

/**
 * Enumeration for Vhf queues.
 *
 * @author formica
 *
 */
public enum NatsQueue {
    /**
     * VHF_DATA.
     */
    VHF_DATA("data.vhf"),
    /**
     * TEST.
     */
    TEST("TEST"),
    /**
     * IFSC-TOOLS-BA.
     */
    VHF_BA("data.vhfba"),
    /**
     * ACTIVITY_VHFMGR.
     */
    ACTIVITY_VHFMGR("activity.infrastructure");

    /**
     * The queue.
     */
    private final String queue;

    /**
     * Default Ctor.
     *
     * @param queue
     *            the String
     */
    NatsQueue(String queue) {
        this.queue = queue;
    }

    /**
     * @return the queue
     */
    public String getQueue() {
        return queue;
    }
}
