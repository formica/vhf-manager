package fr.svom.vhf.server.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.svom.vhf.data.serialization.converters.CustomTimeDeserializer;
import fr.svom.vhf.data.serialization.converters.CustomTimeSerializer;
import fr.svom.vhf.server.controllers.SvomRestDetectorPacketDecoderBean;
import fr.svom.vhf.server.externals.IDecoder;
import fr.svom.vhf.server.externals.NatsConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Locale;

/**
 * Configuration class for Spring managed Beans.
 *
 * @author formica
 */
@Configuration
@ComponentScan("fr.svom.vhf")
@EnableAspectJAutoProxy
@Slf4j
public class ServicesConfig {

    /**
     * Create the NATs factory bean.
     *
     * @param natsprops the NatsProperties
     * @return NatsConnectionFactory
     */
    @Bean
    public NatsConnectionFactory getNatsConnectionFactory(@Autowired NatsProperties natsprops) {
        // Build NATs URL.
        String natsurl = "nats://" + natsprops.getServer() + ":" + natsprops.getPort();
        if (!"none".equals(natsprops.getUser())) {
            // Take properties from the configuration files.
            natsurl = "nats://" + natsprops.getUser() + ":" + natsprops.getPassword() + "@"
                      + natsprops.getServer() + ":" + natsprops.getPort();
        }
        log.info("Creating NatsConnectionFactory Bean");
        // Build the factorry.
        final NatsConnectionFactory natsFactory = new NatsConnectionFactory(natsprops.getCluster(),
                natsprops.getClientId(),
                natsurl,
                natsprops.getVhfQueue());
        natsFactory.initNatsConnectionFactory();
        return natsFactory;
    }

    /**
     * Create the Decoder bean instance.
     *
     * @return IDecoder
     */
    @Bean(name = "decoder")
    @Profile({"!swig"})
    public IDecoder getRestDecoder() {
        // This bean deals with the connection to the decoder.
        log.info("getting REST decoder bean");
        return new SvomRestDetectorPacketDecoderBean();
    }

    /**
     * Create the ObjectMapper for object serialization.
     * The old was using: com.fasterxml.jackson.databind.ObjectMapper.
     *
     * @return ObjectMapper
     */
    @Bean(name = "jacksonMapper")
    public ObjectMapper getJacksonMapper() {
        // Build a date formatter.
        // This object allow to pass from a string to an offset datettime and viceversa.
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
                .optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd()
                .appendPattern("x")
                .toFormatter();
        // Create the mapper.
        ObjectMapper mapper = new ObjectMapper();
        // Configure the mapper.
        mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        // Add the java time module.
        JavaTimeModule module = new JavaTimeModule();
        // Add dedicated serializers to that module.
        module.addSerializer(OffsetDateTime.class, new CustomTimeSerializer(formatter));
        module.addDeserializer(OffsetDateTime.class, new CustomTimeDeserializer(formatter));
        // Register the module.
        mapper.registerModule(module);
        return mapper;
    }

    /**
     * @return LocaleResolver
     */
    @Bean
    public LocaleResolver localeResolver() {
        final SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.US);
        return slr;
    }
}
