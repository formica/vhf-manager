package fr.svom.vhf.server.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import fr.svom.vhf.data.config.VhfProperties;

/**
 * Configuration for web services.
 *
 * @author formica
 *
 */
@Configuration
class WebConfigurer implements WebMvcConfigurer {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(WebConfigurer.class);

    /**
     * Configuration properties.
     */
    @Autowired
    private VhfProperties vhfprops;

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer#
     * addResourceHandlers(org.springframework.web.servlet.config.annotation.
     * ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        final String extfpath = "file://" + vhfprops.getWebstaticdir();
        log.info("Adding external path for static web resources => {}", extfpath);
        registry.addResourceHandler("/ext/**").addResourceLocations(extfpath);
    }
}
