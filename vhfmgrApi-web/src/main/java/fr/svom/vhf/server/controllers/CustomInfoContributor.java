package fr.svom.vhf.server.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

/**
 * Component to add contributor informations.
 *
 * @author formica
 *
 */
@Component
public class CustomInfoContributor implements InfoContributor {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(CustomInfoContributor.class);

    /**
     * Environment.
     */
    @Autowired
    private ServerEnvironment serverEnvironment;

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.boot.actuate.info.InfoContributor#contribute(org.
     * springframework.boot.actuate.info.Info.Builder)
     */
    @Override
    public void contribute(Info.Builder builder) {
        log.debug("Adding info in contributor about service descriptor");
        builder.withDetail("descriptor", serverEnvironment.getDescriptor());
    }

}
