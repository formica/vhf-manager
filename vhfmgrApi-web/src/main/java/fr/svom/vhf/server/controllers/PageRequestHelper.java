/**
 *
 */
package fr.svom.vhf.server.controllers;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.config.VhfProperties;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria;
import fr.svom.vhf.data.repositories.querydsl.SearchCriteria;
import fr.svom.vhf.swagger.model.GenericStringMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class to be used to read request parameters from URLs.
 *
 * @author formica
 *
 */
@Component
public class PageRequestHelper {

    /**
     * QRY_PATTERN.
     */
    private static final String QRY_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:|<|>|~)([a-zA-Z0-9_\\-\\/\\.\\*\\%]+?),";
    /**
     * SORT_PATTERN.
     */
    private static final String SORT_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:)([ASC|DESC]+?),";

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PageRequestHelper.class);

    /**
     * KEY, 1.
     */
    private static final int KEY = 1;
    /**
     * OP, 2.
     */
    private static final int OP = 2;
    /**
     * VAL, 3.
     */
    private static final int VAL = 3;
    /**
     * Configuraton properties.
     */
    @Autowired
    private VhfProperties cprops;

    /**
     * Default Ctor.
     */
    public PageRequestHelper() {
        super();
    }

    /**
     * Create a PageRequest.
     *
     * @param page
     *            the Integer
     * @param size
     *            the Integer
     * @param sort
     *            the String
     * @return PageRequest
     */
    public PageRequest createPageRequest(Integer page, Integer size, String sort) {

        if (page < 0 || size < 0) {
            throw new VhfCriteriaException("Wrong page parameters");
        }
        final Pattern sortpattern = Pattern.compile(SORT_PATTERN);
        final Matcher sortmatcher = sortpattern.matcher(sort + ",");
        final List<Order> orderlist = new ArrayList<>();
        // Create a SORT statement.
        while (sortmatcher.find()) {
            Direction direc = Direction.ASC;
            if ("DESC".equals(sortmatcher.group(3))) {
                direc = Direction.DESC;
            }
            final String field = sortmatcher.group(1);
            log.trace("Creating new order: {} {} ", direc, field);
            orderlist.add(new Order(direc, field));
        }
        final Order[] orders = new Order[orderlist.size()];
        int i = 0;
        // Create orders list.
        for (final Order order : orderlist) {
            log.trace("Order @ {} = {}", i, order);
            orders[i] = order;
            i++;
        }
        final Sort msort = Sort.by(orders);
        return PageRequest.of(page, size, msort);
    }

    /**
     * Create a list of SearchCriteria.
     *
     * @param by
     *            the String
     * @return List of SearchCriteria
     */
    public List<SearchCriteria> createMatcherCriteria(String by) {

        log.debug("create matcher from by = {}", by);
        final Pattern pattern = Pattern.compile(QRY_PATTERN);
        final Matcher matcher = pattern.matcher(by + ",");
        final List<SearchCriteria> params = new ArrayList<>();
        // Create criteria list using input string.
        while (matcher.find()) {
            String val = matcher.group(VAL);
            val = val.replaceAll("\\*", "\\%");
            params.add(new SearchCriteria(matcher.group(KEY).toLowerCase(Locale.ENGLISH), matcher.group(OP), val));
        }
        log.trace("List of search criteria: {}", params.size());
        return params;
    }

    /**
     * Create a list of SearchCriteria using a more detailed date format input.
     *
     * @param by
     *            the String
     * @param dateformat
     *            the String
     * @return List of SearchCriteria
     */
    public List<SearchCriteria> createMatcherCriteria(String by, String dateformat) {
        log.debug("create matcher from by = {}, dateformat = {}", by, dateformat);
        final Pattern pattern = Pattern.compile(QRY_PATTERN);
        final Matcher matcher = pattern.matcher(by + ",");
        final List<SearchCriteria> params = new ArrayList<>();
        while (matcher.find()) {
            final String varname = matcher.group(KEY).toLowerCase(Locale.ENGLISH);
            final String op = matcher.group(OP);
            String val = matcher.group(VAL);
            val = val.replaceAll("\\*", "\\%");
            if (varname.contains("time")) {
                Long tepoch = getTimeFromArg(val, dateformat, "msec");
                if (varname.contains("packettime") || varname.contains("alerttime")) {
                    if (tepoch > cprops.getTimeOffset() * 1000L) {
                        // Determine the value as the time - offset all in milliseconds.
                        log.trace("Time for {} : {} - offset(sec) {}", varname, tepoch, cprops.getTimeOffset());
                        tepoch = tepoch - cprops.getTimeOffset() * 1000L;
                    }
                    else {
                        log.trace("Time for {} : {}", varname, tepoch);
                    }
                }
                // Use tepoch directly
                val = String.valueOf(tepoch);
                log.trace("Time for {} : {}", varname, val);
            }
            params.add(new SearchCriteria(varname, op, val));
        }
        log.debug("List of search criteria: {}", params.size());
        return params;
    }

    /**
     * Return time in millisec since epoch, using the format provided in dateformat.
     * If the dateformat is ms then it will just interpret the string as a long.
     * The value will then depend on the privded input argument.
     *
     * @param val the time string
     * @param dateformat the time format
     * @param unit the time unit (sec, ms)
     * @return Long
     */
    public Long getTimeFromArg(String val, String dateformat, String unit) {
        DateTimeFormatter dtformatter = null;
        Long tepoch = null;
        if (!"ms".equals(dateformat)) {
            if ("iso".equalsIgnoreCase(dateformat)) {
                dateformat = "yyyyMMdd'T'HHmmssz";
            }
            dtformatter = DateTimeFormatter.ofPattern(dateformat);
        }
        else {
            log.debug("Assuming time in ms: {}", val);
            tepoch = Long.parseLong(val);
            if (unit == null || "sec".equalsIgnoreCase(unit)) {
                tepoch = tepoch / 1000L;
            }
        }
        // The dtformatter was set to some value, so interpret the string using it.
        if (dtformatter != null) {
            final ZonedDateTime zdtInstanceAtOffset = ZonedDateTime.parse(val, dtformatter);
            final ZonedDateTime zdtInstanceAtUTC = zdtInstanceAtOffset
                    .withZoneSameInstant(ZoneOffset.UTC);
            tepoch = zdtInstanceAtUTC.toInstant().toEpochMilli();
            log.trace("Parsed date using format {}; time from epoch is {}", zdtInstanceAtUTC, tepoch);
        }
        if (unit == null || "sec".equalsIgnoreCase(unit)) {
            tepoch = tepoch / 1000L;
            log.warn("unit is null or sec, change tepoch divide by 1000 => {}", tepoch);
        }
        return tepoch;
    }

    /**
     * @param expressions
     *            the List<BooleanExpression>
     * @return BooleanExpression
     */
    public BooleanExpression getWhere(List<BooleanExpression> expressions) {
        BooleanExpression wherepred = null;

        for (final BooleanExpression exp : expressions) {
            if (wherepred == null) {
                wherepred = exp;
            }
            else {
                wherepred = wherepred.and(exp);
            }
        }
        return wherepred;
    }

    /**
     * @param params
     *            the List<SearchCriteria>
     * @return GenericStringMap
     */
    public GenericStringMap getFilters(List<SearchCriteria> params) {
        final GenericStringMap filters = new GenericStringMap();
        for (final SearchCriteria sc : params) {
            filters.put(sc.getKey(), sc.getValue().toString());
        }
        return filters;
    }

    /**
     * Convert a List of SearchCriteria into a String.
     *
     * @param params
     *            the List<SearchCriteria>
     * @return String
     */
    public String dumpCriteria(List<SearchCriteria> params) {
        final StringBuilder bld = new StringBuilder();
        for (final SearchCriteria searchCriteria : params) {
            bld.append(searchCriteria.dump() + ",");
        }
        return bld.toString();
    }

    /**
     * @param filter
     *            the IFilteringCriteria
     * @param by
     *            the String
     * @return BooleanExpression
     */
    public BooleanExpression buildWhere(IFilteringCriteria filter, String by) {
        final List<SearchCriteria> params = createMatcherCriteria(by);
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        return getWhere(expressions);
    }

    /**
     * @param filter
     *            the IFilteringCriteria
     * @param params
     *            the List<SearchCriteria>
     * @return BooleanExpression
     */
    public BooleanExpression buildWhere(IFilteringCriteria filter, List<SearchCriteria> params) {
        final List<BooleanExpression> expressions = filter.createFilteringConditions(params);
        return getWhere(expressions);
    }

}
