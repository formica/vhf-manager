/**
 * 
 */
package fr.svom.vhf.server.controllers;

import fr.svom.messages.model.Instrument;
import fr.svom.messages.model.Program;
import fr.svom.messages.model.ServiceDescriptor;
import fr.svom.vhf.data.config.VhfProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.ZoneOffset;

/**
 * Component to extract Server environment configuration.
 *
 * @author formica
 *
 */
@Component
public class ServerEnvironment {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ServerEnvironment.class);

    /**
     * The host of the server.
     */
    private String hostname = "localhost";
    /**
     * The port of the server.
     */
    private String port = "none";
    /**
     * The server baseurl.
     */
    private String baseurl = "none";

    /**
     * A ServiceDescriptor for this server.
     */
    private ServiceDescriptor descriptor = null;

    /** The running mode. */
    private String runningMode = "prod";
    /**
     * Environment.
     */
    @Autowired
    private Environment env;
    /**
     * Vhfmgr properties.
     */
    @Autowired
    private VhfProperties vprops;

    /**
     * Init params.
     *
     * @return
     */
    @PostConstruct
    protected void init() {
        try {
            hostname = InetAddress.getLocalHost().getHostName();
            log.info("local server {}", hostname);

        }
        catch (final UnknownHostException e) {
            log.error("Host is not known : {}", e.getMessage());
        }
    }

    /**
     * @return String
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * @return String
     */
    public String getPort() {
        if ("none".equals(port)) {
            port = env.getProperty("local.server.port");
        }
        return port;
    }

    /**
     * @return ServiceDescriptor
     */
    public ServiceDescriptor getDescriptor() {
        if (descriptor == null) {
            descriptor = createDescriptor();
        }
        return descriptor;
    }

    /**
     * @return the baseurl
     */
    public String getBaseurl() {
        // Create the base url for requests.
        if ("none".equals(baseurl)) {
            baseurl = "https://" + getHostname() + ":" + getPort();
            //baseurl = ServletUriComponentsBuilder.fromCurrentContextPath().toUriString();
        }
        return baseurl;
    }

    /**
     * Sets the baseurl.
     *
     * @param baseurl the new baseurl
     */
    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
        if (baseurl.startsWith("http://")) {
            this.baseurl = baseurl.replace("http", "https");
        }
    }
    
    /**
     * Gets the running mode.
     *
     * @return the running mode
     */
    public String getRunningMode() {
        return runningMode;
    }

    /**
     * Sets the running mode.
     *
     * @param runningMode the new running mode
     */
    public void setRunningMode(String runningMode) {
        this.runningMode = runningMode;
    }

    /**
     * Create the Descriptor.
     *
     * @return ServiceDescriptor
     */
    protected ServiceDescriptor createDescriptor() {
        // Create a service descriptor.
        final ServiceDescriptor sd = new ServiceDescriptor();
        final Instant now = Instant.now();
        sd.creationDate(now.atOffset(ZoneOffset.UTC));
        // Add instrument filter.
        final Instrument inst = Instrument.NONE;
        sd.addInstrumentItem(inst);
        sd.addLinksItem("vhfdb");
        sd.addLinksItem("messaging");
        sd.addLinksItem("decoder");
        // Add default Observation Mode.
        sd.program(Program.ALL);
        // Add default service name.
        sd.name("vhfmgr");
        // Add default URI.
        sd.uri(hostname + ":" + getPort());
        sd.version(vprops.getVersion());
        return sd;
    }

}
