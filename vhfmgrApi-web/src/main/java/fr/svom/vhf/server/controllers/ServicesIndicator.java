package fr.svom.vhf.server.controllers;

import fr.svom.vhf.server.config.NatsProperties;
import fr.svom.vhf.server.externals.NatsConnectionFactory;
import io.nats.client.Connection;
import io.nats.client.Connection.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * An Health indicator.
 *
 * @author formica
 */
@Component
public class ServicesIndicator implements HealthIndicator {

   
    /**
     * Factory.
     */
    @Autowired
    private NatsConnectionFactory natsConnectionFactory;
    /**
     * Configuration properties.
     */
    @Autowired
    private NatsProperties natsprops;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.boot.actuate.health.HealthIndicator#health()
     */
    @Override
    public Health health() {
        // Define a health method.
        Connection nats;
        // Init the map for health
        final Map<String, Object> natsmap = new HashMap<>();
        natsmap.put("host", natsprops.getServer());
        natsmap.put("port", natsprops.getPort());
        // Check NATS connection.
        Boolean isconnected = natsConnectionFactory.testConnection();
        Status natsisconnected = Status.DISCONNECTED;
        if (isconnected) {
            // Connection is available, get status
            natsisconnected = Status.CONNECTED;
        }
        natsmap.put("isconnected", natsisconnected);
        if (natsisconnected == Status.CONNECTED) {
            // Nats is connected, set relative health to UP.
            return Health.up().withDetail("nats", natsmap).build();
        }
        else {
            // Nats is not connected, set relative health to DOWN.
            return Health.down().withDetail("nats", natsmap).build();
        }
    }
}
