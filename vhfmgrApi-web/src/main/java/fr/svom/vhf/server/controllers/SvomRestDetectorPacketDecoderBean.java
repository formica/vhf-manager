/**
 * 
 */
package fr.svom.vhf.server.controllers;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.server.externals.DecoderRestTemplate;
import fr.svom.vhf.server.externals.IDecoder;
import fr.svom.vhf.swagger.model.HTTPResponse;

/**
 * Implementation of a Decoder.
 *
 * @author formica
 *
 */
public class SvomRestDetectorPacketDecoderBean implements IDecoder {

    /**
     * The Decoder Rest template.
     */
    @Autowired
    private DecoderRestTemplate restDecoder;

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SvomRestDetectorPacketDecoderBean.class);

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.server.externals.IDecoder#decodeBytes(byte[])
     */
    @Override
    public HTTPResponse decodeBytes(byte[] bytes) throws VhfDecodingException {
        final String hexa = Hex.encodeHexString(bytes);
        return decodeString(hexa);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.server.externals.IDecoder#decodeString(java.lang.String)
     */
    @Override
    public HTTPResponse decodeString(String s) throws VhfDecodingException {
        log.trace("REST http request to external service for decoding : {}", s);
        // Decode the hexadecimal string and send a JSON message back.
        final HTTPResponse resp = restDecoder.getDecodedPacket(s);
        if (resp != null) {
            log.trace("Retrieved decoded packet : {} {}", resp.getAction(), resp.getMessage());
            return resp;
        }
        else {
            log.error("Error in decoding string {}", s);
            String message = "{\"type\": \"error\", \"content\":\"Cannot decode, may be service is down...\"}";
            return new HTTPResponse().code(HttpStatus.BAD_REQUEST.value()).message(message);
        }
    }
}
