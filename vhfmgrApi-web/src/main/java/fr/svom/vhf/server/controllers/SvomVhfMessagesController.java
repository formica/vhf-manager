package fr.svom.vhf.server.controllers;

import fr.svom.messages.model.DataNotification;
import fr.svom.messages.model.ServiceStatus;
import fr.svom.vhf.data.mappers.NotifStatus;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.VhfNotificationRepository;
import fr.svom.vhf.server.config.NatsQueue;
import fr.svom.vhf.server.externals.NatsController;
import fr.svom.vhf.server.externals.VhfNatsMessagesConverter;
import fr.svom.vhf.server.services.VhfLogMessageService;
import fr.svom.vhf.server.swagger.api.impl.HeaderFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;

/**
 * A Controller to post messages to NATs.
 *
 * @author formica
 */
@Component
public class SvomVhfMessagesController {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SvomVhfMessagesController.class);

    /**
     * Converter.
     */
    @Autowired
    private VhfNatsMessagesConverter vhfNatsMessagesConverter;

    /**
     * Service.
     */
    @Autowired
    private VhfLogMessageService vhfLogMessageService;

    /**
     * Repository.
     */
    @Autowired
    private VhfNotificationRepository vhfNotificationRepository;

    /**
     * Controller.
     */
    @Autowired
    private NatsController nats;

    /**
     * FAILED.
     */
    private static final String FAILED = "failed";

    /**
     * DataNotification.
     */
    private static final String DATA_NOTIFICATION = "DataNotification";

    /**
     * Log Queue.
     */
    private static final String LOG_MSG = "mon_vhf_alerts";

    /**
     * @param frame   the VhfTransferFrame.
     * @param maxtime the Long.
     * @return NotifStatus
     */
    public NotifStatus sendNotification(VhfTransferFrame frame, Long maxtime) {

        // Check if we are in test mode
        if (frame.getFrameStatus().contains(HeaderFormat.PKT_MODE_TEST.value)) {
            log.warn(
                    "The server request is in test mode: no notification are sent in this case for {}",
                    frame.getFrameId());
            return NotifStatus.SENT;
        }

        // Default notification.
        NotifStatus status = NotifStatus.FAILED;
        final PacketApid apid = frame.getApid();
        VhfNotification notif = new VhfNotification();
        notif.setFrame(frame);
        notif.setPktflag(apid.getName());
        try {
            // Create a data notification.
            final DataNotification dn = vhfNatsMessagesConverter.createDataNotificationOnFrameList(
                    frame, apid.getName(), maxtime, nats.getServiceClientId());
            log.trace("Created data notification : {}", dn);
            // Transform the notification into a String.
            final String msg = vhfNatsMessagesConverter.writeSvomObjectAsString(dn);
            // Publish the message to NATs default queue (data.vhf) in async mode.
            final String natsguid = nats.publishToQueue(null, msg);
            if (natsguid.equals(FAILED)) {
                // Something wrong, just log the message as a warning here
                log.warn("NATs ==> notification failed for packet APID {} : {}",
                        apid.getName(), msg);
                // Save notification
                notif.setNotifFormat("FAILED");
                notif.setNotifMessage("Notification failed for packet APID " + apid.getName() + " at time " + maxtime);
            }
            else {
                // Publish did succeed, status is SENT.
                log.trace("NATs ==> notification has been sent for packet APID {} : {}",
                        apid.getName(), msg);
                status = NotifStatus.SENT;
                // Save notification
                notif.setNotifFormat("SENT");
                notif.setNotifMessage(
                        "Notification: " + msg.substring(0, Math.min(msg.length(), 1000)) + " at time " + maxtime);
            }
            vhfNotificationRepository.save(notif);

            // Publish the status in dedicated NATs channel.
            // TODO verify if this is really needed. The only purpose here is to send
            // information to NATs server
            // without nats streaming to profit of telegraf process which will read data and
            // store them in influx.
            // FIXME: the related code has been removed in this version.
        }
        catch (final IOException e) {
            log.error("IO problems when trying to generate notification for frame {}: {}",
                    frame.getFrameId(), e.getMessage());
            notif.setNotifFormat("ERROR");
            notif.setNotifMessage("IOException "
                                  + e.getMessage().substring(0, Math.min(e.getMessage().length(), 1000)));
            vhfNotificationRepository.save(notif);
            log.error("Notification Error registered: {}", notif);
        }
        /*
         * final Optional<VhfTransferFrame> entity = vhfTransferFrameRepository
         * .findById(frame.getFrameId()); if (entity.isPresent()) { final
         * VhfTransferFrame f = entity.get(); f.setNotifStatus(status.status()); //
         * Register the status in the DB. vhfTransferFrameRepository.save(frame); }
         */
        return status;
    }

    /**
     * Send notification for BA.
     *
     * @param frame
     */
    public void sendNotificationBA(VhfTransferFrame frame) {

        // Check if we are in test mode
        if (frame.getFrameStatus().contains(HeaderFormat.PKT_MODE_TEST.value)) {
            log.warn(
                    "The server request is in test mode: no notification are sent in this case for {}",
                    frame.getFrameId());
            return;
        }
        // Default notification.
        final PacketApid apid = frame.getApid();
        try {
            // Create a data notification.
            final DataNotification dn = vhfNatsMessagesConverter.createDataNotificationForBA(frame);
            log.debug("Created data notification for BA: {}", dn);
            // Transform the notification into a String.
            final String msg = vhfNatsMessagesConverter.writeSvomObjectAsString(dn);
            // Publish the message to NATs default queue (data.vhf) in async mode.
            final String natsguid = nats.publishToQueue(NatsQueue.VHF_BA.getQueue(), msg);
            if (natsguid.equals(FAILED)) {
                // Something wrong, just log the message as a warning here
                log.warn("NATs ==> BA notification failed for packet APID {} : {}",
                        apid.getName(), msg);
            }
        }
        catch (final IOException e) {
            log.error("IO problems when trying to generate notification for frame {}: {}",
                    frame.getFrameId(), e.getMessage());

        }
    }

    /**
     * Send service status.
     *
     * @param content the content
     * @param from    the from
     */
    public void sendServiceStatus(String content, Instant from) {
        // Create the service status object
        ServiceStatus serviceStatus = vhfNatsMessagesConverter.createServiceStatus(content);
        log.debug("Send service status information: {}", serviceStatus);

        final String msg = vhfNatsMessagesConverter.writeSvomObjectAsString(serviceStatus);
        // Publish the message to NATs default queue (data.vhf) in async mode.
        final String natsguid = nats.publishToQueue(NatsQueue.ACTIVITY_VHFMGR.getQueue(), msg);
        VhfNotification notif = new VhfNotification();
        notif.setFrame(null);
        notif.setNotifFormat("ServiceStatus");
        notif.setNotifMessage(msg.substring(0, Math.min(msg.length(), 1000)));
        notif.setPktflag("service");
        if (natsguid.equals(FAILED)) {
            // Something wrong, just log the message as a warning here
            log.warn("NATs error ==> notification failed for {}", msg);
            notif.setNotifFormat(FAILED);
        }
        else {
            // Publish did succeed, status is SENT.
            log.trace("NATs success ==> notification has been sent");
        }
        vhfNotificationRepository.save(notif);
    }
}
