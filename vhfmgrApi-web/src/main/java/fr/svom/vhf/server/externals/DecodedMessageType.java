package fr.svom.vhf.server.externals;

/**
 * Enumeration for message types. Used to classify the raw packets.
 *
 * @author formica
 *
 */
public enum DecodedMessageType {
    /**
     * TO_BE_DECODED.
     */
    TO_BE_DECODED("TO_BE_DECODED"),
    /**
     * TEST.
     */
    TEST("TEST"),
    /**
     * DECODED.
     */
    DECODED("OK"),
    /**
     * JSON.
     */
    JSON("JSON"),
    /**
     * ERROR.
     */
    ERROR("ERROR"),
    /**
     * UNPROCESSABLE.
     */
    UNPROCESSABLE("UNPROCESSABLE"),
    /**
     * DUPLICATE.
     */
    DUPLICATE("DUPLICATE");

    /**
     * The type.
     */
    private final String typename;

    /**
     * Default Ctor.
     *
     * @param name
     *            the String
     */
    DecodedMessageType(String name) {
        this.typename = name;
    }

    /**
     * @return the typename
     */
    public String getTypename() {
        return typename;
    }
}
