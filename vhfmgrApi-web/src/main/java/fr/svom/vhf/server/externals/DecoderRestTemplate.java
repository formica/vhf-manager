/**
 * 
 */
package fr.svom.vhf.server.externals;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.svom.vhf.swagger.model.HTTPResponse;

/**
 * A Component to decode packets. It connects to an external service.
 *
 * @author formica
 *
 */
@Component
@ConfigurationProperties("decoder")
public class DecoderRestTemplate {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(DecoderRestTemplate.class);

    /**
     * RestTemplate.
     */
    private final RestTemplate restTemplate = new RestTemplate();
    /**
     * The service URL.
     */
    private String baseurl = "http://localhost:5000/api";

    /**
     * Mapper.
     */
    @Autowired
    private ObjectMapper jacksonMapper;

    /**
     * Get the VhfDecodedPacket from an hexadecimal String.
     *
     * @param hexpkt
     *            the String
     * @return HTTPResponse
     */
    public HTTPResponse getDecodedPacket(String hexpkt) {
        ResponseEntity<String> response = null;
        try {
            log.trace("Calling REST decoder service for hexa {} using url {}", hexpkt, baseurl);
            response = restTemplate.getForEntity(baseurl + "/decode/vhf/" + hexpkt, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                log.trace("getDecodedPacket received status code {}", response.getStatusCode());
                final String resp = response.getBody();
                log.trace("Decoder server response body : {}", resp);
                return getHttpResponse(resp);
            }
            else {
                log.error("Received error code {} not considered as exception...should not happen",
                        response.getStatusCode());
                log.error(response.getBody());
            }
        }
        catch (final HttpClientErrorException e) {
            log.error("Exception in calling REST method for decoding : {}", e.getStatusCode());
            if (e.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
                log.error(
                        "Received error code when decoding {}: {}, means CRC is wrong, the content will be registered.",
                        hexpkt, e.getStatusCode());
                log.error("Decoder Exception is : {}", e);
                return getHttpResponse(e.getResponseBodyAsString());
            }
        }
        return null;
    }

    /**
     * Convert a JSON String into an HTTPResponse.
     *
     * @param json
     *            the String
     * @return HTTPResponse
     */
    protected HTTPResponse getHttpResponse(String json) {
        try {
            log.trace("Parsing decoded response to extract action, code message and id");
            final JsonNode jsonNode = jacksonMapper.readTree(json);
            final HTTPResponse resp = new HTTPResponse();
            resp.setAction(jsonNode.get("action").textValue());
            resp.setCode(jsonNode.get("code").intValue());

            // Do not interpret the string as a JSON node....
            resp.setMessage(jsonNode.get("message").textValue());
            resp.setId(jsonNode.get("id").textValue());
            log.trace("getHttpResponse: {}", resp);
            return resp;
        }
        catch (final IOException e) {
            log.error("Exception : {}", e.getMessage());
        }
        // Sending back null will be transformed in an error json response upper in the stack.
        return null;
    }

    /**
     * @return the baseurl
     */
    public String getBaseurl() {
        return baseurl;
    }

    /**
     * @param baseurl
     *            the baseurl to set
     */
    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }

}
