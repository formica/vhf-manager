/**
 * 
 */
package fr.svom.vhf.server.externals;

import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.swagger.model.HTTPResponse;

/**
 * An interface for decoding.
 *
 * @author formica
 *
 */
public interface IDecoder {

    /**
     * Decode an hexadecimal String.
     *
     * @param s
     *            the String
     * @return HTTPResponse
     * @throws VhfDecodingException
     *             If a decoding Exception occurred
     */
    HTTPResponse decodeString(String s) throws VhfDecodingException;

    /**
     * Decode a byte array.
     *
     * @param bytes
     *            the byte array
     * @return HTTPResponse
     * @throws VhfDecodingException
     *             If a decoding Exception occurred
     */
    HTTPResponse decodeBytes(byte[] bytes) throws VhfDecodingException;
}
