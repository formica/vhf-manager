package fr.svom.vhf.server.externals;

import io.nats.client.Connection;
import io.nats.client.Nats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * A factory for NATs connections.
 *
 * @author formica
 */
public class NatsConnectionFactory {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(NatsConnectionFactory.class);

    /**
     * The NATs streaming cluster identifier.
     */
    private String cluster = "test-cluster";
    /**
     * The client ID in NATs streaming.
     */
    private String clientId = "spring-vhfmgr";
    /**
     * The default NATs URL.
     */
    private String natsUrl = "nats://svomtest.svom.fr:4222";
    /**
     * The vhf queue.
     */
    private String queue = "test";
    /**
     * The decoded queue. Not used.
     */
    private String vhfDecodedQueue = "test";

    /**
     * NATs connection.
     */
    private Connection conn = null;

    /**
     * Ctor with arguments.
     *
     * @param cluster the cluster name.
     * @param clientId the client ID.
     * @param natsUrl the nats server.
     * @param queue the queue.
     */
    public NatsConnectionFactory(String cluster, String clientId, String natsUrl, String queue) {
        this.cluster = cluster;
        this.clientId = clientId;
        this.natsUrl = natsUrl;
        this.queue = queue;
    }

    /**
     * Initialize the connection factory.
     *
     * @return
     */
    public void initNatsConnectionFactory() {
        log.info(
                "Start NATs connection factory bean...create NATs connection and streaming factory");
        conn = initNatsConnection();
        if (conn == null) {
            log.error("Cannot start the factory, underlying NATs connection is null...");
            return;
        }
    }

    /**
     * Initialize NATs connection.
     *
     * @return Connection
     */
    private Connection initNatsConnection() {
        final io.nats.client.Options options = new io.nats.client.Options.Builder().server(natsUrl)
                .maxReconnects(5).build();
        try {
            return Nats.connect(options);
        }
        catch (final IOException e) {
            log.error("Failed connecting to NATs: IOException => {}", e);
        }
        catch (final InterruptedException e) {
            log.error("Failed connecting to NATs: connection creation, interrupt detected => {}",
                    e.getMessage());
            Thread.currentThread().interrupt();
        }
        return null;
    }

    /**
     * Test if connection is active.
     *
     * @return boolean
     */
    public boolean testConnection() {
        // This returns immediately. The result of the publish can be handled in the ack
        // handler.
        log.info("Test NATS JETstreaming connection...");
        boolean connok = false;
        final Connection natsconn = conn;
        if (natsconn != null) {
            final String urlconn = natsconn.getConnectedUrl();
            if (urlconn != null) {
                log.debug("Underlying NATS connection is active....");
                connok = true;
            }
        }
        log.warn("NATS streaming connection is {}", connok ? "ACTIVE" : "NOT ACTIVE");
        return connok;
    }

    /**
     * Get NATs connection.
     *
     * @return Connection
     */
    public Connection getNatsConnection() {
        if (this.conn == null) {
            this.conn = initNatsConnection();
        }
        return this.conn;
    }

    /**
     * Close JetStreamingConnection.
     *
     * @return
     */
    public void close() {
        try {
            log.info("Closing NATS JETstreaming connection : {}", conn);
            conn.close();
        }
        catch (final InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error("Error closing NATs connection: interrupt detected => {}", e);
        }
    }
}
