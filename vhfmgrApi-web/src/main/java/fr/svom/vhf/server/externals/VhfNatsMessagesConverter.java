/**
 *
 */
package fr.svom.vhf.server.externals;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.messages.model.DataNotification;
import fr.svom.messages.model.DataNotificationContent;
import fr.svom.messages.model.DataProvider;
import fr.svom.messages.model.DataStream;
import fr.svom.messages.model.PipelineStatus;
import fr.svom.messages.model.ProcStatus;
import fr.svom.messages.model.ServiceStatus;
import fr.svom.messages.model.ServiceStatusContent;
import fr.svom.vhf.data.config.VhfProperties;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.server.controllers.ServerEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

/**
 * A service to convert messages from NATs.
 *
 * @author formica
 *
 */
@Service
public class VhfNatsMessagesConverter {

    /**
     * Mapper.
     */
    @Autowired
    private ObjectMapper jacksonMapper;

    /**
     * Environment.
     */
    @Autowired
    private ServerEnvironment serverEnvironment;

    /**
     * Configuraton properties.
     */
    @Autowired
    private VhfProperties cprops;


    /**
     * The product type.
     */
    private static final String PRODUCT_TYPE = "product_type";
    /**
     * The product id.
     */
    private static final String PRODUCT_ID = "product_id";
    /**
     * The obsid.
     */
    private static final String OBSID = "obs_id";
    /**
     * The burstid.
     */
    private static final String BURSTID = "burst_id";
    /**
     * The clientid.
     */
    private static final String CLIENTID = "client_id";
    /**
     * The pkt_since.
     */
    private static final String PKT_SINCE = "pkt_since";
    /**
     * The pkt_until.
     */
    private static final String PKT_UNTIL = "pkt_until";

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfNatsMessagesConverter.class);

    /**
     * Convert a VhfTransferFrame into a DataNotification.
     *
     * @param pkt            the VhfTransferFrame
     * @param datatype            the String
     * @param fromtime            the Long (time in ms)
     * @param clientid the clientid
     * @return DataNotification
     * @throws IOException             If an Exception occurred in the conversion
     */
    public DataNotification createDataNotificationOnFrameList(VhfTransferFrame pkt, String datatype,
                                                              Long fromtime, String clientid) throws IOException {
        final Instant now = Instant.now();
        // Create datanotification.
        final DataNotification datanotification = new DataNotification();
        final DataNotificationContent dn = new DataNotificationContent();
        datanotification.messageDate(now.atOffset(ZoneOffset.UTC));
        // pkt time in seconds, but with the eopch shift to bring it back to a normal date.
        long pktime = pkt.getHeader().getPacketTime() + cprops.getTimeOffset();
        Instant pktdate = Instant.ofEpochSecond(pktime);
        dn.date(pktdate.atOffset(ZoneOffset.UTC));
        dn.productCard(pkt.getApid().getName());
        dn.dataStream(DataStream.VHF);
        dn.dataProvider(DataProvider.VHFDB);
        // Get the burst id: this should be set before the notification
        String burstid = pkt.getBurstId();
        // Use the minimum packet time and the obsid from the packet header.
        final Long mintime = pkt.getHeader().getPacketTime();
        final Long obsid = pkt.getHeader().getPacketObsid();
        String baseurl = serverEnvironment.getBaseurl();
        // Set the packettime search range. Use the satellite convention.
        Long mincut = mintime;
        long maxcut = Math.max(mincut, now.getEpochSecond() - cprops.getTimeOffset());
        if (fromtime != null) {
            mincut = Math.min(fromtime / 1000L, mintime);
        }
        long since = (mincut - 1);
        long until = (maxcut + 600L);
        String timecut = "packettime>" + since + ",packettime<" + until; // default 10 min
        // after now
        final String urlcond = "/vhf/packets?by=apidname:" + datatype + ",obsid:" + obsid
                + "," + timecut;

        dn.setResourceLocator(baseurl + urlcond);
        dn.obsid(obsid.toString());
        // Create the map.
        final Map<String, Object> msgmap = new HashMap<>();
        msgmap.put(PRODUCT_TYPE, "VHF frame list");
        msgmap.put(PRODUCT_ID, pkt.getBinaryHash());
        msgmap.put(OBSID, obsid);
        msgmap.put(BURSTID, burstid);
        msgmap.put(PKT_SINCE, since);
        msgmap.put(PKT_UNTIL, until);
        msgmap.put(CLIENTID, clientid);
        final String msg = jacksonMapper.writeValueAsString(msgmap);
        dn.setMessage(msg);
        datanotification.content(dn);
        return datanotification;
    }

    /**
     * Create a DataNotification for BA.
     * @param pkt
     * @return the DataNotification
     * @throws IOException
     */
    public DataNotification createDataNotificationForBA(VhfTransferFrame pkt) throws IOException {
        final Instant now = Instant.now();
        // Create datanotification.
        final DataNotification datanotification = new DataNotification();
        // Create datanotification content.
        final DataNotificationContent dn = new DataNotificationContent();
        datanotification.messageDate(now.atOffset(ZoneOffset.UTC));
        // Use reception time in the content date. The packet time is stored with the decoded packet.
        // pkt time in seconds
        Instant pktinst = Instant.ofEpochMilli(pkt.getInsertionTime().getTime());
        PacketApid apid = pkt.getApid();
        dn.date(pktinst.atOffset(ZoneOffset.UTC));
        dn.dataProvider(DataProvider.VHFDB);
        dn.dataStream(DataStream.VHF);
        dn.productCard(pkt.getApid().getName());
        // Read the decoded packet as a JsonNode
        final JsonNode jsonNode = jacksonMapper.readTree(pkt.getPacketJson().getPacket());
        // Check the packet type and extract packetnumber.
        // The default is for ECL shadowgram and subimages
        String packetnumkey = "PieceNumber";
        if ("lightcurve".equals(apid.getClassName())) {
            packetnumkey = "LightCurvePacketNumber";
        }
        else if ("spectra".equals(apid.getClassName())) {
            packetnumkey = "SubPktId";
        }
        String burstId = pkt.getBurstId();
        Map<String, Integer> jmap = DataModelsHelper.getPacketNumber(jsonNode, packetnumkey);

        // Build a map with 2 fields: the expected number of packets and the decoded packet.
        Map<String, Object> msgmap = new HashMap<>();
        msgmap.put("expectedPackets", apid.getExpectedPackets());
        msgmap.put("packetNumber", jmap);
        msgmap.put("burstId", burstId);
        final String msg = jacksonMapper.writeValueAsString(msgmap);
        // Dump the message.
        dn.setMessage(msg);
        dn.setObsid(String.valueOf(pkt.getHeader().getPacketObsid()));
        dn.setResourceLocator(pkt.getBinaryHash());
        datanotification.content(dn);
        return datanotification;
    }

    /**
     * Creates the service status.
     *
     * @param content
     *            the content
     * @return the service status
     */
    public ServiceStatus createServiceStatus(String content) {

        final Instant now = Instant.now();
        // Create datanotification.
        //  - service status and content.
        final ServiceStatus serviceStatus = new ServiceStatus();
        final ServiceStatusContent ss = new ServiceStatusContent();
        // - message.
        serviceStatus.messageClass(ServiceStatus.MessageClassEnum.SERVICESTATUS);
        serviceStatus.messageDate(now.atOffset(ZoneOffset.UTC));
        ss.date(now.atOffset(ZoneOffset.UTC));
        ss.info(content);
        // - activity information.
        ss.activity(ProcStatus.COMPLETE);
        ss.status(PipelineStatus.RUNNING);
        ss.serviceDescriptor(serverEnvironment.getDescriptor());
        serviceStatus.content(ss);
        return serviceStatus;
    }

    /**
     * Template method to read a NATs message and convert it to a SvomObject.
     *
     * @param natsmessage
     *            the String
     * @param typename
     *            the String
     * @param ref
     *            the TypeReference<Map<String, T>>
     * @param <T>
     *            the template
     * @return T the Object class used.
     */
    public <T> T getSvomObjectFromMap(String natsmessage, String typename,
                                      TypeReference<Map<String, T>> ref) {
        Map<String, T> amapnot = new HashMap<>();
        try {
            amapnot = jacksonMapper.readValue(natsmessage, ref);
            return amapnot.get(typename);
        }
        catch (final RuntimeException | IOException e) {
            log.error("Error in conversion {}", e.getMessage());
        }
        return null;
    }

    /**
     * Template method to transform a SvomObject into JSON String.
     *
     * @param typename
     *            the String
     * @param clazz
     *            the template class T
     * @param <T>
     *            the template
     * @return String
     */
    public <T> String putSvomObjectInMap(String typename, T clazz) {
        final Map<String, T> amapnot = new HashMap<>();
        try {
            amapnot.put(typename, clazz);
            return jacksonMapper.writeValueAsString(amapnot);
        }
        catch (final RuntimeException | IOException e) {
            log.error("Error in putting svom obj in map {}", e.getMessage());
        }
        return null;
    }

    /**
     * Method to transform a Svom Message Object into JSON String.
     *
     * @param msg
     *            the template message instance
     * @param <T>
     *            the template
     * @return String
     */
    public <T> String writeSvomObjectAsString(T msg) {
        try {
            return jacksonMapper.writeValueAsString(msg);
        }
        catch (final RuntimeException | IOException e) {
            log.error("Error in putting svom obj in JSON string {}", e.getMessage());
        }
        return null;
    }

    /**
     * Template method to transform a JSON String into a SvomObject.
     *
     * @param natsmessage
     *            the String
     * @param clazz
     *            the template class T
     * @param <T>
     *            the template
     * @return T
     */
    @SuppressWarnings("unchecked")
    public <T> T getSvomObject(String natsmessage, T clazz) {
        try {
            clazz = (T) jacksonMapper.readValue(natsmessage, clazz.getClass());
            return clazz;
        }
        catch (final RuntimeException | IOException e) {
            log.error("Error in get object {}", e.getMessage());
        }
        return null;
    }

}
