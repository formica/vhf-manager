package fr.svom.vhf.server.filters;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Autowired;

import fr.svom.vhf.server.controllers.ServerEnvironment;

/**
 * A Filter for CORS.
 *
 * @author formica
 *
 */
@Provider
public class CORSFilter implements ContainerResponseFilter {

    /** The server env. */
    @Autowired
    private ServerEnvironment serverEnv;
    
    /*
     * (non-Javadoc)
     *
     * @see
     * javax.ws.rs.container.ContainerResponseFilter#filter(javax.ws.rs.container.
     * ContainerRequestContext, javax.ws.rs.container.ContainerResponseContext)
     */
    @Override
    public void filter(ContainerRequestContext request, ContainerResponseContext response)
            throws IOException {
        serverEnv.setBaseurl(request.getUriInfo().getBaseUri().toASCIIString());
        response.getHeaders().add("Access-Control-Allow-Origin", "*");
        response.getHeaders().add("Access-Control-Allow-Headers",
                "origin, content-type, accept, authorization, X-VHF-PktFormat, dateformat");
        response.getHeaders().add("Access-Control-Allow-Credentials", "true");
        response.getHeaders().add("Access-Control-Allow-Methods",
                "GET, POST, PUT, DELETE, OPTIONS, HEAD");
    }
}
