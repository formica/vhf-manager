/**
 * 
 */
package fr.svom.vhf.server.filters;

import java.lang.reflect.Method;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.svom.vhf.server.swagger.api.VhfApi;

/**
 * A Filter to log requests.
 *
 * @author formica
 *
 *         This class should be registered with the jersey config. In that case
 *         at the first time the servlet is called it will scan all classes that
 *         need to be filtered using the requested filter. Since the filter is
 *         created here, there is no need to have it registered somewhere else,
 *         nore we need to annotate it.
 *
 *         An alternative to this method is to directly use NameBindings
 *         annotations.
 *
 */
@Provider
public class LoggerFeature implements DynamicFeature {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LoggerFeature.class);

    /*
     * (non-Javadoc)
     *
     * @see javax.ws.rs.container.DynamicFeature#configure(javax.ws.rs.container.
     * ResourceInfo, javax.ws.rs.core.FeatureContext)
     */
    @Override
    public void configure(ResourceInfo ri, FeatureContext ctx) {
        if (resourceFiltered(ri)) {
            final LoggerRequestFilter filter = new LoggerRequestFilter();
            ctx.register(filter);
        }
    }

    /**
     * Check if the resource was filtered.
     *
     * @param ri
     *            the ResourceInfo
     * @return boolean
     */
    private boolean resourceFiltered(ResourceInfo ri) {
        final Class<?> clazz = ri.getResourceClass();
        log.debug("Found resource for class {}", clazz.getName());
        if (clazz.isAssignableFrom(VhfApi.class)) {
            final Method mth = ri.getResourceMethod();
            log.debug("Registering VhfApi for LoggerRequestFilter, intercepting method {}",
                    mth.getName());
            return true;
        }
        return false;
    }
}
