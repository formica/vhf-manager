package fr.svom.vhf.server.filters;

import java.io.IOException;
import java.time.Instant;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.svom.vhf.swagger.model.HTTPResponse;

/**
 * A Filter to log requests.
 *
 * @author formica
 *
 */
public class LoggerRequestFilter implements ContainerRequestFilter, ContainerResponseFilter {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(LoggerRequestFilter.class);

    /**
     * WARN_TIME. Expressed in milliseconds.
     */
    private static final long WARN_TIME = 1000L;

    /**
     * REQUEST start time Header.
     */
    private static final String REQ_TIME = "X-VHF-Req-starttime";
    /**
     * REQUEST total time Header.
     */
    private static final String REQ_TOTTIME = "X-VHF-Req-tottime";

    /**
     * REQUEST parameters Header.
     */
    private static final String REQ_PARAMS = "X-VHF-Req-params";
    /*
     * (non-Javadoc)
     *
     * @see
     * javax.ws.rs.container.ContainerResponseFilter#filter(javax.ws.rs.container.
     * ContainerRequestContext, javax.ws.rs.container.ContainerResponseContext)
     */
    @Override
    public void filter(ContainerRequestContext requestContext,
            ContainerResponseContext responseContext) throws IOException {
        try {
            Long reqt = -1L;
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();
            // Check starting time of this request.
            if (headers != null) {
                final String start = headers.get(REQ_TIME).get(0);
                final Long ts = Long.valueOf(start);
                final Instant now = Instant.now();
                final Long tn = now.toEpochMilli();
                // Comput the total request time.
                reqt = tn - ts; 
                headers.add(REQ_TOTTIME, "time-req: " + reqt);
                // The request time is large.
                if (reqt > WARN_TIME) {
                    log.warn("Exceeded warn level for time....{}", reqt);
                    final String params = headers.get(REQ_PARAMS).get(0);
                    log.warn("Large time spent : {} millisec for {}", reqt, params);
                }
            }
            final Object resp = responseContext.getEntity();
            String msg = "";
            // Get the information from the response entity.
            if (resp instanceof HTTPResponse) {
                msg = "ID=" + ((HTTPResponse) resp).getId() + ";"
                        + ((HTTPResponse) resp).getMessage();
            }
            // Log the times.
            log.info("{};REQTIME={}", msg, reqt);
        }
        catch (final RuntimeException e) {
            log.error("Exception : {}", e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.ws.rs.container.ContainerRequestFilter#filter(javax.ws.rs.container.
     * ContainerRequestContext)
     */
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("User: ")
                .append(requestContext.getSecurityContext().getUserPrincipal() == null ? "unknown"
                        : requestContext.getSecurityContext().getUserPrincipal());
        sb.append(" - Path: ").append(requestContext.getUriInfo().getPath());
        sb.append(" - Header: ").append(requestContext.getHeaders());
        final MultivaluedMap<String, String> queryparams = requestContext.getUriInfo()
                .getQueryParameters();
        final StringBuilder b = new StringBuilder();
        // Loop over params to dump the query as seen by the filter.
        for (final String param : queryparams.keySet()) {
            b.append(param + "=");
            b.append(queryparams.getFirst(param) + ",");
        }
        final Instant now = Instant.now();
        final Long tn = now.toEpochMilli();
        // Trace time information for requests.
        log.trace("REQ START TIME: {}", tn);
        requestContext.getHeaders().add(REQ_TIME, tn.toString());
        requestContext.getHeaders().add(REQ_PARAMS, b.toString());
        log.trace("HTTP REQUEST : {}", sb);
    }
}
