/**
 * 
 */
package fr.svom.vhf.server.security;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

/**
 * A Configuration class for Authentication.
 *
 * @author formica
 *
 */
@Profile({"x509"})
@Configuration
public class AuthenticationProviderConfig {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(AuthenticationProviderConfig.class);

    /**
     * Get UserDetails from database.
     *
     * @param ds
     *            the DataSource
     * @return UserDetailsService
     */
    @Bean(name = "userDetailsService")
    public UserDetailsService userDetails(@Autowired DataSource ds) {
        final JdbcDaoImpl jdbc = new JdbcDaoImpl();
        // Set the datasource
        jdbc.setDataSource(ds);
        // The SQL for username.
        jdbc.setUsersByUsernameQuery(
                "select svom_usrname as username, svom_usrpss as password, 1 from SVOM_USERS where svom_usrname=?");
        // The SQL for authorities.
        jdbc.setAuthoritiesByUsernameQuery(
                "select u.svom_usrname as username, r.svom_usrrole as authority from SVOM_USERS u, SVOM_ROLES r "
              + " where u.svom_usrid=r.svom_usrid and u.svom_usrname=?");
        return jdbc;
    }

    /**
     * Get USerDetails from certificates.
     *
     * @return UserDetailsService
     */
    @Bean(name = "userDetailsX509Service")
    public UserDetailsService userDetailsService() {
        // An implementation on one method only.
        return username -> {
            log.debug("Try to get user details for {}", username);
            if (username.startsWith("STSVOM")) {
                return new User(username, "",
                        AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));
            }
            return null;
        };
    }
}
