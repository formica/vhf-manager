/**
 * 
 */
package fr.svom.vhf.server.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;

import fr.svom.vhf.data.config.VhfProperties;

/**
 * Configuration for security.
 *
 * @author formica
 *
 */
@Profile({"x509"})
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityX509Config extends WebSecurityConfigurerAdapter {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SecurityX509Config.class);

    /**
     * Configuraton properties.
     */
    @Autowired
    private VhfProperties cprops;

    /**
     * UserDetails.
     */
    @Autowired
    @Qualifier(value = "userDetailsX509Service")
    private UserDetailsService userDetailsService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.config.annotation.web.configuration.
     * WebSecurityConfigurerAdapter#configure(org.springframework.security.config.
     * annotation.web.builders.HttpSecurity)
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.debug("Configure http security rules");
        log.debug("Use vhf properties {}", cprops);
        // Start checking security level.
        if ("active".equals(cprops.getSecurity())) {
            // Security is active.
            // Define roles for POST and DELETE.
            http.authorizeRequests().antMatchers(HttpMethod.GET, "/**").hasRole("user")
                    .antMatchers(HttpMethod.POST, "/**").hasRole("VHF")
                    .antMatchers(HttpMethod.DELETE, "/**").hasRole("GURU").and().httpBasic().and()
                    .csrf().disable();
        }
        else if ("none".equals(cprops.getSecurity())) {
            // Security is none.
            log.info("No security enabled for this server....");
            http.authorizeRequests().antMatchers("/**").permitAll().and().httpBasic().disable().csrf()
                    .disable();
        }
        else if ("reco".equals(cprops.getSecurity())) {
            // Security is reco. No DELETE, POST or PUT are allowed.
            // No DELETE, POST or PUT are allowed.
            http.authorizeRequests().antMatchers(HttpMethod.POST, "/**").denyAll()
                    .antMatchers(HttpMethod.PUT, "/**").denyAll()
                    .antMatchers(HttpMethod.DELETE, "/**").denyAll().and().httpBasic().and().csrf()
                    .disable();
        }
        else if ("weak".equals(cprops.getSecurity())) {
            // Security is weak.
            // DELETE, POST or PUT are allowed in certain roles.
            log.info("Low security enabled for this server....");
            http.authorizeRequests().antMatchers(HttpMethod.DELETE, "/**").hasRole("GURU")
                    .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                    .antMatchers(HttpMethod.HEAD, "/**").permitAll()
                    .antMatchers(HttpMethod.GET, "/**").permitAll()
                    .antMatchers(HttpMethod.POST, "/**").permitAll()
                    .antMatchers(HttpMethod.DELETE, "/admin/**").hasRole("GURU")
                    .antMatchers(HttpMethod.PUT, "/admin/**").hasRole("GURU").and().httpBasic()
                    .and().csrf().disable();
        }
        else if ("x509".equals(cprops.getSecurity())) {
            // Security is x509. POST should be authenticated.
            // This is the official way with certificate delivered by CNES.
            log.info("x509 security enabled for this server....");
            http.csrf().disable().sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                    .authorizeRequests().antMatchers(HttpMethod.POST, "/api/vhf").authenticated()
                    .antMatchers(HttpMethod.GET, "/**").permitAll().and().requiresChannel()
                    .antMatchers(HttpMethod.POST, "/api/apids").requiresInsecure().and().x509()
                    .subjectPrincipalRegex("CN=(.*?)(?:,|$)")
                    .userDetailsService(userDetailsService);
        }
    }
}
