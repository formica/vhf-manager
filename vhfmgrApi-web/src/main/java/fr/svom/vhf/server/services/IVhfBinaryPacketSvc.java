package fr.svom.vhf.server.services;

import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfTransferFrame;

import java.io.InputStream;

/**
 * Interface for binary services.
 *
 * @author formica
 */
public interface IVhfBinaryPacketSvc {

    /**
     * Save Inputstream as an hexadecimal String.
     *
     * @param uploadedInputStream the InputStream
     * @param format              the String
     * @return String
     */
    String saveStreamInHexa(InputStream uploadedInputStream, String format);

    /**
     * Store VhfBinaryPacket contained in a VhfTransferFrame. This method is called immediately when we have
     * the raw packet. It has to be called before trying to store a frame. Internally it modifies the input frame
     * to keep track of the steps in framestatus.
     *
     * @param vhftf the VhfTransferFrame
     * @return VhfTransferFrame
     */
    VhfTransferFrame storeBinaryVhfPacket(VhfTransferFrame vhftf);

    /**
     * Check duplicated for VhfBinaryPacket.
     *
     * @param entity the VhfBinaryPacket
     * @return VhfBinaryPacket
     */
    VhfBinaryPacket checkDuplicates(VhfBinaryPacket entity);

    /**
     * Deserialize byte array into VhfTransferFrame.
     *
     * @param data the byte array
     * @return VhfTransferFrame
     */
    VhfTransferFrame deserialize(byte[] data);

}
