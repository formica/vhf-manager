/**
 *
 */
package fr.svom.vhf.server.services;

import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.repositories.IJdbcRepository;
import fr.svom.vhf.swagger.model.ObsIdApidCountDto;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * A Service to manage ObsIdApidCount.
 *
 * @author formica
 *
 */
@Service
public class ObsIdApidCountService {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ObsIdApidCountService.class);
    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;
    /**
     * Repository.
     */
    @Autowired
    @Qualifier("jdbcrepo")
    private IJdbcRepository jdbcrepo;

    /**
     * Find a list of counters for a given obsid and apid, and in a time range. The
     * time should be provided in seconds because they are used to query on packetTime.
     *
     * @param apid
     *            the String
     * @param product the String
     * @param obsid
     *            the Long
     * @param from
     *            the Long
     * @param to
     *            the Long
     * @return List of ObsIdApidCountDto
     */
    public List<ObsIdApidCountDto> findObsIdCount(String apid, String product, Long obsid, Long from, Long to) {
        log.debug("Find obsid count for apid {}", apid);
        if (from == null || to == null || apid == null || obsid == null) {
            throw new VhfCriteriaException("Cannot count obsid with null search params");
        }
        return jdbcrepo.getCountObsIdSummaryInfo(apid, product, obsid, from, to);
    }

    /**
     * Find total packet count.
     *
     * @param from the from
     * @param to the to
     * @return the long
     */
    public Long findTotalPacketCount(Long from, Long to) {
        try {
            return jdbcrepo.getPacketCountInTimeRange(from, to);
        }
        catch (final RuntimeException e) {
            log.error("Cannot find pkt count for range {} {} : {}", from, to, e);
        }
        return 0L;
    }

    /**
     * Find obsid list.
     *
     * @param from the Long
     * @param to the Long
     * @return List<Map < String, Object>>
     */
    public List<Map<String, Object>> findObsidList(Long from, Long to) {
        if (from == null || to == null) {
            throw new VhfCriteriaException("Cannot check obsid in time range with null values");
        }
        return jdbcrepo.getObsidListInTimeRange(from, to);
    }

    /**
     * Delete the packets related to the obsid provided.
     * @param obsid
     * @return number of deleted packets.
     * @throws AbstractCdbServiceException
     */
    public Integer deletePacketsForObsid(Long obsid) throws AbstractCdbServiceException {
        return jdbcrepo.deletePacketsByObsid(obsid);
    }
}
