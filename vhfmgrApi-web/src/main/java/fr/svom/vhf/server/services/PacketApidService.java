/**
 *
 */
package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.AlreadyExistsPojoException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Service to manage PacketApid.
 *
 * @author aformic
 *
 */
@Service
@Slf4j
public class PacketApidService {

    /**
     * Repository.
     */
    @Autowired
    private PacketApidRepository packetApidRepository;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Register a new APID.
     *
     * @param entity
     *            the PacketApid
     * @return PacketApid
     * @throws AbstractCdbServiceException
     *             If a service Exception occurred
     */
    @Transactional
    public PacketApid registerApid(PacketApid entity) throws AbstractCdbServiceException {
        // Check if APID id is there.
        log.debug("Create APID: {}", entity);
        PacketApid exists = packetApidRepository.findById(entity.getApid()).orElse(null);
        if (exists != null) {
            throw new AlreadyExistsPojoException("APID already exists: " + entity.getApid());
        }
        // The APID is saved.
        final PacketApid saved = packetApidRepository.save(entity);
        log.trace("Saved APID entity: {}", saved);
        return saved;
    }

    /**
     * Find a PacketApid using an apid int identifier.
     *
     * @param apid
     *            int
     * @return PacketApid
     * @throws AbstractCdbServiceException
     *             If a service Exception occurred
     */
    public PacketApid findApid(int apid) throws AbstractCdbServiceException {
        // Search using apid.
        return packetApidRepository.findById(apid).orElseThrow(
                () -> new NotExistsPojoException("Cannot find APID for " + apid)
        );
    }

    /**
     * Update PacketApid.
     *
     * @param packetapid
     *            the PacketApid
     * @return PacketApid
     * @throws AbstractCdbServiceException
     *             If a service Exception occurred
     */
    public PacketApid updateApid(PacketApid packetapid)
            throws AbstractCdbServiceException {
        log.debug("Search for Packet Apid {} in order to update it", packetapid);
        // Search using apid.
        PacketApid tbu = findApid(packetapid.getApid());
        // Start updating.
        if (packetapid.getExpectedPackets() != null
            && packetapid.getExpectedPackets() > 0) {
            // Expected packets.
            tbu.setExpectedPackets(packetapid.getExpectedPackets());
        }
        if (packetapid.getPriority() != null && packetapid.getPriority() > 0) {
            // Priority.
            tbu.setPriority(packetapid.getPriority());
        }
        if (packetapid.getName() != null && !packetapid.getName().isEmpty()) {
            // Name.
            tbu.setName(packetapid.getName());
        }
        if (packetapid.getDescription() != null
            && !packetapid.getDescription().isEmpty()) {
            // Description.
            tbu.setDescription(packetapid.getDescription());
        }
        if (packetapid.getInstrument() != null && !packetapid.getInstrument().isEmpty()) {
            tbu.setInstrument(packetapid.getInstrument());
        }
        if (packetapid.getAlias() != null && !packetapid.getAlias().isEmpty()) {
            tbu.setAlias(packetapid.getAlias());
        }
        if (packetapid.getDetails() != null && !packetapid.getDetails().isEmpty()) {
            tbu.setDetails(packetapid.getDetails());
        }
        if (packetapid.getDestination() != null && !packetapid.getDestination().isEmpty()) {
            tbu.setDestination(packetapid.getDestination());
        }
        final PacketApid upd = packetApidRepository.save(tbu);
        log.trace("Apid updated: {}", upd);
        return upd;
    }

    /**
     * Find all PacketApid using criteria in input.
     *
     * @param qry
     *            the Predicate
     * @param req
     *            the Pageable
     * @return Page of PacketApid
     */
    public Page<PacketApid> findAllPacketApids(Predicate qry, Pageable req) {
        Page<PacketApid> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = packetApidRepository.findAll(req);
        }
        else {
            entitylist = packetApidRepository.findAll(qry, req);
        }
        log.trace("Retrieved list of apids {}", entitylist);
        return entitylist;
    }

}
