package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfDeserializeException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.helpers.HashGenerator;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.VhfBinaryPacketRepository;
import fr.svom.vhf.data.serialization.converters.VhfTransferFrameDeserializer;
import fr.svom.vhf.server.swagger.api.impl.HeaderFormat;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service to manage VhfBinaryPacket.
 *
 * @author formica
 */
@Service
public class VhfBinaryPacketService implements IVhfBinaryPacketSvc {

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Repository.
     */
    @Autowired
    private VhfBinaryPacketRepository vhfBinaryRepository;

    /**
     * Deserializer.
     */
    @Autowired
    private VhfTransferFrameDeserializer vhfTransferFrameDeserializer;

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfBinaryPacketService.class);
    /**
     * Logger.
     */
    private static final Logger logger = LoggerFactory.getLogger("fr.svom.vhf.server.rawdata");

    /**
     * VHF total length.
     */
    private static final int VHF_LEN = 128;
    /**
     * CCSDS length.
     */
    private static final int CCSDS_LEN = 100;
    /**
     * Packet length.
     */
    private static final int PKT_LEN = 94;

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.server.services.IVhfBinaryPacketSvc#storeBinaryVhfPacket(
     * VhfTransferFrame)
     */
    @Override
    @Transactional
    public VhfTransferFrame storeBinaryVhfPacket(VhfTransferFrame vhftf) {
        VhfBinaryPacket binentity = vhftf.getPacket();
        String status = vhftf.getFrameStatus();
        // Check is duplicate.
        VhfBinaryPacket isdup = checkDuplicates(binentity);
        if (isdup == null) {
            // it is not a duplicate.
            log.trace("Store new binary packet {}", binentity);
            binentity.setPktformat("HEX");
            VhfBinaryPacket saved = vhfBinaryRepository.save(binentity);
            vhftf.setPacket(saved);
            vhftf.setIsFrameDuplicate(Boolean.FALSE);
        }
        else {
            // it is a duplicate. Get the original frame id.
            log.warn("Frame has binary duplicate");
            vhftf.setIsFrameDuplicate(Boolean.TRUE);
            vhftf.setPacket(isdup);
        }
        // If this status is not present something was wrong with the storage of the binary.
        vhftf.setFrameStatus(status + ",BinaryOK");

        // Just to be sure the frame has now the correct hash id.
        vhftf.setBinaryHash(binentity.getHashId());
        log.trace("Stored binary packet, frame updated : {}", vhftf);
        return vhftf;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.services.IVhfBinaryPacketSvc#saveStreamInHexa(java.io.
     * InputStream, java.lang.String)
     */
    @Override
    public String saveStreamInHexa(InputStream uploadedInputStream, String format) {
        // Set binary in case of null format
        if (format == null) {
            format = HeaderFormat.STREAM_FORMAT_BINARY.getFormat();
        }
        // Check format
        if (format.equals(HeaderFormat.STREAM_FORMAT_HEXA.getFormat())) {
            // Hexa string
            try (BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(uploadedInputStream))) {
                // Read file and join all lines.
                final String data = buffer.lines().collect(Collectors.joining("\n"));
                log.trace("Received hexa string : {}", data);
                logger.info("Received(Hexa): {}", data);
                return data;
            }
            catch (final IOException e) {
                throw new VhfDeserializeException("Cannot create string from HEXA format", e);
            }
        }
        else if (format.equals(HeaderFormat.STREAM_FORMAT_BINARY.getFormat())) {
            // Binary
            try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
                // Generate the hash for reference to this.
                // Are we sure about hash collisions probability on this kind of small packets ?
                HashGenerator.hashoutstream(uploadedInputStream, out);
                final byte[] binpkt = out.toByteArray();
                // Check length.
                if (binpkt.length != PKT_LEN && binpkt.length != CCSDS_LEN
                    && binpkt.length != VHF_LEN) {
                    log.error("PKTLEN={}"
                              + ";MSG=Packet length is unknown...impredictible results from now on",
                            binpkt.length);
                }
                // Hexa version of the binary
                final String gshex = DatatypeConverter.printHexBinary(binpkt);
                log.trace("Received binary array : {}", gshex);
                final String hexa = Hex.encodeHexString(binpkt);
                logger.info("Received(Bin): {}", hexa);
                return hexa;
            }
            catch (IOException | NoSuchAlgorithmException e) {
                throw new VhfDeserializeException("Cannot create string from BINARY format", e);
            }
        }
        throw new VhfDeserializeException("Unknown data format: " + format);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.services.IVhfBinaryPacketSvc#checkDuplicates(fr.svom.vhf.
     * data.packets.VhfBinaryPacket)
     */
    @Override
    public VhfBinaryPacket checkDuplicates(VhfBinaryPacket entity) {
        final String hash = entity.getHashId();
        log.trace("Checking duplicates for hash {}", hash);
        final Optional<VhfBinaryPacket> existing = vhfBinaryRepository.findById(hash);
        if (existing.isPresent()) {
            log.warn("Found an existing packet with hash {} "
                     + " ... sending back the existing binary packet...", hash);
            return existing.get();
        }
        log.trace(".....no duplicate found");
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.server.services.IVhfBinaryPacketSvc#deserialize(byte[])
     */
    @Override
    public VhfTransferFrame deserialize(byte[] data) throws VhfServiceException {
        log.trace("Deserialize binary packet data of length {}", data.length);
        VhfTransferFrame tf = vhfTransferFrameDeserializer.deserialize(data);
        if (tf == null) {
            log.warn("binary data cannot not be parsed....returning null");
        }
        return tf;
    }


    /**
     * Get a VhfBinaryPacket by hash.
     *
     * @param hash the String
     * @return VhfBinaryPacket
     * @throws AbstractCdbServiceException If a service Exception occurred
     */
    public VhfBinaryPacket getVhfBinaryPacketByHash(String hash)
            throws AbstractCdbServiceException {
        return vhfBinaryRepository.findById(hash).orElseThrow(
                () -> new NotExistsPojoException("Cannot find binary packet for hash " + hash));
    }

    /**
     * Find all VhfBinaryPacket using criteria in input.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page of VhfBinaryPacket
     * @throws VhfServiceException If a service Exception occurred
     */
    public Page<VhfBinaryPacket> findAllVhfBinaryPackets(Predicate qry, Pageable req)
            throws VhfServiceException {
        Page<VhfBinaryPacket> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfBinaryRepository.findAll(req);
        }
        else {
            entitylist = vhfBinaryRepository.findAll(qry, req);
        }
        log.trace("Retrieved list of binary packets {}", entitylist);
        return entitylist;
    }

}
