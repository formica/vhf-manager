package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.repositories.VhfBurstIdRepository;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Service to manage VhfBurstId.
 *
 * @author formica
 */
@Service
public class VhfBurstIdService {

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Repository.
     */
    @Autowired
    private VhfBurstIdRepository vhfBurstIdRepository;

    /**
     * Service.
     */
    @Autowired
    private VhfDecodedPacketService vhfDecodedPacketService;

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfBurstIdService.class);


    /**
     * Create or update a burst ID.
     *
     * @param svomburst
     * @return VhfBurstId
     */
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public VhfBurstId storeOrUpdateVhfBurstId(VhfBurstId svomburst) {
        VhfBurstId saved = null;
        Optional<VhfBurstId> optburst = vhfBurstIdRepository.findById(svomburst.getBurstId());
        if (optburst.isPresent()) {
            VhfBurstId tbu = optburst.get();
            if (svomburst.getEclairObsid() > 0) {
                tbu.setAlertTimeTb(svomburst.getAlertTimeTb());
                tbu.setEclairObsid(svomburst.getEclairObsid());
                saved = vhfBurstIdRepository.save(tbu);
            }
        }
        else {
            saved = vhfBurstIdRepository.save(svomburst);
        }
        return saved;
    }

    /**
     * Get a VhfBinaryPacket by hash.
     *
     * @param id the String
     * @return VhfBurstId
     * @throws AbstractCdbServiceException If a service Exception occurred
     */
    public VhfBurstId getVhfBurstById(String id)
            throws AbstractCdbServiceException {
        log.debug("Search for VHF burst by id {}", id);
        return vhfBurstIdRepository.findById(id).orElseThrow(
                () -> new NotExistsPojoException("Cannot find burst with id " + id)
        );
    }

    /**
     * Find all VhfBurstId using criteria in input.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page of VhfBurstId
     * @throws VhfServiceException If a service Exception occurred
     */
    public Page<VhfBurstId> findAllVhfBurstIds(Predicate qry, Pageable req)
            throws VhfServiceException {
        Page<VhfBurstId> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfBurstIdRepository.findAll(req);
        }
        else {
            entitylist = vhfBurstIdRepository.findAll(qry, req);
        }
        log.debug("Retrieved list of vhf burstids {}", entitylist);
        return entitylist;
    }

    /**
     * Get a list of burst ID matching with the input arguments.
     *
     * @param packettime
     * @param obsid
     * @param mode
     * @return List<VhfBurstId>
     */
    public List<VhfBurstId> getLastByTimeAndObsid(Long packettime, Long obsid, String mode) {
        List<VhfBurstId> entityList = null;
        // First check utilize the obsid
        if (("lightcurve".equalsIgnoreCase(mode) || "alert".equalsIgnoreCase(mode))) {
            // If no bursts are found, attempt with the time only
//                entityList = vhfBurstIdRepository.findByTimeWindow(packettime, 900L);
            // Use also obsid, this is useful while testing. We may generate alert packets
            // which are outside the short time window. So we use ECL obsid in order to see
            // if a burst was already created. We allow in the request to find an existing burst
            // also if eclairObsid of the burst stored is 0. 
            entityList = vhfBurstIdRepository.findByTimeWindowAndObsid(packettime, 900L, obsid);
        }
        else {
            entityList = vhfBurstIdRepository.findByTimeAndObsid(packettime, obsid);
        }
        return entityList;
    }

    /**
     * Get a list of burst ID matching with the input arguments. The burst ID alert time should be
     * greater the specified time. This method is useful to get a list of last burst IDs.
     *
     * @param packettime
     * @return List<VhfBurstId>
     */
    public List<VhfBurstId> getLastByTime(Long packettime) {
        List<VhfBurstId> entityList = null;
        Pageable topTwenty = PageRequest.of(0, 20);
        return vhfBurstIdRepository.findByTimeGreaterThan(packettime, topTwenty);
    }
}
