/**
 *
 */
package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import fr.svom.vhf.data.config.VhfProperties;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.VhfDecodedPacketRepository;
import fr.svom.vhf.server.externals.DecodedMessageType;
import fr.svom.vhf.server.externals.IDecoder;
import fr.svom.vhf.swagger.model.HTTPResponse;
import fr.svom.vhf.swagger.model.VhfDecodedPacketDto;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * A service to manage VhfDecodedPacket.
 *
 * @author formica
 *
 */
@Service
public class VhfDecodedPacketService {

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Repository.
     */
    @Autowired
    private VhfSatelliteService vhfSatelliteService;

    /**
     * Repository.
     */
    @Autowired
    private VhfDecodedPacketRepository vhfDecodedPacketRepository;

    /**
     * Decoder. This object is defined via Spring profile in the configuration Bean.
     */
    @Autowired
    @Qualifier("decoder")
    private IDecoder svomdecoder;

    /**
     * Configuraton properties.
     */
    @Autowired
    private VhfProperties cprops;

    /**
     * String.
     */
    private static final String INSTRUMENT_MODE = "InstrumentMode";

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfDecodedPacketService.class);

    /**
     * Resource bundle.
     */
    private final ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("US"));

    /**
     * Error message.
     */
    private static final String ERROR_MSG = "error.decodedpacket.service";

    /**
     * Method called after the construction of the object.
     */
    @PostConstruct
    private void postConstruct() {
        DataModelsHelper.setTimeOffset(cprops.getTimeOffset());
    }

    /**
     * Find a list of VhfDecodedPacket using format and time.
     *
     * @param format
     *            the String
     * @param since
     *            the Timestamp
     * @return List of VhfDecodedPacket
     */
    public List<VhfDecodedPacket> findDecodedPacketByFormat(String format, Timestamp since) {
        String defformat = DecodedMessageType.TO_BE_DECODED.getTypename();
        if (format != null && !"none".equals(format)) {
            defformat = format;
        }
        final Instant runtime = Instant.now();
        final Timestamp now = new Timestamp(runtime.toEpochMilli());
        return vhfDecodedPacketRepository.findByFormatDecodedPackets(defformat, since, now);
    }

    /**
     * Decode the packet represented as a String.
     *
     * @param data
     *            the String
     * @return String
     * @throws AbstractCdbServiceException
     *             If a service Exception occurred
     */
    public HTTPResponse decodePacket(String data) throws AbstractCdbServiceException {
        HTTPResponse jsondecoded = svomdecoder.decodeString(data);
        log.debug("Decoded packet received from server : http response is {}", jsondecoded.getCode());
        return jsondecoded;
    }

    /**
     * Get a VhfDecodedPacketDto by hash.
     *
     * @param hash
     *            the String
     * @return VhfDecodedPacketDto
     * @throws AbstractCdbServiceException
     *             If a service Exception occurred
     */
    public VhfDecodedPacketDto getVhfDecodedPacketByHash(String hash)
            throws AbstractCdbServiceException {
        log.trace("Search getVhfDecodedPacketByHash by hash {}", hash);
        final VhfDecodedPacket entity = findVhfDecodedPacketByHash(hash);
        return mapper.map(entity, VhfDecodedPacketDto.class);
    }

    /**
     * Get a VhfDecodedPacket by hash.
     *
     * @param hash
     *            the String
     * @return VhfDecodedPacket
     * @throws AbstractCdbServiceException
     *             If a service Exception occurred
     */
    public VhfDecodedPacket findVhfDecodedPacketByHash(String hash)
            throws AbstractCdbServiceException {
        log.trace("Search findVhfDecodedPacketByHash by hash {}", hash);
        return vhfDecodedPacketRepository.findById(hash).orElseThrow(
                () -> new NotExistsPojoException(bundle.getString("log.packet.notfound") + hash)
        );
    }

    /**
     * Convert byte array and hash into a VhfDecodedPacket.
     *
     * @param data
     *            the byte array
     * @param hashid
     *            the String
     * @return String
     * @throws VhfDecodingException
     *             If a service decoding Exception occurred
     */
    public String decodeBytes(byte[] data, String hashid) throws VhfDecodingException {
        // Check if data and hash are null
        if (data == null || hashid == null) {
            throw new VhfDecodingException("Cannot decode null values");
        }
        log.trace("Try to decode binary array of size {}", data.length);
        // Check if hash exists.
        if (vhfDecodedPacketRepository.existsById(hashid)) {
            return vhfDecodedPacketRepository
                    .findById(hashid).orElseThrow(
                            () -> new VhfServiceException("Hash should exists but packet cannot be found: " + hashid)
                    ).getPacket();
        }
        // Call the decoding service.
        // For the moment it is an external REST service (H.Louvin)
        HTTPResponse resp = svomdecoder.decodeBytes(data);
        String jsondecoded = resp.getMessage();
        log.trace("decodeBytes(): Decoded packet for hash={} is {}", hashid, jsondecoded);
        return jsondecoded;
    }

    /**
     * Save or update the VhfDecodedPacket represented by hash.
     *
     * @param entity
     *            the VhfDecodedPacket
     * @return VhfDecodedPacket
     */
    @Transactional
    public VhfDecodedPacket saveOrUpdateDecodedPacket(VhfDecodedPacket entity) {
        final Optional<VhfDecodedPacket> tobeupdated = vhfDecodedPacketRepository
                .findById(entity.getHashId());
        VhfDecodedPacket tbu = null;
        // The decoded packet exists, then just update it.
        if (tobeupdated.isPresent()) {
            tbu = tobeupdated.get();
            if (DecodedMessageType.TO_BE_DECODED.getTypename().equals(tbu.getPktformat())) {
                tbu.setPacket(entity.getPacket());
                tbu.setPktformat("JSON");
                tbu.setPkttype(entity.getPkttype());
                tbu.setSize((long) entity.getPacket().length());
            }
            else {
                log.warn("Cannot update existing decoded packet: status is {}", tbu.getPktformat());
            }
        }
        else {
            // The decoded packet does not exists.
            log.warn("The decoded packet does not exists: create a new one using {}", entity);
            entity.setSize((long) entity.getPacket().length());
            tbu = entity;
        }
        // Save the decoded packet.
        final VhfDecodedPacket saved = vhfDecodedPacketRepository.save(tbu);
        log.trace("Saved decoded packet {}", saved);
        return saved;
    }

    /**
     * Find all VhfDecodedPacket using criteria in input.
     *
     * @param qry
     *            the Predicate
     * @param req
     *            the Pageable
     * @return Page of VhfDecodedPacket
     * @throws VhfServiceException
     *             If a service Exception occurred
     */
    public Page<VhfDecodedPacket> findAllVhfDecodedPackets(Predicate qry, Pageable req)
            throws VhfServiceException {
        Page<VhfDecodedPacket> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfDecodedPacketRepository.findAll(req);
        }
        else {
            entitylist = vhfDecodedPacketRepository.findAll(qry, req);
        }
        log.trace("Retrieved list of decoded packets {}", entitylist);
        return entitylist;
    }

    /**
     * Decode ccsds header.
     *
     * @param decpkt the decpkt
     * @param pktheader the pktheader
     * @return the primary header packet
     */
    public PrimaryHeaderPacket decodeCcsdsHeader(VhfDecodedPacket decpkt, PrimaryHeaderPacket pktheader) {
        final Map<String, String> decfields = DataModelsHelper
                .parseCcsds(decpkt.getPacket());

        if (decfields.containsKey(INSTRUMENT_MODE)) {
            String im = decfields.get(INSTRUMENT_MODE);
            if (im != null && !"none".equals(im)) {
                pktheader.setInstrumentMode(Integer.valueOf(im));
            }
            else {
                pktheader.setInstrumentMode(-1);
            }
        }
        if (decfields.containsKey("VhfIObsType")) {
            pktheader.setPacketObsidNum(Integer.valueOf(decfields.get("VhfNObsNumber")));
            pktheader.setPacketObsidType(Integer.valueOf(decfields.get("VhfIObsType")));
            //pktheader.setPacketObsid(Long.valueOf(decfields.get("VhfNObsNumber")));
            log.warn("Modify obsid in primary header using decoded packet");
        }
        else {
            // Reset obsid when it is missing from decoded packet.
            pktheader.setPacketObsid(-1L);
            pktheader.setPacketObsidType(-1);
            pktheader.setPacketObsidNum(-1);
            log.warn("Modify primary header using obsid -1 because this is not an AlertSequence packet");
        }
        return pktheader;
    }

    /**
     * Parse the attitude from the decoded packet.
     *
     * @param decpkt
     * @param apid
     * @return VhfSatAttitude
     */
    public VhfSatAttitude decodeAttitude(VhfDecodedPacket decpkt, PacketApid apid) {
        log.warn("Attitude is present...search values");
        VhfSatAttitude attitude = DataModelsHelper.parseAttitude(decpkt.getPacket());
        if (attitude != null) {
            attitude.setApidName(apid.getName());
        }
        return attitude;
    }

    /**
     * Parse the position from the decoded packet.
     *
     * @param decpkt
     * @param apid
     * @return VhfSatPosition
     */
    public VhfSatPosition decodePosition(VhfDecodedPacket decpkt, PacketApid apid) {
        log.warn("Position is present...search values");
        VhfSatPosition position = DataModelsHelper.parsePosition(decpkt.getPacket());
        if (position != null) {
            position.setApidName(apid.getName());
        }
        return position;
    }

    /**
     * Decode the light curve or the alert to create a burst ID.
     *
     * @param frame the VhfTransferFrame
     * @return VhfBurstId
     */
    public VhfBurstId createBurstIdFromPacket(VhfTransferFrame frame) {
        // Extract attitude if present
        PacketApid apid = frame.getApid();
        final String classname = apid.getClassName();
        if ("alert".equalsIgnoreCase(classname) || "lightcurve".equalsIgnoreCase(classname)) {
            log.info("Create burstid for packets of type alert or lightcurve: {} {}", frame.getFrameId(), classname);
            VhfDecodedPacket decpkt = frame.getPacketJson();
            VhfBurstId entity = DataModelsHelper.parseAlertT0(decpkt.getPacket(), apid.getName());
            entity.setPacketTime(frame.getHeader().getPacketTime());
            if (apid.getName().startsWith("TmVhfEcl")) {
                entity.setEclairObsid(frame.getHeader().getPacketObsid());
            }
            entity.setFrame(frame);
            return entity;
        }
        // Cannot decode this frame to extract a burst ID.
        log.warn("Cannot create a burst ID from frame {}", frame.getFrameId());
        return null;
    }

}
