package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.AlreadyExistsPojoException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * A service for managing VHF ground services.
 *
 * @author formica
 */
@Service
public class VhfGroundStationService {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfGroundStationService.class);

    /**
     * Repository.
     */
    @Autowired
    private VhfGroundStationRepository vhfGroundStationRepository;

    /**
     * Resource bundle.
     */
    private final ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("US"));

    /**
     * Error message.
     */
    private static final String ERROR_MSG = "error.groundstation.service";
    /**
     * Log message.
     */
    private static final String NOT_FOUND_MSG = "log.gs.notfound";

    /**
     * Get a VhfGroundStation by ID.
     *
     * @param id the Integer representing the station ID
     * @return VhfGroundStation or null
     * @throws AbstractCdbServiceException If no entity was found or an internal exception was catched.
     */
    public VhfGroundStation getVhfGroundStationById(Integer id)
            throws AbstractCdbServiceException {
        log.debug("Search for VHF ground station by id {}", id);
        return vhfGroundStationRepository.findById(id).orElseThrow(
                () -> new NotExistsPojoException(bundle.getString(ERROR_MSG) + " id = " + id)
        );
    }

    /**
     * Find all stations described in the database.
     *
     * @param qry the Predicate
     * @param req the Pageable
     * @return Page of VhfGroundStation
     * @throws VhfServiceException If a service Exception occurred
     */
    public Page<VhfGroundStation> findAllVhfGroundStations(Predicate qry, Pageable req)
            throws VhfServiceException {
        Page<VhfGroundStation> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfGroundStationRepository.findAll(req);
        }
        else {
            entitylist = vhfGroundStationRepository.findAll(qry, req);
        }
        return entitylist;
    }

    /**
     * Create a new VhfGroundStation.
     *
     * @param entity the VhfGroundStation
     * @return VhfGroundStation
     * @throws AbstractCdbServiceException If a service Exception occurred
     */
    @Transactional
    public VhfGroundStation insertVhfGroundStation(VhfGroundStation entity)
            throws AbstractCdbServiceException {
        log.debug("Create VHF ground station: {}", entity);
        final Optional<VhfGroundStation> tmpgt = vhfGroundStationRepository.findById(entity.getStationId());
        if (tmpgt.isPresent()) {
            log.warn("Station {} already exists.", tmpgt.get());
            throw new AlreadyExistsPojoException(
                    "Station already exists for id " + entity.getStationId());
        }
        // The station is saved.
        final VhfGroundStation saved = vhfGroundStationRepository.save(entity);
        log.trace("Saved ground station entity: {}", saved);
        return saved;
    }

    /**
     * Update the VhfGroundStation.
     *
     * @param entity the VhfGroundStation
     * @return VhfGroundStation
     * @throws AbstractCdbServiceException If a service Exception occurred
     */
    @Transactional
    public VhfGroundStation updateVhfGroundStation(VhfGroundStation entity)
            throws AbstractCdbServiceException {
        log.debug("Update an existing VHF ground station from entity {}", entity);
        VhfGroundStation savedentity = getVhfGroundStationById(entity.getStationId());
        if (savedentity.getDescription() != null && !"none".equals(savedentity.getDescription())) {
            // Change decription.
            savedentity.setDescription(savedentity.getDescription());
        }
        if (entity.getId() != null && !"none".equals(entity.getId())) {
            // Change id station.
            savedentity.setId(entity.getId());
        }
        if (entity.getName() != null && !"none".equals(entity.getName())) {
            // Change name station.
            savedentity.setName(entity.getName());
        }
        if (entity.getLocation() != null && !"none".equals(entity.getLocation())) {
            // Change location station.
            savedentity.setLocation(entity.getLocation());
        }
        if (entity.getCountry() != null && !"none".equals(entity.getCountry())) {
            // Change location station.
            savedentity.setCountry(entity.getCountry());
        }
        if (entity.getLongitude() != null) {
            // Change longitude station.
            savedentity.setLongitude(entity.getLongitude());
        }
        if (entity.getLatitude() != null) {
            // Change latitude station.
            savedentity.setLatitude(entity.getLatitude());
        }
        if (entity.getAltitude() != null) {
            // Change altitude station.
            savedentity.setAltitude(entity.getAltitude());
        }
        if (entity.getMinElevationAngle() != null) {
            // Change altitude station.
            savedentity.setMinElevationAngle(entity.getMinElevationAngle());
        }
        if (entity.getMacaddress() != null && !"none".equals(entity.getMacaddress())) {
            // Change mac address.
            savedentity.setMacaddress(entity.getMacaddress());
        }
        final VhfGroundStation saved = vhfGroundStationRepository.save(savedentity);
        log.trace("Saved entity: {}", saved);
        return saved;
    }

}
