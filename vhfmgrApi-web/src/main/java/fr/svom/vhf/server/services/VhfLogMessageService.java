/**
 *
 */
package fr.svom.vhf.server.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.csv.CsvSchema.Builder;
import fr.svom.messages.model.VhfLogMessage;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.mappers.PacketDateFormat;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

/**
 * This Service is dedicated to the print of special lines in the log files.
 * These lines are parsed by telegraf-grok expressions and stored in InfluxDB.
 * It is meant to be used only for monitoring.
 *
 * @author formica
 *
 */
@Service
public class VhfLogMessageService {

    /**
     * Seconds to millisec.
     */
    private static final int SEC_TO_MS = 1000;
    /**
     * Default empty message.
     */
    private static final String VHF_LOG_IS_NULL = "Empty VhfLogMessage";
    /**
     * Default initializer.
     */
    private static final int DEFAULT_ID = -1;
    /**
     * Default is duplicate.
     */
    private static final int IS_DUP = 1;
    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfLogMessageService.class);

    /**
     * Mapper.
     */
    @Autowired
    private ObjectMapper jacksonMapper;

    /**
     * Helper.
     */
    @Autowired
    private PacketDateFormat packetDateFormat;

    /**
     * Get a log message from a VhfTransferFrame.
     *
     * @param frame
     *            the VhfTransferFrame
     * @return VhfLogMessage
     * @throws VhfServiceException
     *             If a service Exception occurred.
     */
    public VhfLogMessage getVhfLogMessage(VhfTransferFrame frame) throws VhfServiceException {
        if (frame == null) {
            throw new VhfServiceException("Cannot prepare log on null frame");
        }
        // Set the time to be either the frame reception time or 0.
        final Long rectime = frame.getInsertionTime() != null ? frame.getInsertionTime().getTime()
                : 0L;
        // Convert to ms and substract the header packet time.
        // We should also add the offset.
        final long pkttimesec = PacketDateFormat.getInstant(frame.getHeader().getPacketTime()).getEpochSecond();
        final Long totsec = rectime / SEC_TO_MS - pkttimesec;
        // Check if it is a duplicate.
        final Boolean isdup = frame.getIsFrameDuplicate();
        Integer id = DEFAULT_ID;
        if (isdup != null) {
            id = isdup ? IS_DUP : 0;
        }
        // Create the log message pojo.
        final VhfLogMessage vl = getDefaultLogMessage();
        vl.apid(frame.getCcsds().getCcsdsApid());
        vl.frameId(frame.getFrameId() != null ? frame.getFrameId() : DEFAULT_ID);
        vl.messageType("vhf");
        vl.receptionTime(rectime);
        vl.totsec(totsec);
        vl.date(new Date());
        vl.pkttime(frame.getHeader().getPacketTime());
        vl.obsid(frame.getHeader().getPacketObsid());
        vl.errmsg("ok");
        vl.isdup(id);
        // Get the station status.
        final VhfStationStatus sstatus = frame.getStationStatus();
        // Fill the fields corresponding to the station status.
        if (sstatus != null) {
            vl.station(sstatus.getIdStation());
            vl.stationTime(sstatus.getStationTime());
            vl.channel(sstatus.getChannel());
            vl.corrPower(sstatus.getCorrPower());
            vl.dopdrift(sstatus.getDopDrift());
            vl.doppler(sstatus.getDoppler());
            vl.reedsol(sstatus.getReedSolomonCorr());
            vl.srerror(sstatus.getSrerror());
        }

        return vl;
    }

    /**
     * Get the default log message.
     *
     * @return VhfLogMessage
     */
    public VhfLogMessage getDefaultLogMessage() {
        // The default here should be some error values.
        return new VhfLogMessage().apid(DEFAULT_ID).channel(DEFAULT_ID).corrPower(DEFAULT_ID)
                .date(new Date()).dopdrift(0.0F).doppler(0.0F).errmsg("error")
                .frameId((long) DEFAULT_ID).isdup(0).messageType("vhf").obsid((long) DEFAULT_ID)
                .pkttime(0L).receptionTime(0L).reedsol(DEFAULT_ID).reqtime(DEFAULT_ID).srerror(0.0F)
                .station(DEFAULT_ID).stationTime(0L).totsec(0L);
    }

    /**
     * Transform a VhfLogMessage into a log message.
     *
     * @param vl
     *            the VhfLogMessage
     * @return String
     */
    public String getVhfLogAsMap(VhfLogMessage vl) {
        if (vl != null) {

            final StringBuilder sb = new StringBuilder();
            sb.append("VHF_LOG_MESSAGE - ");
            sb.append("FRAMEID=").append(vl.getFrameId()).append(";");
            sb.append("APID=").append(vl.getApid()).append(";");
            sb.append("PKTTIME=").append(vl.getPkttime()).append(";");
            sb.append("OBSID=").append(vl.getObsid()).append(";");
            sb.append("ISDUP=").append(vl.getIsdup()).append(";");
            sb.append("RECEPTTIME=").append(vl.getReceptionTime()).append(";");
            sb.append("TOTSEC=").append(vl.getTotsec()).append(";");
            sb.append("STATION=").append(vl.getStation()).append(";");
            sb.append("STATIONTIME=").append(vl.getStationTime()).append(";");
            sb.append("CHANNEL=").append(vl.getChannel()).append(";");
            sb.append("CORRPOWER=").append(vl.getCorrPower()).append(";");
            sb.append("DOPDRIFT=").append(vl.getDopdrift()).append(";");
            sb.append("DOPPLER=").append(vl.getDoppler()).append(";");
            sb.append("REEDSOLOMON=").append(vl.getReedsol()).append(";");
            sb.append("SRERROR=").append(vl.getSrerror()).append(";");
            sb.append("ERRMSG=").append(vl.getErrmsg()).append(";");
            sb.append("REQTIME=").append(vl.getReqtime()).append("");
            return sb.toString();
        }
        else {
            return VHF_LOG_IS_NULL;
        }
    }

    /**
     * Transform a VhfLogMessage into a log message in Json.
     *
     * @param vl
     *            the VhfLogMessage
     * @return String
     */
    public String getVhfLogAsJson(VhfLogMessage vl) {
        if (vl != null) {
            try {
                return jacksonMapper.writeValueAsString(vl);
            }
            catch (final JsonProcessingException e) {
                log.error("Error in serializing log message to JSON : {}", e);
                return VHF_LOG_IS_NULL;
            }
        }
        else {
            return VHF_LOG_IS_NULL;
        }
    }

    /**
     * Transform a VhfLogMessage into a log message in CSV.
     *
     * @param vl
     *            VhfLogMessage
     * @return String
     */
    public String getVhfLogAsCsv(VhfLogMessage vl) {
        if (vl != null) {
            try {
                final String jsonstring = jacksonMapper.writeValueAsString(vl);
                final JsonNode node = jacksonMapper.readTree(jsonstring);
                final Builder csvSchemaBuilder = CsvSchema.builder();
                node.fieldNames().forEachRemaining(csvSchemaBuilder::addColumn);
                csvSchemaBuilder.setColumnSeparator(';');
                final CsvSchema csvSchema = csvSchemaBuilder.build();
                final CsvMapper csvMapper = new CsvMapper();
                return csvMapper.writerFor(JsonNode.class).with(csvSchema)
                        .writeValueAsString(node).replace(System.getProperty("line.separator"), "");
            }
            catch (final IOException e) {
                log.error("Error in serializing log message to CSV : {}", e);
                return VHF_LOG_IS_NULL;
            }
        }
        else {
            return VHF_LOG_IS_NULL;
        }
    }
}
