/**
 *
 */
package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.AlreadyExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.repositories.RawPacketRepository;
import fr.svom.vhf.server.externals.DecodedMessageType;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * A Service to manage VhfRawPacket.
 *
 * @author formica
 *
 */
@Service
public class VhfRawPacketService {

    /**
     * Logger.
     */
    private static final Logger logger = LoggerFactory.getLogger("fr.svom.vhf.server.rawdata");

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;
    /**
     * Repository.
     */
    @Autowired
    private RawPacketRepository rawPacketRepository;

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfRawPacketService.class);

    /**
     * Resource bundle.
     */
    private final ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("US"));

    /**
     * Error message.
     */
    private static final String ERROR_MSG = "error.rawpacket.service";

    /**
     * Store VhfRawPacket from an hexadecimal String.
     *
     * @param hexa
     *            the String
     * @param mode
     *            the String
     * @return VhfRawPacket
     * @throws AbstractCdbServiceException
     *             If an Exception occurred
     */
    @Transactional
    public VhfRawPacket storeRawPacket(String hexa, String mode) throws AbstractCdbServiceException {
        logger.info("Store raw: {}", hexa);
        VhfRawPacket isthere = rawPacketRepository.findByPacket(hexa).orElse(null);
        if (isthere != null) {
            String message = "Duplicate " + hexa;
            message = message + ";ERRMSG=Raw packet already exists";
            throw new AlreadyExistsPojoException(message);
        }
        final VhfRawPacket vhftf = new VhfRawPacket();
        vhftf.setPacket(hexa);
        vhftf.setPktflag(DecodedMessageType.TO_BE_DECODED.getTypename());
        vhftf.setPktformat(mode);
        vhftf.setSize(hexa.length());

        return rawPacketRepository.save(vhftf);
    }

    /**
     * Update the VhfRawPacket flag.
     *
     * @param raw
     *            the VhfRawPacket
     * @return VhfRawPacket
     */
    @Transactional
    public VhfRawPacket updateRawPacketFlag(VhfRawPacket raw) {
        final Optional<VhfRawPacket> stored = rawPacketRepository.findById(raw.getRawId());
        if (stored.isPresent()) {
            final VhfRawPacket st = stored.get();
            st.setPktflag(raw.getPktflag());
            return rawPacketRepository.save(st);
        }
        return null;
    }

    /**
     * Find a list of VhfRawPacket using flag and time.
     *
     * @param flag
     *            the String
     * @param since
     *            the Timestamp
     * @return List of VhfRawPacket
     * @throws VhfServiceException
     *             If a service Exception occurred
     */
    public List<VhfRawPacket> findRawPacketByFlag(String flag, Timestamp since)
            throws VhfServiceException {
        String defflag = DecodedMessageType.TO_BE_DECODED.getTypename();
        try {
            if (flag != null && !"none".equals(flag)) {
                defflag = flag;
            }
            final Instant runtime = Instant.now();
            final Timestamp now = new Timestamp(runtime.toEpochMilli());
            return rawPacketRepository.findByFlagRawPackets(defflag, since, now);
        }
        catch (final RuntimeException e) {
            log.error("Error in findRawPacketByFlag with args flag {} and since {}", flag,
                    since);
            throw new VhfServiceException(bundle.getString(ERROR_MSG), e);
        }
    }

    /**
     * Find a VhfRawPacket using packet. Null if no packet to be decoded is found.
     *
     * @param packet
     *            the String
     * @return VhfRawPacket
     * @throws AbstractCdbServiceException
     *             If a service Exception occurred
     */
    public VhfRawPacket findRawPacketByPacket(String packet)
            throws AbstractCdbServiceException {
        return rawPacketRepository.findByPacket(packet).orElse(null);
    }

    /**
     * Find all RawPacketDto using criteria in input.
     *
     * @param qry
     *            the Predicate
     * @param req
     *            the Pageable
     * @return Page of VhfRawPacket
     * @throws VhfServiceException
     *             If a service Exception occurred
     */
    public Page<VhfRawPacket> findAllRawPackets(Predicate qry, Pageable req)
            throws VhfServiceException {
        Page<VhfRawPacket> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = rawPacketRepository.findAll(req);
        }
        else {
            entitylist = rawPacketRepository.findAll(qry, req);
        }
        log.trace("Retrieved list of vhf raw packets {}", entitylist);
        return entitylist;
    }

    /**
     * Delete the raw packet.
     *
     * @param id
     */
    public void deleteRawPacket(Long id) {
        rawPacketRepository.deleteById(id);
    }
}
