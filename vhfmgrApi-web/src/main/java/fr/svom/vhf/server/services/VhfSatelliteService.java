package fr.svom.vhf.server.services;

import javax.transaction.Transactional;

import fr.svom.vhf.data.repositories.VhfSatAttitudeRepository;
import fr.svom.vhf.data.repositories.VhfSatPositionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import ma.glasnost.orika.MapperFacade;

/**
 * Service to manage VhfBinaryPacket.
 *
 * @author formica
 *
 */
@Service
public class VhfSatelliteService {

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Repository.
     */
    @Autowired
    private VhfSatAttitudeRepository vhfSatAttitudeRepository;


    /**
     * Repository.
     */
    @Autowired
    private VhfSatPositionRepository vhfSatPositionRepository;

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfSatelliteService.class);
    
     /**
     * Find all VhfSatAttitude using criteria in input.
     *
     * @param qry
     *            the Predicate
     * @param req
     *            the Pageable
     * @return Page of VhfSatAttitude
     * @throws VhfServiceException
     *             If a service Exception occurred
     */
    public Page<VhfSatAttitude> findAllVhfSatAttitudes(Predicate qry, Pageable req)
            throws VhfServiceException {
        Page<VhfSatAttitude> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfSatAttitudeRepository.findAll(req);
        }
        else {
            entitylist = vhfSatAttitudeRepository.findAll(qry, req);
        }
        log.trace("Retrieved list of vhf attitudes {}", entitylist);
        return entitylist;
    }
    
    /**
     * Save the VhfSatAttitude.
     *
     * @param entity
     *            the VhfSatAttitude
     * @return VhfSatAttitude
     */
    @Transactional
    public VhfSatAttitude saveAttitude(VhfSatAttitude entity) {
        log.debug("Save Attitude of the satellite: {} ", entity);
        try {
            final VhfSatAttitude saved = vhfSatAttitudeRepository.save(entity);
            log.trace("Saved attitude {}", saved);
            return saved;
        }
        catch (final RuntimeException e) {
            log.error("Error storing attitude ID (frameId) {} : {}", entity.getAttId(), e);
        }
        return null;
    }

    /**
    * Find all VhfSatPosition using criteria in input.
    *
    * @param qry
    *            the Predicate
    * @param req
    *            the Pageable
    * @return Page of VhfSatPosition
    * @throws VhfServiceException
    *             If a service Exception occurred
    */
   public Page<VhfSatPosition> findAllVhfSatPositions(Predicate qry, Pageable req)
           throws VhfServiceException {
       Page<VhfSatPosition> entitylist = null;
       if (req == null) {
           req = PageRequest.of(0, 1000);
       }
       if (qry == null) {
           entitylist = vhfSatPositionRepository.findAll(req);
       }
       else {
           entitylist = vhfSatPositionRepository.findAll(qry, req);
       }
       log.trace("Retrieved list of vhf positions {}", entitylist);
       return entitylist;
   }
   
   /**
    * Save the VhfSatPosition.
    *
    * @param entity
    *            the VhfSatPosition
    * @return VhfSatPosition
    */
   @Transactional
   public VhfSatPosition savePosition(VhfSatPosition entity) {
       log.debug("Save Position of the satellite: {} ", entity);
       try {
           final VhfSatPosition saved = vhfSatPositionRepository.save(entity);
           log.trace("Saved position {}", saved);
           return saved;
       }
       catch (final RuntimeException e) {
           log.error("Error storing position ID (frameId) {} : {}", entity.getPosId(), e);
       }
       return null;
   }

}
