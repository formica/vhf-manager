package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.repositories.IJdbcRepository;
import fr.svom.vhf.data.repositories.VhfStationStatusRepository;
import fr.svom.vhf.swagger.model.VhfStationStatusCountDto;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * A service for managing Station status.
 *
 * @author formica
 */
@Service
public class VhfStationStatusService {

    /**
     * Repository.
     */
    @Autowired
    private VhfStationStatusRepository vhfStationStatusRepository;

    /**
     * Repository.
     */
    @Autowired
    @Qualifier("jdbcrepo")
    private IJdbcRepository jdbcrepo;

    /**
     * Resource bundle.
     */
    private final ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("US"));

    /**
     * Error message.
     */
    private static final String ERROR_MSG = "error.stationstatus.service";

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Find all VhfStationStatus.
     *
     * @param qry the Predicate query
     * @param req the Pageable request
     * @return Page of VhfStationStatus.
     */
    public Page<VhfStationStatus> findAllVhfStationStatus(Predicate qry, Pageable req) {
        Page<VhfStationStatus> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfStationStatusRepository.findAll(req);
        }
        else {
            entitylist = vhfStationStatusRepository.findAll(qry, req);
        }
        return entitylist;
    }

    /**
     * Find VhfStationStatus counts.
     *
     * @param from time in millisecond
     * @param to   time in millisecond
     * @return List of VhfStationStatusCountDto
     * @throws VhfServiceException If a service Exception occurred
     */
    public List<VhfStationStatusCountDto> findStationStatusCount(Long from, Long to)
            throws VhfServiceException {
        try {
            return jdbcrepo.getCountVhfStationStatus(from, to);
        }
        catch (final RuntimeException e) {
            throw new VhfServiceException(bundle.getString(ERROR_MSG), e);
        }
    }

}
