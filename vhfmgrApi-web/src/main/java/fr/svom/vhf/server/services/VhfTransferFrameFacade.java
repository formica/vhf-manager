/**
 *
 */
package fr.svom.vhf.server.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.messages.model.VhfLogMessage;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.mappers.NotifStatus;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.VhfTransferFrameRepository;
import fr.svom.vhf.server.externals.DecodedMessageType;
import fr.svom.vhf.swagger.model.HTTPResponse;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author formica
 *
 */
@Service
public class VhfTransferFrameFacade {

    /**
     * String.
     */
    private static final String INSTRUMENT_MODE = "InstrumentMode";
    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfTransferFrameFacade.class);
    /**
     * Logger for vhf.
     */
    private static final Logger logger = LoggerFactory.getLogger("fr.svom.vhf.server.vhflog");

    /**
     * Service.
     */
    @Autowired
    private VhfBinaryPacketService vhfBinaryPacketService;
    /**
     * Service.
     */
    @Autowired
    private VhfDecodedPacketService vhfDecodedPacketService;
    /**
     * Service.
     */
    @Autowired
    private VhfTransferFrameService vhfTransferFrameService;
    /**
     * Repository.
     */
    @Autowired
    private VhfTransferFrameRepository vhfTransferFrameRepository;
    /**
     * Repository.
     */
    @Autowired
    private PacketApidRepository packetApidRepository;
    /**
     * Repository.
     */
    @Autowired
    private VhfRawPacketService rawPacketservice;
    /**
     * Repository.
     */
    @Autowired
    private VhfSatelliteService vhfSatelliteService;
    /**
     * Service.
     */
    @Autowired
    private VhfLogMessageService logsvc;
    /**
     * Mapper.
     */
    @Autowired
    private ObjectMapper jacksonMapper;

    /**
     * Used in map.
     */
    private static final String DEC_CONTENT = "content";
    /**
     * Used in map.
     */
    private static final String DEC_TYPE = "type";

    /**
     * Create a vhf frame from Raw packet. This method is asynchronous.
     *
     * @param raw
     *            the VhfRawPacket in input
     * @throws VhfServiceException
     *             If something went wrong with the decoding.
     */
    @Async
    public void createVhfTransferFrameFromRawPacket(VhfRawPacket raw) throws VhfServiceException {
        final String message = "ID=" + raw.getRawId() + ";";
        final Instant now = Instant.now();
        final Long ts = now.toEpochMilli();
        log.info("RAW PACKET PROCESSING @ START TIME: {}", ts);
        try {
            VhfTransferFrame saved = decodeAndSaveFrame(raw);
            final VhfLogMessage vl = getLogMessage(saved, ts);
            logger.info(";{}", logsvc.getVhfLogAsCsv(vl));
            log.info("{} {}", message, logsvc.getVhfLogAsMap(vl));
        }
        catch (RuntimeException e) {
            final VhfLogMessage vl = getLogMessage(null, ts);
            vl.errmsg(e.getMessage());
            logger.info(";{}", logsvc.getVhfLogAsCsv(vl));
            log.error("{} {}", message, logsvc.getVhfLogAsMap(vl));
            throw e;
        }
    }

    /**
     * Decode and save frame.
     *
     * @param raw
     *            the raw
     * @return the vhf transfer frame
     * @throws AbstractCdbServiceException
     *             the exception
     */
    public VhfTransferFrame decodeAndSaveFrame(VhfRawPacket raw)
            throws AbstractCdbServiceException {
        log.debug("DECODE AND SAVE FRAME FROM RAW PACKET");
        final VhfTransferFrame vhftf = this.createVhfTransferFrame(raw);
        if (vhftf != null) {
            log.trace("Step1: raw pktid {} => create new frame: hash {}", raw.getRawId(), vhftf.getBinaryHash());
            final VhfTransferFrame saved = vhfTransferFrameService.storeVhfTransferFrame(vhftf);
            log.trace("Step2: frame saved");
            processNewFrame(saved, raw);
            log.trace("Step3: frame {} processed, raw packet flag is {}", saved.getFrameId(), raw.getPktflag());
            return saved;
        }
        else {
            log.warn("Cannot create frame; set error flag in raw packet id {}", raw.getRawId());
            raw.setPktflag(DecodedMessageType.ERROR.getTypename());
            rawPacketservice.updateRawPacketFlag(raw);
            throw new VhfServiceException("Cannot create frame from raw packet");
        }
    }

    /**
     * This method generates a VhfTransferFrame without associated objects a part the binary packet,
     * which is stored in an internal step inside this method. Other elements like the PacketApid are
     * not yet linked at this level.
     *
     * @param raw
     *            the VhfRawPacket
     * @throws AbstractCdbServiceException
     *             If an Exception occurred
     * @return VhfTransferFrame
     */
    public VhfTransferFrame createVhfTransferFrame(VhfRawPacket raw)
            throws AbstractCdbServiceException {
        log.debug("createVhfTransferFrame from raw packet {}", raw.getPacket());
        // Start processing the raw packet.
        final String hexastring = raw.getPacket();
        byte[] dataarr;
        try {
            dataarr = Hex.decodeHex(hexastring);
        }
        catch (DecoderException e) {
            throw new VhfDecodingException("Cannot decode raw packet from hexa string", e);
        }
        log.trace("Deserialize byte array of length {}: {}", dataarr.length, hexastring);
        VhfTransferFrame vhftf = vhfBinaryPacketService.deserialize(dataarr);
        // Return null value if deserialization is not possible.
        if (vhftf == null) {
            log.warn("Cannot deserialize, return null frame");
            return null;
        }
        // The TransferFrame is created, now store the binary part that shall be used
        // for decoding.
        String framestatus = "Deserialized," + raw.getPktformat();
        try {
            log.trace("Storing binary packet from frame {}", vhftf);
            vhftf = vhfBinaryPacketService.storeBinaryVhfPacket(vhftf);
        }
        catch (DataIntegrityViolationException e) {
            log.warn(
                    "Storage of binary packet throws exception {}\nAssume that there is a duplicate...",
                    e);
            framestatus += "[Error: cannot store binary]";
            VhfBinaryPacket isdup = vhfBinaryPacketService.checkDuplicates(vhftf.getPacket());
            if (isdup != null) {
                log.warn("Changing frame duplicate flag to True for {}", vhftf.getBinaryHash());
                vhftf.setIsFrameDuplicate(Boolean.TRUE);
                vhftf.setPacket(isdup);
                framestatus += ",Duplicate";
            }
        }
        vhftf.setRawPacket(hexastring);
        if (vhftf.getIsFrameDuplicate()) {
            log.warn("Duplicate frame...{}", vhftf.getBinaryHash());
            // Default dup frame id: Unknown duplicate.
            vhftf.setDupFrameId(-1L);
            // Default notification status for duplicates.
            vhftf.setNotifStatus(NotifStatus.IGNORED.status());
            final VhfTransferFrame orig = getDuplicateFrame(vhftf.getPacket());
            if (orig != null) {
                vhftf.setDupFrameId(orig.getFrameId());
            }
            else {
                log.error("Missing duplicate frame for {}, set ID to -999", vhftf.getBinaryHash());
                vhftf.setDupFrameId(-999L);
            }
            framestatus += ",DupId";
        }
        framestatus += ",Created";
        vhftf.setFrameStatus(framestatus);
        log.debug("New frame decoded and binary stored: ");
        log.debug(" --> frame header : {}\n"
                  + " --> ccsds        : {}\n"
                  + " --> data header  : {}\n"
                  + " --> station status : {}",
                vhftf.getFrameHeader(), vhftf.getCcsds(), vhftf.getHeader(),
                vhftf.getStationStatus());

        return vhftf;
    }

    /**
     * @param frame
     *            the VhfTransferFrame.
     * @param raw
     *            the VhfRawPacket.
     * @return
     * @throws VhfServiceException
     *             If an exception occurs in processing the frame
     */
    protected void processNewFrame(VhfTransferFrame frame, VhfRawPacket raw)
            throws VhfServiceException {
        if (!frame.getIsFrameDuplicate()) {
            // It is a new packet
            log.debug("Decoding frame {}: request to decoder service", frame.getFrameId());
            final HTTPResponse pktdecodedresp = vhfDecodedPacketService
                    .decodePacket(frame.getPacket().getPacket());
            final String pktdecoded = pktdecodedresp.getMessage();
            log.trace("Decoded packet is [code:{}]: {} from {}", pktdecodedresp.getCode(),
                    pktdecoded, frame.getPacket().getPacket());
            if (pktdecodedresp.getCode() == HttpStatus.UNPROCESSABLE_ENTITY.value()) {
                // FIXME: should is valid be false when 422 is found ?
                log.warn("Frame {} cannot be processed by decoder service", frame.getFrameId());
                frame = vhfTransferFrameService.updateFrameStatus(frame.getFrameId(), ",[Error: wrong CRC]",
                        Boolean.FALSE);
                raw.setPktflag(DecodedMessageType.UNPROCESSABLE.getTypename());
                rawPacketservice.updateRawPacketFlag(raw);
            }
            final Map<String, String> decpkt = parseDecoded(pktdecoded);
            if ("error".equals(decpkt.get(DEC_TYPE))) {
                log.warn("Frame {} decoding error: decoded message not stored", frame.getFrameId());
                vhfTransferFrameService.updateFrameStatus(frame.getFrameId(),
                        ",[Error: cannot decode]", Boolean.FALSE);
                raw.setPktflag(DecodedMessageType.ERROR.getTypename());
                rawPacketservice.updateRawPacketFlag(raw);
            }
            else {
                // Store the decoded packet but only if isDup is false
                final VhfDecodedPacket tobesaved = DataModelsHelper.buildDecodedPacket(
                        decpkt.get(DEC_CONTENT), decpkt.get(DEC_TYPE), frame.getBinaryHash(),
                        frame.getApid().getName());
                final VhfDecodedPacket storeddecoded = vhfDecodedPacketService
                        .saveOrUpdateDecodedPacket(tobesaved);
                log.trace("Frame id={} decoded", frame.getFrameId());
                frame = vhfTransferFrameService.updateFrameStatus(frame.getFrameId(), ",Decoded",
                        Boolean.TRUE);
                // Update raw packet
                if (storeddecoded != null) {
                    raw.setPktflag(DecodedMessageType.DECODED.getTypename());
                }
                // Update APID if needed
                final PacketApid apid = frame.getApid();
                if (!apid.getName().equals(decpkt.get(DEC_TYPE))) {
                    apid.setName(decpkt.get(DEC_TYPE));
                    log.warn("Change packetName in PacketApid : {}", apid);
                    packetApidRepository.save(apid);
                }
                // Update Obsid in case InstrumentMode is present
                if (apid.getInstrument().contains(INSTRUMENT_MODE)) {
                    log.warn("Frame {} : InstrumentMode is present...modify values for primary header",
                            frame.getFrameId());
                    PrimaryHeaderPacket pktheader = vhfDecodedPacketService
                            .decodeCcsdsHeader(tobesaved, frame.getHeader());
                    VhfTransferFrame modheaderframe = vhfTransferFrameService
                            .saveOrUpdatePrimaryHeader(pktheader, frame);
                    if (modheaderframe == null) {
                        log.error("Cannot update the header, the frame was not found");
                    }
                }
                // Extract attitude and positions if needed
                // Decode Attitude
                if (apid.getDetails().contains("Attitude")) {
                    VhfSatAttitude attitude = vhfDecodedPacketService.decodeAttitude(tobesaved, apid);
                    if (attitude != null) {
                        attitude.setAttId(frame.getFrameId());
                        if (attitude.getPacketTime() == 0) {
                            attitude.setPacketTime(frame.getHeader().getPacketTime());
                        }
                        // Get the attitude and save it in the appropriate table.
                        attitude.setApidName(apid.getName());
                        vhfSatelliteService.saveAttitude(attitude);
                    }
                }
                // Decode Position
                if (apid.getDetails().contains("Position")) {
                    VhfSatPosition position = vhfDecodedPacketService.decodePosition(tobesaved, apid);
                    if (position != null) {
                        position.setPosId(frame.getFrameId());
                        if (position.getPacketTime() == 0) {
                            position.setPacketTime(frame.getHeader().getPacketTime());
                        }
                        // Get the position and save it in the appropriate table.
                        position.setApidName(apid.getName());
                        vhfSatelliteService.savePosition(position);
                    }
                }
                // Publish notification on specific single packets (Alerts and similar)
                // The other classes are notified every couple of minutes from the scheduler
                // execution.
                raw.setPktflag(DecodedMessageType.DECODED.getTypename());
                rawPacketservice.updateRawPacketFlag(raw);
                // Store temporarely into the frame to be used in publishing notifications
                frame.setPacketJson(storeddecoded);
                // Call method to publish notifications when needed.
                log.debug("Frame id={} decoded: apid={}, ground station={}",
                        frame.getFrameId(), frame.getApid().getName(), frame.getStation().getName());
                vhfTransferFrameService.publishNotification(frame);
            }
        }
        else {
            log.warn("Frame id={} is a duplicate of {}: apid={}, ground station={}", frame.getFrameId(),
                    frame.getDupFrameId(), frame.getApid().getName(), frame.getStation().getName());
            raw.setPktflag(DecodedMessageType.DUPLICATE.getTypename());
            rawPacketservice.updateRawPacketFlag(raw);
        }
    }

    /**
     * @param frame
     *            the VhfTransferFrame.
     * @param ts
     *            the Long.
     * @throws VhfServiceException
     *             If an exception occurs in generating log message.
     * @return VhfLogMessage
     */
    protected VhfLogMessage getLogMessage(VhfTransferFrame frame, Long ts)
            throws VhfServiceException {
        final Instant end = Instant.now();
        final Long tn = end.toEpochMilli();
        final Long reqt = tn - ts;
        VhfLogMessage vl = logsvc.getDefaultLogMessage();
        if (frame != null) {
            vl = logsvc.getVhfLogMessage(frame);
        }
        vl.reqtime(reqt.intValue());
        return vl;
    }

    /**
     * @param dup
     *            the VhfBinaryPacket.
     * @return VhfTransferFrame
     */
    protected VhfTransferFrame getDuplicateFrame(VhfBinaryPacket dup) {
        final List<VhfTransferFrame> origlist = vhfTransferFrameRepository
                .findByBinaryHashAndisFrameDuplicate(dup.getHashId(), Boolean.FALSE);
        log.debug("Retrieved original frame list for duplicates of size {}", origlist.size());
        if (!origlist.isEmpty()) {
            return origlist.get(0);
        }
        else {
            log.error("Cannot find duplicates for {}", dup.getHashId());
        }
        return null;
    }

    /**
     * Parse decoded json. Extract the type and the message.
     *
     * @param json
     *            the String in json to be parsed
     * @return the map containing type and message content
     */
    protected Map<String, String> parseDecoded(String json) {
        // here we should parse the decoded packet into 2 parts: type and content
        // The content is equivalent to the old pktdecoded string, while the type should
        // affect the APID packetName.
        // Better also to add the type to VhfDecodedPacket table.
        final Map<String, String> jmap = new HashMap<>();
        try {
            log.trace("Parsing the decoded packet {} as json, extract type and content...", json);
            if ("none".equals(json)) {
                throw new IOException("no json content");
            }
            final JsonNode jsonNode = jacksonMapper.readTree(json);
            jmap.put(DEC_TYPE, jsonNode.get(DEC_TYPE).textValue());
            final JsonNode content = jsonNode.get(DEC_CONTENT);
            final String jsonpktcont = jacksonMapper.writeValueAsString(content);
            jmap.put(DEC_CONTENT, jsonpktcont);
            log.trace("Decoded packet parsed for type {}", jmap.get(DEC_TYPE));
            return jmap;
        }
        catch (final IOException e) {
            log.error("Cannot extract info from decoded pkt {} : {}", json, e.getMessage());
        }
        jmap.put(DEC_TYPE, "error");
        jmap.put(DEC_CONTENT, json);
        log.warn("Decoded packet parsed with error: {}", jmap.get(DEC_CONTENT));
        return jmap;
    }

}
