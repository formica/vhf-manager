package fr.svom.vhf.server.services;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.mappers.NotifStatus;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.IJdbcRepository;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.VhfDecodedPacketRepository;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import fr.svom.vhf.data.repositories.VhfNotificationRepository;
import fr.svom.vhf.data.repositories.VhfStationStatusRepository;
import fr.svom.vhf.data.repositories.VhfTransferFrameRepository;
import fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria;
import fr.svom.vhf.data.repositories.querydsl.SearchCriteria;
import fr.svom.vhf.server.controllers.PageRequestHelper;
import fr.svom.vhf.server.controllers.SvomVhfMessagesController;
import fr.svom.vhf.server.externals.DecodedMessageType;
import fr.svom.vhf.swagger.model.RespPage;
import fr.svom.vhf.swagger.model.VhfBasePacket;
import fr.svom.vhf.swagger.model.VhfPacketSearchDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import ma.glasnost.orika.MapperFacade;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * A service for managing VHF transfer frames.
 *
 * @author formica
 */
@Service
public class VhfTransferFrameService {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(VhfTransferFrameService.class);

    /**
     * Repository.
     */
    @Autowired
    private VhfTransferFrameRepository vhfTransferFrameRepository;
    /**
     * Repository.
     */
    @Autowired
    private VhfDecodedPacketService vhfDecodedPacketService;
    /**
     * Repository.
     */
    @Autowired
    private VhfBinaryPacketService vhfBinaryPacketService;
    /**
     * Controller.
     */
    @Autowired
    private SvomVhfMessagesController svomVhfMessagesController;
    /**
     * Repository.
     */
    @Autowired
    private VhfStationStatusRepository vhfStationStatusRepository;
    /**
     * Repository.
     */
    @Autowired
    private PacketApidRepository packetApidRepository;
    /**
     * Repository.
     */
    @Autowired
    private VhfGroundStationRepository vhfGroundStationRepository;
    /**
     * Repository.
     */
    @Autowired
    private VhfDecodedPacketRepository vhfDecodedPacketRepository;
    /**
     * Repository.
     */
    @Autowired
    private VhfNotificationRepository vhfNotificationRepository;

    /**
     * Repository.
     */
    @Autowired
    @Qualifier("jdbcrepo")
    private IJdbcRepository jdbcrepo;
    /**
     * Helper.
     */
    @Autowired
    private PageRequestHelper prh;

    /**
     * Criteria filtering.
     */
    @Autowired
    @Qualifier("vhfBinaryPacketFiltering")
    private IFilteringCriteria vhfBinaryPacketFiltering;

    /**
     * Criteria filtering.
     */
    @Autowired
    @Qualifier("vhfDecodedPacketFiltering")
    private IFilteringCriteria vhfDecodedPacketFiltering;
    /**
     * Criteria filtering.
     */
    @Autowired
    @Qualifier("vhfTransferFrameFiltering")
    private IFilteringCriteria vhfTransferFrameFiltering;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Resource bundle.
     */
    private final ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("US"));

    /**
     * Error message.
     */
    private static final String ERROR_MSG = "error.frame.service";

    /**
     * Find a transfer frames by ID.
     *
     * @param id the transfer frame ID
     * @return the VhfTransferFrameDto
     * @throws VhfServiceException    If an exception occurs in finding the frame
     * @throws NotExistsPojoException If the entity was not found
     */
    public VhfTransferFrameDto getVhfTransferFrameById(Long id)
            throws AbstractCdbServiceException {
        log.debug("Search for VHF transfer frame by id {}", id);
        if (id == null) {
            throw new VhfCriteriaException("Frame id cannot be null");
        }
        VhfTransferFrame entity =
                vhfTransferFrameRepository.findById(id)
                        .orElseThrow(() -> new NotExistsPojoException("Cannot find frame " + id));
        return mapper.map(entity, VhfTransferFrameDto.class);
    }

    /**
     * Find all transfer frames.
     *
     * @param qry the Predicate query
     * @param req the Pageable request
     * @return Page of VhfTransferFrame.
     */
    public Page<VhfTransferFrame> findAllVhfTransferFrames(Predicate qry, Pageable req) {
        Page<VhfTransferFrame> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfTransferFrameRepository.findAll(req);
        }
        else {
            entitylist = vhfTransferFrameRepository.findAll(qry, req);
        }
        return entitylist;
    }

    /**
     * Find all notifications.
     *
     * @param qry the Predicate query
     * @param req the Pageable request
     * @return Page of VhfNotification.
     */
    public Page<VhfNotification> findAllVhfNotifications(Predicate qry, Pageable req) {
        Page<VhfNotification> entitylist = null;
        if (req == null) {
            req = PageRequest.of(0, 1000);
        }
        if (qry == null) {
            entitylist = vhfNotificationRepository.findAll(req);
        }
        else {
            entitylist = vhfNotificationRepository.findAll(qry, req);
        }
        return entitylist;
    }

    /**
     * Search for transfer frames using filtering. Used in VhfScheduler.
     *
     * @param by   the String for search.
     * @param page the int
     * @param size the int
     * @param sort the String
     * @return the Page of VhfTransferFrame
     * @throws AbstractCdbServiceException If a service exception occurred
     */
    public Page<VhfTransferFrame> searchVhfTransferFrameEntities(String by, int page, int size,
                                                                 String sort) throws AbstractCdbServiceException {
        final PageRequest preq = prh.createPageRequest(page, size, sort);

        final List<SearchCriteria> params = prh.createMatcherCriteria(by);
        final List<BooleanExpression> expressions = vhfTransferFrameFiltering
                .createFilteringConditions(params);
        final BooleanExpression wherepred = prh.getWhere(expressions);
        return vhfTransferFrameRepository.findAll(wherepred, preq);
    }

    /**
     * Search for transfer frames using filtering. It is partially duplicate of the
     * previous method, but sends back the VhfBasePacket.
     *
     * @param arglist the List of SearchCriteria
     * @param req     the Pageable request
     * @return the VhfPacketSearchDto
     * @throws VhfServiceException If a service exception occurred
     */
    public VhfPacketSearchDto searchVhfTransferFrames(List<SearchCriteria> arglist, Pageable req)
            throws AbstractCdbServiceException {
        try {
            List<VhfBasePacket> dtolist = null;
            Page<VhfTransferFrame> entitypage = null;
            final List<BooleanExpression> expressions = vhfTransferFrameFiltering
                    .createFilteringConditions(arglist);
            final BooleanExpression wherepred = prh.getWhere(expressions);
            log.debug("searchVhfTransferFrames: use where {} and page {}", wherepred, req);
            entitypage = vhfTransferFrameRepository.findAll(wherepred, req);
            RespPage respPage = new RespPage().size(entitypage.getSize())
                    .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                    .number(entitypage.getNumber());

            dtolist = StreamSupport.stream(entitypage.toList().spliterator(), false)
                    .map(s -> mapper.map(s, VhfBasePacket.class)).collect(Collectors.toList());
            final VhfPacketSearchDto setdto = new VhfPacketSearchDto().packets(dtolist)
                    .size(dtolist.size())
                    .page(respPage)
                    .pkttype("vhftransferframe");
            if (arglist != null) {
                String criteria = prh.dumpCriteria(arglist);
                setdto.criteria(criteria);
            }
            return setdto;
        }
        catch (final RuntimeException e) {
            log.debug(
                    "Exception in retrieving VHF transfer frame list using searchVhfTransferFrames");
            throw new VhfServiceException(bundle.getString(ERROR_MSG), e);
        }
    }

    /**
     * Search for binary packets using filtering.
     *
     * @param arglist the List of SearchCriteria
     * @param req     the Pageable request
     * @return the VhfPacketSearchDto
     * @throws VhfServiceException If a service exception occurred
     */
    public VhfPacketSearchDto searchVhfBinaryPackets(List<SearchCriteria> arglist, Pageable req)
            throws VhfServiceException {
        try {
            List<VhfBasePacket> dtolist = null;
            Page<VhfBinaryPacket> entitypage = null;
            final List<BooleanExpression> expressions = vhfBinaryPacketFiltering
                    .createFilteringConditions(arglist);
            final BooleanExpression wherepred = prh.getWhere(expressions);
            entitypage = vhfBinaryPacketService.findAllVhfBinaryPackets(wherepred, req);

            RespPage respPage = new RespPage().size(entitypage.getSize())
                    .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                    .number(entitypage.getNumber());

            dtolist = StreamSupport.stream(entitypage.toList().spliterator(), false)
                    .map(s -> mapper.map(s, VhfBasePacket.class)).collect(Collectors.toList());
            final VhfPacketSearchDto setdto = new VhfPacketSearchDto().packets(dtolist)
                    .size(dtolist.size())
                    .page(respPage)
                    .pkttype("vhfbinarypacket");
            if (arglist != null) {
                String criteria = prh.dumpCriteria(arglist);
                setdto.criteria(criteria);
            }
            return setdto;
        }
        catch (final RuntimeException e) {
            log.debug(
                    "Exception in retrieving VHF binary packets list using searchVhfBinaryPackets");
            throw new VhfServiceException(bundle.getString(ERROR_MSG), e);
        }
    }

    /**
     * Search for decoded packets using filtering.
     *
     * @param arglist the List of SearchCriteria
     * @param req     the Pageable request
     * @return the VhfPacketSearchDto
     * @throws VhfServiceException If a service exception occurred
     */
    public VhfPacketSearchDto searchVhfDecodedPackets(List<SearchCriteria> arglist, Pageable req)
            throws VhfServiceException {
        try {
            List<VhfBasePacket> dtolist = null;
            Page<VhfDecodedPacket> entitypage = null;
            final List<BooleanExpression> expressions = vhfDecodedPacketFiltering
                    .createFilteringConditions(arglist);
            final BooleanExpression wherepred = prh.getWhere(expressions);
            entitypage = vhfDecodedPacketService.findAllVhfDecodedPackets(wherepred, req);

            RespPage respPage = new RespPage().size(entitypage.getSize())
                    .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                    .number(entitypage.getNumber());

            dtolist = StreamSupport.stream(entitypage.toList().spliterator(), false)
                    .map(s -> mapper.map(s, VhfBasePacket.class)).collect(Collectors.toList());
            final VhfPacketSearchDto setdto = new VhfPacketSearchDto().packets(dtolist)
                    .size(dtolist.size())
                    .page(respPage)
                    .pkttype("vhfdecodedpacket");
            if (arglist != null) {
                String criteria = prh.dumpCriteria(arglist);
                setdto.criteria(criteria);
            }
            return setdto;
        }
        catch (final VhfServiceException e) {
            throw e;
        }
        catch (final RuntimeException e) {
            log.debug(
                    "Exception in retrieving VHF decoded packets list using searchVhfDecodedPackets");
            throw new VhfServiceException(bundle.getString(ERROR_MSG), e);
        }

    }

    /**
     * Store transfer frame.
     *
     * @param data the VhfTransferFrame to store
     * @return VhfTransferFrame
     * @throws AbstractCdbServiceException In an Exception occurred.
     */
    @Transactional
    public VhfTransferFrame storeVhfTransferFrame(VhfTransferFrame data)
            throws AbstractCdbServiceException {
        // Check if a frame with the same rawpacket exists
        // Check if there is a frame with the same raw packet
        VhfTransferFrame isthere = existsRawPacket(data.getRawPacket());
        if (isthere != null) {
            log.warn("Skip storage of frame: frame id {} is in the DB for raw packet {}, send back frame {}",
                    isthere.getFrameId(),
                    data.getRawPacket(),
                    isthere);
            return isthere;
        }
        // Create a storable frame from input entity object.
        // This method will search for child objects essentially.
        final VhfTransferFrame tobesaved = createStorableParsedFrame(data);
        return storeFrameAndChild(tobesaved);
    }

    /**
     * Check existence of rawpacket in the frame table.
     *
     * @param rawpacket
     * @return VhfTransferFrame or null.
     */
    public VhfTransferFrame existsRawPacket(String rawpacket) {
        List<VhfTransferFrame> frameList = vhfTransferFrameRepository.findByRawPacket(rawpacket);
        if (frameList.isEmpty()) {
            return null;
        }
        if (frameList.size() > 1) {
            log.error("Check raw packet existence on {} gave more than one entry", rawpacket);
        }
        return frameList.get(0);
    }

    /**
     * The purpose here is to load the entities that should be associated to the
     * frame before the transaction.
     *
     * @param data the VhfTransferFrame to create
     * @return VhfTransferFrame
     * @throws AbstractCdbServiceException In an Exception occurred.
     */
    public VhfTransferFrame createStorableParsedFrame(VhfTransferFrame data)
            throws AbstractCdbServiceException {
        if (data == null) {
            throw new VhfServiceException("Cannot create storable frame from null");
        }
        VhfTransferFrame frame = data;
        Instant now = Instant.now();
        log.info("Create storable frame: received @ time {} with hashId={} ",
                now, data.getBinaryHash());
        // FIXME: Replace the following with a real validation step
        log.warn("ATTENTION: HERE WE SHOULD DO SOME VALIDATION");
        frame.setIsFrameValid(true);
        // ATTENTION TO THE FOLLOWING...
        // FIXME: the APID should already be present in the table, as well as the
        // VhfGroundStation which appears in the StationStatus word.
        frame = associateApid(frame);
        log.trace("APID has been associated to frame : {}", frame);

        // For the moment we look for a station id which should exist...
        frame = associateGroundStation(frame);
        log.trace("Station has been associated to frame : {}", frame);

        // Get the binary packet which has been created during the deserialization
        frame = associateBinaryPacket(frame);
        log.trace("Binary packet has been associated to frame : {}", frame);
        log.debug("Apid, Station and binary packet have been associated to raw packet : {}", frame.getRawPacket());
        return frame;
    }

    /**
     * Associate the APID to a frame.
     *
     * @param data the VhfTransferFrame for which we need to associate an element
     * @return VhfTransferFrame
     * @throws VhfServiceException    If a service exception occurred
     * @throws NotExistsPojoException If the entity was not found.
     */
    protected VhfTransferFrame associateApid(VhfTransferFrame data)
            throws VhfServiceException, NotExistsPojoException {

        final Integer apid = data.getCcsds().getCcsdsApid();
        final Optional<PacketApid> papid = packetApidRepository.findById(apid);
        if (!papid.isPresent()) {
            log.error("Cannot find APID {}", apid);
            throw new NotExistsPojoException(bundle.getString(ERROR_MSG) + "apid does not exists: " + apid);
        }
        else {
            data.setApid(papid.get());
        }
        log.trace("Packet APID in frame is {} => parsed frame is now {}", data.getApid(), data);
        return data;
    }

    /**
     * @param frameid the Long.
     * @param status  the NotifStatus.
     * @return VhfTransferFrame
     */
    public VhfTransferFrame updateNotifStatus(Long frameid, NotifStatus status) {
        final Optional<VhfTransferFrame> frame = vhfTransferFrameRepository.findById(frameid);
        if (frame.isPresent()) {
            final VhfTransferFrame entity = frame.get();
            if (entity.getNotifStatus().equals(NotifStatus.SENT.status())) {
                log.warn("Ignore update of notification status. The frame {} is already SENT",
                        frameid);
                return entity;
            }
            entity.setNotifStatus(status.status());
            return vhfTransferFrameRepository.save(entity);
        }
        return null;
    }

    /**
     * @param frameid the Long.
     * @param status  the String.
     * @param valid   the Boolean.
     * @return VhfTransferFrame
     */
    @Transactional
    public VhfTransferFrame updateFrameStatus(Long frameid, String status, Boolean valid) {
        final Optional<VhfTransferFrame> frame = vhfTransferFrameRepository.findById(frameid);
        if (frame.isPresent()) {
            final VhfTransferFrame entity = frame.get();
            String fstatus = entity.getFrameStatus() + status;
            entity.setFrameStatus(fstatus);
            entity.setIsFrameValid(valid);
            return vhfTransferFrameRepository.save(entity);
        }
        log.warn("Cannot find frame ID {} for status update", frameid);
        return null;
    }

    /**
     * @param frameid the Long.
     * @param dupid   the Long duplicate frameid.
     * @return VhfTransferFrame or null if frame was not found
     */
    @Transactional
    public VhfTransferFrame updateIsDuplicate(Long frameid, Long dupid) {
        log.debug("Update duplicate flag and dup frame id for frame {} -> {}", frameid, dupid);
        final Optional<VhfTransferFrame> frame = vhfTransferFrameRepository.findById(frameid);
        if (frame.isPresent()) {
            final VhfTransferFrame entity = frame.get();
            if (!entity.getIsFrameDuplicate()) {
                entity.setIsFrameDuplicate(Boolean.TRUE);
            }
            if (entity.getDupFrameId() != null && entity.getDupFrameId() < 0) {
                entity.setDupFrameId(dupid);
            }
            log.warn("updateIsDuplicate frame {} to true using frame id {}", entity.getFrameId(),
                    entity.getDupFrameId());
            return vhfTransferFrameRepository.save(entity);
        }
        log.warn("Cannot find frame ID {} for isduplicate update", frameid);
        return null;
    }

    /**
     * Associate the binary packet to a frame.
     *
     * @param data the VhfTransferFrame for which we need to associate an element
     * @return VhfTransferFrame
     * @throws AbstractCdbServiceException If a service exception occurred. For example if a binary packet is not
     *                                     found in the DB.
     */
    protected VhfTransferFrame associateBinaryPacket(VhfTransferFrame data)
            throws AbstractCdbServiceException {
        if (data == null) {
            throw new VhfServiceException("Cannot associate binary packet: input frame is null");
        }
        final VhfBinaryPacket binentity = data.getPacket();
        log.debug("Load the binary packet from the DB for the storage of transfer frame: {}",
                binentity);
        if (binentity == null || binentity.getHashId() == null) {
            log.warn("associateBinaryPacket : binary packet or hash for searching the packet is null...");
            return data;
        }
        // Search for stored binary packet.
        VhfBinaryPacket saved = vhfBinaryPacketService.getVhfBinaryPacketByHash(binentity.getHashId());
        // Check hash in case is different. This may happen because binary hash is a separate field in the frame.
        if (!data.getBinaryHash().equals(saved.getHashId())) {
            data.setBinaryHash(saved.getHashId());
        }
        log.trace("Binary packet found in DB has been associated: parsed frame is now {}", data);
        return data;
    }

    /**
     * Associate the station to a frame.
     *
     * @param data the VhfTransferFrame for which we need to associate an element
     * @return VhfTransferFrame
     * @throws AbstractCdbServiceException If a service exception occurred
     */
    protected VhfTransferFrame associateGroundStation(VhfTransferFrame data)
            throws AbstractCdbServiceException {
        if (data == null) {
            throw new VhfServiceException("Cannot associate ground station: input frame is null");
        }
        if (data.getStationStatus() == null) {
            throw new VhfServiceException("Cannot find station status in frame, got null");
        }
        Integer stationid = null;
        final VhfStationStatus ss = data.getStationStatus();
        log.debug("associateGroundStation for frame {} : station status = {}", data.getFrameId(), ss);
        final Optional<VhfGroundStation> gs = vhfGroundStationRepository
                .findById(ss.getIdStation());
        // FIXME: this should send back an exception !!
        if (!gs.isPresent()) {
            log.warn("Dynamically create ground station...");
            final VhfGroundStation station = new VhfGroundStation();
            station.setStationId(ss.getIdStation());
            station.setId(String.format("0x%08X", ss.getIdStation()));
            station.setCountry("UNK");
            station.setLocation("FSC");
            station.setName("auto-added-" + ss.getIdStation());
            station.setMacaddress("00:00:00:" + ss.getIdStation());
            VhfGroundStation saved = vhfGroundStationRepository.save(station);
            log.warn("Created auto station {}", saved);
            data.setStation(saved);
            ss.setIdStation(saved.getStationId());
            stationid = saved.getStationId();
        }
        else {
            data.setStation(gs.get());
        }
        log.trace("Station id is {} => parsed frame is now {}", stationid, data);
        return data;
    }

    /**
     * Check if binary packet and ground station exist.
     *
     * @param data the VhfTransferFrame for which we need to store an element
     * @return VhfTransferFrame
     */
    @Transactional
    protected VhfTransferFrame storeFrameAndChild(VhfTransferFrame data) {

        log.trace(
                "Storing frame and associated objects (binary pkt, ground station and decoded pkt) for hashId={}",
                data.getBinaryHash());
        // This is the method to properly save every entity.
        // Get the decoded packet, this part has been removed because it never happens here.
        // Check the code in processNewFrame method of the Facade object.
        // Save a fake decoded packet which says TO_BE_DECODED.
        String framestatus = data.getFrameStatus();
        if (!data.getIsFrameDuplicate()) {
            // It is a new packet.
            log.trace(
                    "Not a duplicate packet: register temporary decoded packet with empty content");
            final VhfDecodedPacket tbs = DataModelsHelper.buildDecodedPacket("none",
                    DecodedMessageType.TO_BE_DECODED.getTypename(), data.getBinaryHash(), "fake");

            final VhfDecodedPacket dd = vhfDecodedPacketService.saveOrUpdateDecodedPacket(tbs);
            log.trace("Saved temporary decoded packet....{}", dd);
            data.setPacketJson(dd);
            framestatus += ",[Fake Decoded]";
        }
        else {
            // It is a duplicate.
            log.warn("Duplicate packet: should not be decoded again...");
            framestatus += ",[Duplicate]";
        }
        // Save the entity frame.
        // Check apid class name. Ignore Idle frame in notifications.
        log.trace("Saving frame {}", data);
        // Set notif status for ignorable packets
        if ("idle".equals(data.getApid().getClassName())) {
            data.setNotifStatus(NotifStatus.IGNORED.status());
        }
        data.setFrameStatus(framestatus + ",Saved");
        // Save the frame.
        final VhfTransferFrame savedframe = vhfTransferFrameRepository.save(data);
        // Link station status to the frame which has been saved.
        if (data.getStationStatus() != null) {
            // The station status.
            final VhfStationStatus tbs = data.getStationStatus();
            tbs.setFrame(savedframe);
            final VhfStationStatus ss = vhfStationStatusRepository.save(tbs);
            log.trace("Saved station status....{}", ss);
        }
        log.debug("Stored frame id={} from ground station {}", savedframe.getFrameId(), data.getStation().getName());
        log.trace("Frame content is : {}", savedframe);
        return savedframe;
    }

    /**
     * Save or update primary header.
     *
     * @param packet the packet
     * @param frame  the frame
     * @return the frame
     */
    @Transactional
    public VhfTransferFrame saveOrUpdatePrimaryHeader(PrimaryHeaderPacket packet,
                                                      VhfTransferFrame frame) {
        Optional<VhfTransferFrame> storedframeopt = vhfTransferFrameRepository
                .findById(frame.getFrameId());
        if (storedframeopt.isPresent()) {
            // update packet
            VhfTransferFrame storedframe = storedframeopt.get();
            PrimaryHeaderPacket stored = storedframe.getHeader();
            stored.setInstrumentMode(packet.getInstrumentMode());
            stored.setPacketObsid(packet.getPacketObsid());
            stored.setPacketObsidNum(packet.getPacketObsidNum());
            stored.setPacketObsidType(packet.getPacketObsidType());
            stored.setPacketTime(packet.getPacketTime());
            storedframe.setHeader(stored);
            return vhfTransferFrameRepository.save(storedframe);
        }
        return frame;
    }

    /**
     * Save or update transfer frame.
     *
     * @param frameid
     * @param burstId
     * @return VhfTransferFrame
     */
    @Transactional
    public VhfTransferFrame updateFrameBurstId(Long frameid, String burstId) {
        final Optional<VhfTransferFrame> frame = vhfTransferFrameRepository.findById(frameid);
        if (frame.isPresent()) {
            final VhfTransferFrame entity = frame.get();
            entity.setBurstId(burstId);
            return vhfTransferFrameRepository.save(entity);
        }
        log.warn("Cannot find frame ID {} for status update, return null", frameid);
        return null;
    }

    /**
     * Gets the duplicates frames.
     *
     * @param from the from
     * @param to   the to
     * @return the duplicates frames
     */
    public Map<String, List<VhfTransferFrame>> getDuplicatesFrames(Long from, Long to) {
        List<String> duphashes = jdbcrepo.getDuplicateHashes(from, to);
        Map<String, List<VhfTransferFrame>> allframesdup = new HashMap<>();
        log.debug("Found list of {} hashes that should be duplicate but are not flagged as such: time range is {} - {}",
                duphashes.size(), from, to);
        for (String hash : duphashes) {
            List<VhfTransferFrame> dupframes = vhfTransferFrameRepository.findByHashId(hash);
            log.trace("Retrieved list of frame {} having hash {}", dupframes.size(), hash);
            allframesdup.put(hash, dupframes);
        }
        return allframesdup;
    }

    /**
     * Modify notification status for frame and send a notification to other systems.
     *
     * @param frameid
     * @param tend
     * @return Map<String, Integer>
     */
    public Map<String, Integer> requestNotification(Long frameid, Long tend) {
        Optional<VhfTransferFrame> frameopt = vhfTransferFrameRepository.findById(frameid);
        Map<String, Integer> countermap = new HashMap<>();
        if (frameopt.isPresent()) {
            VhfTransferFrame frame = frameopt.get();
            if (frame.getNotifStatus().equals(NotifStatus.SENT.status())) {
                log.trace("Ignore update of notification status. The frame {} is already SENT",
                        frameid);
                countermap.put("SENT", 0);
                return countermap;
            }
            final NotifStatus status = svomVhfMessagesController.sendNotification(frame, tend);
            frame.setNotifStatus(status.status());
            // Search for all similar frames : same apid and obsid, in the given range in time.
            // They shall all be declared as processed. But the notification is only sent for the first frame.
            // This mathod saves all updated frames.
            if (status.equals(NotifStatus.SENT)) {
                countermap.put("SENT", 1);
                Long obsid = frame.getHeader().getPacketObsid();
                List<VhfTransferFrame> toupdlist = getFramesListForObsid(frame.getApid().getApid(),
                        obsid, frame.getInsertionTime().getTime(), tend);
                int proccount = 0;
                List<VhfTransferFrame> updatablelist = new ArrayList<>();
                for (VhfTransferFrame toup : toupdlist) {
                    toup.setNotifStatus(NotifStatus.PROCESSED.status());
                    updatablelist.add(toup);
                    proccount++;
                }
                updatablelist.add(frame);
                countermap.put("PROCESSED", proccount);
                vhfTransferFrameRepository.saveAll(updatablelist);
            }
            else {
                countermap.put("FAILED", 1);
            }
        }
        return countermap;
    }


    /**
     * Get the list of frames for a given apid and obsid in a range, ordered by
     * ccsds count.
     *
     * @param apid   the Integer.
     * @param obsid  the Long.
     * @param tstart the Long.
     * @param tend   the Long.
     * @return List<VhfTransferFrame>
     */
    private List<VhfTransferFrame> getFramesListForObsid(Integer apid, Long obsid, Long tstart,
                                                         Long tend) {
        log.debug(">>> getFramesListForObsid: apid={},obsid={},tstart={},tend={}", apid, obsid,
                tstart, tend);
        final String by = "apid:" + apid + ",obsid:" + obsid + ",insertionTime>" + (tstart - 1)
                          + ",insertionTime<" + (tend + 1) + ",isFrameDuplicate:FALSE,isFrameValid:TRUE";
        final String sort = "ccsds.ccsdsCounter:ASC";
        Page<VhfTransferFrame> framepage = this.searchVhfTransferFrameEntities(by, 0, 1000, sort);
        if (framepage != null) {
            if (framepage.hasNext()) {
                log.warn(
                        "ATTENTION: the getFramesList function returns more than 1000 frames!");
            }
            return framepage.getContent();
        }
        log.trace("Found 0 frames");
        return new ArrayList<>();
    }

    /**
     * @param frame the VhfTransferFrame.
     * @return
     */
    public void publishNotification(VhfTransferFrame frame) {
        final PacketApid apid = frame.getApid();
        final String classname = apid.getClassName();
        log.trace("publishNotification on frame {}: class {}, category {}", frame.getFrameId(), classname,
                apid.getCategory());
        if ("alert".equalsIgnoreCase(classname) || "descriptor".equalsIgnoreCase(classname)) {
            final NotifStatus status = svomVhfMessagesController.sendNotification(frame, null);
            log.trace("publishNotification: packet apid {} {} => Status of notification : {}", apid.getName(),
                    classname,
                    status);
            updateNotifStatus(frame.getFrameId(), status);
        }
        if (apid.getCategory().matches("(AlertSequence)")) {
            log.trace("New packet belonging to alert sequence notified to NATs: {}", frame);
            svomVhfMessagesController.sendNotificationBA(frame);
        }
        log.trace("End of publishNotification method...");
    }

}
