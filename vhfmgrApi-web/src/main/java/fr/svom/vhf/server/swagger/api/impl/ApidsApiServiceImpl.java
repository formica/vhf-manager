package fr.svom.vhf.server.swagger.api.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.exceptions.VhfRequestFormatException;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria;
import fr.svom.vhf.server.controllers.EntityDtoHelper;
import fr.svom.vhf.server.controllers.PageRequestHelper;
import fr.svom.vhf.server.services.PacketApidService;
import fr.svom.vhf.server.swagger.api.ApidsApiService;
import fr.svom.vhf.server.swagger.api.NotFoundException;
import fr.svom.vhf.swagger.model.GenericStringMap;
import fr.svom.vhf.swagger.model.PacketApidDto;
import fr.svom.vhf.swagger.model.PacketApidSetDto;
import fr.svom.vhf.swagger.model.RespPage;
import fr.svom.vhf.swagger.model.VhfResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * Rest endpoint for APIDs management.
 *
 * @author formica
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen",
        date = "2017-05-02T11:54:10.337+02:00")
@Component
public class ApidsApiServiceImpl extends ApidsApiService {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ApidsApiServiceImpl.class);

    /**
     * Service.
     */
    @Autowired
    PacketApidService packetApidService;
    /**
     * Repository.
     */
    @Autowired
    PacketApidRepository packetApidRepository;

    /**
     * Helper.
     */
    @Autowired
    PageRequestHelper prh;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("packetApidFiltering")
    private IFilteringCriteria packetApidFiltering;
    /**
     * Helper.
     */
    @Autowired
    private EntityDtoHelper edh;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.ApidsApiService#findPacketApids(java.lang.
     * Integer, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response findPacketApids(Integer id, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        // Search packet APID by integer id.
        log.debug("Search packet APID using ID {}", id);
        final PacketApid entity = packetApidService.findApid(id);
        PacketApidDto dto = edh.entityToDto(entity, PacketApidDto.class);
        return Response.ok().entity(dto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.ApidsApiService#listPacketApids(java.lang.
     * String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listPacketApids(String by, Integer page, Integer size, String sort,
                                    SecurityContext securityContext, UriInfo info) throws NotFoundException {
        // Search a list of APIDs.
        log.debug("Search resource list using by={}, page={}, size={}, sort={}", by, page, size,
                sort);
        // Create filters
        final GenericStringMap filters = prh.getFilters(prh.createMatcherCriteria(by));
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(packetApidFiltering, by);
        }
        // Search for vhftransfer frames using where conditions.
        final Page<PacketApid> entitypage = packetApidService.findAllPacketApids(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());

        final List<PacketApidDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                PacketApidDto.class);
        final Response.Status rstatus = Response.Status.OK;
        final VhfResponseDto setdto = new PacketApidSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .datatype("packetapid");
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.ApidsApiService#createPacketApid(fr.svom.vhf.
     * swagger.model.PacketApidDto, javax.ws.rs.core.SecurityContext,
     * javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response createPacketApid(PacketApidDto body, SecurityContext securityContext,
                                     UriInfo info) throws NotFoundException {
        log.debug("Create resource using {} ", body.toString());
        PacketApid entity = edh.entityToDto(body, PacketApid.class);
        // TODO: extract from APID.
        entity.setPcat(0);
        entity.setPid(0);
        entity.setTid(0);
        if (entity.getApid() == null) {
            throw new VhfRequestFormatException("Cannot insert null APID");
        }
        final PacketApid saved = packetApidService.registerApid(entity);
        PacketApidDto dto = edh.entityToDto(saved, PacketApidDto.class);
        return Response.created(info.getRequestUri()).entity(dto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.ApidsApiService#updatePacketApids(java.lang.
     * Integer, fr.svom.vhf.swagger.model.PacketApidDto,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response updatePacketApids(Integer id, PacketApidDto body,
                                      SecurityContext securityContext, UriInfo info) throws NotFoundException {
        // Update information for ground station.
        // Create a new station entity.
        body.setApid(id);
        PacketApid entity = edh.entityToDto(body, PacketApid.class);
        final PacketApid saved = packetApidService.updateApid(entity);
        // Entity to DTO.
        PacketApidDto dto = edh.entityToDto(saved, PacketApidDto.class);
        return Response.ok().entity(dto).build();
    }
}
