package fr.svom.vhf.server.swagger.api.impl;


/**
 * Enum for format of streams and pkt.
 *
 * @author formica
 *
 */
public enum HeaderFormat {
    
	/**
	 * List of enum format.
	 */
	STREAM_FORMAT_BINARY ("binary"),
	STREAM_FORMAT_HEXA ("hexa"),
    PKT_MODE_TEST ("test"),
    PKT_MODE_PROD ("prod"),
	PKT_FORMAT_BINARY ("binary"),
	PKT_FORMAT_DECODED ("decoded"),
	PKT_FORMAT_RAW ("raw");

	/**
	 * The value string.
	 */
	public final String value;

	/**
	 * Private ctor.
	 *
	 * @param value the String
	 */
	private HeaderFormat(String value) {
		this.value= value;
	}
	
	/**
	 * @return String
	 */
	public String getFormat() {
		return value;
	}
}
