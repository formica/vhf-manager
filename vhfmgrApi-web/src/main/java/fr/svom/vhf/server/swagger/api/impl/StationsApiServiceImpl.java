package fr.svom.vhf.server.swagger.api.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.exceptions.VhfRequestFormatException;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria;
import fr.svom.vhf.server.controllers.EntityDtoHelper;
import fr.svom.vhf.server.controllers.PageRequestHelper;
import fr.svom.vhf.server.services.VhfGroundStationService;
import fr.svom.vhf.server.swagger.api.NotFoundException;
import fr.svom.vhf.server.swagger.api.StationsApiService;
import fr.svom.vhf.swagger.model.GenericStringMap;
import fr.svom.vhf.swagger.model.RespPage;
import fr.svom.vhf.swagger.model.StationSetDto;
import fr.svom.vhf.swagger.model.VhfGroundStationDto;
import fr.svom.vhf.swagger.model.VhfResponseDto;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * Rest endpoint for stations.
 *
 * @author formica
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen",
        date = "2017-04-23T11:53:22.235+02:00")
@Component
public class StationsApiServiceImpl extends StationsApiService {

    /**
     * Logger.
     */
    private static final Logger log = LoggerFactory.getLogger(StationsApiServiceImpl.class);

    /**
     * Service.
     */
    @Autowired
    private VhfGroundStationService vhfGroundStationService;
    /**
     * Repository.
     */
    @Autowired
    private VhfGroundStationRepository vhfGroundStationRepository;

    /**
     * Helper.
     */
    @Autowired
    private PageRequestHelper prh;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("vhfGroundStationFiltering")
    private IFilteringCriteria vhfGroundStationFiltering;
    /**
     * Helper.
     */
    @Autowired
    private EntityDtoHelper edh;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.StationsApiService#createVhfGroundStations(fr.
     * svom.vhf.swagger.model.VhfGroundStationDto, javax.ws.rs.core.SecurityContext,
     * javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response createVhfGroundStations(VhfGroundStationDto body,
                                            SecurityContext securityContext, UriInfo info) throws NotFoundException {
        // Create a new station entity.
        VhfGroundStation entity = mapper.map(body, VhfGroundStation.class);
        if (entity == null || entity.getStationId() == null) {
            throw new VhfRequestFormatException("Cannot insert null station");
        }
        // Check if station id is there.
        final VhfGroundStation saved = vhfGroundStationService.insertVhfGroundStation(entity);
        // Entity to DTO.
        VhfGroundStationDto dto = mapper.map(saved, VhfGroundStationDto.class);
        return Response.created(info.getRequestUri()).entity(dto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.StationsApiService#findVhfGroundStations(java.
     * lang.Integer, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response findVhfGroundStations(Integer id, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        // Search a station by ID.
        final VhfGroundStation entity = vhfGroundStationService.getVhfGroundStationById(id);
        VhfGroundStationDto dto = mapper.map(entity, VhfGroundStationDto.class);
        return Response.ok().entity(dto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.StationsApiService#listVhfGroundStations(java.
     * lang.String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listVhfGroundStations(String by, Integer page, Integer size, String sort,
                                          SecurityContext securityContext, UriInfo info) throws NotFoundException {
        log.debug("Search resource list using by={}, page={}, size={}, sort={}", by, page, size,
                sort);
        // Create filters
        final GenericStringMap filters = prh.getFilters(prh.createMatcherCriteria(by));
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(vhfGroundStationFiltering, by);
        }
        // Search for vhftransfer frames using where conditions.
        final Page<VhfGroundStation> entitypage = vhfGroundStationService.findAllVhfGroundStations(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        // Create the DTO list.
        final List<VhfGroundStationDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                VhfGroundStationDto.class);
        final Response.Status rstatus = Response.Status.OK;
        // Build the response.
        final VhfResponseDto setdto = new StationSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .datatype("vhfgroundstation");
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.StationsApiService#updateVhfGroundStation(java
     * .lang.Integer, fr.svom.vhf.swagger.model.VhfGroundStationDto,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response updateVhfGroundStation(Integer id, VhfGroundStationDto body,
                                           SecurityContext securityContext, UriInfo info) throws NotFoundException {
        // Update information for ground station.
        // Create a new station entity.
        body.setStationId(id);
        VhfGroundStation entity = mapper.map(body, VhfGroundStation.class);
        final VhfGroundStation saved = vhfGroundStationService.updateVhfGroundStation(entity);
        // Entity to DTO.
        VhfGroundStationDto dto = mapper.map(saved, VhfGroundStationDto.class);
        return Response.ok().entity(dto).build();
    }

}
