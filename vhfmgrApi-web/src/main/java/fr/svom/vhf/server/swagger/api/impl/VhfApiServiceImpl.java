package fr.svom.vhf.server.swagger.api.impl;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.vhf.data.config.VhfProperties;
import fr.svom.vhf.data.exceptions.AlreadyExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.exceptions.VhfRequestFormatException;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria;
import fr.svom.vhf.data.repositories.querydsl.SearchCriteria;
import fr.svom.vhf.server.annotations.ProfileAndLog;
import fr.svom.vhf.server.controllers.EntityDtoHelper;
import fr.svom.vhf.server.controllers.PageRequestHelper;
import fr.svom.vhf.server.externals.DecodedMessageType;
import fr.svom.vhf.server.externals.IDecoder;
import fr.svom.vhf.server.services.ObsIdApidCountService;
import fr.svom.vhf.server.services.VhfBinaryPacketService;
import fr.svom.vhf.server.services.VhfBurstIdService;
import fr.svom.vhf.server.services.VhfDecodedPacketService;
import fr.svom.vhf.server.services.VhfRawPacketService;
import fr.svom.vhf.server.services.VhfSatelliteService;
import fr.svom.vhf.server.services.VhfStationStatusService;
import fr.svom.vhf.server.services.VhfTransferFrameFacade;
import fr.svom.vhf.server.services.VhfTransferFrameService;
import fr.svom.vhf.server.swagger.api.ApiResponseMessage;
import fr.svom.vhf.server.swagger.api.NotFoundException;
import fr.svom.vhf.server.swagger.api.VhfApiService;
import fr.svom.vhf.swagger.model.*;
import ma.glasnost.orika.MapperFacade;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;
import java.io.InputStream;
import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author formica
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen",
        date = "2017-05-02T11:54:10.337+02:00")
@Component
public class VhfApiServiceImpl extends VhfApiService {

    /**
     * Logger.
     */
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Service.
     */
    @Autowired
    private VhfBinaryPacketService vhfBinaryPacketService;
    /**
     * Service.
     */
    @Autowired
    private VhfDecodedPacketService vhfDecodedPacketService;
    /**
     * Service.
     */
    @Autowired
    private IDecoder decoderService;
    /**
     * Service.
     */
    @Autowired
    private VhfTransferFrameService vhfTransferFrameService;
    /**
     * Service.
     */
    @Autowired
    private VhfTransferFrameFacade vhfTransferFrameFacade;
    /**
     * Service.
     */
    @Autowired
    private VhfRawPacketService rawPacketService;
    /**
     * Service.
     */
    @Autowired
    private VhfStationStatusService vhfStationStatusService;
    /**
     * Service.
     */
    @Autowired
    private ObsIdApidCountService obsIdApidCountService;
    /**
     * Service.
     */
    @Autowired
    private VhfSatelliteService vhfSatelliteService;

    /**
     * Service.
     */
    @Autowired
    private VhfBurstIdService vhfBurstIdService;

    /**
     * Helper.
     */
    @Autowired
    private PageRequestHelper prh;

    /**
     * Mapper.
     */
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("vhfTransferFrameFiltering")
    private IFilteringCriteria vhfTransferFrameFiltering;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("vhfStationStatusFiltering")
    private IFilteringCriteria vhfStationStatusFiltering;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("vhfRawPacketFiltering")
    private IFilteringCriteria vhfRawPacketFiltering;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("vhfsatattitudeFiltering")
    private IFilteringCriteria vhfsatattitudeFiltering;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("vhfsatpositionFiltering")
    private IFilteringCriteria vhfsatpositionFiltering;

    /**
     * Filtering.
     */
    @Autowired
    @Qualifier("vhfBurstIdFiltering")
    private IFilteringCriteria vhfburstidFiltering;
    /**
     * Criteria filtering.
     */
    @Autowired
    @Qualifier("vhfNotificationFiltering")
    private IFilteringCriteria vhfNotificationFiltering;

    /**
     * Helper.
     */
    @Autowired
    private EntityDtoHelper edh;
    /**
     * Configuraton properties.
     */
    @Autowired
    private VhfProperties cprops;



    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#findVhfTransferFrames(java.lang.
     * Long, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response findVhfTransferFrames(Long frame, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.info("VhfApiService processing request to get vhf transfer frame with id {}", frame);
        // Search frame by ID.
        final VhfTransferFrameDto dto = vhfTransferFrameService.getVhfTransferFrameById(frame);
        return Response.ok().entity(dto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#listVhfTransferFrames(java.lang.
     * String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listVhfTransferFrames(String by, Integer page, Integer size, String sort,
                                          String dateformat, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.info(
                "listVhfTransferFrames: search resource list using by={}, page={}, size={}, sort={}",
                by, page, size, sort);
        try {
            if (dateformat == null) {
                dateformat = "ms";
            }
            // Create filters
            List<SearchCriteria> criteriaList = prh.createMatcherCriteria(by, dateformat);
            final GenericStringMap filters = prh.getFilters(criteriaList);
            // Create pagination request
            final PageRequest preq = prh.createPageRequest(page, size, sort);
            BooleanExpression wherepred = null;
            if (!"none".equals(by)) {
                // Create search conditions for where statement in SQL
                wherepred = prh.buildWhere(vhfTransferFrameFiltering, criteriaList);
            }
            // Search for vhftransfer frames using where conditions.
            final Page<VhfTransferFrame> entitypage = vhfTransferFrameService.findAllVhfTransferFrames(wherepred, preq);
            RespPage respPage = new RespPage().size(entitypage.getSize())
                    .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                    .number(entitypage.getNumber());

            final List<VhfTransferFrameDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                    VhfTransferFrameDto.class);
            final Response.Status rstatus = Response.Status.OK;
            final VhfResponseDto setdto = new VhfTransferFrameSetDto().resources(dtolist)
                    .size((long) dtolist.size())
                    .page(respPage)
                    .format("VhfTransferFrameSetDto")
                    .datatype("vhftransferframes");
            if (filters != null) {
                setdto.filter(filters);
            }
            return Response.ok().entity(setdto).build();
        }
        catch (final RuntimeException e) {
            // Exception from server.
            log.error("Exception from listVhfTransferFrames: {}", e);
            final String message = e.getMessage();
            final ApiResponseMessage resp = new ApiResponseMessage(ApiResponseMessage.ERROR,
                    message);
            // Send a 500.
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(resp).build();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#savePacketFromStream(java.io.
     * InputStream, org.glassfish.jersey.media.multipart.FormDataContentDisposition,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    @ProfileAndLog
    public Response savePacketFromStream(String format, String mode, FormDataBodyPart streamBodypart,
                                         SecurityContext securityContext,
                                         UriInfo info)
            throws NotFoundException {
        log.info(
                "savePacketFromStream processing request to store a frame from vhf station using stream {}",
                streamBodypart.getName());
        String hexastring = "none";
        String message = "";
        if (format == null) {
            format = HeaderFormat.STREAM_FORMAT_BINARY.getFormat();
        }
        if (mode == null) {
            mode = HeaderFormat.PKT_MODE_PROD.value;
        }
        FormDataContentDisposition fileDetail = streamBodypart.getFormDataContentDisposition();
        InputStream streamInputStream = streamBodypart.getValueAs(InputStream.class);

        // Create a response object.
        final HTTPResponse resp = new HTTPResponse();
        // Transform the binary stream in hexa.
        hexastring = vhfBinaryPacketService.saveStreamInHexa(streamInputStream, format);
        log.debug("Generated hexadecimal string {}", hexastring);
        VhfRawPacket raw = null;
        String okmsg = "ERRMSG=ok";
        try {
            // Store VHF raw packet.
            raw = rawPacketService.storeRawPacket(hexastring, mode);
            log.info("Generated raw packet {}", raw);
            resp.setAction("STORED");
            // Create and store transfer frame.
            vhfTransferFrameFacade.createVhfTransferFrameFromRawPacket(raw);
        }
        catch (AlreadyExistsPojoException e) {
            log.warn("Raw packet already exists: {}", e.getMessage());
            resp.setAction("SKIP_DUPLICATE");
            resp.setId(hexastring);
        }
        // Send back a short response with error code and 200.
        resp.code(Response.Status.OK.getStatusCode()).message(okmsg);
        return Response.created(info.getRequestUri()).entity(resp).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.server.swagger.api.VhfApiService#searchVhfPackets(java.lang.
     * String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response searchVhfPackets(@NotNull String by, String xVHFPktFormat, String dateformat, Integer page,
                                     Integer size, String sort,
                                     SecurityContext securityContext,
                                     UriInfo info)
            throws NotFoundException {

        if (by.contains("reception")) {
            by = by.replace("reception", "insertion");
        }
        if (sort.contains("reception")) {
            sort = sort.replace("reception", "insertion");
        }
        if ("none".equals(by)) {
            // No where condition.
            log.error(
                    "Cannot call the method search with no arguments: use at least apid/apidname or obsId/obsidnum "
                    + "selection");
            throw new VhfCriteriaException("Cannot process request: by parameter is mandatory, use at least "
                                           + "apid/apidname or obsId/obsidnum selection");
        }
        // Check the header for the type of requested data.
        if (xVHFPktFormat == null) {
            xVHFPktFormat = HeaderFormat.PKT_FORMAT_DECODED.value;
        }

        // Check the date format.
        if (dateformat == null) {
            dateformat = "ms";
        }
        log.info("searchVhfPackets: search resource list using by={}, page={}, size={}, sort={}, header={}",
                by, page, size, sort, xVHFPktFormat);
        // Build the response object.
        final VhfPacketSearchDto respdto = new VhfPacketSearchDto().pkttype(xVHFPktFormat);
        // Create the SQL where statement.
        List<SearchCriteria> params = prh.createMatcherCriteria(by, dateformat);
        // Create the page request.
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        // Get the criteria after burst id substitution
        respdto.criteria(prh.dumpCriteria(params));

        log.trace("Filling dto object with request params: {}", respdto);
        VhfPacketSearchDto setdto = null;
        if (xVHFPktFormat.equals(HeaderFormat.PKT_FORMAT_BINARY.value)) {
            // The client is asking for binary data.
            log.debug("Retrieve BINARY packets");
            setdto = vhfTransferFrameService
                    .searchVhfBinaryPackets(params, preq);
        }
        else if (xVHFPktFormat.equals(HeaderFormat.PKT_FORMAT_DECODED.value)) {
            // The client is asking for decoded packets.
            log.debug("Retrieve DECODED packets");
            setdto = vhfTransferFrameService
                    .searchVhfDecodedPackets(params, preq);
        }
        else if (xVHFPktFormat.equals(HeaderFormat.PKT_FORMAT_RAW.value)) {
            // The client is asking for raw data.
            log.debug("Retrieve RAW packets");
            setdto = vhfTransferFrameService
                    .searchVhfTransferFrames(params, preq);
        }
        else {
            log.error("Bad request, cannot process format {}", xVHFPktFormat);
            throw new VhfCriteriaException("Cannot process format " + xVHFPktFormat);
        }
        respdto.setPage(setdto.getPage());
        respdto.setSize(setdto.getSize());
        respdto.setPackets(setdto.getPackets());
        respdto.setPkttype(setdto.getPkttype());
        return Response.ok().entity(respdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#getVhfPackets(java.lang.String,
     * java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response getVhfPackets(String hash, String xVHFPktFormat, SecurityContext securityContext,
                                  UriInfo info) throws NotFoundException {
        log.info("getVhfPackets: search resource using hash={}", hash);
        // Check the packet format.
        if (xVHFPktFormat == null) {
            xVHFPktFormat = HeaderFormat.PKT_FORMAT_DECODED.value;
        }
        if (xVHFPktFormat.equals(HeaderFormat.PKT_FORMAT_BINARY.value)) {
            // The client is asking for binary data.
            final VhfBinaryPacket entity = vhfBinaryPacketService
                    .getVhfBinaryPacketByHash(hash);

            final VhfBinaryPacketDto dto = mapper.map(entity, VhfBinaryPacketDto.class);
            dto.setPkttype("VhfBinaryPacketDto");
            return Response.ok().entity(dto).build();
        }
        else if (xVHFPktFormat.equals(HeaderFormat.PKT_FORMAT_DECODED.value)) {
            // The client is asking for decoded data.
            final VhfDecodedPacketDto dto = vhfDecodedPacketService
                    .getVhfDecodedPacketByHash(hash);
            dto.setPkttype("VhfDecodedPacketDto");
            return Response.ok().entity(dto).build();
        }
        // Bad packet format request. Send a 400.
        log.error("Bad request, cannot process format {}", xVHFPktFormat);
        throw new VhfRequestFormatException("Cannot process format " + xVHFPktFormat);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#listVhfStationStatusCount(java.
     * lang.String, java.lang.String, javax.ws.rs.core.SecurityContext,
     * javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listVhfStationStatusCount(String from, String to, String timeformat,
                                              SecurityContext securityContext, UriInfo info) throws NotFoundException {
        log.info("listVhfStationStatusCount: search resource count using from={}, to={}, timeformat={}", from,
                to, timeformat);
        List<VhfStationStatusCountDto> dtolist = null;
        Long since = Instant.now().getEpochSecond();
        Long until = since - 3600L;
        // Set times in seconds from input arguments
        if (from != null) {
            if (timeformat.equalsIgnoreCase("iso")) {
                since = prh.getTimeFromArg(from, "yyyyMMdd'T'HHmmssz", "sec");
            }
            else {
                // Consider them as seconds.
                since = Long.valueOf(from);
            }
        }
        if (to != null) {
            if (timeformat.equalsIgnoreCase("iso")) {
                until = prh.getTimeFromArg(to, "yyyyMMdd'T'HHmmssz", "sec");
            }
            else {
                // Consider them as seconds.
                until = Long.valueOf(to);
            }
        }
        // Find station status count.
        dtolist = vhfStationStatusService.findStationStatusCount(since, until);
        final GenericEntity<List<VhfStationStatusCountDto>> entitylist =
                new GenericEntity<List<VhfStationStatusCountDto>>(
                        dtolist) {
                };
        // Send a 200.
        return Response.ok().entity(entitylist).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#listVhfStationStatus(java.lang.
     * String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listVhfStationStatus(String by, Integer page, Integer size, String sort,
                                         SecurityContext securityContext, UriInfo info) throws NotFoundException {
        log.info(
                "listVhfStationStatus: search resource list using by={}, page={}, size={}, sort={}",
                by, page, size, sort);
        // Create filters
        final GenericStringMap filters = prh.getFilters(prh.createMatcherCriteria(by));
        List<SearchCriteria> criteriaList = prh.createMatcherCriteria(by);
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(vhfStationStatusFiltering, criteriaList);
        }
        // Search for vhftransfer frames using where conditions.
        final Page<VhfStationStatus> entitypage = vhfStationStatusService.findAllVhfStationStatus(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());

        final List<VhfStationStatusDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                VhfStationStatusDto.class);
        final Response.Status rstatus = Response.Status.OK;
        final VhfResponseDto setdto = new StationStatusSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .datatype("vhfstationstatus");
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#decodePacket(java.lang.String,
     * java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response decodePacket(String format, String pkt, SecurityContext securityContext,
                                 UriInfo info) throws NotFoundException {
        log.debug("decodePacket: received vhf hexa for packet={} with length {}", pkt,
                pkt.length());
        // Prepare the response.
        final HTTPResponse resp = new HTTPResponse();
        // Check the type of decoding requested.
        if ("swig".equals(format)) {
            // SWIG here represents external decoding service.
            log.trace("Start swig processing...this is in reality REST...");
            final HTTPResponse decodedresp = decoderService.decodeString(pkt);
            final String decoded = decodedresp.getMessage();
            resp.action("decodePacket").code(Response.Status.OK.getStatusCode()).id("REST")
                    .message(decoded + " : " + decoded.toString());
        }
        else if ("java".equals(format)) {
            // Request java only decoding.
            log.trace("Start java processing");
            final BigInteger ipkt = new BigInteger(pkt, 16);
            final String message = "JAVA DECODE PACKET FROM HEXA";
            final VhfTransferFrame decoded = vhfBinaryPacketService
                    .deserialize(ipkt.toByteArray());
            resp.action("decodePacket").code(Response.Status.OK.getStatusCode()).id("JAVA")
                    .message(message + " : " + decoded.toString());

        }
        else if ("all".equals(format)) {
            // Request a full decoding.
            log.trace("Start java+swig processing");
            final BigInteger ipkt = new BigInteger(pkt, 16);
            final String message = "JAVA+SWIG DECODE PACKET FROM HEXA";
            final VhfTransferFrame decoded = vhfBinaryPacketService
                    .deserialize(ipkt.toByteArray());
            final byte[] detpacketarr = decoded.getPacket().getBinaryPacket();
            log.info("Decoding byte array of size: " + detpacketarr.length + "\nHEX: "
                     + DatatypeConverter.printHexBinary(detpacketarr));
            // final VhfDecodedPacket alldecoded = vhfDecodedPacketService
            // .decodeBytes(detpacketarr, decoded.getPacket().getHashId());
            String alldecoded = vhfDecodedPacketService.decodeBytes(detpacketarr,
                    decoded.getPacket().getHashId());
            log.debug("Decoded packet is: " + alldecoded);

            resp.action("decodePacket").code(Response.Status.OK.getStatusCode()).id("ALL")
                    .message(message + " : " + alldecoded.toString());
        }
        else {
            log.error("Bad request, cannot process format {}", format);
            throw new VhfCriteriaException("Cannot process format " + format);
        }
        return Response.ok(info.getRequestUri()).entity(resp).build();
    }

    @Override
    public Response decodePacketList(RawPacketSetDto rawPacketSetDto, String format,
                                     SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.info("Decode a packet list flagged as not decoded : size {} ", rawPacketSetDto.getSize());
        List<RawPacketDto> pktlist = rawPacketSetDto.getResources();
        int ndec = 0;
        List<VhfBasePacket> declist = new ArrayList<>();
        for (RawPacketDto bp : pktlist) {
            String tobedecoded = bp.getPacket();
            VhfRawPacket raw = rawPacketService.findRawPacketByPacket(tobedecoded);
            if (raw != null && raw.getPktflag().equals(DecodedMessageType.TO_BE_DECODED.getTypename())) {
                if (raw.getPacket().length() != 256) {
                    raw.setPktflag(DecodedMessageType.ERROR.getTypename());
                }
                else {
                    ndec++;
                    VhfTransferFrame decoded = vhfTransferFrameFacade.decodeAndSaveFrame(raw);
                    raw.setPktflag(DecodedMessageType.DECODED.getTypename());
                    VhfBasePacket b = new VhfBasePacket();
                    b.setPacket(raw.getPacket());
                    b.setHashId(decoded.getBinaryHash());
                    b.setPkttype("RAW");
                    declist.add(b);
                    log.debug("++++ Raw packet {} is decoded, frame is {}", raw, decoded.getBinaryHash());
                }
                rawPacketService.updateRawPacketFlag(raw);
            }
        }
        String msg = String.format("{ \"decoded_packets\" : %s }", ndec);
        HTTPResponse response = new HTTPResponse();
        response.action("decodeList").code(Response.Status.OK.getStatusCode()).id("decoded").message(msg);
        // Prepare the response. Store number of decoded packets.
        return Response.created(info.getRequestUri()).entity(response).build();
    }

    @Override
    public Response flagDuplicateFrames(Map<String, Long> requestBody, String format, SecurityContext securityContext
            , UriInfo info)
            throws NotFoundException {
        log.info("Find duplicate in time range {} ", requestBody);
        Long from = requestBody.get("tfrom");
        Long to = requestBody.get("tto");
        Map<String, Integer> updcounter = new HashMap<>();
        Map<String, Integer> failedcounter = new HashMap<>();

        Map<String, List<VhfTransferFrame>> alldupframes = vhfTransferFrameService
                .getDuplicatesFrames(from, to);
        for (Map.Entry<String, List<VhfTransferFrame>> hashframes : alldupframes.entrySet()) {
            String hash = hashframes.getKey();
            if (updcounter.keySet().size() < 10) {
                updcounter.put(hash, 0);
                failedcounter.put(hash, 0);
            }
            log.debug("Found duplicate for hash {}", hash);
            int first = 0;
            Long frameid = null;
            for (VhfTransferFrame frame : hashframes.getValue()) {
                log.trace("Analyze frame {}", frame.getFrameId());
                if (first > 0) {
                    // Load the frame and update the isDuplicate field
                    VhfTransferFrame updated = vhfTransferFrameService
                            .updateIsDuplicate(frame.getFrameId(), frameid);
                    if (updcounter.keySet().size() < 10) {
                        if (updated == null) {
                            Integer ff = failedcounter.get(hash);
                            failedcounter.put(hash, ff + 1);
                        }
                        else {
                            Integer ff = updcounter.get(hash);
                            updcounter.put(hash, ff + 1);
                        }
                    }
                    log.trace("Updated frame id {} as duplicate of {}", frame.getFrameId(),
                            frameid);
                }
                else {
                    log.trace("Setting as reference frame id {}", frame.getFrameId());
                    frameid = frame.getFrameId();
                    first++;
                }
            }
        }
        String msg = String.format("{ \"duplicate_packets\" : %s }", updcounter.size());
        HTTPResponse response = new HTTPResponse();
        response.action("flagDuplicateFrames").code(Response.Status.OK.getStatusCode()).id("duplicates").message(msg);
        // Prepare the response. Store number of decoded packets.
        return Response.created(info.getRequestUri()).entity(response).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.svom.vhf.server.swagger.api.VhfApiService#listVhfCounters(java.lang.
     * String, java.lang.Long, java.lang.Long, java.lang.Long,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listVhfCounters(String apidname, String productname, Long obsid,
                                    String pktmintime, String pktmaxtime, String timeformat, String mode,
                                    SecurityContext securityContext, UriInfo info) throws NotFoundException {
        log.debug("listVhfCounters: search counters list using apid={}, obsid={}, from={}, to={}",
                apidname, obsid, pktmintime, pktmaxtime);
        List<ObsIdApidCountDto> dtolist = null;
        // Init parameters for counter.
        // All apids.
        String apid = "%";
        if (!apidname.isEmpty() && !apidname.equals("all")) {
            apid = apidname;
        }
        // All products.
        String product = "%";
        if (!productname.isEmpty() && !productname.equals("all")) {
            product = productname;
        }
        // Interpret as Dates
        Long tsfrom = null;
        Long tsto = null;
        // Set now-1h in seconds for packet min time.
        if (pktmintime.contains("now")) {
            Instant since = Instant.now().minusSeconds(3600);
            pktmintime = Long.toString(since.getEpochSecond());
            timeformat = "number";
        }
        // Set now+1h in seconds for packet max time.
        if (pktmaxtime.contains("now")) {
            Instant until = Instant.now().plusSeconds(3600);
            pktmaxtime = Long.toString(until.getEpochSecond());
            timeformat = "number";
        }
        // Check the time format. In case it is ISO then interpret the min and max time in a different way.
        if (timeformat.equals("iso")) {
            log.debug("Using from and to as times in yyyyMMddTHHmmssz");
            tsfrom = prh.getTimeFromArg(pktmintime, "iso", "sec");
            tsto = prh.getTimeFromArg(pktmaxtime, "iso", "sec");
        }
        else if (timeformat.equals("number")) {
            // If it is number parse it as long.
            tsfrom = Long.parseLong(pktmintime);
            tsto = Long.parseLong(pktmaxtime);
            if (tsfrom > cprops.getTimeOffset()) {
                // Determine the value as the time - offset all in seconds.
                tsfrom = tsfrom - cprops.getTimeOffset();
            }
            if (tsto > cprops.getTimeOffset()) {
                // Determine the value as the time - offset all in seconds.
                tsto = tsto - cprops.getTimeOffset();
            }
        }
        if (obsid == null || obsid < 0) {
            obsid = 1L;
        }
        VhfResponseDto setdto = null;
        if ("detailed".equals(mode)) {
            // Get counters. Use time in seconds.
            dtolist = obsIdApidCountService.findObsIdCount(apid, product, obsid, tsfrom, tsto);
            // Create the Set.
            GenericStringMap criteria = new GenericStringMap();
            criteria.put("apidname", apid);
            criteria.put("obsid", obsid.toString());
            criteria.put("pktmintime", ">=" + tsfrom.toString());
            criteria.put("pktmaxtime", "<" + tsto.toString());
            setdto = new PacketCounterSetDto().resources(dtolist).size((long) dtolist.size())
                    .filter(criteria);
        }
        else {
            // Here we do not use resources array in output, only number of packets retrieved.
            // The range in time is defined before.
            // Get total pkt counter: use time in ms.
            Long countpkt = obsIdApidCountService.findTotalPacketCount(tsfrom * 1000L,
                    tsto * 1000L);
            GenericStringMap criteria = new GenericStringMap();
            criteria.put("from", tsfrom.toString());
            criteria.put("to", tsto.toString());
            setdto = new VhfResponseDto().size(countpkt).filter(criteria);
        }
        // Send a 200.
        return Response.ok().entity(setdto).build();
    }

    @Override
    public Response listVhfNotifications(String by, Integer page, Integer size, String sort, String dateformat,
                                         SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.info("listVhfNotifications: search resource list using by={}, page={}, size={}, sort={}",
                by, page, size, sort);
        // Create filters
        final GenericStringMap filters = prh.getFilters(prh.createMatcherCriteria(by));
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(vhfNotificationFiltering, by);
        }
        if (dateformat == null) {
            dateformat = "ms";
        }
        // Search for vhftransfer frames using where conditions.
        final Page<VhfNotification> entitypage = vhfTransferFrameService.findAllVhfNotifications(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        // Create the DTO list.
        final List<VhfNotificationDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                VhfNotificationDto.class);
        final Response.Status rstatus = Response.Status.OK;
        // Build the response object.
        final VhfResponseDto setdto = new VhfNotificationSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .datatype("vhfnotification");
        // Set the filters applied.
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#listVhfRawPackets(java.lang.
     * String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response listVhfRawPackets(String by, Integer page, Integer size, String sort,
                                      SecurityContext securityContext, UriInfo info) throws NotFoundException {
        log.info("listVhfRawPackets: search resource list using by={}, page={}, size={}, sort={}",
                by, page, size, sort);
        // Create filters
        final List<SearchCriteria> criteriaList = prh.createMatcherCriteria(by);
        final GenericStringMap filters = prh.getFilters(criteriaList);
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(vhfRawPacketFiltering, criteriaList);
        }
        // Search for vhftransfer frames using where conditions.
        final Page<VhfRawPacket> entitypage = rawPacketService.findAllRawPackets(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        // Create the DTO list.
        final List<RawPacketDto> dtolist = edh.entityToDtoList(entitypage.toList(), RawPacketDto.class);
        final Response.Status rstatus = Response.Status.OK;
        // Build the response object.
        final VhfResponseDto setdto = new RawPacketSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .format("RawPacketSetDto")
                .datatype("vhfrawpackets");
        // Set the filters applied.
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#searchObsids(java.lang.String,
     * java.lang.String, java.lang.String, javax.ws.rs.core.SecurityContext,
     * javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response searchObsids(String pktmintime, String pktmaxtime, String timeformat,
                                 SecurityContext securityContext, UriInfo info) throws NotFoundException {
        log.debug(
                "searchObsids: search resource list using pktmintime={}, pktmaxtime={}, timeformat={}",
                pktmintime, pktmaxtime, timeformat);
        // Set times in seconds as default
        Long since = Instant.now().getEpochSecond();
        Long until = since - 3600L;
        // Set times in seconds from input arguments
        if (pktmintime != null) {
            // Check if format is ISO.
            if (timeformat.equalsIgnoreCase("iso")) {
                since = prh.getTimeFromArg(pktmintime, "yyyyMMdd'T'HHmmssz", "sec");
            }
            else {
                // Consider them as seconds.
                since = Long.valueOf(pktmintime);
            }
        }
        if (pktmaxtime != null) {
            if (timeformat.equalsIgnoreCase("iso")) {
                until = prh.getTimeFromArg(pktmaxtime, "yyyyMMdd'T'HHmmssz", "sec");
            }
            else {
                // Consider them as seconds.
                until = Long.valueOf(pktmaxtime);
            }
        }
        // Build the response.
        VhfResponseDto setdto = null;
        // Get total pkt counter: use time in ms.
        List<Map<String, Object>> obsidlist = obsIdApidCountService.findObsidList(since * 1000L,
                until * 1000L);
        GenericStringMap criteria = new GenericStringMap();
        criteria.put("minreceptiontime", since.toString());
        criteria.put("maxreceptiontime", until.toString());
        // Create the Set.
        setdto = new ObsidSetDto().resources(obsidlist).size((long) obsidlist.size())
                .filter(criteria);
        setdto.format("ObsidSetDto");
        // Send a 200.
        return Response.ok().entity(setdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#searchVhfSatAttitudes(java.lang.
     * String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response searchVhfSatAttitudes(@NotNull String by, Integer page, Integer size,
                                          String sort, String dateformat, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.debug(
                "searchVhfSatAttitudes: search resource list using by={}, page={}, size={}, sort={}",
                by, page, size, sort);
        if (dateformat == null) {
            dateformat = "ms";
        }
        // Create filters
        List<SearchCriteria> criteriaList = prh.createMatcherCriteria(by, dateformat);
        final GenericStringMap filters = prh.getFilters(criteriaList);
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(vhfsatattitudeFiltering, criteriaList);
        }
        // Search for vhftransfer frames using where conditions.
        // Get the attitudes.
        final Page<VhfSatAttitude> entitypage = vhfSatelliteService.findAllVhfSatAttitudes(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        // Create the DTO list.
        final List<VhfSatAttitudeDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                VhfSatAttitudeDto.class);
        final Response.Status rstatus = Response.Status.OK;
        // Build the response.
        final VhfResponseDto setdto = new VhfSatAttitudeSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .format("VhfSatAttitudeDto")
                .datatype("vhfsatattitude");
        // Set the filters applied.
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.svom.vhf.server.swagger.api.VhfApiService#searchVhfSatPositions(java.lang.
     * String, java.lang.Integer, java.lang.Integer, java.lang.String,
     * java.lang.String, javax.ws.rs.core.SecurityContext, javax.ws.rs.core.UriInfo)
     */
    @Override
    public Response searchVhfSatPositions(@NotNull String by, Integer page, Integer size,
                                          String sort, String dateformat, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.debug(
                "searchVhfSatPositions: search resource list using by={}, page={}, size={}, sort={}",
                by, page, size, sort);
        if (dateformat == null) {
            dateformat = "ms";
        }
        // Create filters
        List<SearchCriteria> criteriaList = prh.createMatcherCriteria(by, dateformat);
        final GenericStringMap filters = prh.getFilters(criteriaList);
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(vhfsatpositionFiltering, criteriaList);
        }
        // Search for vhftransfer frames using where conditions.
        // Get the sat positions.
        final Page<VhfSatPosition> entitypage = vhfSatelliteService.findAllVhfSatPositions(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        // Create the DTO list.
        final List<VhfSatPositionDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                VhfSatPositionDto.class);
        final Response.Status rstatus = Response.Status.OK;
        // Build the response.
        final VhfResponseDto setdto = new VhfSatPositionSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .format("VhfSatPositionDto")
                .datatype("vhfsatposition");
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    @Override
    public Response listVhfBurstIds(String by, Integer page, Integer size, String sort, String dateformat,
                                    SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.info(
                "listVhfBurstIds: search resource list using by={}, page={}, size={}, sort={} time format {}",
                by, page, size, sort, dateformat);
        if (dateformat == null) {
            dateformat = "ms";
        }
        // Create filters
        List<SearchCriteria> criteriaList = prh.createMatcherCriteria(by, dateformat);
        final GenericStringMap filters = prh.getFilters(criteriaList);
        // Create pagination request
        final PageRequest preq = prh.createPageRequest(page, size, sort);
        BooleanExpression wherepred = null;
        if (!"none".equals(by)) {
            // Create search conditions for where statement in SQL
            wherepred = prh.buildWhere(vhfburstidFiltering, criteriaList);
        }
        // Search for vhftransfer frames using where conditions.
        // Get the burst IDs.
        final Page<VhfBurstId> entitypage = vhfBurstIdService.findAllVhfBurstIds(wherepred, preq);
        RespPage respPage = new RespPage().size(entitypage.getSize())
                .totalElements(entitypage.getTotalElements()).totalPages(entitypage.getTotalPages())
                .number(entitypage.getNumber());
        // Create the DTO list.
        final List<VhfBurstIdDto> dtolist = edh.entityToDtoList(entitypage.toList(),
                VhfBurstIdDto.class);
        // Build the response object.
        final Response.Status rstatus = Response.Status.OK;
        final VhfResponseDto setdto = new VhfBurstIdSetDto().resources(dtolist)
                .size((long) dtolist.size())
                .page(respPage)
                .format("VhfBurstIdDto")
                .datatype("vhfburstid");
        // Add the filters applied.
        if (filters != null) {
            setdto.filter(filters);
        }
        return Response.ok().entity(setdto).build();
    }

    @Override
    public Response updateVhfBurstId(String id, Map<String, Long> body, SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        log.debug(
                "updateVhfBurstId: update resource with if {} using {}", id, body);
        // Search resource.
        VhfBurstId toupd = vhfBurstIdService.getVhfBurstById(id);
        if (body.containsKey("burstEndtime")) {
            toupd.setBurstEndtime(body.get("burstEndtime"));
        }
        else if (body.containsKey("burstStartime")) {
            toupd.setBurstStartime(body.get("burstStartime"));
        }
        // Update the burst id.
        VhfBurstId saved = vhfBurstIdService.storeOrUpdateVhfBurstId(toupd);
        // Create the DTO.
        VhfBurstIdDto dto = mapper.map(saved, VhfBurstIdDto.class);
        // Create the Set.
        final VhfResponseDto setdto = new VhfBurstIdSetDto().addResourcesItem(dto)
                .size((long) 1);
        setdto.format("VhfBurstIdDto");
        return Response.ok().entity(setdto).build();
    }

    @Override
    public Response notifyVhfTransferFrames(Long frameid, Map<String, Long> requestBody,
                                            SecurityContext securityContext, UriInfo info)
            throws NotFoundException {
        // Search resource. This is the packet on which we send the notification.
        VhfTransferFrameDto framedto = vhfTransferFrameService.getVhfTransferFrameById(frameid);
        Map<String, Integer> notified = null;
        Long obsid = null;
        // The framedto is not null because otherwise an exception would be thrown by the previous method.
        Boolean donotify = Boolean.FALSE;
        Long tend = -1L;
        // Extract parameter from the request body.
        if (requestBody.containsKey("notify")) {
            donotify = Boolean.TRUE;
        }
        if (requestBody.containsKey("tend")) {
            tend = requestBody.get("tend");
        }
        if (requestBody.containsKey("obsid")) {
            obsid = requestBody.get("obsid");
        }
        if (donotify && tend > 0) {
            // Send notifications.
            log.info("Send notifications for frame {} and obsid {} until time {}", frameid, obsid, tend);
            notified = vhfTransferFrameService.requestNotification(frameid, tend);
        }
        return Response.ok().entity(notified).build();
    }

    @Override
    public Response deleteObsids(String id, SecurityContext securityContext, UriInfo info) throws NotFoundException {
        log.info(
                "deleteObsids: delete all packets for obsid {}", id);
        // Search resource.
        Long obsid = Long.valueOf(id);
        Integer ndel = obsIdApidCountService.deletePacketsForObsid(obsid);
        // Build the response.
        final HTTPResponse resp =
                new HTTPResponse().message("Removed " + ndel
                                           + " packets").action("deleteObsids").code(Response.Status.OK.getStatusCode()).id(id);
        return Response.ok().entity(resp).build();
    }

    /**
     * Check the criteria and eventually remove the burstId
     *
     * @param params
     * @return String the burstId
     */
    protected String checkCriteriaForBurstId(final List<SearchCriteria> params) {
        String val = "none";
        for (SearchCriteria crit : params) {
            if ("burstid".equalsIgnoreCase(crit.getKey())) {
                val = (String) crit.getValue();
            }
        }
        return val;
    }
}
