/**
 * 
 */
package fr.svom.vhf.server.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.messages.model.DataNotification;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.server.config.NatsProperties;
import fr.svom.vhf.server.messages.VhfPacketMessage;
import fr.svom.vhf.server.messages.VhfPacketMessage.MessageTypeEnum;
import fr.svom.vhf.server.test.generators.DataGenerator;
import fr.svom.vhf.swagger.model.VhfNotificationDto;
import fr.svom.vhf.swagger.model.VhfSatAttitudeDto;
import io.micrometer.core.instrument.util.StringEscapeUtils;
import ma.glasnost.orika.MapperFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class VhfJacksonTest {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ObjectMapper jacksonMapper;
    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    final String s = String.join("\n"
            , " {"
            ,	"\"message_date\" : \"2018-11-22T09:32:33+0100\",  "
            ,	"\"message_class\" : \"DataNotification\",  "
            , " \"content\" : {"
            , " \"data_stream\" : \"VHF\", "
            , " \"obsid\" : 1000, "
            , " \"product_card\": \"raw-packet\", "
            , " \"date\": \"2018-11-22T09:32:33+0100\", "
            , " \"message\": \"0222400100570000003e000000010000001d47524d4c4355524849500047524d4c4355524849500047524d4c4355524849500047524d4c4355524849500047524d4c4355524849500047524d4c435552484950000000000000000000bf27\", "
            , " \"resource_locator\": \"http://irfumcj144.extra.cea.fr:8080/api/vhf/packet/5a1c348a0a8b881f5e0f7e934be52cb3609f6d645f05fad9cd3555a1bba10deb\" "
            , "}}"
    );

    @Test
    public void testA_customSerializers() {
        VhfSatAttitude sat = DataGenerator.generateAttitude();
        VhfSatAttitudeDto dto = mapper.map(sat, VhfSatAttitudeDto.class);
        try {
            String json = jacksonMapper.writeValueAsString(dto);
            assertThat(json).contains("TmVhfEclairsAlert");
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        VhfNotification notif = DataGenerator.generateNotification();
        VhfNotificationDto ndto = mapper.map(notif, VhfNotificationDto.class);
        try {
            String json = jacksonMapper.writeValueAsString(ndto);
            assertThat(json).contains("insertionTime");
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testB_offsetDate() {
        Map<String, Object> amap = new HashMap<String, Object>();
        log.info("Deserialize json {}", s);
        try {
            amap = jacksonMapper.readValue(s, new TypeReference<Map<String, Object>>(){});
            amap.forEach((k, v) -> log.info(String.format("%s ==>> %s",k, v)));
            for (String kk : amap.keySet()) {
                log.info("Found key {}", kk);
                Object val = amap.get(kk);
            }
            if (amap.get("message_class").equals("DataNotification")) {
                DataNotification amapnot = jacksonMapper.readValue(s,
                        new TypeReference<DataNotification>(){});
               log.info("Parsed string into DataNotification : {}", amapnot.toString());
            }
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testC_jsonEscape() {
        String jsonString = "{\"Age\":40, \"Name\":\"Sample User\"}";
        try {
            JsonNode jnode = jacksonMapper.readValue(jsonString, JsonNode.class);
            log.debug("Json string is {}", jsonString);
            log.debug("Json node age is {}", jnode.path("Age").asText());
            for (Iterator<Map.Entry<String, JsonNode>> it = jnode.fields(); it.hasNext(); ) {
                Map.Entry<String, JsonNode> entries = it.next();
                log.debug("Field is {} = {}", entries.getKey(), entries.getValue().asText());
            }
        }
        catch (JsonProcessingException e) {
            log.error("Cannot parse json....");
            e.printStackTrace();
        }
    }

    @Test
    public void testC_jsonNode() {
        try {
            // JSON string
            String json = "{\"name\":\"John Doe\",\"email\":\"john.doe@example.com\"," +
                          "\"roles\":[\"Member\",\"Admin\"],\"admin\":true,\"city\"" +
                          ":\"New York City\",\"country\":\"United States\"}";

            // create object mapper instance
            ObjectMapper mapper = new ObjectMapper();

            // convert JSON string to `JsonNode`
            JsonNode node = mapper.readTree(json);

            // print JSON nodes
            System.out.println(node.path("name").asText());
            System.out.println(node.path("email").asText());
            System.out.println(node.path("roles").get(0).asText());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testD_Vhf() {
        String js = "{\"CcsdsTmHeadr\":{\"CcsdsApid\":577,\"CcsdsGFlag\":0,"
                    + "\"CcsdsCounter\":1,\"CcsdsPLength\":87},"
                    + "\"VhfTmHeader\":{\"PacketTimeS\":1592775223},"
                    + "\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":200},"
                    + "\"AlertTimeTb0AbsSeconds\":1592775219,\"AlertTimeTb0AbsMilliseconds\":69,"
                    + "\"LightCurvePacketNumber\":2,\"SatAttitudeLCQ0\":14058,\"SatAttitudeLCQ1\":21521,"
                    + "\"SatAttitudeLCQ2\":9840,\"SatAttitudeLCQ3\":17781,\"SatPositionLon\":69,"
                    + "\"SatPositionLat\":17228,\"Sample0Eband0IntCount\":18297,"
                    + "\"Sample0Eband1IntCount\":6168,\"Sample0Eband2IntCount\":6067,"
                    + "\"Sample0Eband3IntCount\":6757,\"Sample0EsatIntCount\":0,"
                    + "\"Sample0MultipleIntCount\":0,\"Sample1Eband0DiffCount\":2054,"
                    + "\"Sample1Eband1DiffCount\":642,\"Sample1Eband2DiffCount\":588,"
                    + "\"Sample1Eband3DiffCount\":548,\"Sample1EsatDiffCount\":0,"
                    + "\"Sample1MultipleDiffCount\":0,\"Sample2Eband0DiffCount\":3023,"
                    + "\"Sample2Eband1DiffCount\":1140,\"Sample2Eband2DiffCount\":1238,"
                    + "\"Sample2Eband3DiffCount\":1467,\"Sample2EsatDiffCount\":0,"
                    + "\"Sample2MultipleDiffCount\":0,\"Sample3Eband0DiffCount\":2730,"
                    + "\"Sample3Eband1DiffCount\":1048,\"Sample3Eband2DiffCount\":1238,"
                    + "\"Sample3Eband3DiffCount\":1348,\"Sample3EsatDiffCount\":0,"
                    + "\"Sample3MultipleDiffCount\":0,\"VhfCrc\":64116}";

        try {
            log.debug("Analyse string {}", js);
            JsonNode jnode = jacksonMapper.readTree(js);
            log.debug("Json string is {}", js);
            log.debug("Json node CcsdsTmHeadr is {}", jnode.path("CcsdsTmHeadr").asText());
            for (Iterator<Map.Entry<String, JsonNode>> it = jnode.fields(); it.hasNext(); ) {
                Map.Entry<String, JsonNode> entries = it.next();
                log.debug("Field is {} = {}", entries.getKey(), entries.getValue().asText());
            }
        }
        catch (JsonProcessingException e) {
            log.error("Cannot parse json....");
            log.error("Error is {}", e.getMessage());
        }

    }

    @Test
    public void testD_Date() {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
                .optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd()
                .appendPattern("x")
                .toFormatter();

        String adatestr = "2013-09-20T07:00:33.09999+0100";
        System.out.println(" - input date is: " + adatestr);
        OffsetDateTime odt = OffsetDateTime.parse(adatestr, formatter);
        System.out.println("odt is: " + odt.toString());
        adatestr = "2013-09-20T07:00:33+0100";
        System.out.println(" - input date is: " + adatestr);
        odt = OffsetDateTime.parse(adatestr, formatter);
        System.out.println("odt is: " + odt.toString());
    }
}
