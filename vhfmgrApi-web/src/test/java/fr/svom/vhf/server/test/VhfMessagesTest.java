/**
 * 
 */
package fr.svom.vhf.server.test;

import static org.assertj.core.api.Assertions.assertThat;

import fr.svom.vhf.server.config.NatsProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.server.messages.VhfPacketMessage;
import fr.svom.vhf.server.messages.VhfPacketMessage.MessageTypeEnum;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class VhfMessagesTest {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Test
    public void natsPropertiesTest() {
        NatsProperties props = new NatsProperties();
        props.setVhfDecodedQueue("none");
        props.setVhfQueue("tmp");
        props.setClientId("svom");
        props.setCluster("VHF");
        props.setServer("natssrv");
        props.setPassword("none");
        props.setPort("8888");
        props.setUser("svom");
        assertThat(props.toString().length()).isGreaterThan(0);

        assertThat(props.getVhfDecodedQueue()).isEqualTo("none");
        assertThat(props.getVhfQueue()).isEqualTo("tmp");
        assertThat(props.getClientId()).isEqualTo("svom");
        assertThat(props.getCluster()).isEqualTo("VHF");
        assertThat(props.getServer()).isEqualTo("natssrv");
        assertThat(props.getPassword()).isEqualTo("none");
        assertThat(props.getPort()).isEqualTo("8888");
        assertThat(props.getUser()).isEqualTo("svom");
    }

    @Test
    public void messages1Test() throws VhfServiceException {
        final VhfPacketMessage msg = new VhfPacketMessage();
        msg.data("SomePacketType");
        msg.messageType(MessageTypeEnum.FITS);
        msg.messageQueue("anatsqueue");
        assertThat(msg.toString().length()).isGreaterThan(0);
    }
    
    @Test
    public void messages2Test() throws VhfServiceException {
        final VhfPacketMessage msg1 = new VhfPacketMessage();
        msg1.setData("SomePacketType");
        msg1.setMessageType(MessageTypeEnum.FITS);
        msg1.setMessageQueue("anatsqueue");
        msg1.setResource("someresource");
        msg1.setResourceUrl("someresourceurl");

        final VhfPacketMessage msg2 = new VhfPacketMessage();
        msg2.setData("SomePacketType");
        msg2.setMessageType(MessageTypeEnum.FITS);
        msg2.setMessageQueue("anatsqueue");
        msg2.resource("someresource");
        msg2.resourceUrl("someresourceurl");
        assertThat(msg1.equals(msg2)).isTrue();
        
        assertThat(msg2.hashCode()).isNotNull();
        assertThat(msg2.getData()).isEqualTo("SomePacketType");
        assertThat(msg2.getMessageQueue()).isEqualTo("anatsqueue");
        assertThat(msg2.getResource()).isEqualTo("someresource");
        assertThat(msg2.getResourceUrl()).isEqualTo("someresourceurl");
        
        final MessageTypeEnum ej = MessageTypeEnum.fromValue("encoded/json");
        assertThat(ej).isNotNull();

    }
}
