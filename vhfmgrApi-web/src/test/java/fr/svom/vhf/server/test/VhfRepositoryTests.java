package fr.svom.vhf.server.test;

import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.RawPacketRepository;
import fr.svom.vhf.data.repositories.VhfBinaryPacketRepository;
import fr.svom.vhf.data.repositories.VhfBurstIdRepository;
import fr.svom.vhf.data.repositories.VhfDecodedPacketRepository;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import fr.svom.vhf.server.test.generators.APIDS;
import fr.svom.vhf.server.test.generators.ApidGenerator;
import fr.svom.vhf.server.test.generators.DataGenerator;
import fr.svom.vhf.server.test.generators.StationGenerator;
import org.apache.commons.codec.binary.Hex;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class VhfRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VhfGroundStationRepository repository;

    @Autowired
    private PacketApidRepository apidrepository;

    @Autowired
    private RawPacketRepository vhfrawrepository;

    @Autowired
    private VhfBinaryPacketRepository vhfBinaryPacketRepository;

    @Autowired
    private VhfBurstIdRepository vhfBurstIdRepository;

    @Autowired
    private VhfDecodedPacketRepository vhfDecodedPacketRepository;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    List<VhfGroundStation> stationlist = null;
    List<PacketApid> apidlist = null;

    @Before
    public void initData() {
        stationlist = new StationGenerator().getStationList();
        apidlist = new ApidGenerator().getApidList();
    }

    @Test
    public void testApidEnum() {
        final APIDS ap = APIDS.ECLALERTL1;
        final Integer apid = ap.apid();
        assertThat(apid).isEqualTo(576);
    }

    @Test
    public void testVhfGroundStation() throws Exception {
        Optional<VhfGroundStation> station = this.repository.findById(1);
        if (station.isPresent()) {
            log.info("Found ground station " + station.get());
        } else {
            for (final VhfGroundStation st : stationlist) {
                log.info("Persist station {}", st);
                this.entityManager.persist(st);
            }
            this.entityManager.flush();
            this.entityManager.getEntityManager().getTransaction().commit();
            final Optional<VhfGroundStation> stationd = this.repository.findById(1);
            assertThat(stationd.get()).isNotNull();
            station = stationd;
        }
        assertThat(station.get().getId()).isEqualTo("stvhf1");
        assertThat(station.get().getLocation()).isEqualTo("Paris");
    }

    @Test
    public void testPacketApid() throws Exception {
        Optional<PacketApid> apid = this.apidrepository.findById(576);
        if (apid.isPresent()) {
            log.info("Found apid " + apid.get());
        } else {
            // There is no apid defined probably...store and retry.
            for (final PacketApid packetApid : apidlist) {
                log.info("Persist apid {}", packetApid);
                this.entityManager.persist(packetApid);
            }
            this.entityManager.flush();
            this.entityManager.getEntityManager().getTransaction().commit();
            final Optional<PacketApid> napid = this.apidrepository.findById(576);
            log.error("cannot find apid....something is wrong with the storage");
            assertThat(napid.get()).isNotNull();
            apid = napid;
        }
        assertThat(apid.get().getClassName()).isEqualTo("TmVhfEclairsAlert");
        assertThat(apid.get().getName()).isEqualTo("ECLALERTL1");
    }

    @Test
    public void testVhfRawPacket() throws Exception {
        final VhfRawPacket vhftf = DataGenerator.generateVhfRawPacket("ASCII", "TEST",
                "ALONGHEXASTRING");
        final VhfRawPacket saved = vhfrawrepository.save(vhftf);
        log.info("Saved frame {}", saved);
        assertThat(saved.getPacket()).isEqualTo("ALONGHEXASTRING");
    }

    @Test
    public void testVhfBinaryPacket() throws Exception {
        final VhfBinaryPacket pkt = DataGenerator.generateVhfBinaryPacket("anhashid", "binary",
                "ABINARYPKT");
        final VhfBinaryPacket saved = vhfBinaryPacketRepository.save(pkt);
        log.info("Saved binary packet {}", saved);
        assertThat(saved.getPacket()).isEqualTo(Hex.encodeHexString("ABINARYPKT".getBytes()));
        assertThat(saved.getHashId()).isEqualTo("anhashid");
    }

    @Test
    public void testVhfDecodedPacket() throws Exception {
        final VhfDecodedPacket pkt = DataGenerator.generateVhfDecodedPacket("anhashid", "json",
                "ADECODEDPKT");
        final VhfDecodedPacket saved = vhfDecodedPacketRepository.save(pkt);
        log.info("Saved decoded packet {}", saved);
        assertThat(saved.getPacket()).isEqualTo("ADECODEDPKT");
        assertThat(saved.getHashId()).isEqualTo("anhashid");
    }

    @Test
    public void testVhfBurstId() throws Exception {
        final VhfBurstId pkt = DataGenerator.generateBurstId();
        final VhfBurstId saved = vhfBurstIdRepository.save(pkt);
        log.info("Saved burstid {}", saved);
        assertThat(saved.getBurstId()).isEqualTo("sb20050599");
        assertThat(saved.getPacketTime()).isEqualTo(pkt.getPacketTime());
    }

}
