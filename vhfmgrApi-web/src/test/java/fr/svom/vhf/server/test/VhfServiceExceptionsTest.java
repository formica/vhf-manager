/**
 *
 */
package fr.svom.vhf.server.test;

import fr.svom.messages.model.VhfLogMessage;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.repositories.VhfTransferFrameRepository;
import fr.svom.vhf.server.controllers.PageRequestHelper;
import fr.svom.vhf.server.services.ObsIdApidCountService;
import fr.svom.vhf.server.services.PacketApidService;
import fr.svom.vhf.server.services.VhfBinaryPacketService;
import fr.svom.vhf.server.services.VhfGroundStationService;
import fr.svom.vhf.server.services.VhfLogMessageService;
import fr.svom.vhf.server.services.VhfRawPacketService;
import fr.svom.vhf.server.services.VhfTransferFrameService;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import ma.glasnost.orika.MapperFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration
@ActiveProfiles("test")
public class VhfServiceExceptionsTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    @Autowired
    VhfBinaryPacketService vhfBinaryPacketService;
    @Autowired
    VhfTransferFrameRepository vhfTransferFrameRepository;
    @Autowired
    VhfTransferFrameService vhfTransferFrameService;
    @Autowired
    VhfGroundStationService vhfGroundStationService;
    @Autowired
    PacketApidService packetApidService;
    @Autowired
    PageRequestHelper prh;
    @Autowired
    VhfRawPacketService vhfRawPacketService;
    @Autowired
    ObsIdApidCountService obsIdApidCountService;
    @Autowired
    VhfLogMessageService vhfLogMessageService;

    @Test(expected = VhfServiceException.class)
    public void missingIdTest() throws VhfServiceException {
        // Test log message
        VhfLogMessage msg = null;
        try {
            msg = vhfLogMessageService.getVhfLogMessage(null);
        }
        catch (final VhfServiceException e) {
            throw e;
        }
    }

    @Test(expected = AbstractCdbServiceException.class)
    public void nullIdTest() throws AbstractCdbServiceException {
        final VhfTransferFrameDto wrongid = vhfTransferFrameService.getVhfTransferFrameById(null);
        log.debug("Wrong id transfer frame {}", wrongid);
    }

    @Test(expected = AbstractCdbServiceException.class)
    public void bysearchTest() throws AbstractCdbServiceException {
        vhfTransferFrameService.searchVhfTransferFrameEntities(null, -1, 10, "FRAMEID:ASC");
    }
}
