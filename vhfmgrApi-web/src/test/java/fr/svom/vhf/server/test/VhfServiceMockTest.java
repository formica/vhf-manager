/**
 * 
 */
package fr.svom.vhf.server.test;

import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.mappers.NotifStatus;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.VhfBinaryPacketRepository;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import fr.svom.vhf.data.repositories.VhfNotificationRepository;
import fr.svom.vhf.data.repositories.VhfTransferFrameRepository;
import fr.svom.vhf.server.controllers.SvomRestDetectorPacketDecoderBean;
import fr.svom.vhf.server.controllers.SvomVhfMessagesController;
import fr.svom.vhf.server.externals.DecodedMessageType;
import fr.svom.vhf.server.externals.DecoderRestTemplate;
import fr.svom.vhf.server.externals.NatsController;
import fr.svom.vhf.server.services.VhfDecodedPacketService;
import fr.svom.vhf.server.services.VhfRawPacketService;
import fr.svom.vhf.server.services.VhfTransferFrameFacade;
import fr.svom.vhf.server.services.VhfTransferFrameService;
import fr.svom.vhf.server.swagger.api.impl.HeaderFormat;
import fr.svom.vhf.server.test.generators.ApidGenerator;
import fr.svom.vhf.server.test.generators.DataGenerator;
import fr.svom.vhf.server.test.generators.StationGenerator;
import fr.svom.vhf.swagger.model.HTTPResponse;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration
@EnableAsync
public class VhfServiceMockTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VhfTransferFrameFacade vhfTransferFrameFacade;
    
    @Autowired
    private VhfDecodedPacketService vhfDecodedPacketService;
    @Autowired
    private VhfRawPacketService vhfRawPacketService;
    @Autowired
    private VhfTransferFrameService vhfTransferFrameService;
    @Autowired
    private VhfGroundStationRepository vhfGroundStationRepository;
    @Autowired
    private PacketApidRepository packetApidRepository;
    @Autowired
    private VhfBinaryPacketRepository vhfBinaryPacketRepository;
    @Autowired
    private VhfTransferFrameRepository vhfTransferFrameRepository;
    /**
     * Repository.
     */
    @Autowired
    private VhfNotificationRepository vhfNotificationRepository;

    /**
     * Controller.
     */
    @Autowired
    private SvomVhfMessagesController svomVhfMessagesController;

    @MockBean
    private DecoderRestTemplate restDecoder;

    @MockBean
    private NatsController nats;

    //@MockBean(name = "decoder")
    @Autowired
    private SvomRestDetectorPacketDecoderBean decoderBean;
    
    
    List<VhfGroundStation> stationlist = null;
    List<PacketApid> apidlist = null;
    VhfTransferFrame entity = DataGenerator.generateFrame("none");
    VhfTransferFrame toberemoved = null;

    @BeforeAll
    public void initData() {
        log.info("Start data initialization in VhfServiceMockTest");
        stationlist = new StationGenerator().getStationList();
        apidlist = new ApidGenerator().getApidList();
        for (VhfGroundStation station : stationlist) {
            vhfGroundStationRepository.save(station);
        }
       for (PacketApid apid : apidlist) {
           log.info("Save apid {}", apid);
           packetApidRepository.save(apid);
       }
       VhfBinaryPacket bin = new VhfBinaryPacket();
       bin.setBinaryPacket("test".getBytes());
       bin.setHashId("anhash");
       bin.setSize(4);
       bin.setPktformat("BIN");
       vhfBinaryPacketRepository.save(bin);
       entity.setBinaryHash("anhash");
       entity.setPacket(bin);
       entity.setIsFrameDuplicate(Boolean.FALSE);
       entity.setIsFrameValid(Boolean.FALSE);
       log.info("Finished data initialization in VhfServiceMockTest");
    }

    @Test
    public void test0_init() {
        log.info("Init test for VhfServiceMockTest");
        stationlist = new StationGenerator().getStationList();
        apidlist = new ApidGenerator().getApidList();
        for (VhfGroundStation station : stationlist) {
            log.info("Save station {}", station);
            vhfGroundStationRepository.save(station);
        }
        for (PacketApid apid : apidlist) {
            log.info("Save apid {}", apid);
            packetApidRepository.save(apid);
        }
        Optional<PacketApid> apid = packetApidRepository.findById(576);
        if (apid.isPresent()) {
            assertThat(apid.get().getApid()).isEqualTo(576);
        }
        else {
            assertThat(apid.get()).isNotNull();
        }
        VhfBinaryPacket bin = new VhfBinaryPacket();
        bin.setBinaryPacket("test".getBytes());
        bin.setHashId("anhash");
        bin.setSize(4);
        bin.setPktformat("BIN");
        vhfBinaryPacketRepository.save(bin);
        entity.setBinaryHash("anhash");
        entity.setPacket(bin);
        entity.setIsFrameDuplicate(Boolean.FALSE);
        entity.setIsFrameValid(Boolean.FALSE);
        log.info("Finish initialization test");
        vhfTransferFrameService.storeVhfTransferFrame(entity);
        log.info("Vhf frame saved for {}", entity);
    }

    @Test
    public void testA_mockvhfTransferFrameTest() throws VhfDecodingException {
        log.info("=============== Transfer Frame test with mock ================");
        
        final String json = "{ \"type\" : \"TestAlert\", \"content\" : \"some other content\"}";
        final HTTPResponse resp = new HTTPResponse();
        final String pktdec = DataGenerator.generateBinaryString1();

        resp.action("decode");
        resp.id(pktdec);
        resp.message(json);
        resp.code(200);

        Mockito.when(restDecoder.getDecodedPacket(any(String.class))).thenReturn(resp);
        //Mockito.when(decoderBean.decodeString(any(String.class))).thenReturn(resp.getMessage());
        //Mockito.when(mockvhfDecodedPacketService.decodePacket(any(String.class))).thenReturn(json);
       

        final String pkt = DataGenerator.generateAlertFramePacket();
        final VhfRawPacket vhftf = new VhfRawPacket();
        vhftf.setPacket(pkt);
        vhftf.setPktflag(DecodedMessageType.TO_BE_DECODED.getTypename());
        vhftf.setPktformat("HEX");
        vhftf.setSize(pkt.length());

        final HTTPResponse respret = restDecoder.getDecodedPacket(pktdec);
        assertThat(respret.getCode()).isEqualTo(resp.getCode());
        log.info("Received response from mock rest decoder {}", respret.getMessage());
        final HTTPResponse jsonresp = decoderBean.decodeString(pktdec);
        final String jsonret = jsonresp.getMessage();
        assertThat(jsonret).isEqualTo(json);
        log.info("Received response from mock decoder bean {}", jsonret);
        try {
            final HTTPResponse decodedresp = vhfDecodedPacketService.decodePacket(pktdec);
            final String decoded = decodedresp.getMessage();
            log.info("Found decoded packet {}", decoded);
            assertThat(decoded).isNotEmpty();
            vhfTransferFrameFacade.createVhfTransferFrameFromRawPacket(vhftf);
            log.info("Called method createVhfTransferFrameFromRawPacket = {}", vhftf.getPacket());
            Page<VhfTransferFrame> frames = vhfTransferFrameService.findAllVhfTransferFrames(null, null);

            for (VhfTransferFrame dto : frames.toList()) {
                log.info("Found registered frame {}", dto);
            }
        }
        catch (final VhfServiceException e) {
            // TODO Auto-generated catch block
            log.error("Error in testA_mockvhfTransferFrameTest");
            e.printStackTrace();
        }
    }

    @Test
    public void testB_mockvhfTransferFrameTest() throws VhfDecodingException {
        log.info("=============== Transfer Frame test with mock ================");
        
        final String json = "{ \"type\" : \"TmVhfEclairsAlert\", \"content\" : \"{\"CcsdsTmHeadr\":{\"CcsdsApid\":576,\"CcsdsGFlag\":3,\"CcsdsCounter\":2,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592721094},\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":100},\"AlertMsgCount\":2,\"AlertTimeTbAbsSeconds\":1592721093,\"AlertTimeTbAbsMicroseconds\":4410433,\"AlertTimeTb\":1279611476,\"AlertSlewRequest\":0,\"AlertSkyTheta\":1242959,\"AlertSkyPhi\":9995402,\"AlertCatSrcNum\":19525,\"AlertSkyIPix\":82,\"AlertSkyJPix\":84,\"AlertSkyIFit\":19505,\"AlertSkyJFit\":69,\"AlertSkyCnt\":204.25506591796875,\"AlertSkyCntFit\":3365.2685546875,\"AlertSkyVar\":1.8665822221208828E-9,\"AlertQuality\":76,\"AlertTriggerCriterium\":16716,\"AlertDetCounts\":4543060,\"AlertDetBkg\":4993280,\"SatPositionX\":5228229.5,\"SatPositionY\":-4190497.5,\"SatPositionZ\":2015292.875,\"SatAttitudeQ0\":0.4290030896663666,\"SatAttitudeQ1\":0.6567589044570923,\"SatAttitudeQ2\":0.30030471086502075,\"SatAttitudeQ3\":0.5426243543624878,\"SatAttQuality\":0,\"Spare08\":0,\"VhfCrc\":35088}\"}";
        final HTTPResponse resp = new HTTPResponse();
        final String pktdec = DataGenerator.generateBinaryString2();

        resp.action("decode");
        resp.id(pktdec);
        resp.message(json);
        resp.code(200);

        Mockito.when(restDecoder.getDecodedPacket(any(String.class))).thenReturn(resp);
        String hexpkt = "280000005eefd23600000000000000000000000000000000000100001876eb0018000240c00100575eefd234000000c8015eefd233434c414c455254001306c998bc584c4552544c310045434c414c4552544c310045434c414c4552544c31004a9f8d8bca7fc48649f601e73edba64b3f28215a3e99c18a3f0ae96e00008c10";
        
        VhfRawPacket raw;
        try {
            raw = vhfRawPacketService.storeRawPacket(hexpkt, HeaderFormat.PKT_MODE_PROD.value);
            log.info("Generated raw packet {}", raw);
            // Create and store transfer frame.
            vhfTransferFrameFacade.createVhfTransferFrameFromRawPacket(raw);
            
            List<VhfRawPacket> rawlist = vhfRawPacketService.findRawPacketByFlag("OK", new Timestamp(0L));
            assertThat(rawlist.size()).isGreaterThanOrEqualTo(0);
        }
        catch (VhfServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testC_mockvhfSvomMessageTest() throws VhfDecodingException {
        
        //Mockito.when(nats.asyncPublishToQueue(any(String.class), any(String.class))).thenReturn(NotifStatus.SENT.name());
        Mockito.when(nats.publishToQueue(any(), any(String.class))).thenReturn(NotifStatus.SENT.name());

        try {
            // Store the frame.
            VhfTransferFrame saved = vhfTransferFrameRepository.save(entity);
            log.info("testC_mockvhfSvomMessageTest use entity: {}", entity);
            String notifstr = nats.publishToQueue("test", "test");
            assertThat(notifstr).isEqualTo(NotifStatus.SENT.name());
            log.info("Testing nats mock directly ok: {}", notifstr);
            NotifStatus notif = svomVhfMessagesController.sendNotification(saved, 2000L);
            assertThat(notif.name()).isEqualTo(NotifStatus.SENT.name());
            log.info("Testing nats mock from svomVhfMessagesController ok: {}", notifstr);
        }
        catch (VhfServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

/////    @Test
    public void testD_mockvhfRemoveTest() {
        log.info("Removing data for frame {} and binary anhash", toberemoved);
        if (toberemoved != null) {
            Long nnotif = vhfNotificationRepository.deleteByFrame(toberemoved);
            log.info("Cleaned notification : {}", nnotif);
            vhfTransferFrameRepository.deleteById(toberemoved.getFrameId());
        }
        vhfBinaryPacketRepository.deleteById("anhash");
        log.info("Data removal in VhfServiceMockTest");
    }

}
