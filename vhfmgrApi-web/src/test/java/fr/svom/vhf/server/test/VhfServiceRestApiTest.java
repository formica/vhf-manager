/**
 * 
 */
package fr.svom.vhf.server.test;

/**
 * @author formica
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.server.externals.DecoderRestTemplate;
import fr.svom.vhf.server.services.VhfBurstIdService;
import fr.svom.vhf.server.test.generators.ApidGenerator;
import fr.svom.vhf.server.test.generators.DataGenerator;
import fr.svom.vhf.server.test.handlers.RestUtils;
import fr.svom.vhf.server.test.handlers.VhfErrorHandler;
import fr.svom.vhf.swagger.model.GenericMap;
import fr.svom.vhf.swagger.model.HTTPResponse;
import fr.svom.vhf.swagger.model.PacketApidDto;
import fr.svom.vhf.swagger.model.RawPacketDto;
import fr.svom.vhf.swagger.model.RawPacketSetDto;
import fr.svom.vhf.swagger.model.VhfGroundStationDto;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration
@EnableAsync
public class VhfServiceRestApiTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    static {
        System.setProperty("nats.server.ip", "localhost");
        System.setProperty("nats.server.port", "4222");
    }

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    @Qualifier("jacksonMapper")
    private ObjectMapper jacksonMapper;

    @MockBean
    DecoderRestTemplate restDecoder;

    @Autowired
    VhfBurstIdService vhfBurstIdService;
   
    @Test
    public void vhf02_PacketApidTest() throws Exception {
        log.info(">>>>> START vhf02_PacketApidTest ");

        final ApidGenerator agen = new ApidGenerator();
        final List<PacketApidDto> apidlist = agen.getApidDtoList();
        PacketApidDto tobeused = null;
        for (final PacketApidDto dto : apidlist) {
            if (tobeused == null) {
                tobeused = dto;
            }
            log.info(">>>>> Storing apid object: " + dto);
            log.info("Check if exists...retrieve apid {}", dto.getApid());
            final ResponseEntity<String> response = restTemplate.exchange(
                    "/api/apids/" + dto.getApid(), HttpMethod.GET, null, String.class);
            final String responseBody = response.getBody();
            try {
                if (RestUtils.isError(response.getStatusCode())) {
                    final HTTPResponse error = jacksonMapper.readValue(responseBody,
                            HTTPResponse.class);
                    log.error("Response got error " + error.getError() + " " + error.getMessage());
                    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
                }
                else {
                    log.info("Response from server is: " + responseBody);
                    final PacketApidDto storedto = jacksonMapper.readValue(responseBody,
                            PacketApidDto.class);
                    if (storedto.getApid().equals(dto.getApid())) {
                        log.info("Apid already stored...skip insertion for " + dto.getApid());
                        continue;
                    }
                }
            }
            catch (final IOException e) {
                throw new RuntimeException(e);
            }
            final HttpEntity<PacketApidDto> request = new HttpEntity<>(dto);
            final PacketApidDto storeddto = this.restTemplate.postForObject("/api/apids", request,
                    PacketApidDto.class);
            log.info(">>>>> Stored object: " + storeddto);
            assertThat(storeddto.getApid()).isEqualTo(dto.getApid());

        }
        tobeused.apid(null);
        final HttpEntity<PacketApidDto> request2 = new HttpEntity<>(tobeused);
        final ResponseEntity<String> storednullapiddto = this.restTemplate.postForEntity("/api/apids", request2,
                String.class);
        log.info(">>>>> Stored null apid object: " + storednullapiddto.getStatusCode());
        assertThat(storednullapiddto.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        final ResponseEntity<String> getnull = restTemplate.exchange("/api/apids/7",
                HttpMethod.GET, null,String.class);
        assertThat(getnull.getStatusCode()).isGreaterThan(HttpStatus.OK);
        log.info("Retrieved unexisting apid " + getnull);

        final HttpHeaders headers = new HttpHeaders();
        final HttpEntity<?> httpentity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange("/api/apids?by=name:%,id>500",
                HttpMethod.GET, httpentity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved apid list " + respmon);

        final ResponseEntity<String> respmonall = restTemplate.exchange("/api/apids?by=none&sort=NOTHERE:DESC",
                HttpMethod.GET, httpentity, String.class);
        assertThat(respmonall.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        log.info("Retrieved error apid list " + respmonall.getBody());

        final PacketApidDto body = restTemplate.getForObject("/api/apids/576",
                PacketApidDto.class);
        assertThat(body.getApid()).isEqualTo(576);
        log.info("Retrieved apid alert " + body);
        
        body.setCategory("modify description");
        final HttpEntity<PacketApidDto> updrequest = new HttpEntity<PacketApidDto>(body);
        
        final HttpEntity<PacketApidDto> updatedto = restTemplate.exchange("/api/apids/576",
                HttpMethod.PUT, updrequest, PacketApidDto.class);
        log.info(">>>>> Updated object: " + updatedto.getBody());
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
 
        final ResponseEntity<String>  updatenulldto = restTemplate.exchange("/api/apids/1",
                HttpMethod.PUT, updrequest, String.class);
        log.info(">>>>> Updated non existing object: " + updatenulldto.getBody());
        assertThat(updatenulldto.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        
        final ResponseEntity<String> respcreateexisting = restTemplate.exchange("/api/apids",
                HttpMethod.POST, updrequest, String.class);
        log.info(">>>>> Create existing object: " + respcreateexisting.getBody());
        assertThat(respcreateexisting.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        log.info(">>>>> END vhf02_PacketApidTest ");

    }

    @Test
    public void vhf03_GroundStationApiTest() {
//        log.info("Running in " + System.getProperty("java.library.path"));
        log.info(">>>>> START vhf03_GroundStationApiTest ");

        final VhfGroundStation entity = DataGenerator.generateVhfGroundStation(0x55, "stvhf55",
                "00:01:55:55");
        final VhfGroundStationDto dto = new VhfGroundStationDto();
        dto.setMacaddress(entity.getMacaddress());
        dto.setName(entity.getName());
        dto.setId(entity.getId());
        dto.setStationId(55);
        dto.setLocation("PARIS");
        log.info(">>>>> Storing object: " + dto);
        assertThat(entity.getId()).isEqualTo(dto.getId());

        final HttpEntity<VhfGroundStationDto> request = new HttpEntity<>(dto);
        final VhfGroundStationDto storeddto = this.restTemplate.postForObject("/api/stations",
                request, VhfGroundStationDto.class);
        log.info(">>>>> Stored object: " + storeddto);

        final HttpEntity<VhfGroundStationDto> updrequest = new HttpEntity<>(
                dto.description("modify description"));

        final HttpEntity<VhfGroundStationDto> updatedto = restTemplate.exchange("/api/stations/55",
                HttpMethod.PUT, updrequest, VhfGroundStationDto.class);
        log.info(">>>>> Updated object: " + updatedto.getBody());

        final ResponseEntity<String> updatenull = restTemplate.exchange("/api/stations/55",
                HttpMethod.PUT, null, String.class);
        log.info(">>>>> Updated null object: " + updatenull.getBody());

        final VhfGroundStationDto body = restTemplate.getForObject("/api/stations/55",
                VhfGroundStationDto.class);
        assertThat(body.getId()).isEqualTo("stvhf55");
        log.info("Retrieved station " + body);

        final ResponseEntity<String> getnull = restTemplate.exchange("/api/stations/8",
                HttpMethod.GET, null,String.class);
        assertThat(getnull.getStatusCode()).isGreaterThan(HttpStatus.OK);
        log.info("Retrieved unexisting station " + getnull);

        final HttpHeaders headers = new HttpHeaders();
        final HttpEntity<?> httpentity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange("/api/stations?by=name:%,loc:PAR",
                HttpMethod.GET, httpentity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved station list " + respmon);

        final ResponseEntity<String> respmonnull = restTemplate.exchange("/api/stations?by=loc:ITA",
                HttpMethod.GET, httpentity, String.class);
        assertThat(respmonnull.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved null station list " + respmonnull);

        final ResponseEntity<String> respmonnone = restTemplate.exchange("/api/stations?by=none",
                HttpMethod.GET, httpentity, String.class);
        assertThat(respmonnone.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved station list " + respmonnone);
        log.info(">>>>> END vhf03_GroundStationApiTest ");

    }

    @Test
    public void vhf04_GroundStationApiErrorTest() {
        log.info(">>>>> START vhf04_GroundStationApiErrorTest ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        final VhfGroundStation entity = DataGenerator.generateVhfGroundStation(0x65, "stvhf65",
                "00:01:65:65");

        final VhfGroundStationDto dto = new VhfGroundStationDto();
        dto.setMacaddress(entity.getMacaddress());
        dto.setName(entity.getName());
        dto.setId(entity.getId());
        dto.setStationId(65);
        dto.setLocation("PARIS");
        log.info(">>>>> Storing object: " + dto);
        assertThat(entity.getId()).isEqualTo(dto.getId());

        final HttpEntity<VhfGroundStationDto> request = new HttpEntity<>(dto);
        final VhfGroundStationDto storeddto = this.restTemplate.postForObject("/api/stations",
                request, VhfGroundStationDto.class);
        log.info(">>>>> Stored object: " + storeddto);

        final ResponseEntity<String> response = restTemplate.exchange("/api/stations/65",
                HttpMethod.GET, request, String.class);
        final String responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        final ResponseEntity<String> responsenullid = restTemplate.exchange("/api/stations/null",
                HttpMethod.GET, null, String.class);
        final String responsenullBody = responsenullid.getBody();
        assertThat(responsenullid.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        dto.setStationId(65);
        final HttpEntity<VhfGroundStationDto> request2 = new HttpEntity<>(dto);
        final ResponseEntity<String> response2 = this.restTemplate.postForEntity("/api/stations",
                request2, String.class);
        log.info(">>>>> Stored object2 (conflict because of a unique key constraint): " + response2);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        dto.setStationId(null);
        final HttpEntity<VhfGroundStationDto> request2null = new HttpEntity<>(dto);
        final ResponseEntity<String> response2null = this.restTemplate.postForEntity("/api/stations",
                request2null, String.class);
        log.info(">>>>> Stored object2 (null id): " + response2null);
        assertThat(response2null.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        // Now an update with a null id
        dto.setStationId(null);
        final HttpEntity<VhfGroundStationDto> requestbad = new HttpEntity<>(dto);
        final ResponseEntity<String> response3 = this.restTemplate.exchange("/api/stations/65", HttpMethod.PUT,
                requestbad, String.class);
        log.info(">>>>> Stored updated object3: " + response3);
        assertThat(response3.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        final ResponseEntity<String> response4 = this.restTemplate.exchange("/api/stations/15", HttpMethod.PUT,
                requestbad, String.class);
        log.info(">>>>> Stored object3: " + response4);
        assertThat(response4.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        final ResponseEntity<String> response5 = this.restTemplate.exchange("/api/stations", HttpMethod.POST,
                null, String.class);
        log.info(">>>>> Stored null object4: " + response5);
        assertThat(response5.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        log.info(">>>>> END vhf04_GroundStationApiErrorTest ");

    }

    @Test
    public void vhf05_UploadHexaTest() {
        log.info(">>>>> START vhf05_UploadHexaTest ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        final String pktcnes2 = DataGenerator.generateAlertFramePacket2();
        final BigInteger mpkt2 = new BigInteger(pktcnes2, 16);
        final byte[] data = mpkt2.toByteArray();

        log.info(">>>>>vhf05_UploadHexaTest: upload HEXA stream from byte array of length : " + data.length);
        assertThat(data.length).isEqualTo(128);

        final HttpHeaders headers = new HttpHeaders();
        // set header to : application/x-www-form-urlencoded, or multipart/form-data
        // ....to be checked
        // headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("format", "hexa");
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("stream", pktcnes2);
        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
                map, headers);
        final ResponseEntity<String> response = this.restTemplate.postForEntity("/api/vhf/upload",
                request, String.class);
        log.info(">>>>> Stored Hexa object has given response,code: " + response.getBody() + ", "
                + response.getStatusCodeValue());
        final HttpStatus rcode = response.getStatusCode();
        assertThat(rcode.value()).isBetween(200, 205);
        try {
            log.info("sleep 4 sec to wait for async insertion");
            Thread.sleep(4000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info(">>>>> END vhf05_UploadHexaTest ");
    }

    @Test
    public void vhf05_UploadHexaTestConflict() {
        log.info(">>>>> START vhf05_UploadHexaTestConflict ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        final String pktcnes2 = DataGenerator.generateAlertFramePacket2();
        final BigInteger mpkt2 = new BigInteger(pktcnes2, 16);
        final byte[] data = mpkt2.toByteArray();

        log.info(">>>>>vhf05_UploadHexaTestConflict: upload HEXA stream from byte array of length : " + data.length);
        assertThat(data.length).isEqualTo(128);

        final HttpHeaders headers = new HttpHeaders();
        // set header to : application/x-www-form-urlencoded, or multipart/form-data
        // ....to be checked
        // headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("format", "hexa");
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("stream", pktcnes2);
        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
                map, headers);
        final ResponseEntity<String> response = this.restTemplate.postForEntity("/api/vhf/upload",
                request, String.class);
        log.info(">>>>> Stored Hexa object has given response,code: " + response.getBody() + ", "
                 + response.getStatusCodeValue());
        final HttpStatus rcode = response.getStatusCode();
        assertThat(rcode.value()).isBetween(200, 410);
        try {
            log.info("sleep 4 sec to wait for async insertion");
            Thread.sleep(2000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info(">>>>> END vhf05_UploadHexaTestConflict");
    }

    @Test
    public void vhf06_GetVhfFrameTest() {
        log.info(">>>>> START vhf06_GetVhfFrameTest ");

        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of vhf packets
        log.info("list by apidname % ");
        final ResponseEntity<String> respmon = restTemplate.exchange("/api/vhf?by=apidname:%",
                HttpMethod.GET, null, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);

        log.info("list by none and wrong sort ");
        final ResponseEntity<String> respmonnoby = restTemplate.exchange("/api/vhf?by=none&sort=NOTTHERE:DESC",
                HttpMethod.GET, null, String.class);
        assertThat(respmonnoby.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

        // search a packet by frame id which does not exists.
        log.info("search frame -1 which is not existing");
        final ResponseEntity<String> respmona = restTemplate.exchange("/api/vhf/-1",
                HttpMethod.GET, null, String.class);
        assertThat(respmona.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        // search a packet by frame id which does  exists.
        log.info("search frame 1 which should exists");
        final ResponseEntity<String> respmonb = restTemplate.exchange("/api/vhf/1",
                HttpMethod.GET, null, String.class);
        assertThat(respmonb.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);

        log.info(">>>>> END vhf06_GetVhfFrameTest ");
    }

    @Test
    public void vhf07_GetVhfRawTest() {
        log.info(">>>>> START vhf07_GetVhfRawTest ");

        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        // Bad request
        final HttpEntity<?> entitybad = new HttpEntity<>(headers);
        log.info("list packets using return type as string ");
        final ResponseEntity<String> respmonbad = restTemplate.exchange(
                "/api/vhf/packets?by=apidname:%,packettime>0&sort=insertionTime:DESC",
                HttpMethod.GET, entitybad, String.class);
        assertThat(respmonbad.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved bad request body: {}",respmonbad.getBody());

        headers.set("X-Vhf-PktFormat", "nothing");
        final HttpEntity<?> entitybad2 = new HttpEntity<>(headers);
        log.info("list packets using wrong header ");
        final ResponseEntity<String> respmonbad2 = restTemplate.exchange(
                "/api/vhf/packets?by=apidname:%,packettime>0&sort=insertionTime:DESC",
                HttpMethod.GET, entitybad2, String.class);
        assertThat(respmonbad2.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        log.info("Retrieved bad request body: {}",respmonbad2.getBody());

        headers.set("dateformat", "ms");
        // raw format
        headers.set("X-Vhf-PktFormat", "raw");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        log.info("list packets in raw format ");
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/packets?by=apidname:%,packettime>0&sort=insertionTime:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved raw packet list: {}",respmon.getBody());
        // binary format
        headers.set("X-Vhf-PktFormat", "binary");
        final HttpEntity<?> entityb = new HttpEntity<>(headers);
        log.info("list packets in binary format ");
        final ResponseEntity<String> respmonb = restTemplate.exchange(
                "/api/vhf/packets?by=apidname:%,packettime>0&sort=insertionTime:DESC",
                HttpMethod.GET, entityb, String.class);
        assertThat(respmonb.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved binary packet list: {}",respmonb.getBody());
        headers.set("X-Vhf-PktFormat", "binary");
        final HttpEntity<?> entityc = new HttpEntity<>(headers);
        final ResponseEntity<String> respmonc = restTemplate.exchange(
                "/api/vhf/packets?by=none&sort=insertionTime:DESC",
                HttpMethod.GET, entityc, String.class);
        assertThat(respmonc.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        final ResponseEntity<String> respmonc1 = restTemplate.exchange(
                "/api/vhf/packets?by=insertiontime:0,isframevalid:false,packettime<999999999999&sort=insertionTime:DESC",
                HttpMethod.GET, entityc, String.class);
        assertThat(respmonc1.getStatusCode()).isEqualTo(HttpStatus.OK);
        final ResponseEntity<String> respmonc2 = restTemplate.exchange(
                "/api/vhf/packets?by=insertiontime>-1,isframeduplicate:true,insertiontime<999999999999&sort=insertionTime:DESC",
                HttpMethod.GET, entityc, String.class);
        assertThat(respmonc2.getStatusCode()).isEqualTo(HttpStatus.OK);
        // decoded format
        headers.set("X-Vhf-PktFormat", "decoded");
        final HttpEntity<?> entityd = new HttpEntity<>(headers);
        final ResponseEntity<String> respmond = restTemplate.exchange(
                "/api/vhf/packets?by=id>0,obsid>0&sort=insertionTime:DESC",
                HttpMethod.GET, entityd, String.class);
        assertThat(respmond.getStatusCode()).isEqualTo(HttpStatus.OK);

        // Test count for obsid
        final HttpEntity<?> entitye = new HttpEntity<>(headers);
        final ResponseEntity<String> respmone = restTemplate.exchange(
                "/api/vhf/count?apidname=all",
                HttpMethod.GET, entitye, String.class);
        assertThat(respmone.getStatusCode()).isEqualTo(HttpStatus.OK);

        final HttpEntity<?> entityf = new HttpEntity<>(headers);
        final ResponseEntity<String> respmonf = restTemplate.exchange(
                "/api/vhf/count?apidname=some&pktmintime=10000000000000&pktmaxtime=20000000000000&timeformat=number",
                HttpMethod.GET, entityf, String.class);
        assertThat(respmonf.getStatusCode()).isEqualTo(HttpStatus.OK);

        final HttpEntity<?> entityg = new HttpEntity<>(headers);
        final ResponseEntity<String> respmong = restTemplate.exchange(
                "/api/vhf/count?apidname=some&pktmintime=20201201T000000GMT&pktmaxtime=20201201T000000GMT&timeformat=iso",
                HttpMethod.GET, entityg, String.class);
        assertThat(respmong.getStatusCode()).isEqualTo(HttpStatus.OK);

        log.info(">>>>> END vhf07_GetVhfRawTest ");
    }

    @Test
    public void vhf08_UploadStreamBinTest() {
        log.info(">>>>> START vhf08_UploadStreamBinTest ");

        final String json = "{ \"type\" : \"TestAlert\", \"content\" : \"some content\"}";
        final HTTPResponse resp = new HTTPResponse();
        final String pktdec = DataGenerator.generateBinaryString1();

        resp.action("decode");
        resp.id(pktdec);
        resp.message(json);
        resp.code(200);

        Mockito.when(restDecoder.getDecodedPacket(any(String.class))).thenReturn(resp);

        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        //final byte[] data = DataGenerator.generatePacket();
        final String pktcnes2 = DataGenerator.generateAlertFramePacket1();
        final BigInteger mpkt2 = new BigInteger(pktcnes2, 16);
        final byte[] data = mpkt2.toByteArray();

        log.info(">>>>>vhf08_UploadStreamBinTest: upload HEXA stream from byte array of length : " + data.length);
        assertThat(data.length).isEqualTo(128);

        // get a filesystem resource for the post
        final FileSystemResource fr = DataGenerator.getBinStreamForUpload(pktcnes2, true);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.set("format", "binary");
        final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("stream", fr);
        final HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(
                map, headers);
        final ResponseEntity<String> response = this.restTemplate.postForEntity("/api/vhf/upload",
                entity, String.class);
        log.info(">>>>> Stored object: " + response);

        headers.set("format", "hexa");
        final MultiValueMap<String, Object> map2 = new LinkedMultiValueMap<>();
        map.add("stream", fr);
        final HttpEntity<MultiValueMap<String, Object>> entity2 = new HttpEntity<MultiValueMap<String, Object>>(
                map2, headers);

        final ResponseEntity<String> response2 = this.restTemplate.postForEntity("/api/vhf/upload",
                entity2, String.class);
        log.info(">>>>> Stored object2: " + response2);

        headers.set("format", "someother");
        final MultiValueMap<String, Object> map3 = new LinkedMultiValueMap<>();
        map.add("stream", fr);
        final HttpEntity<MultiValueMap<String, Object>> entity3 = new HttpEntity<MultiValueMap<String, Object>>(
                map3, headers);

        final ResponseEntity<String> response3 = this.restTemplate.postForEntity("/api/vhf/upload",
                entity3, String.class);
        log.info(">>>>> Stored object3: " + response3);


        final String hash="6bd54079a543ffa1274826aa4f83db6934127fbd1dd4ebafcfd882611c427d21";
        final ResponseEntity<String> respmonb = restTemplate.exchange(
                "/api/vhf/"+hash,
                HttpMethod.GET, entity, String.class);
        assertThat(respmonb.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        log.info("Retrieved packet from hash: {}",respmonb.getBody());

        final HttpHeaders headers2 = new HttpHeaders();
        final HttpEntity<?> hentity = new HttpEntity<>(headers2);
        final ResponseEntity<String> respmonc = restTemplate.exchange(
                "/api/vhf/"+hash,
                HttpMethod.GET, hentity, String.class);
        assertThat(respmonc.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        log.info("Retrieved packet from hash in decoded format: {}",respmonc.getBody());

        final ResponseEntity<String> respmond = restTemplate.exchange(
                "/api/vhf/rawpackets",
                HttpMethod.GET, hentity, String.class);
        assertThat(respmond.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved raw packets: {}",respmond.getBody());

        log.info(">>>>> END vhf08_UploadStreamBinTest ");
    }

    @Test
    public void vhf09_MockUploadStreamBinTest() {
        log.info(">>>>> START vhf09_MockUploadStreamBinTest ");

        final String json = "{ \"type\" : \"error\", \"content\" : \"some content\"}";
        final HTTPResponse resp = new HTTPResponse();
        final String pktdec = DataGenerator.generateBinaryString2();

        resp.action("decode");
        resp.id(pktdec);
        resp.message(json);
        resp.code(200);

        Mockito.when(restDecoder.getDecodedPacket(any(String.class))).thenReturn(resp);

        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        //final byte[] data = DataGenerator.generatePacket();
        final String pktcnes2 = DataGenerator.generateAlertFramePacket1();
        final BigInteger mpkt2 = new BigInteger(pktcnes2, 16);
        final byte[] data = mpkt2.toByteArray();

        log.info(">>>>>vhf09_MockUploadStreamBinTest: upload HEXA stream from byte array of length : " + data.length);
        assertThat(data.length).isEqualTo(128);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.set("format", "binary");
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("stream", new String(data));

        final HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(
                map, headers);
        final ResponseEntity<String> response = this.restTemplate.postForEntity("/api/vhf/upload",
                request, String.class);
        log.info(">>>>> Stored object: " + response);

        headers.set("format", "hexa");
        final MultiValueMap<String, String> map2 = new LinkedMultiValueMap<String, String>();
        map2.add("stream", pktcnes2);
        final HttpEntity<MultiValueMap<String, String>> entity2 = new HttpEntity<MultiValueMap<String, String>>(
                map2, headers);
        final ResponseEntity<String> response2 = this.restTemplate.postForEntity("/api/vhf/upload",
                entity2, String.class);
        log.info(">>>>> Stored object2: " + response2);

        headers.set("format", "someother");
        final MultiValueMap<String, String> map3 = new LinkedMultiValueMap<String, String>();
        map3.add("stream", pktcnes2);

        final HttpEntity<MultiValueMap<String, String>> entity3 = new HttpEntity<MultiValueMap<String, String>>(
                map3, headers);
        final ResponseEntity<String> response3 = this.restTemplate.postForEntity("/api/vhf/upload",
                entity3, String.class);
        log.info(">>>>> Stored object3: " + response3);


        final String hash="6bd54079a543ffa1274826aa4f83db6934127fbd1dd4ebafcfd882611c427d21";
        headers.set("X-Vhf-PktFormat", "binary");
        final HttpEntity<?> hentity1 = new HttpEntity<>(headers);
        final ResponseEntity<String> respmonb = restTemplate.exchange(
                "/api/vhf/"+hash,
                HttpMethod.GET, hentity1, String.class);
        assertThat(respmonb.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        log.info("Retrieved packet from hash: {}",respmonb.getBody());


        headers.set("X-Vhf-PktFormat", "decoded");
        final HttpEntity<?> hentity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmonc = restTemplate.exchange(
                "/api/vhf/"+hash,
                HttpMethod.GET, hentity, String.class);
        assertThat(respmonc.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        log.info("Retrieved packet from hash in decoded format: {}",respmonc.getBody());
        
        log.info(">>>>> END vhf08_MockUploadStreamBinTest ");
    }

    @Test
    public void vhf09_DecodeHexaTest() {
        log.info(">>>>> START vhf09_DecodeHexaTest ");

        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        final String data = DataGenerator.generateBinaryString3();
        log.info(">>>>> decode hexa string of length : " + data.length());
        assertThat(data.length()).isEqualTo(94 * 2);

        final HttpHeaders headers = new HttpHeaders();
        headers.set("format", "swig");
        final HttpEntity<?> entity = new HttpEntity<>(headers);

        final ResponseEntity<String> response = restTemplate.exchange(
                "/api/vhf/packets/decode?pkt={hstr}", HttpMethod.GET, entity, String.class, data);
        final String responseBody = response.getBody();
        log.info(">>>>> Decoded object: " + responseBody);
        final HttpStatus rcode = response.getStatusCode();
        assertThat(rcode.value()).isBetween(200, 505);

        headers.set("format", "java");
        final HttpEntity<?> entityb = new HttpEntity<>(headers);
        final ResponseEntity<String> responseb = restTemplate.exchange(
                "/api/vhf/packets/decode?pkt={hstr}", HttpMethod.GET, entityb, String.class, data);
        final String responseBodyb = responseb.getBody();
        log.info(">>>>> Decoded object with java: " + responseBodyb);
        final HttpStatus rcodeb = responseb.getStatusCode();
        assertThat(rcodeb.value()).isBetween(200, 505);
        
        headers.set("format", "all");
        final HttpEntity<?> entityc = new HttpEntity<>(headers);
        final ResponseEntity<String> responsec = restTemplate.exchange(
                "/api/vhf/packets/decode?pkt={hstr}", HttpMethod.GET, entityc, String.class, data);
        final String responseBodyc = responsec.getBody();
        log.info(">>>>> Decoded object with all: " + responseBodyc);
        final HttpStatus rcodec = responsec.getStatusCode();
        assertThat(rcodec.value()).isBetween(200, 505);
        
        headers.set("format", "pippo");
        final HttpEntity<?> entityd = new HttpEntity<>(headers);
        final ResponseEntity<String> responsed = restTemplate.exchange(
                "/api/vhf/packets/decode?pkt={hstr}", HttpMethod.GET, entityd, String.class, data);
        final String responseBodyd = responsed.getBody();
        log.info(">>>>> Decoded object with pippo: " + responseBodyd);
        final HttpStatus rcoded = responsed.getStatusCode();
        assertThat(rcoded.value()).isBetween(200, 505);

        final String wronghexa = "ffeeaabb99884477";
        headers.set("format", "all");
        final HttpEntity<?> entitye = new HttpEntity<>(headers);
        final ResponseEntity<String> responsee = restTemplate.exchange(
                "/api/vhf/packets/decode?pkt={hstr}", HttpMethod.GET, entitye, String.class, wronghexa);
        final String responseBodye = responsee.getBody();
        log.info(">>>>> Decoded object with all but bad hexa: " + responseBodye);
        final HttpStatus rcodee = responsee.getStatusCode();
        assertThat(rcodee.value()).isBetween(200, 505);

        RawPacketSetDto pset = new RawPacketSetDto();
        RawPacketDto dto = new RawPacketDto();
        dto.setPacket(data);
        dto.setPktformat("HEXA");
        dto.setSize(data.length());
        pset.addResourcesItem(dto);
        pset.size(1L);

        final HttpEntity<RawPacketSetDto> entityset = new HttpEntity<>(pset);
        final ResponseEntity<String> responseset = restTemplate.postForEntity("/api/vhf/packets/decode", entityset,
                String.class);

        final String responseBodyset = responseset.getBody();
        log.info(">>>>> Decoded list of raw packets: " + responseBodyset);
        final HttpStatus setstatus = responseset.getStatusCode();
        assertThat(setstatus.value()).isBetween(200, 505);

        String jsondup = "{ \"tfrom\": 0, \"tto\": 999999999999 }";
        final HttpEntity<String> entityj = new HttpEntity<>(jsondup);
        final ResponseEntity<String> responsedup = restTemplate.postForEntity("/api/vhf/packets/duplicates", entityj,
                String.class);

        final String responseBodydup = responsedup.getBody();
        log.info(">>>>> Deal with duplicates: " + responseBodyset);
        final HttpStatus dupstatus = responsedup.getStatusCode();
        assertThat(dupstatus.value()).isBetween(200, 505);

        log.info(">>>>> END vhf09_DecodeHexaTest ");

    }
    
    @Test
    public void vhf10_GetVhfPacketTest() {
        log.info(">>>>> START vhf10_GetVhfPacketTest ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        headers.set("dateformat", "ms");
        headers.set("X-Vhf-PktFormat", "binary");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final String hash="6bd54079a543ffa1274826aa4f83db6934127fbd1dd4ebafcfd882611c427d21";
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/packets/"+hash,
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        log.info("Retrieved vhf packet from hash: {}",respmon.getBody());

        headers.set("X-Vhf-PktFormat", "decoded");
        final HttpEntity<?> entityd = new HttpEntity<>(headers);
        final String hashd="6bd54079a543ffa1274826aa4f83db6934127fbd1dd4ebafcfd882611c427d21";
        final ResponseEntity<String> respmond = restTemplate.exchange(
                "/api/vhf/packets/"+hashd,
                HttpMethod.GET, entityd, String.class);
        assertThat(respmond.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        log.info("Retrieved vhf packet from hash: {}",respmond.getBody());
        final String hashe="xxxxx";
        final ResponseEntity<String> respmone = restTemplate.exchange(
                "/api/vhf/packets/"+hashe,
                HttpMethod.GET, entityd, String.class);
        assertThat(respmone.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        log.info("Retrieved vhf packet from hash: {}",respmone.getBody());

        headers.set("X-Vhf-PktFormat", "pippo");
        final HttpEntity<?> entitybad = new HttpEntity<>(headers);
        final ResponseEntity<String> respmonbad = restTemplate.exchange("/api/vhf/packets/" + hashd,
                HttpMethod.GET, entitybad, String.class);
        assertThat(respmonbad.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        log.info("Retrieved vhf packet from hash: {}", respmonbad.getBody());

        headers.set("X-Vhf-PktFormat", null);
        final HttpEntity<?> entitynull = new HttpEntity<>(headers);
        final ResponseEntity<String> respmonnull = restTemplate.exchange("/api/vhf/packets/" + hashd,
                HttpMethod.GET, entitynull, String.class);
        assertThat(respmonnull.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        log.info("Retrieved vhf packet from hash with null header: {}", respmonnull.getBody());

        log.info(">>>>> END vhf10_GetVhfPacketTest ");
    }


    @Test
    public void vhf10_GetVhfStationStatus() {
        log.info(">>>>> START vhf10_GetVhfStationStatus ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        headers.set("dateformat", "ms");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/stationstatus?by=idstation:7,stationtime>10,stationtime<999999999999999,doppler>-10,doppler<100000",
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved station status list: {}",respmon.getBody());
        final ResponseEntity<String> respmona = restTemplate.exchange(
                "/api/vhf/stationstatus?sort=NOTTHERE:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        final ResponseEntity<String> respmona1 = restTemplate.exchange(
                "/api/vhf/stationstatus?by=stationtime:999,doppler:999&sort=idStation:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona1.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved station status list: {}",respmon.getBody());
        final ResponseEntity<String> respmonb = restTemplate.exchange(
                "/api/vhf/stationstatus/count?from=0&to=999999999999999&timeformat=number",
                HttpMethod.GET, entity, String.class);
        assertThat(respmonb.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved station status count: {}",respmonb.getBody());
        final ResponseEntity<String> respmonc = restTemplate.exchange(
                "/api/vhf/stationstatus/count",
                HttpMethod.GET, entity, String.class);
        assertThat(respmonc.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved station status count: {}",respmonc.getBody());
        final ResponseEntity<String> respmond = restTemplate.exchange(
                "/api/vhf/stationstatus/count?from=20010101T000000Z&to=20300101T000000Z",
                HttpMethod.GET, entity, String.class);
        assertThat(respmond.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved station status count using ISO date: {}",respmond.getBody());
        log.info(">>>>> END vhf10_GetVhfStationStatus ");
    }

    @Test
    public void vhf11_GetVhfPositions() {
        log.info(">>>>> START vhf11_GetVhfPositions ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        headers.set("dateformat", "ms");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/positions?by=apidname:%,packettime>0,id:2,packettime<99999999999,packettime:2000,insertiontime>0,insertiontime<999999999999",
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved positions list: {}",respmon.getBody());
        final ResponseEntity<String> respmona = restTemplate.exchange(
                "/api/vhf/positions?sort=NOTTHERE:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        final ResponseEntity<String> respmona1 = restTemplate.exchange(
                "/api/vhf/positions?by=insertiontime:999&sort=insertionTime:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona1.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved positions list: {}",respmona1.getBody());
        log.info(">>>>> END vhf11_GetVhfPositions ");
    }

    @Test
    public void vhf11_GetVhfAttitudes() {
        log.info(">>>>> START vhf11_GetVhfAttitudes ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        headers.set("dateformat", "ms");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/attitudes?by=apidname:%,packettime>0,id:2,packettime<99999999999,packettime:2000,insertiontime>0,insertiontime<999999999999",
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved attitudes list: {}",respmon.getBody());
        final ResponseEntity<String> respmona = restTemplate.exchange(
                "/api/vhf/attitudes?sort=NOTTHERE:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        final ResponseEntity<String> respmona1 = restTemplate.exchange(
                "/api/vhf/attitudes?by=insertiontime:999&sort=insertionTime:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona1.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved attitudes list: {}",respmona1.getBody());
        log.info(">>>>> END vhf11_GetVhfAttitudes ");
    }

    @Test
    public void vhf11_GetVhfObsids() {
        log.info(">>>>> START vhf11_GetVhfObsids ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        headers.set("dateformat", "ms");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/obsids?minreceptiontime=20200101T000000Z&maxreceptiontime=20201101T000000Z&timeformat=iso",
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved obsids list: {}",respmon.getBody());
        final ResponseEntity<String> respmona = restTemplate.exchange(
                "/api/vhf/obsids?minreceptiontime=100000&timeformat=number",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info(">>>>> END vhf11_GetVhfObsids ");
    }

    @Test
    public void vhf12_GetVhfNotification() {
        log.info(">>>>> START vhf12_GetVhfNotification ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        headers.set("dateformat", "ms");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/notifications?by=pktflag:SENT,insertiontime>0,insertiontime<999999999999",
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved notification list: {}",respmon.getBody());
        log.info(">>>>> END vhf12_GetVhfNotification ");
    }

    @Test
    public void vhf12_DeleteObsid() {
        log.info(">>>>> START vhf12_DeleteObsid ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/obsid/1",
                HttpMethod.DELETE, entity, String.class);
        assertThat(respmon.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        log.info("Delete obsid 1: {}",respmon.getBody());
        log.info(">>>>> END vhf12_DeleteObsid ");
    }

    @Test
    public void vhf12_GetVhfBurstId() {
        log.info(">>>>> START vhf12_GetVhfBurstId ");
        restTemplate.getRestTemplate().setErrorHandler(new VhfErrorHandler());
        // search list of execution for a scheduler with new statuss
        final HttpHeaders headers = new HttpHeaders();
        headers.set("dateformat", "ms");
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        final ResponseEntity<String> respmon = restTemplate.exchange(
                "/api/vhf/bursts?by=apidname:%,packettime>0,burstid:sb,alerttimetb0absseconds<99999999999,packettime:2000,"
                + "insertiontime>0,insertiontime<999999999999",
                HttpMethod.GET, entity, String.class);
        assertThat(respmon.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved bursts list: {}",respmon.getBody());
        final ResponseEntity<String> respmona = restTemplate.exchange(
                "/api/vhf/bursts?sort=NOTTHERE:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        final ResponseEntity<String> respmona1 = restTemplate.exchange(
                "/api/vhf/bursts?by=insertiontime:999&sort=insertionTime:DESC",
                HttpMethod.GET, entity, String.class);
        assertThat(respmona1.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved bursts list: {}",respmona1.getBody());
        final HttpHeaders headers2 = new HttpHeaders();
        headers2.set("dateformat", "iso");
        final HttpEntity<?> entity2 = new HttpEntity<>(headers2);
        final ResponseEntity<String> respmona2 = restTemplate.exchange(
                "/api/vhf/bursts?by=packettime>20000101T000000Z,packettime<20300101T000000Z&sort=insertionTime:DESC",
                HttpMethod.GET, entity2, String.class);
        assertThat(respmona2.getStatusCode()).isEqualTo(HttpStatus.OK);
        log.info("Retrieved bursts list: {}",respmona2.getBody());

        // Test update of burst id, even though it may not exists
        GenericMap upddto = new GenericMap();
        upddto.put("burstEndtime", 1111L);
        final HttpEntity<GenericMap> updrequest = new HttpEntity<>(upddto);
        final ResponseEntity<String> updatedto = restTemplate.exchange("/api/bursts/sb20050512",
                HttpMethod.PUT, updrequest, String.class);
        assertThat(respmon.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);

        log.info(">>>>> END vhf12_GetVhfBurstId ");
    }

    @Test
    public void actuatorTest() {
        final ResponseEntity<String> respmoninfo = restTemplate.exchange(
                "/mgmt/info",
                HttpMethod.GET, null, String.class);
        assertThat(respmoninfo.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        log.info("Calling actuator info {}", respmoninfo.getBody());
        final ResponseEntity<String> respmonhealth = restTemplate.exchange(
                "/mgmt/health",
                HttpMethod.GET, null, String.class);
        assertThat(respmonhealth.getStatusCode()).isGreaterThanOrEqualTo(HttpStatus.OK);
        log.info("Calling actuator health {}", respmonhealth.getBody());
    }
}
