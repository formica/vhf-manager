/**
 *
 */
package fr.svom.vhf.server.test;

import com.querydsl.core.types.dsl.BooleanExpression;
import fr.svom.messages.model.ServiceDescriptor;
import fr.svom.messages.model.VhfLogMessage;
import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.exceptions.VhfDecodingException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.mappers.NotifStatus;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.PacketApidRepository;
import fr.svom.vhf.data.repositories.VhfGroundStationRepository;
import fr.svom.vhf.data.repositories.VhfTransferFrameRepository;
import fr.svom.vhf.data.repositories.querydsl.IFilteringCriteria;
import fr.svom.vhf.data.repositories.querydsl.SearchCriteria;
import fr.svom.vhf.data.serialization.converters.VhfTransferFrameDeserializer;
import fr.svom.vhf.server.controllers.PageRequestHelper;
import fr.svom.vhf.server.controllers.ServerEnvironment;
import fr.svom.vhf.server.controllers.ServicesIndicator;
import fr.svom.vhf.server.controllers.SvomVhfMessagesController;
import fr.svom.vhf.server.externals.DecodedMessageType;
import fr.svom.vhf.server.services.ObsIdApidCountService;
import fr.svom.vhf.server.services.PacketApidService;
import fr.svom.vhf.server.services.VhfBinaryPacketService;
import fr.svom.vhf.server.services.VhfBurstIdService;
import fr.svom.vhf.server.services.VhfDecodedPacketService;
import fr.svom.vhf.server.services.VhfGroundStationService;
import fr.svom.vhf.server.services.VhfLogMessageService;
import fr.svom.vhf.server.services.VhfRawPacketService;
import fr.svom.vhf.server.services.VhfSatelliteService;
import fr.svom.vhf.server.services.VhfStationStatusService;
import fr.svom.vhf.server.services.VhfTransferFrameFacade;
import fr.svom.vhf.server.services.VhfTransferFrameService;
import fr.svom.vhf.server.swagger.api.impl.HeaderFormat;
import fr.svom.vhf.server.test.generators.APIDS;
import fr.svom.vhf.server.test.generators.ApidGenerator;
import fr.svom.vhf.server.test.generators.DataGenerator;
import fr.svom.vhf.server.test.generators.StationGenerator;
import fr.svom.vhf.swagger.model.HTTPResponse;
import fr.svom.vhf.swagger.model.ObsIdApidCountDto;
import fr.svom.vhf.swagger.model.PacketApidDto;
import fr.svom.vhf.swagger.model.VhfBasePacket;
import fr.svom.vhf.swagger.model.VhfDecodedPacketDto;
import fr.svom.vhf.swagger.model.VhfPacketSearchDto;
import fr.svom.vhf.swagger.model.VhfStationStatusCountDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import ma.glasnost.orika.MapperFacade;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.LocaleResolver;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@ContextConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VhfServiceTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    @Autowired
    ServicesIndicator servicesIndicator;
    @Autowired
    ServerEnvironment serverEnvironment;

    @Autowired
    VhfBinaryPacketService vhfBinaryPacketService;
    @Autowired
    VhfTransferFrameRepository vhfTransferFrameRepository;
    @Autowired
    VhfTransferFrameFacade vhfTransferFrameFacade;
    @Autowired
    VhfTransferFrameService vhfTransferFrameService;
    @Autowired
    VhfGroundStationService vhfGroundStationService;
    @Autowired
    PacketApidService packetApidService;
    @Autowired
    VhfDecodedPacketService vhfDecodedPacketService;
    @Autowired
    VhfBurstIdService vhfBurstIdService;
    @Autowired
    VhfSatelliteService vhfSatelliteService;
    @Autowired
    PageRequestHelper prh;
    @Autowired
    VhfRawPacketService vhfRawPacketService;
    @Autowired
    VhfStationStatusService vhfStationStatusService;
    @Autowired
    ObsIdApidCountService obsIdApidCountService;
    @Autowired
    VhfLogMessageService vhfLogMessageService;
    @Autowired
    SvomVhfMessagesController svomVhfMessagesController;
    @Autowired
    @Qualifier("vhfsatattitudeFiltering")
    private IFilteringCriteria vhfSatAttitudeFiltering;
    @Autowired
    @Qualifier("vhfsatpositionFiltering")
    private IFilteringCriteria vhfSatPositionFiltering;
    @Autowired
    @Qualifier("vhfRawPacketFiltering")
    private IFilteringCriteria vhfRawPacketFiltering;
    @Autowired
    @Qualifier("vhfStationStatusFiltering")
    private IFilteringCriteria vhfStationStatusFiltering;
    @Autowired
    @Qualifier("vhfGroundStationFiltering")
    private IFilteringCriteria vhfGroundStationFiltering;
    @Autowired
    @Qualifier("vhfTransferFrameFiltering")
    private IFilteringCriteria vhfTransferFrameFiltering;
    @Autowired
    @Qualifier("packetApidFiltering")
    private IFilteringCriteria packetApidFiltering;
    @Autowired
    private VhfGroundStationRepository vhfGroundStationRepository;
    @Autowired
    private PacketApidRepository packetApidRepository;

    @Autowired
    LocaleResolver localeres;

    @Autowired
    private VhfTransferFrameDeserializer vhfTransferFrameDeserializer;

    List<VhfGroundStation> stationlist = null;
    List<PacketApid> apidlist = null;

    @Before
    public void initData() {
        stationlist = new StationGenerator().getStationList();
        apidlist = new ApidGenerator().getApidList();
        for (VhfGroundStation station : stationlist) {
            vhfGroundStationRepository.save(station);
        }
        for (PacketApid apid : apidlist) {
            packetApidRepository.save(apid);
        }
    }

    @Test
    public void test0_LocaleTest() {
        final Locale locale = new Locale("US");
        final ResourceBundle exampleBundle = ResourceBundle.getBundle("messages", locale);
        log.info("Found message {}", exampleBundle.getString("error.service"));
        assertThat(exampleBundle.getString("error.service"))
                .isEqualToIgnoringCase("Service exception");
    }

    @Test
    public void test0_HealthTest() {
        final Health h = servicesIndicator.health();
        assertThat(h).isNotNull();
    }

    @Test
    public void test0_EnvTest() {
        final ServiceDescriptor h = serverEnvironment.getDescriptor();
        assertThat(h).isNotNull();
    }

    @Test
    public void testA_rawPacketApidServiceTest() {
        log.debug("=============== Raw packet Apid service test ================");
        final String rawpkt = DataGenerator.generateAlertFramePacket();
        try {
            log.debug("Insert raw packet");
            final VhfRawPacket entity = vhfRawPacketService.storeRawPacket(rawpkt, HeaderFormat.PKT_MODE_PROD.value);
            assertThat(entity.getPacket()).isEqualTo(rawpkt);
        }
        catch (final Exception e) {
            // TODO Auto-generated catch block
            log.error("raw packet insertion exception : {}", e.getMessage());
        }
        // Retrieve the raw packets.
        try {
            log.info("Retrieve raw packets");
            Page<VhfRawPacket> entitylist = vhfRawPacketService.findAllRawPackets(null, null);
            assertThat(entitylist.toList().size()).isGreaterThan(0);

            final PageRequest preq = prh.createPageRequest(0, 10, "insertionTime:ASC");
            final List<SearchCriteria> params = prh.createMatcherCriteria("flag:%,insertionTime>0");
            final List<BooleanExpression> expressions = vhfRawPacketFiltering
                    .createFilteringConditions(params);
            BooleanExpression wherepred = null;

            for (final BooleanExpression exp : expressions) {
                if (wherepred == null) {
                    wherepred = exp;
                }
                else {
                    wherepred = wherepred.and(exp);
                }
            }
            log.info("Retrieve raw packets using query conditions {} {}", wherepred, preq);
            entitylist = vhfRawPacketService.findAllRawPackets(wherepred, preq);
            assertThat(entitylist.toList().size()).isGreaterThan(0);

            for (VhfRawPacket p : entitylist) {
                vhfRawPacketService.deleteRawPacket(p.getRawId());
            }
        }
        catch (final Exception e) {
            // TODO Auto-generated catch block
            log.error("raw packet retrieval exception : {}", e.getMessage());
        }
    }

    @Test
    public void testA_packetApidServiceTest() {
        log.debug("=============== Packet Apid service test ================");
        final ApidGenerator apidgen = new ApidGenerator();
        final PacketApid entity = apidgen.getAlertApid();
        final PacketApidDto dto = mapper.map(entity, PacketApidDto.class);
        try {
            final PacketApid searched = packetApidService.findApid(entity.getApid());
            log.info("Found apid {}", searched);
            if (searched == null) {
                final PacketApid saved = packetApidService.registerApid(searched);
                assertThat(saved.getApid()).isEqualTo(dto.getApid());
            }

            Page<PacketApid> dtolist = packetApidService.findAllPacketApids(null, null);
            assertThat(dtolist.toList().size()).isGreaterThan(0);

            final PageRequest preq = prh.createPageRequest(0, 10, "name:ASC");
            final List<SearchCriteria> params = prh.createMatcherCriteria("id:576,description:%");
            final List<BooleanExpression> expressions = packetApidFiltering
                    .createFilteringConditions(params);
            BooleanExpression wherepred = null;

            for (final BooleanExpression exp : expressions) {
                if (wherepred == null) {
                    wherepred = exp;
                }
                else {
                    wherepred = wherepred.and(exp);
                }
            }

            dtolist = packetApidService.findAllPacketApids(wherepred, preq);
            assertThat(dtolist.toList().size()).isGreaterThan(0);

        }
        catch (final VhfServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (final NotExistsPojoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testA_vhfStationServiceTest() {
        log.debug("=============== Station service test ================");
        VhfGroundStation entity = new VhfGroundStation();
        entity.setDescription("station for test");
        entity.setStationId(299);
        entity.setName("0x299");
        entity.setId("0x299");
        entity.setMacaddress("00:299");
        entity.setCountry("PAR");
        entity.setLocation("Paris");
        try {

            final VhfGroundStation saved = vhfGroundStationService.insertVhfGroundStation(entity);
            log.info("Saved ground station {}", saved);
            final PageRequest preq = prh.createPageRequest(0, 10, "id:ASC");

            Page<VhfGroundStation> dtolist = vhfGroundStationService
                    .findAllVhfGroundStations(null, preq);
            assertThat(dtolist.toList().size()).isGreaterThan(0);

            final VhfGroundStation ret = vhfGroundStationService.getVhfGroundStationById(299);
            assertThat(ret.getStationId()).isEqualTo(299);

            final List<SearchCriteria> params = prh
                    .createMatcherCriteria("stationId:9,loc:%,idstation:%,location:%");
            final List<BooleanExpression> expressions = vhfGroundStationFiltering
                    .createFilteringConditions(params);
            BooleanExpression wherepred = null;

            for (final BooleanExpression exp : expressions) {
                if (wherepred == null) {
                    wherepred = exp;
                }
                else {
                    wherepred = wherepred.and(exp);
                }
            }

            dtolist = vhfGroundStationService.findAllVhfGroundStations(wherepred, preq);
            assertThat(dtolist.toList().size()).isGreaterThan(0);

        }
        catch (final VhfServiceException e) {
            e.printStackTrace();
        }
        catch (final NotExistsPojoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testB_svomVhfMessagesControllerTest() {
        log.debug("=============== Message controller test ================");
        final VhfDecodedPacket pkt = DataGenerator.generateDecodedPacket();
        final VhfBinaryPacket binpkt = DataGenerator.generateVhfBinaryPacket("messagetesthash", "binary",
                "somekindofbinary");
        pkt.setHashId("messagetesthash");
        pkt.setPkttype("AlertTest");
        VhfGroundStation entity = new VhfGroundStation();
        entity.setDescription("station for test");
        entity.setStationId(1);
        entity.setName("st1");
        entity.setId("0x1");
        entity.setMacaddress("00:01");
        entity.setCountry("PAR");
        entity.setLocation("Paris");

        VhfGroundStation saved = null;
        try {
            saved = vhfGroundStationService.insertVhfGroundStation(entity);
            log.debug("Saved station {}", saved);
        }
        catch (final AbstractCdbServiceException e) {
            log.error("cannot insert station {}", entity);
            saved = vhfGroundStationService.getVhfGroundStationById(entity.getStationId());
        }
        final PacketApid apid = APIDS.ECLALERTL1.getPacketApid();
        VhfTransferFrame frame = DataGenerator.generateFrame(null);
        frame.setApid(apid);
        frame.setStation(saved);
        frame.setIsFrameDuplicate(Boolean.FALSE);
        frame.setPacket(binpkt);
        try {
            log.info("Store binary before...");
            frame = vhfBinaryPacketService.storeBinaryVhfPacket(frame);
            log.info("Storing frame {}", frame);
            vhfTransferFrameService.storeVhfTransferFrame(frame);
            svomVhfMessagesController.sendNotification(frame, null);
            assertThat(pkt.getPktformat()).isEqualTo("JSON");
            assertThat(apid.getApid()).isEqualTo(576);
            svomVhfMessagesController.sendServiceStatus("some status in json", Instant.now());
        }
        catch (VhfServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void testB_vhfDecodedServiceTest() {
        log.debug("=============== Decoded service test ================");
        final VhfDecodedPacket pkt = DataGenerator.generateDecodedPacket();
        pkt.setHashId("anhash");
        pkt.setPkttype("AlertTest");
        final VhfDecodedPacket saved = vhfDecodedPacketService.saveOrUpdateDecodedPacket(pkt);
        assertThat(saved).isNotNull();
        try {
            final VhfDecodedPacketDto entity = vhfDecodedPacketService
                    .getVhfDecodedPacketByHash("anhash");
            assertThat(entity).isNotNull();
        }
        catch (final AbstractCdbServiceException e) {
            log.error("Cannot save decoded packet...");
            e.printStackTrace();
        }
        final VhfDecodedPacket pkt1 = DataGenerator.generateDecodedPacket();
        pkt1.setPktformat("TO_BE_DECODED");
        pkt1.setHashId("anotherhash");
        pkt1.setPkttype("AlertTest");
        final VhfDecodedPacket saved1 = vhfDecodedPacketService.saveOrUpdateDecodedPacket(pkt1);
        assertThat(saved1).isNotNull();
        try {
            final Page<VhfDecodedPacket> entitylist = vhfDecodedPacketService
                    .findAllVhfDecodedPackets(null, null);
            assertThat(entitylist).isNotNull();
            final PageRequest req = PageRequest.of(0, 10);
            final Page<VhfDecodedPacket> entitylist1 = vhfDecodedPacketService
                    .findAllVhfDecodedPackets(null, req);
            assertThat(entitylist1).isNotNull();

            pkt1.setPacket("anupdatedcontent");
            final VhfDecodedPacket saved2 = vhfDecodedPacketService.saveOrUpdateDecodedPacket(pkt1);
            assertThat(saved2).isNotNull();
            final VhfDecodedPacketDto stored = vhfDecodedPacketService
                    .getVhfDecodedPacketByHash("anotherhash");
            assertThat(stored).isNotNull();
            final List<VhfDecodedPacket> tbd = vhfDecodedPacketService.findDecodedPacketByFormat(
                    DecodedMessageType.TO_BE_DECODED.getTypename(),
                    new Timestamp(new Date().getTime() - 3600000));
            assertThat(tbd).isNotNull();
            // assertThat(tbd).isNotEmpty();
            final String decoded = vhfDecodedPacketService.decodeBytes("somepkt".getBytes(),
                    "anotherhash");
            assertThat(decoded).isNotNull();
        }
        catch (final AbstractCdbServiceException e) {
            log.error("Error in saving decoded packet : {}", e);
        }

        // test with null data
        try {
            final String decoded = vhfDecodedPacketService.decodeBytes(null, "somehash");
            assertThat(decoded).isNotNull();
            final HTTPResponse decodedresp = vhfDecodedPacketService.decodePacket(null);
            log.info("bytes decoded for null data...should not work");
        }
        catch (final VhfDecodingException e) {
            log.error("Error in decodeBytes getting a decoded packet : {}", e);
        }
    }

    @Test
    public void testD_logMessageServiceTest() throws IOException {
        log.debug("=============== Log message service test ================");
        final String pkt = DataGenerator.generateAlertFramePacket1();
        VhfTransferFrame vhftf = null;
        try {
            final byte[] dataarr = Hex.decodeHex(pkt);
            vhftf = vhfBinaryPacketService.deserialize(dataarr);
            vhftf.setRawPacket(pkt);
            VhfLogMessage msg = vhfLogMessageService.getVhfLogMessage(vhftf);
            String tojson = vhfLogMessageService.getVhfLogAsJson(msg);
            log.debug("Got log message in json : {}", tojson);
            String tocsv = vhfLogMessageService.getVhfLogAsCsv(msg);
            log.debug("Got log message in csv : {}", tocsv);
            assertThat(msg.getApid()).isEqualTo(vhftf.getCcsds().getCcsdsApid());

            final String storemsg = vhfLogMessageService.getVhfLogAsMap(msg);
            assertThat(storemsg).contains("VHF_LOG_MESSAGE");

            final VhfLogMessage errmsg = vhfLogMessageService.getDefaultLogMessage();
            String errormsg = vhfLogMessageService.getVhfLogAsMap(errmsg);
            assertThat(errormsg).contains("FRAMEID=-1");

            // Now set station status as null
            vhftf.setStationStatus(null);
            msg = vhfLogMessageService.getVhfLogMessage(vhftf);
            tojson = vhfLogMessageService.getVhfLogAsJson(msg);
            log.debug("Got log message with null station status in json : {}", tojson);
            tocsv = vhfLogMessageService.getVhfLogAsCsv(msg);
            log.debug("Got log message with null station status in csv : {}", tocsv);
            assertThat(msg.getApid()).isEqualTo(vhftf.getCcsds().getCcsdsApid());
            assertThat(msg.getStation()).isEqualTo(-1);

            // test exception
            tojson = vhfLogMessageService.getVhfLogAsJson(null);
            tocsv = vhfLogMessageService.getVhfLogAsCsv(null);
            errormsg = vhfLogMessageService.getVhfLogAsMap(null);
            assertThat(tojson).isEqualTo("Empty VhfLogMessage");
            assertThat(tocsv).isEqualTo("Empty VhfLogMessage");
            assertThat(errormsg).isEqualTo("Empty VhfLogMessage");

        }
        catch (VhfServiceException | DecoderException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testC_vhfTransferFrameTest() {
        log.debug("=============== Transfer Frame test ================");

        final String pkt = DataGenerator.generateAlertFramePacket2();
        VhfTransferFrame vhftf = null;
        try {
            final byte[] dataarr = Hex.decodeHex(pkt);
            vhftf = vhfBinaryPacketService.deserialize(dataarr);
            vhftf.setRawPacket(pkt);
            VhfTransferFrame storedbin = vhfBinaryPacketService.storeBinaryVhfPacket(vhftf);
            assertThat(storedbin.getBinaryHash()).isEqualTo(DataGenerator.generateAlertHash2());
            if (!vhftf.getIsFrameDuplicate()) {
                log.info("No duplicate found...stored {}", vhftf.getPacket());
            }
            else {
                VhfBinaryPacket dup = storedbin.getPacket();
                log.info("Retrieved duplicate binary: {}", dup);
                final List<VhfTransferFrame> origlist = vhfTransferFrameRepository
                        .findByBinaryHashAndisFrameDuplicate(dup.getHashId(), Boolean.FALSE);
                if (origlist.isEmpty()) {
                    log.debug("cannot find the original frame for this duplicate...");
                }
                else {
                    final VhfTransferFrame orig = origlist.get(0);
                    if (orig != null) {
                        log.debug("Retrieved original frame for duplicate: {}", orig.getFrameId());
                        vhftf.setIsFrameDuplicate(true);
                        vhftf.setDupFrameId(orig.getFrameId());
                    }
                    else {
                        log.error("This should not happen here");
                        vhftf.setIsFrameDuplicate(false);
                    }
                }
            }
            log.debug("Checked duplicates, and eventually stored the binary packet: {}", vhftf);

            final VhfTransferFrame saved = vhfTransferFrameService.storeVhfTransferFrame(vhftf);
            log.debug("Stored transfer frame {}", saved);
            assertThat(saved.getApid().getApid()).isEqualTo(vhftf.getApid().getApid());
            final List<SearchCriteria> arglist = prh.createMatcherCriteria("insertiontime>0,stationId:7");
            final PageRequest req = PageRequest.of(0, 10);
            final VhfPacketSearchDto dto = vhfTransferFrameService
                    .searchVhfTransferFrames(arglist, req);
            List<VhfBasePacket> entitylist = dto.getPackets();
            assertThat(entitylist.size()).isGreaterThanOrEqualTo(0);
            final VhfPacketSearchDto dtobase = vhfTransferFrameService
                    .searchVhfBinaryPackets(arglist, req);
            List<VhfBasePacket> baselist = dtobase.getPackets();
            assertThat(baselist.size()).isGreaterThanOrEqualTo(0);
            final VhfPacketSearchDto decodeddto = vhfTransferFrameService
                    .searchVhfDecodedPackets(arglist, req);
            List<VhfBasePacket> decodedlist = decodeddto.getPackets();
            assertThat(decodedlist.size()).isGreaterThanOrEqualTo(0);

            // Try to use bad arguments to trigger exceptions
            final VhfTransferFrameDto wrongid = vhfTransferFrameService
                    .getVhfTransferFrameById(null);
            log.debug("Wrong id transfer frame {}", wrongid);
        }
        catch (VhfServiceException | DecoderException | NotExistsPojoException | VhfCriteriaException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testC_vhfTransferFrameAlertFromRaw() {
        log.info("=============== testC_vhfTransferFrameAlertFromRaw ================");
        String hexpkt =
                "280000005eefd23600000000000000000000000000000000000100001876eb0018000240c00100575eefd234000000c8015eefd233434c414c455254001306c998bc584c4552544c310045434c414c4552544c310045434c414c4552544c31004a9f8d8bca7fc48649f601e73edba64b3f28215a3e99c18a3f0ae96e00008c10";

        VhfRawPacket raw = null;
        try {
            raw = vhfRawPacketService.storeRawPacket(hexpkt, HeaderFormat.PKT_MODE_PROD.value);
            log.info("Generated raw packet {}", raw);
            // Create and store transfer frame.
            vhfTransferFrameFacade.createVhfTransferFrameFromRawPacket(raw);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            log.error("Cannot store packet {} or problems in decoding for {}", hexpkt, raw);
        }
    }

    @Test
    public void testC_vhfTransferFrameLCFromRaw() {
        log.info("=============== testC_vhfTransferFrameLCFromRaw ================");
        String hexpkt = DataGenerator.generateHexaLC();
        String hash = DataGenerator.generateHashLC();
        String decoded = DataGenerator.generateDecodedLCNoSlash();
        log.info("Using decoded string as: {}", decoded);
        //String jpkt = decoded.replaceAll("\"", Matcher.quoteReplacement("\\\""));
        VhfRawPacket raw = null;
        try {
            raw = vhfRawPacketService.storeRawPacket(hexpkt, HeaderFormat.PKT_MODE_PROD.value);
            log.info("Generated raw packet {}", raw);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            log.error("Cannot store packet");
            raw = vhfRawPacketService.findRawPacketByPacket(hexpkt);
        }
        try {
            log.info("Test decoding parsing...");
            final VhfTransferFrame vhftf = vhfTransferFrameFacade.createVhfTransferFrame(raw);
            VhfDecodedPacket decentity = new VhfDecodedPacket();
            decentity.setHashId(hash);
            decentity.setPacket(decoded);
            decentity.setPkttype("TmVhfEclairsHighPriorityLightCurve");
            vhfDecodedPacketService.saveOrUpdateDecodedPacket(decentity);
            log.info("Decoded entity is {}", decentity);
            String jpktjson = decentity.getPacket();
            log.info("Using jpktjson string as: {}", jpktjson);

            final Map<String, String> decfields = DataModelsHelper
                    .parseCcsds(jpktjson);
            decfields.forEach((k, v) -> log.info("decoded field: {} = {}", k, v));
            // Create and store transfer frame.
            vhfTransferFrameFacade.createVhfTransferFrameFromRawPacket(raw);

            // Test decoded packet method
            log.info("Decode ccsds header from frame {}", vhftf.getHeader());
            PrimaryHeaderPacket php = vhfDecodedPacketService.decodeCcsdsHeader(decentity, vhftf.getHeader());
            log.info("Primary header packet {}", php);
            assertThat(php.getPacketObsid()).isEqualTo(200);
            // Try to decode position
            ApidGenerator apidgen = new ApidGenerator();
            PacketApid ecllc = apidgen.getLightCurveApid();
            log.info("Decoding position for apid {} and packet {}", ecllc, decentity.getPacket());
            VhfSatPosition pos = vhfDecodedPacketService.decodePosition(decentity, ecllc);
            assertThat(pos).isNotNull();
            VhfSatAttitude att = vhfDecodedPacketService.decodeAttitude(decentity, ecllc);
            assertThat(att).isNotNull();

            String newfmt = "{\"CcsdsTmHeadr\": {\"CcsdsApid\": 586, \"CcsdsGFlag\": 0, \"CcsdsCounter\": 0, "
                            + "\"CcsdsPLength\": 0}, \"VhfTmHeader\": {\"DPacketTimeS\": 1565687742}, "
                            + "\"InstrumentMode\":255,\"CurrentObsId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":1280}}";
            decentity.setPacket(newfmt);
            decentity.setPkttype("TmVhfEclairsRecurrent1");
            log.info("Decoded entity 2 is {}", decentity);
            // Test decoded packet method
            PrimaryHeaderPacket php1 = vhfDecodedPacketService.decodeCcsdsHeader(decentity, vhftf.getHeader());
            log.info("Primary header packet 2 {}", php1);
            assertThat(php1.getPacketObsidType()).isEqualTo(0);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            log.error("Cannot create frame or decode: {}", e.getMessage());
        }
    }

    @Test
    public void testC_vhfStationStatusCountTest() {
        log.info("=============== Station status count test ================");

        final String pkt = DataGenerator.generateAlertFramePacket();
        VhfTransferFrame vhftf = null;
        VhfRawPacket raw = null;
        try {
            raw = vhfRawPacketService.storeRawPacket(pkt, HeaderFormat.PKT_MODE_PROD.value);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            log.error("Raw packet already exists, load it from DB");
            raw = vhfRawPacketService.findRawPacketByPacket(pkt);
            log.info("Found raw packet {}", raw);
        }

        try {
            vhftf = vhfTransferFrameFacade.createVhfTransferFrame(raw);
            log.info("Checked duplicates, and eventually store the binary packet: {}", vhftf);
            final VhfTransferFrame saved = vhfTransferFrameService.storeVhfTransferFrame(vhftf);
            log.info("Stored transfer frame {}", saved);
            assertThat(saved.getApid().getApid()).isEqualTo(vhftf.getApid().getApid());
        }
        catch (VhfServiceException e) {
            // TODO Auto-generated catch block
            log.error("Frame creation failed with {}", e.getMessage());
        }
        // Update notif and check station counts.
        try {
            // Try update notif status
            final long frameid = 1;
            log.info("Update notif status for {}", frameid);

            final VhfTransferFrame upd = vhfTransferFrameService.updateNotifStatus(frameid,
                    NotifStatus.SENT);
            // assertThat(upd).isNotNull();
            final VhfTransferFrame upd1 = vhfTransferFrameService.updateNotifStatus(-1L,
                    NotifStatus.FAILED);
            assertThat(upd1).isNull();

            log.info("Testing station status count");
            final Long since = 0L;
            final Long until = Instant.now().toEpochMilli();
            log.debug("Get station status counts in range {} {}", since, until);
            final List<VhfStationStatusCountDto> sscountdtos = vhfStationStatusService
                    .findStationStatusCount(since, until);
            log.debug("Received list of station status counts of length {}", sscountdtos.size());
            assertThat(sscountdtos.size()).isGreaterThanOrEqualTo(0);
            final List<VhfStationStatusCountDto> sscountdtosnull = vhfStationStatusService
                    .findStationStatusCount(null, until);
            assertThat(sscountdtosnull).isNotNull();

        }
        catch (final VhfServiceException e) {
            log.error("Exception from service : {}", e);
        }
        // Station status
        try {
            final Page<VhfStationStatus> sslist = vhfStationStatusService
                    .findAllVhfStationStatus(null, null);
            assertThat(sslist).isNotNull();
            final List<VhfStationStatusCountDto> ssclist = vhfStationStatusService
                    .findStationStatusCount(null, null);
            assertThat(ssclist).isNotNull();

            final PageRequest preq = prh.createPageRequest(0, 10, "sstatusId:ASC");
            final List<SearchCriteria> params = prh.createMatcherCriteria(
                    "doppler>-1,stationtime>0");
            final List<BooleanExpression> expressions = vhfStationStatusFiltering
                    .createFilteringConditions(params);
            BooleanExpression wherepred = null;

            for (final BooleanExpression exp : expressions) {
                if (wherepred == null) {
                    wherepred = exp;
                }
                else {
                    wherepred = wherepred.and(exp);
                }
            }
            final Page<VhfStationStatus> ssclist1 = vhfStationStatusService
                    .findAllVhfStationStatus(wherepred, preq);
            assertThat(ssclist1).isNotNull();
            final Page<VhfStationStatus> ssclist2 = vhfStationStatusService
                    .findAllVhfStationStatus(null, preq);
            assertThat(ssclist2).isNotNull();

        }
        catch (final VhfServiceException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testVhfDeserialization() throws Exception {
        log.info("=============== Vhf deserialization test ================");
        final String hexa = DataGenerator.generateAlertFramePacket();
        final BigInteger mpkt2 = new BigInteger(hexa, 16);
        final byte[] data = mpkt2.toByteArray();
        log.info("Try to deserialize {}", hexa);
        final VhfTransferFrame vhftf = vhfTransferFrameDeserializer.deserialize(data);
        assertThat(vhftf.toString().length()).isGreaterThan(0);
    }

    @Test
    public void serviceBinaryPacketTest() throws VhfServiceException {
        log.info("=============== Vhf binary packet test ================");
        final String test = "This is a test for binary content";
        final byte[] data = test.getBytes();
        final ByteArrayInputStream istream = new ByteArrayInputStream(data);
        final String hexa = vhfBinaryPacketService.saveStreamInHexa(istream,
                HeaderFormat.STREAM_FORMAT_BINARY.getFormat());
        assertThat(hexa).isNotEmpty();
        // Test failures on binary service
        try {
            final VhfBinaryPacket entity = vhfBinaryPacketService.getVhfBinaryPacketByHash(null);
        }
        catch (AbstractCdbServiceException | InvalidDataAccessApiUsageException e) {
            log.error("Null hash should trigger exception");
        }
        final Page<VhfBinaryPacket> plist = vhfBinaryPacketService.findAllVhfBinaryPackets(null,
                null);
        assertThat(plist).isNotNull();
    }

    @Test
    public void serviceGroundStationTest() throws VhfServiceException {
        log.info("=============== Vhf ground station test ================");
        final Page<VhfGroundStation> dtolist = vhfGroundStationService
                .findAllVhfGroundStations(null, null);
        assertThat(dtolist).isNotNull();
        final VhfGroundStation notexists = DataGenerator.generateVhfGroundStation(-1, "notthere",
                "10.0.0.1");
        try {
            vhfGroundStationService.updateVhfGroundStation(notexists);
        }
        catch (final NotExistsPojoException e) {
            log.error("entity not found {}", e);
        }
        final VhfGroundStation newstation = DataGenerator.generateVhfGroundStation(99, "anewstat",
                "10.0.0.2");
        vhfGroundStationService.insertVhfGroundStation(newstation);
        newstation.setDescription("a modified description");
        newstation.setId("none");
        newstation.setMacaddress("none");
        try {
            final VhfGroundStation staupd = vhfGroundStationService.updateVhfGroundStation(newstation);
            assertThat(staupd).isNotNull();
        }
        catch (final NotExistsPojoException e) {
            log.error("entity not found {}", e);
        }

    }

    @Test
    public void servicePacketApidTest() throws VhfServiceException {
        log.info("=============== Packet apid test ================");
        final Page<PacketApid> dtolist = packetApidService.findAllPacketApids(null, null);
        assertThat(dtolist).isNotNull();

        final PacketApid apid = new PacketApid();
        apid.setApid(null);
        apid.setName("WrongTypeOfApid");
        apid.setDescription("Contains an invalid APID");

        final PacketApidDto dto = mapper.map(apid, PacketApidDto.class);
        try {
            packetApidService.registerApid(apid);
        }
        catch (final RuntimeException e) {
            log.error("Cannot insert null apid");
        }
        try {
            packetApidService.findApid((Integer) null);
        }
        catch (final RuntimeException e) {
            log.error("Cannot search null apid");
        }

        PacketApid saved = null;
        dto.apid(999);
        dto.category("test");
        dto.className("alert");
        dto.expectedPackets(1);
        final PacketApid apid2 = mapper.map(dto, PacketApid.class);
        try {
            saved = packetApidService.registerApid(apid2);
            assertThat(saved).isNotNull();
            saved.setDescription("");
            packetApidService.updateApid(saved);
            final PageRequest req = PageRequest.of(0, 10);
            final Page<PacketApid> apidlist = packetApidService.findAllPacketApids(null, req);
            assertThat(apidlist).isNotNull();
        }
        catch (final RuntimeException e) {
            log.error("Cannot insert null apid");
        }

    }

    @Test
    public void testE_SatelliteServiceTest() throws IOException {
        log.debug(
                "=============== Satellite service test : attitude and positions ================");
        final VhfSatAttitude pkt = DataGenerator.generateAttitude();
        try {
            VhfSatAttitude saved = vhfSatelliteService.saveAttitude(pkt);
            assertThat(saved).isNotNull();
            Long instime = saved.getInsertionTime().getTime();
            final PageRequest preq = prh.createPageRequest(0, 10, "insertionTime:ASC");
            final List<SearchCriteria> params = prh.createMatcherCriteria(
                    "apidname:%,packetTime>0,id:2,packettime<99999999999,packettime:2000,insertiontime>0,"
                    + "insertiontime<999999999999,insertiontime:"
                    + instime);
            final List<BooleanExpression> expressions = vhfSatAttitudeFiltering
                    .createFilteringConditions(params);
            BooleanExpression wherepred = null;

            for (final BooleanExpression exp : expressions) {
                if (wherepred == null) {
                    wherepred = exp;
                }
                else {
                    wherepred = wherepred.and(exp);
                }
            }
            Page<VhfSatAttitude> dtolist = vhfSatelliteService.findAllVhfSatAttitudes(wherepred,
                    preq);
            assertThat(dtolist.toList().size()).isGreaterThanOrEqualTo(0);
            Page<VhfSatAttitude> dtolist1 = vhfSatelliteService.findAllVhfSatAttitudes(null,
                    null);
            assertThat(dtolist1.toList().size()).isGreaterThanOrEqualTo(0);
            log.info("Retrieved list of attitude of size {}", dtolist1.toList().size());
        }
        catch (final RuntimeException e) {
            log.error("Cannot insert attitude: {}", e);
        }

        final VhfSatPosition pktpos = DataGenerator.generatePosition();
        try {
            VhfSatPosition saved = vhfSatelliteService.savePosition(pktpos);
            assertThat(saved).isNotNull();
            Long instime = saved.getInsertionTime().getTime();

            final PageRequest preq = prh.createPageRequest(0, 10, "insertionTime:ASC");
            final List<SearchCriteria> params = prh.createMatcherCriteria(
                    "apidname:%,packetTime>0,id:2,packettime<99999999999,packettime:2000,insertiontime>0,"
                    + "insertiontime<999999999999,insertiontime:"
                    + instime);
            final List<BooleanExpression> expressions = vhfSatPositionFiltering
                    .createFilteringConditions(params);
            BooleanExpression wherepred = null;

            for (final BooleanExpression exp : expressions) {
                if (wherepred == null) {
                    wherepred = exp;
                }
                else {
                    wherepred = wherepred.and(exp);
                }
            }
            Page<VhfSatPosition> dtolist = vhfSatelliteService.findAllVhfSatPositions(wherepred,
                    preq);
            assertThat(dtolist.toList().size()).isGreaterThanOrEqualTo(0);

            Page<VhfSatPosition> dtolist1 = vhfSatelliteService.findAllVhfSatPositions(null,
                    null);
            assertThat(dtolist1.toList().size()).isGreaterThanOrEqualTo(0);
            log.info("Retrieved list of position of size {}", dtolist1.toList().size());
        }
        catch (final RuntimeException e) {
            log.error("Cannot insert attitude: {}", e);
        }
    }

    @Test
    public void testF_obsidcountTest() {
        log.info("=============== Obsid counts test ================");
        final String apidname = APIDS.ECLALERTL1.getPacketApid().getName();
        final List<ObsIdApidCountDto> obl = obsIdApidCountService.findObsIdCount(apidname, "all",
                1L, 0L, 999999999999L);
        assertThat(obl).isNotNull();
        final List<ObsIdApidCountDto> obl2 = obsIdApidCountService.findObsIdCount("all", "OBLC_ECL",
                1L, 0L, 999999999999L);
        assertThat(obl2).isNotNull();
        List<Map<String, Object>> oblist = obsIdApidCountService.findObsidList(0L, 999999999999L);
        assertThat(oblist).isNotNull();
        Long obscount = obsIdApidCountService.findTotalPacketCount(0L, 999999999999L);
        assertThat(obscount).isNotNull();
        Long obscountfail = obsIdApidCountService.findTotalPacketCount(null, null);
        assertThat(obscountfail).isEqualTo(0L);
        try {
            List<Map<String, Object>> oblistfail = obsIdApidCountService.findObsidList(null, null);
            assertThat(oblistfail.size()).isEqualTo(0);
        }
        catch (AbstractCdbServiceException e) {
            log.error("Expected error: {}", e.getMessage());
        }
        try {
            final List<ObsIdApidCountDto> oblfail = obsIdApidCountService.findObsIdCount(null, "all",
                    1L, 0L, 999999999999L);
            assertThat(oblfail.size()).isEqualTo(0);
            final List<ObsIdApidCountDto> obl1 = obsIdApidCountService.findObsIdCount(null, null, 1L,
                    0L, null);
            assertThat(obl1).isNotNull();

        }
        catch (AbstractCdbServiceException e) {
            log.error("Expected error: {}", e.getMessage());
        }

    }

    @Test
    public void testF_burstIdTest() {
        log.info("=============== BurstId test ================");
        VhfBurstId bid = DataGenerator.generateBurstId();
        bid.setBurstId("sb20050512");
        VhfBurstId saved = vhfBurstIdService.storeOrUpdateVhfBurstId(bid);
        assertThat(saved.getBurstId()).isEqualTo(bid.getBurstId());
        log.info("Stored burst {}", saved);
        Page<VhfBurstId> biddtoList = vhfBurstIdService.findAllVhfBurstIds(null, null);
        assertThat(biddtoList.toList()).isNotNull();
        assertThat(biddtoList.toList()).isNotEmpty();
        String bname = null;
        for (VhfBurstId dto : biddtoList.toList()) {
            bname = dto.getBurstId();
            log.info("Found burstId {}", dto.getBurstId());
        }
        // Try to insert the same burst ID
        try {
            log.info("Storing the same burst id sb20050512");
            VhfBurstId bid1 = DataGenerator.generateBurstId();
            bid1.setBurstId("sb20050512");
            VhfBurstId saved1 = vhfBurstIdService.storeOrUpdateVhfBurstId(bid1);
            log.info("Stored new burst {}", saved1);
        }
        catch (RuntimeException e) {
            log.error("Exception in saving burst ID: same ID was inserted {}", e);
        }
        // Get burst ID
        log.info("Search burst id by ID: {}", bid);
        VhfBurstId stored = vhfBurstIdService.getVhfBurstById(bid.getBurstId());
        assertThat(stored.getBurstId()).isEqualTo(bid.getBurstId());
        try {
            VhfBurstId notthere = vhfBurstIdService.getVhfBurstById("notthere");
        }
        catch (NotExistsPojoException e) {
            log.info("Entity with burstId notthere does not exists.");
        }
        // Get last by time
        log.info("Search burst id by packet time: {}", bid);
        List<VhfBurstId> lastids = vhfBurstIdService.getLastByTime(bid.getPacketTime() - 1388530800L);
        log.info("Found list of burst ids : {}", lastids);
        assertThat(lastids).isNotEmpty();
        try {
            List<VhfBurstId> lastidswithexp = vhfBurstIdService.getLastByTimeAndObsid(null, null, "none");
        }
        catch (RuntimeException e) {
            log.info("Exception because of null obsid");
        }
    }
}
