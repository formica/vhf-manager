/**
 *
 */
package fr.svom.vhf.server.test;

import fr.svom.vhf.data.exceptions.AbstractCdbServiceException;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.server.controllers.EntityDtoHelper;
import fr.svom.vhf.server.test.generators.ApidGenerator;
import fr.svom.vhf.swagger.model.PacketApidDto;
import ma.glasnost.orika.MapperFacade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Iterator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration
@ActiveProfiles("test")
public class VhfToolsTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EntityDtoHelper edh;

    @Test
    public void entityToDto() throws AbstractCdbServiceException {
        ApidGenerator gen = new ApidGenerator();
        List<PacketApid> apids = gen.getApidList();
        Iterator<PacketApid> iterator = apids.iterator();
        List<PacketApidDto> dtolist = edh.entityToDtoList((Iterable<PacketApid>)apids , PacketApidDto.class);
        assertThat(dtolist.size()).isPositive();
        log.debug("Entity to Dto");
    }
}
