/**
 *
 */
package fr.svom.vhf.server.test;

import fr.svom.vhf.data.exceptions.NotExistsPojoException;
import fr.svom.vhf.data.exceptions.VhfCriteriaException;
import fr.svom.vhf.data.exceptions.VhfServiceException;
import fr.svom.vhf.data.mappers.NotifStatus;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.data.repositories.VhfBinaryPacketRepository;
import fr.svom.vhf.data.repositories.VhfDecodedPacketRepository;
import fr.svom.vhf.data.repositories.querydsl.SearchCriteria;
import fr.svom.vhf.server.controllers.PageRequestHelper;
import fr.svom.vhf.server.controllers.ServerEnvironment;
import fr.svom.vhf.server.controllers.ServicesIndicator;
import fr.svom.vhf.server.externals.NatsController;
import fr.svom.vhf.server.services.VhfTransferFrameService;
import fr.svom.vhf.server.test.generators.DataGenerator;
import fr.svom.vhf.swagger.model.VhfPacketSearchDto;
import fr.svom.vhf.swagger.model.VhfTransferFrameDto;
import ma.glasnost.orika.MapperFacade;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@ContextConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VhfTranferFrameServiceTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("mapper")
    private MapperFacade mapper;

    @Autowired
    ServicesIndicator servicesIndicator;
    @Autowired
    ServerEnvironment serverEnvironment;

    @Autowired
    VhfTransferFrameService vhfTransferFrameService;
    @Autowired
    VhfBinaryPacketRepository vhfBinaryPacketRepository;
    @Autowired
    VhfDecodedPacketRepository vhfDecodedPacketRepository;
    @Autowired
    PageRequestHelper prh;
    @MockBean
    private NatsController nats;

    @Test
    public void testA_vhfTransferFrameTest() {
        log.debug("=============== Transfer Frame test A ================");

        VhfTransferFrame vhftf = DataGenerator.generateFrame(null);
        try {
            VhfBinaryPacket sbin = getStoredBinPacket();
            VhfDecodedPacket sdec = getStoredDecodedPacket();
            vhftf.setBinaryHash("atestpacket1");
            vhftf.setPacket(sbin);
            vhftf.setPacketJson(sdec);
            log.debug("store frame: {}", vhftf);
            final VhfTransferFrame saved = vhfTransferFrameService.storeVhfTransferFrame(vhftf);
            log.debug("Stored transfer frame {}", saved);
            assertThat(saved.getApid().getName()).isEqualTo(vhftf.getApid().getName());
            final List<SearchCriteria> arglist = prh.createMatcherCriteria("stationId:7");
            final PageRequest req = PageRequest.of(0, 10);
            final VhfPacketSearchDto entitylist = vhfTransferFrameService
                    .searchVhfTransferFrames(arglist, req);
            assertThat(entitylist.getPackets().size()).isGreaterThanOrEqualTo(0);
            final VhfPacketSearchDto baselist = vhfTransferFrameService
                    .searchVhfBinaryPackets(arglist, req);
            assertThat(baselist.getPackets().size()).isGreaterThanOrEqualTo(0);
            final VhfPacketSearchDto decodedlist = vhfTransferFrameService
                    .searchVhfDecodedPackets(arglist, req);
            assertThat(decodedlist.getPackets().size()).isGreaterThanOrEqualTo(0);

            // Try to use bad arguments to trigger exceptions
            final VhfTransferFrameDto wrongid = vhfTransferFrameService
                    .getVhfTransferFrameById(null);
            log.debug("Wrong id transfer frame {}", wrongid);
        }
        catch (VhfServiceException | NotExistsPojoException | VhfCriteriaException e) {
            log.error("Service exception {}", e.getMessage());
        }
    }

    @Test
    public void testB_vhfTransferFrameTest() {
        log.debug("=============== Transfer Frame test B================");
        Mockito.when(nats.publishToQueue(any(), any(String.class))).thenReturn(NotifStatus.SENT.name());

        VhfTransferFrame vhftf = DataGenerator.generateFrame(null);
        try {
            VhfBinaryPacket sbin = getStoredBinPacket();
            VhfDecodedPacket sdec = getStoredDecodedPacket();
            vhftf.setBinaryHash("atestpacket1");
            vhftf.setPacket(sbin);
            vhftf.setPacketJson(sdec);
            VhfGroundStation stat = vhftf.getStation();
            stat.setStationId(9);
            stat.setId("0x09");
            stat.setMacaddress("127.0.0.9");
            vhftf.setStation(stat);
            VhfStationStatus status = vhftf.getStationStatus();
            status.setIdStation(9);
            vhftf.setStationStatus(status);
            log.debug("store frame: {}", vhftf);
            final VhfTransferFrame saved = vhfTransferFrameService.storeVhfTransferFrame(vhftf);
            log.debug("Stored transfer frame {}", saved);
            assertThat(saved.getApid().getName()).isEqualTo(vhftf.getApid().getName());
            Map<String, Integer> reqmap = vhfTransferFrameService.requestNotification(saved.getFrameId(),
                    Instant.now().toEpochMilli());
            assertThat(reqmap).isNotNull();
            // This is a method which is only publishing for specific frames
            // In this case it will not do much because we do not have an alert
            vhfTransferFrameService.publishNotification(vhftf);

            // Now try update methods isduplicate
            VhfTransferFrame upd1 = vhfTransferFrameService.updateIsDuplicate(vhftf.getFrameId(), -1L);
            assertThat(upd1.getIsFrameDuplicate()).isTrue();
            // Now try update methods isvalid
            VhfTransferFrame upd2 = vhfTransferFrameService.updateFrameStatus(vhftf.getFrameId(), "Updated",
                    Boolean.FALSE);
            assertThat(upd2.getIsFrameValid()).isFalse();
            log.debug("Store modified Primary header only");
            PrimaryHeaderPacket ph = vhftf.getHeader();
            ph.setPacketObsid(567L);
            VhfTransferFrame phsaved = vhfTransferFrameService.saveOrUpdatePrimaryHeader(ph, vhftf);
            assertThat(phsaved.getHeader().getPacketObsid()).isEqualTo(567L);
        }
        catch (VhfServiceException | NotExistsPojoException e) {
            e.printStackTrace();
        }
    }

    protected VhfBinaryPacket getStoredBinPacket() {
        VhfBinaryPacket binp = new VhfBinaryPacket();
        binp.setBinaryPacket(DataGenerator.generatePacket());
        binp.setHashId("atestpacket1");
        binp.setPktformat("BIN");
        binp.setSize(binp.getBinaryPacket().length);
        VhfBinaryPacket sbin = vhfBinaryPacketRepository.save(binp);
        return sbin;
    }

    protected VhfDecodedPacket getStoredDecodedPacket() {
        VhfDecodedPacket binp = DataGenerator.generateDecodedPacket();
        binp.setHashId("atestpacket1");
        binp.setPktformat("DECODED");
        VhfDecodedPacket sbin = vhfDecodedPacketRepository.save(binp);
        return sbin;
    }

}
