/**
 * 
 */
package fr.svom.vhf.server.test.generators;

import java.util.ArrayList;
import java.util.List;

import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.swagger.model.PacketApidDto;

/**
 * @author formica
 *
 */
public class ApidGenerator {

    private final List<PacketApid> apidList = new ArrayList<>();

    public ApidGenerator() {
        init();
    }

    protected void init() {
        final APIDS[] lapids = APIDS.values();
        for (final APIDS apids : lapids) {
            final PacketApid pa = apids.getPacketApid();
            apidList.add(pa);
        }
    }

    public List<PacketApid> getApidList() {
        return apidList;
    }

    public List<PacketApidDto> getApidDtoList() {
        List<PacketApidDto> apidDtoList = new ArrayList<>();
        for (PacketApid apid : apidList) {
            PacketApidDto dto = new PacketApidDto();
            dto.setApid(apid.getApid());
            dto.setName(apid.getName());
            dto.setCategory(apid.getCategory());
            dto.setClassName(apid.getClassName());
            dto.setInstrument(apid.getInstrument());
            dto.setDescription(apid.getDescription());
            dto.setExpectedPackets(apid.getExpectedPackets());
            apidDtoList.add(dto);
        }
        return apidDtoList;
    }

    public PacketApid getAlertApid() {
        return APIDS.ECLALERTL1.getPacketApid();
    }

    public PacketApid getLightCurveApid() {
        return APIDS.ECLLCURHP1.getPacketApid();
    }

}
