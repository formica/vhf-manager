/**
 * 
 */
package fr.svom.vhf.server.test.generators;

import fr.svom.vhf.data.packets.CcsdsPacket;
import fr.svom.vhf.data.packets.FrameHeaderPacket;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.PrimaryHeaderPacket;
import fr.svom.vhf.data.packets.VhfBinaryPacket;
import fr.svom.vhf.data.packets.VhfBurstId;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.data.packets.VhfNotification;
import fr.svom.vhf.data.packets.VhfRawPacket;
import fr.svom.vhf.data.packets.VhfSatAttitude;
import fr.svom.vhf.data.packets.VhfSatPosition;
import fr.svom.vhf.data.packets.VhfStationStatus;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;

/**
 * @author aformic
 *
 */
public final class DataGenerator {

    // / Size of a CCSDS frame [Bytes]
    private static final int FRAME_HEADER_SIZE = 6;
    private static final int CCSDS_FRAME_HEADER_SIZE = 6;
    private static final int CCSDS_FRAME_DATA_SIZE = 88;
    private static final int CCSDS_FRAME_SIZE = CCSDS_FRAME_HEADER_SIZE + CCSDS_FRAME_DATA_SIZE;
    private static final int FRAME_SIZE = FRAME_HEADER_SIZE + CCSDS_FRAME_SIZE;

    private static final int STATION_HEADER_SIZE = 28;

    private static Logger log = LoggerFactory.getLogger(DataGenerator.class);

    protected static Random apidRandom = new Random();

    public static byte[] generatePacket() {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream(
                FRAME_SIZE + STATION_HEADER_SIZE);
        final DataOutputStream dos = new DataOutputStream(baos);
        byte[] frame = null;
        try {
            // The station header (28 bits)
            final byte[] datastation = new byte[STATION_HEADER_SIZE];
            apidRandom.nextBytes(datastation);
            dos.write(datastation);

            final byte[] frameHeader = new byte[FRAME_HEADER_SIZE];
            apidRandom.nextBytes(frameHeader);
            dos.write(frameHeader);

            final byte[] ccsdsframeHeader = new byte[CCSDS_FRAME_HEADER_SIZE];
            apidRandom.nextBytes(ccsdsframeHeader);
            dos.write(ccsdsframeHeader);

            System.out.println(
                    ">>>>> Frame header value: 0x" + new BigInteger(frameHeader).toString(16));

            // The main frame (88 bits)
            final byte[] data = new byte[CCSDS_FRAME_DATA_SIZE];
            apidRandom.nextBytes(data);
            dos.write(data);

        }
        catch (final IOException e) {
            e.printStackTrace();
        }
        frame = baos.toByteArray();
        log.debug(">>>>> Frame size: " + frame.length);
        log.debug(">>>>> Frame value: 0x" + new BigInteger(frame).toString(16));
        return frame;
    }

    public static String generateAlertFramePacket() {
        final String pktnewhexa = "070000005DB055BA000000000000000000000000000000000001000018761E0018000240C00100575DB055BA000000960005480EFE434C414C4552544C0000010000014C4552544C310045434C414C4552544C000000004C414C4552544C310045434C414C4552544C310045434C414C4552544C31000000000000000000CDD0";
        // final String pkthexa =
        // "070000005BF41527000000000000000000000000000000000001000018761E0018000240C00100570000003D000000010000000145434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C31000000000000000000CDD0";
        String hexpkt = "280000005eefd25200000000000000000000000000000000000100001876f90018000240c00700575eefd251000000c8075eefd250434c414c455254ff12fe8498263a4c4552544c310045434c414c4552544c310045434c414c4552544c31004a9f8d8bca7fc48649f601e73edba64b3f28215a3e99c18a3f0ae96e0000e098";
        return hexpkt;
    }
    public static String generateAlertHash() {
        String hexpkt = "a3158da889103b0d80197c3a92d0aa8c3cc900649b5705a72eb6aa22ce7fb7d2";
        return hexpkt;
    }
    public static String generateAlertDecoded() {
        String hexpkt = "{\"CcsdsTmHeadr\":{\"CcsdsApid\":576,\"CcsdsGFlag\":3,\"CcsdsCounter\":7,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592775249},\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":200},\"AlertMsgCount\":7,\"AlertTimeTbAbsSeconds\":1592775248,\"AlertTimeTbAbsMicroseconds\":4410433,\"AlertTimeTb\":1279611476,\"AlertSlewRequest\":255,\"AlertSkyTheta\":1244804,\"AlertSkyPhi\":9971258,\"AlertCatSrcNum\":19525,\"AlertSkyIPix\":82,\"AlertSkyJPix\":84,\"AlertSkyIFit\":19505,\"AlertSkyJFit\":69,\"AlertSkyCnt\":204.25506591796875,\"AlertSkyCntFit\":3365.2685546875,\"AlertSkyVar\":1.8665822221208828E-9,\"AlertQuality\":76,\"AlertTriggerCriterium\":16716,\"AlertDetCounts\":4543060,\"AlertDetBkg\":4993280,\"SatPositionX\":5228229.5,\"SatPositionY\":-4190497.5,\"SatPositionZ\":2015292.875,\"SatAttitudeQ0\":0.4290030896663666,\"SatAttitudeQ1\":0.6567589044570923,\"SatAttitudeQ2\":0.30030471086502075,\"SatAttitudeQ3\":0.5426243543624878,\"SatAttQuality\":0,\"Spare08\":0,\"VhfCrc\":57496}";
        return hexpkt;
    }

    public static String generateAlertFramePacket1() {
        // final String pkthexa =
        // "070000005BF41527000000000000000000000000000000000001000018761E0018000240C00100570000003D000000010000000145434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C31000000000000000000CDD0";
        String hexpkt = "280000005eefecb100000000000000000000000000000000000100001876eb0018000240c00100575eefecaf0000012c015eefecae434c414c455254001306c998bc584c4552544c310045434c414c4552544c310045434c414c4552544c31004a9f8d8bca7fc48649f601e73edba64b3f28215a3e99c18a3f0ae96e00008c10";
        return hexpkt;
    }
    public static String generateAlertHash1() {
        return "7ca770774aca5b205425e83b61186eb0980c26e4019231ef6830b03349710106";
    }
    public static String generateAlertDecoded1() {
        return "{\"CcsdsTmHeadr\":{\"CcsdsApid\":576,\"CcsdsGFlag\":3,\"CcsdsCounter\":1,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592781999},\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":300},\"AlertMsgCount\":1,\"AlertTimeTbAbsSeconds\":1592781998,\"AlertTimeTbAbsMicroseconds\":4410433,\"AlertTimeTb\":1279611476,\"AlertSlewRequest\":0,\"AlertSkyTheta\":1246921,\"AlertSkyPhi\":10009688,\"AlertCatSrcNum\":19525,\"AlertSkyIPix\":82,\"AlertSkyJPix\":84,\"AlertSkyIFit\":19505,\"AlertSkyJFit\":69,\"AlertSkyCnt\":204.25506591796875,\"AlertSkyCntFit\":3365.2685546875,\"AlertSkyVar\":1.8665822221208828E-9,\"AlertQuality\":76,\"AlertTriggerCriterium\":16716,\"AlertDetCounts\":4543060,\"AlertDetBkg\":4993280,\"SatPositionX\":5228229.5,\"SatPositionY\":-4190497.5,\"SatPositionZ\":2015292.875,\"SatAttitudeQ0\":0.4290030896663666,\"SatAttitudeQ1\":0.6567589044570923,\"SatAttitudeQ2\":0.30030471086502075,\"SatAttitudeQ3\":0.5426243543624878,\"SatAttQuality\":0,\"Spare08\":0,\"VhfCrc\":35856}";
    }

    public static String generateAlertFramePacket2() {
        // final String pkthexa =
        // "070000005BF41527000000000000000000000000000000000001000018761E0018000240C00100570000003D000000010000000145434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C310045434C414C4552544C31000000000000000000CDD0";
        String hexpkt = "280000005eeefece00000000000000000000000000000000000100001876ef0018000240c00400575eeefecd00000064045eeefecc434c414c4552540012e7ee984c524c4552544c310045434c414c4552544c310045434c414c4552544c31004a9f8d8bca7fc48649f601e73edba64b3f28215a3e99c18a3f0ae96e0000860a";
        return hexpkt;
    }

    public static String generateAlertHash2() {
        String hexpkt = "6632c246800a395069a5dddc974e89d990e2dbaa21d3917c68f088fb7db261c7";
        return hexpkt;
    }

    public static String generateAlertDecoded2() {
        String hexpkt = "{\"CcsdsTmHeadr\":{\"CcsdsApid\":576,\"CcsdsGFlag\":3,\"CcsdsCounter\":4,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592721101},\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":100},\"AlertMsgCount\":4,\"AlertTimeTbAbsSeconds\":1592721100,\"AlertTimeTbAbsMicroseconds\":4410433,\"AlertTimeTb\":1279611476,\"AlertSlewRequest\":0,\"AlertSkyTheta\":1239022,\"AlertSkyPhi\":9981010,\"AlertCatSrcNum\":19525,\"AlertSkyIPix\":82,\"AlertSkyJPix\":84,\"AlertSkyIFit\":19505,\"AlertSkyJFit\":69,\"AlertSkyCnt\":204.25506591796875,\"AlertSkyCntFit\":3365.2685546875,\"AlertSkyVar\":1.8665822221208828E-9,\"AlertQuality\":76,\"AlertTriggerCriterium\":16716,\"AlertDetCounts\":4543060,\"AlertDetBkg\":4993280,\"SatPositionX\":5228229.5,\"SatPositionY\":-4190497.5,\"SatPositionZ\":2015292.875,\"SatAttitudeQ0\":0.4290030896663666,\"SatAttitudeQ1\":0.6567589044570923,\"SatAttitudeQ2\":0.30030471086502075,\"SatAttitudeQ3\":0.5426243543624878,\"SatAttQuality\":0,\"Spare08\":0,\"VhfCrc\":34314}";
        return hexpkt;
    }

    public static FileSystemResource getBinStreamForUpload(String pkt, boolean bin) {
        String fpath = "/tmp/pkt.bin";
        try {
            if (bin) {
                byte[] dataarr = Hex.decodeHex(pkt);
                try (FileOutputStream os = (new FileOutputStream(fpath))) {
                    os.write(dataarr);
                }
            }
            else {
                try (PrintStream out = new PrintStream(new FileOutputStream(fpath))) {
                    out.print(pkt);
                }
            }
            FileSystemResource fr = new FileSystemResource(fpath);
            return fr;
        }
        catch (DecoderException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String generateBinaryString1() {
        return "0240c0000057eec0a2a354860b390ba74983a5a294de65e9e4243720cee2b8e5e8198902e7254c290c63f0e30d6a1b6c672355913c53276b25d9d8eb914ff7ad48c47cb4dc87eabe1bd59a83ec440d5b2144a28cad075f215fc82306ac52";
    }

    public static String generateBinaryString2() {
        return "0240c0000057eec0a2a354860b390ba74983a5a294de65e9e4243720aee2b8e5e8198902e7254c290c63f0e30d6a1b6c672355913c53276b25d9d8eb914ff7ad48c47cb4dc87eabe1bd59a83ec440d5b2144a28cad075f215fc82306ac52";
    }

    public static String generateBinaryString3() {
        return "0240c0000057eec0a2a354860b390ba74983a5a294de65e9e4243720bee2b8e5e8198902e7254c290c63f0e30d6a1b6c672355913c53276b25d9d8eb914ff7ad48c47cb4dc87eabe1bd59a83ec440d5b2144a28cad075f215fc82306ac52";
    }

    public static String generateHashLC() {
        return "e5798bdec39a71fc6093d02fa6d27cfba1f95fe458abc1f1f3c8f3dd9ff88d2a";
    }

    public static String generateHexaLC() {
        return "280000005eefd24000000000000000000000000000000000000100001876f00018000241000100575eefd237000000c85eefd233450236ea5411267045750045434c0000477900001818000017b300001a65000000000000000008060282024c0224000000000bcf047404d605bb000000000aaa041804d6054400000000fa74";
    }

    public static String generateBinaryLC() {
        return "0241000100575eefd237000000c85eefd233450236ea5411267045750045434c0000477900001818000017b300001a65000000000000000008060282024c0224000000000bcf047404d605bb000000000aaa041804d6054400000000fa74";
    }

    public static String generateDecodedLC() {
        return "\"{\\\"CcsdsTmHeadr\\\":{\\\"CcsdsApid\\\":577,\\\"CcsdsGFlag\\\":0,\\\"CcsdsCounter\\\":1,"
        + "\\\"CcsdsPLength\\\":87},\\\"VhfTmHeader\\\":{\\\"PacketTimeS\\\":1592775223},"
        + "\\\"VhfIObservId\\\":{\\\"VhfIObsType\\\":0,\\\"VhfNObsNumber\\\":200},"
        + "\\\"AlertTimeTb0AbsSeconds\\\":1592775219,\\\"AlertTimeTb0AbsMilliseconds\\\":69,"
        + "\\\"LightCurvePacketNumber\\\":2,\\\"SatAttitudeLCQ0\\\":14058,\\\"SatAttitudeLCQ1\\\":21521,"
        + "\\\"SatAttitudeLCQ2\\\":9840,\\\"SatAttitudeLCQ3\\\":17781,\\\"SatPositionLon\\\":69,"
        + "\\\"SatPositionLat\\\":17228,\\\"Sample0Eband0IntCount\\\":18297,\\\"Sample0Eband1IntCount\\\":6168,"
        + "\\\"Sample0Eband2IntCount\\\":6067,\\\"Sample0Eband3IntCount\\\":6757,\\\"Sample0EsatIntCount\\\":0,"
        + "\\\"Sample0MultipleIntCount\\\":0,\\\"Sample1Eband0DiffCount\\\":2054,\\\"Sample1Eband1DiffCount\\\":642,"
        + "\\\"Sample1Eband2DiffCount\\\":588,\\\"Sample1Eband3DiffCount\\\":548,\\\"Sample1EsatDiffCount\\\":0,"
        + "\\\"Sample1MultipleDiffCount\\\":0,\\\"Sample2Eband0DiffCount\\\":3023,\\\"Sample2Eband1DiffCount\\\":1140,"
        + "\\\"Sample2Eband2DiffCount\\\":1238,\\\"Sample2Eband3DiffCount\\\":1467,\\\"Sample2EsatDiffCount\\\":0,"
        + "\\\"Sample2MultipleDiffCount\\\":0,\\\"Sample3Eband0DiffCount\\\":2730,\\\"Sample3Eband1DiffCount\\\":1048,"
        + "\\\"Sample3Eband2DiffCount\\\":1238,\\\"Sample3Eband3DiffCount\\\":1348,\\\"Sample3EsatDiffCount\\\":0,"
        + "\\\"Sample3MultipleDiffCount\\\":0,\\\"VhfCrc\\\":64116}\"";
    }

    public static String generateDecodedLCNoSlash() {
        return "{\"CcsdsTmHeadr\":{\"CcsdsApid\":577,\"CcsdsGFlag\":0,\"CcsdsCounter\":1,"
               + "\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592775223},"
               + "\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":200},"
               + "\"AlertTimeTb0AbsSeconds\":1592775219,\"AlertTimeTb0AbsMilliseconds\":69,"
               + "\"LightCurvePacketNumber\":2,\"SatAttitudeLCQ0\":14058,\"SatAttitudeLCQ1\":21521,"
               + "\"SatAttitudeLCQ2\":9840,\"SatAttitudeLCQ3\":17781,\"SatPositionLon\":69,"
               + "\"SatPositionLat\":17228,\"Sample0Eband0IntCount\":18297,\"Sample0Eband1IntCount\":6168,"
               + "\"Sample0Eband2IntCount\":6067,\"Sample0Eband3IntCount\":6757,\"Sample0EsatIntCount\":0,"
               + "\"Sample0MultipleIntCount\":0,\"Sample1Eband0DiffCount\":2054,\"Sample1Eband1DiffCount\":642,"
               + "\"Sample1Eband2DiffCount\":588,\"Sample1Eband3DiffCount\":548,\"Sample1EsatDiffCount\":0,"
               + "\"Sample1MultipleDiffCount\":0,\"Sample2Eband0DiffCount\":3023,\"Sample2Eband1DiffCount\":1140,"
               + "\"Sample2Eband2DiffCount\":1238,\"Sample2Eband3DiffCount\":1467,\"Sample2EsatDiffCount\":0,"
               + "\"Sample2MultipleDiffCount\":0,\"Sample3Eband0DiffCount\":2730,\"Sample3Eband1DiffCount\":1048,"
               + "\"Sample3Eband2DiffCount\":1238,\"Sample3Eband3DiffCount\":1348,\"Sample3EsatDiffCount\":0,"
               + "\"Sample3MultipleDiffCount\":0,\"VhfCrc\":64116}";
    }

    public static String generateHexaLC1() {
        return "050000005eefd83800000000000000000000000000000000000100001876ec0018000241004500575eefd23a000000c85eefd233450436ea5411267045750045434c0000a00a0000392200003b890000453e00000000000000000932033002f50367000000000d0505420605072300000000142a09390b140e39000000004921";
    }

    public static String generateHashLC1() {
        return "f9566a1d7b80b73e4c0a92658546b4a9503da23adf9cd3de50fc01583e4e97b4";
    }

    public static String generateBinaryLC1() {
        return "0241004500575eefd23a000000c85eefd233450436ea5411267045750045434c0000a00a0000392200003b890000453e00000000000000000932033002f50367000000000d0505420605072300000000142a09390b140e39000000004921";
    }

    public static String generateDecodedLC1() {
        return "{\"CcsdsTmHeadr\":{\"CcsdsApid\":577,\"CcsdsGFlag\":0,\"CcsdsCounter\":69,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592775226},\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":200},\"AlertTimeTb0AbsSeconds\":1592775219,\"AlertTimeTb0AbsMilliseconds\":69,\"LightCurvePacketNumber\":4,\"SatAttitudeLCQ0\":14058,\"SatAttitudeLCQ1\":21521,\"SatAttitudeLCQ2\":9840,\"SatAttitudeLCQ3\":17781,\"SatPositionLon\":69,\"SatPositionLat\":17228,\"Sample0Eband0IntCount\":40970,\"Sample0Eband1IntCount\":14626,\"Sample0Eband2IntCount\":15241,\"Sample0Eband3IntCount\":17726,\"Sample0EsatIntCount\":0,\"Sample0MultipleIntCount\":0,\"Sample1Eband0DiffCount\":2354,\"Sample1Eband1DiffCount\":816,\"Sample1Eband2DiffCount\":757,\"Sample1Eband3DiffCount\":871,\"Sample1EsatDiffCount\":0,\"Sample1MultipleDiffCount\":0,\"Sample2Eband0DiffCount\":3333,\"Sample2Eband1DiffCount\":1346,\"Sample2Eband2DiffCount\":1541,\"Sample2Eband3DiffCount\":1827,\"Sample2EsatDiffCount\":0,\"Sample2MultipleDiffCount\":0,\"Sample3Eband0DiffCount\":5162,\"Sample3Eband1DiffCount\":2361,\"Sample3Eband2DiffCount\":2836,\"Sample3Eband3DiffCount\":3641,\"Sample3EsatDiffCount\":0,\"Sample3MultipleDiffCount\":0,\"VhfCrc\":18721}}";
    }

    public static PrimaryHeaderPacket generatePrimaryHeader() {

        final PrimaryHeaderPacket phpkt = new PrimaryHeaderPacket();
        phpkt.setPacketObsid(123L);
        phpkt.setPacketTime(427870798L);

        return phpkt;
    }

    public static VhfGroundStation generateVhfGroundStation(Integer id, String name,
            String macadd) {

        final VhfGroundStation entity = new VhfGroundStation();
        entity.setStationId(id);
        entity.setId(name);
        entity.setCountry("PAR");
        entity.setLocation("Paris");
        entity.setName(name);
        entity.setMacaddress(macadd);
        return entity;
    }

    public static VhfStationStatus generateVhfStationStatus() {

        final VhfStationStatus vhfstatus = new VhfStationStatus();
        vhfstatus.setIdStation(0x01);
        vhfstatus.setChannel(1);
        vhfstatus.setPadding1(0);
        vhfstatus.setPadding2(0);
        vhfstatus.setPadding3(0);
        vhfstatus.setStationTime(1500000000L);
        vhfstatus.setCorrPower(1000);
        vhfstatus.setDoppler((float) 20.3);
        vhfstatus.setDopDrift((float) 2.3);
        vhfstatus.setSrerror((float) 0.3);
        vhfstatus.setReedSolomonCorr(3);
        vhfstatus.setCkSum(1);
        return vhfstatus;
    }

    public static VhfRawPacket generateVhfRawPacket(String format, String flag, String packet) {

        final VhfRawPacket entity = new VhfRawPacket();
        entity.setPacket(packet);
        entity.setPktflag(flag);
        entity.setPktformat(format);
        entity.setSize(packet.length());
        return entity;
    }

    public static VhfBinaryPacket generateVhfBinaryPacket(String hash, String format,
            String packet) {

        final VhfBinaryPacket entity = new VhfBinaryPacket();
        entity.setPacket(packet);
        entity.setBinaryPacket(packet.getBytes());
        entity.setHashId(hash);
        entity.setPktformat(format);
        entity.setSize(packet.length());
        return entity;
    }

    public static VhfDecodedPacket generateVhfDecodedPacket(String hash, String format,
            String packet) {

        final VhfDecodedPacket entity = new VhfDecodedPacket();
        entity.setPacket(packet);
        entity.setHashId(hash);
        entity.setPktformat(format);
        entity.setSize((long) packet.length());
        return entity;
    }

    public static VhfTransferFrame generateFrame(final String qual) {
        final Timestamp receptionTime = new Timestamp(new Date().getTime());
        final Boolean isFrameValid = true;
        final VhfTransferFrame locvhfframe = new VhfTransferFrame();
        locvhfframe.setInsertionTime(receptionTime);
        locvhfframe.setIsFrameValid(isFrameValid);

        locvhfframe.setIsFrameDuplicate(Boolean.FALSE);
        final VhfGroundStation station = new VhfGroundStation();
        station.setId("0x01");
        station.setMacaddress("0.0.0.1");
        station.setStationId(1);
        station.setName("test");
        station.setCountry("France");
        station.setLocation("PARIS");
        station.setAltitude(10.);
        station.setLongitude(20.);
        station.setLatitude(30.);
        station.setDescription("a test station");

        final VhfStationStatus vhfstatus = new VhfStationStatus();
        vhfstatus.setIdStation(0x01);
        vhfstatus.setChannel(1);
        vhfstatus.setPadding1(0);
        vhfstatus.setPadding2(0);
        vhfstatus.setPadding3(0);
        vhfstatus.setStationTime(1500000000L);
        vhfstatus.setCorrPower(1000);
        vhfstatus.setDoppler((float) 20.3);
        vhfstatus.setDopDrift((float) 2.3);
        vhfstatus.setSrerror((float) 0.3);
        vhfstatus.setReedSolomonCorr(3);
        vhfstatus.setCkSum(1);

        final FrameHeaderPacket fhpkt = new FrameHeaderPacket();
        fhpkt.setSpaceCraftId(10);
        fhpkt.setVcId(8);
        fhpkt.setMcfCount(90);
        fhpkt.setVcfCount(80);
        fhpkt.setDfStatus(1);

        final PrimaryHeaderPacket phpkt = new PrimaryHeaderPacket();
        phpkt.setPacketObsid(123L);
        phpkt.setPacketTime(427870798L);
        phpkt.setPacketObsidType(0);
        phpkt.setPacketObsidNum(123);

        final CcsdsPacket cspkt = new CcsdsPacket();
        cspkt.setCcsdsApid(576);
        cspkt.setCcsdsCounter(400);
        cspkt.setCcsdsGFlag(1);
        cspkt.setCcsdsPlength(86);
        cspkt.setCcsdsCrc(2);

        final PacketApid apid = new PacketApid();
        apid.setApid(cspkt.getCcsdsApid());
        apid.setName("AlertPacket");
        apid.setClassName("alert");
        apid.setCategory("AlertSequence");
        apid.setDescription("Contains the description of the Alert send by UTS");

        locvhfframe.setFrameHeader(fhpkt);
        locvhfframe.setCcsds(cspkt);
        locvhfframe.setHeader(phpkt);
        locvhfframe.setStationStatus(vhfstatus);
        locvhfframe.setApid(apid);
        locvhfframe.setStation(station);
        return locvhfframe;
    }

    public static VhfDecodedPacket generateDecodedPacket() {
        final VhfDecodedPacket pkt = new VhfDecodedPacket();
        pkt.setHashId("anhashid");
        pkt.setPacket("{ key: \"value\" }");
        pkt.setPktformat("JSON");
        pkt.setSize((long) pkt.getPacket().length());
        return pkt;
    }

    public static VhfSatAttitude generateAttitude() {
        final VhfSatAttitude vhfatt = new VhfSatAttitude();
        vhfatt.setAttId(2L);
        vhfatt.setApidName("TmVhfEclairsAlert");
        vhfatt.setPacketTime(2000L);
        vhfatt.setSatAttitudeq0(1.0);
        vhfatt.setSatAttitudeq1(2.0);
        vhfatt.setSatAttitudeq2(3.0);
        vhfatt.setSatAttitudeq3(4.0);
        vhfatt.setInsertionTime(new Timestamp(Instant.now().getMillis()));
        return vhfatt;
    }

    public static VhfSatPosition generatePosition() {
        final VhfSatPosition vhfpos = new VhfSatPosition();
        vhfpos.setPosId(2L);
        vhfpos.setApidName("TmVhfEclairsAlert");
        vhfpos.setPacketTime(2000L);
        vhfpos.setSatPositionX(1.0);
        vhfpos.setSatPositionY(1.0);
        vhfpos.setSatPositionZ(1.0);
        vhfpos.setSatPositionLat(1.0);
        vhfpos.setSatPositionLon(1.0);
        return vhfpos;
    }

    public static VhfBurstId generateBurstId() {
        final VhfBurstId vhfbid = new VhfBurstId();
        Instant now = Instant.now();
        vhfbid.setBurstId("sb20050599");
        vhfbid.setPacketTime(now.getMillis()/1000L);
        vhfbid.setAlertTimeTb0AbsSeconds(now.getMillis()/1000L);
        vhfbid.setAlertTimeTb(vhfbid.getPacketTime()-10);
        vhfbid.setBurstEndtime(vhfbid.getAlertTimeTb0AbsSeconds()+864);
        vhfbid.setBurstStartime(vhfbid.getAlertTimeTb0AbsSeconds()-300);
        vhfbid.setEclairObsid(42L);
        return vhfbid;
    }

    public static VhfNotification generateNotification() {
        final VhfNotification notif = new VhfNotification();
        notif.setNotifId(1L);
        VhfTransferFrame frame = DataGenerator.generateFrame("none");
        notif.setFrame(frame);
        notif.setInsertionTime(new Timestamp(Instant.now().getMillis()));
        notif.setNotifMessage("Test message");
        notif.setSize(notif.getNotifMessage().length());
        notif.setPktflag("DECODED");
        return notif;
    }
}
