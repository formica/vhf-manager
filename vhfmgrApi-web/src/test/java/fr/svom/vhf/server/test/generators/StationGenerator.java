/**
 * 
 */
package fr.svom.vhf.server.test.generators;

import java.util.ArrayList;
import java.util.List;

import fr.svom.vhf.data.packets.VhfGroundStation;
import fr.svom.vhf.swagger.model.VhfGroundStationDto;

/**
 * @author formica
 *
 */
public class StationGenerator {

	private final List<VhfGroundStation> stationList = new ArrayList<>();
	
	public StationGenerator() {
		init();
	}
	
	protected void init() {
        final VhfGroundStation entity1 = DataGenerator.generateVhfGroundStation(0x01, "stvhf1", "00:01:01:01");
        final VhfGroundStation entity2 = DataGenerator.generateVhfGroundStation(0x02, "stvhf2", "00:01:01:02");
        final VhfGroundStation entity3 = DataGenerator.generateVhfGroundStation(0x03, "stvhf3", "00:01:01:03");
        final VhfGroundStation entity4 = DataGenerator.generateVhfGroundStation(0x07, "stvhf7", "00:01:01:07");
        final VhfGroundStation entity4a = DataGenerator.generateVhfGroundStation(0x05, "stvhf5", "00:01:01:05");
        final VhfGroundStation entity5 = DataGenerator.generateVhfGroundStation(40, "stvhf40", "00:01:01:40");

        stationList.add(entity1);
        stationList.add(entity2);
        stationList.add(entity3);
        stationList.add(entity4);
        stationList.add(entity4a);
        stationList.add(entity5);
	}

	public List<VhfGroundStation> getStationList() {
		return this.stationList;
	}

    public List<VhfGroundStationDto> getStationDtoList() {
        List<VhfGroundStationDto> stationDtoList = new ArrayList<>();
        for (VhfGroundStation st : stationList) {
            VhfGroundStationDto dto = new VhfGroundStationDto();
            dto.stationId(st.getStationId());
            dto.setName(st.getName());
            dto.setMacaddress(st.getMacaddress());
            stationDtoList.add(dto);
        }
	    return stationDtoList;
    }


}
