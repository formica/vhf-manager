/**
 * 
 */
package fr.svom.vhf.server.test.handlers;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

/**
 * @author formica
 *
 */
public class VhfErrorHandler implements ResponseErrorHandler {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		log.error("Error occurred in client http response: {} {}", response.getStatusCode(), response.getStatusText());
	}

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
        return RestUtils.isError(response.getStatusCode());
	}
}