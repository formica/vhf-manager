/**
 * 
 */
package fr.svom.vhf.server.test.nats;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.svom.messages.model.DataNotification;
import fr.svom.messages.model.DataNotificationContent;
import fr.svom.messages.model.DataProvider;
import fr.svom.messages.model.DataStream;
import fr.svom.messages.model.ServiceStatus;
import fr.svom.vhf.data.config.VhfProperties;
import fr.svom.vhf.data.packets.PacketApid;
import fr.svom.vhf.data.packets.VhfDecodedPacket;
import fr.svom.vhf.data.packets.VhfTransferFrame;
import fr.svom.vhf.server.externals.NatsController;
import fr.svom.vhf.server.externals.VhfNatsMessagesConverter;
import fr.svom.vhf.server.test.generators.APIDS;
import fr.svom.vhf.server.test.generators.DataGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class VhfNatsServiceTest {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private NatsController nats;

    @Inject
    private VhfNatsMessagesConverter vhfNatsMessagesConverter;

    @Test
    public void vhfSendNatsMessageTest() {
        final VhfDecodedPacket dp = new VhfDecodedPacket();
        dp.setHashId("MYTESTHASH");
        dp.setPacket("{ \"key\" : \"MYTESTPKT\"}");
        dp.setPktformat("JSON");
        dp.setSize((long) "MYTESTPKT".length());
        final PacketApid apid = new PacketApid();
        apid.setApid(APIDS.ECLALERTL1.apid());
        apid.setName(APIDS.ECLALERTL1.name());
        assertThat(apid.getApid()).isEqualTo(576);

        final Instant now = Instant.now();

        final DataNotification datanotification = new DataNotification();
        final DataNotificationContent dn = new DataNotificationContent();
        dn.date(now.atOffset(ZoneOffset.UTC));
        dn.dataStream(DataStream.VHF);
        dn.dataProvider(DataProvider.VHFDB);
        dn.productCard(apid.getName());
        dn.message("some message");
        datanotification.content(dn);
        datanotification.messageDate(now.atOffset(ZoneOffset.UTC));
        final String msg = vhfNatsMessagesConverter.writeSvomObjectAsString(datanotification);
        log.info("Convert data notification to string: {}", msg);
        DataNotification dnr = vhfNatsMessagesConverter.getSvomObject(msg, datanotification);
        assertThat(dnr).isNotNull();

        String mmap = vhfNatsMessagesConverter.putSvomObjectInMap("DataNotification",
                datanotification);
        log.info("Created string from map {}", mmap);
        DataNotification dnr1 = vhfNatsMessagesConverter.getSvomObjectFromMap(mmap,
                "DataNotification", new TypeReference<Map<String, DataNotification>>() {
                    @Override
                    public Type getType() {
                        return super.getType();
                    }
                });
        assertThat(dnr1.getContent()).isEqualTo(dn);

        VhfTransferFrame frame = DataGenerator.generateFrame("test");
        try {
            DataNotification dng = vhfNatsMessagesConverter.createDataNotificationOnFrameList(frame,
                    apid.getName(), 1000L, "vhfmgr");
            assertThat(dng).isNotNull();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        try {

            DataNotification dnfail = vhfNatsMessagesConverter.getSvomObject(null,
                    datanotification);
            final String msgfail = vhfNatsMessagesConverter.writeSvomObjectAsString(null);
            final String msgfail1 = vhfNatsMessagesConverter
                    .writeSvomObjectAsString(new VhfProperties());
            String mmapfail = vhfNatsMessagesConverter.putSvomObjectInMap("DataNotification", null);
            DataNotification dnfail1 = vhfNatsMessagesConverter.getSvomObjectFromMap(null,
                    "DataNotification", null);
        }
        catch (RuntimeException e) {
            log.error("Exception from null arguments in data notification conversions");
        }
        /*
         * try { final DataNotification dn =
         * vhfNatsMessagesConverter.createDataNotificationOnVhfDecoded(dp,
         * APIDS.ECLALERTL1.name(), 11L);
         * nats.asyncPublishToQueue(null,vhfNatsMessagesConverter.putSvomObjectInMap(
         * "DataNotification",dn)); assertThat(dn.toString().length()).isGreaterThan(0);
         * log.debug("Notification has been sent for test data notification {}",dn); }
         * catch (final IOException e) { // TODO Auto-generated catch block
         * e.printStackTrace(); }
         */
    }

    @Test
    public void vhfGenerateNatsServiceStatusTest() {

        String msg = "test for scheduler execution";
        Instant now = Instant.now();
        ServiceStatus sstatus = vhfNatsMessagesConverter.createServiceStatus(msg);
        assertThat(sstatus).isNotNull();
        
        try {

            final String msgstatus = vhfNatsMessagesConverter.writeSvomObjectAsString(sstatus);
            assertThat(msgstatus).isNotNull();

        }
        catch (RuntimeException e) {
            log.error("Exception from null arguments in data notification conversions");
        }
  
    }
}
