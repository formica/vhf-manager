/**
 * 
 */
package fr.svom.vhf.server.test.security;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import fr.svom.cs.authentication.Authorizer;
import fr.svom.cs.authentication.FscUser;

/**
 * @author formica
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class VhfSecurityTest {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Test
    public void securityTest() {
        final FscUser user = new FscUser("pippo", "guest");
        assertThat(user.getRole()).isEqualTo("guest");
        assertThat(user.getUsername()).isEqualTo("pippo");
        assertThat(user.toString().length()).isGreaterThan(0);
        final Authorizer auth = new Authorizer(user);
        final Principal pr = auth.getUserPrincipal();
        assertThat(pr.getName()).isEqualTo(user.getUsername());
        assertThat(auth.isUserInRole("guest")).isTrue();
        assertThat(auth.isSecure()).isTrue();
        assertThat(auth.getAuthenticationScheme()).isEqualTo(SecurityContext.BASIC_AUTH);

    }
		
}
