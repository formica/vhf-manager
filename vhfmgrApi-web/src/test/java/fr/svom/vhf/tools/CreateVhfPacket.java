package fr.svom.vhf.tools;

import fr.svom.vhf.data.packets.PacketApid;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.DatatypeConverter;

public class CreateVhfPacket {

	public CreateVhfPacket() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {

		Path pathgs = Paths.get("/tmp/data/trame_vhf_sol.bin");
		Path pathccsds = Paths.get("/tmp/data/testpacket.bin");
		try {
			byte[] packet = new byte[128];
			byte[] gspacket = Files.readAllBytes(pathgs);
			byte[] ccsdspacket = Files.readAllBytes(pathccsds);
			String gshex = DatatypeConverter.printHexBinary(gspacket);
			String ccsdshex = DatatypeConverter.printHexBinary(ccsdspacket);
			System.out.println(" found GS: "+gshex+" of length "+gshex.length());
			System.out.println(" found CCSDS: "+ccsdshex+ " of length "+ccsdshex.length());
			for (int i=0;i<34;i++) {
				packet[i] = gspacket[i];
			}
			for (int i=0;i<94;i++) {
				packet[i+34] = ccsdspacket[i];
			}
			FileOutputStream fos = new FileOutputStream("/tmp/data/vhf-packet.bin");
			fos.write(packet);
			fos.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
