package fr.svom.vhf.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.svom.messages.model.DataNotification;
import fr.svom.vhf.data.helpers.DataModelsHelper;
import fr.svom.vhf.data.serialization.converters.CustomTimeDeserializer;
import fr.svom.vhf.data.serialization.converters.CustomTimeSerializer;
import fr.svom.vhf.server.test.generators.DataGenerator;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JacksonMapperTests {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DateTimeFormatter formatter = new DateTimeFormatterBuilder()
				.parseCaseInsensitive()
				.appendPattern("yyyy-MM-dd HH:mm:ss")
				.optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd()
				.appendPattern("x")
				.toFormatter();

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		JavaTimeModule module = new JavaTimeModule();
		module.addSerializer(OffsetDateTime.class, new CustomTimeSerializer(formatter));
		module.addDeserializer(OffsetDateTime.class, new CustomTimeDeserializer(formatter));
		mapper.registerModule(module);

		String model = "{\"timestamp\": \"2017-09-17 13:45:42.710576+02\"}";
		try {
			Model mm = mapper.readValue(model, new TypeReference<Model>(){});
			System.out.println("Parsed short model " + mm.toString() + " " + mm.getTimestamp());
		}
		catch (JsonProcessingException e) {
			System.out.println("Error in parsing model....");
			e.printStackTrace();
		}

//		ObjectMapper jacksonMapper = new com.fasterxml.jackson.databind.ObjectMapper();
		ObjectMapper jacksonMapper = mapper;
		String s = String.join("\n"
		         , " {"
				,	"\"message_date\" : \"2018-11-22 09:32:33+02\",  "
				,	"\"message_class\" : \"DataNotification\",  "
				 , " \"content\" : {"
				, " \"data_stream\" : \"VHF\", "
				, " \"obsid\" : 1000, "
		         , " \"product_card\": \"raw-packet\", "
		         , " \"date\": \"2018-11-22 09:32:33+02\", "
		         , " \"message\": \"0222400100570000003e000000010000001d47524d4c4355524849500047524d4c4355524849500047524d4c4355524849500047524d4c4355524849500047524d4c4355524849500047524d4c435552484950000000000000000000bf27\", "
		         , " \"resource_locator\": \"http://irfumcj144.extra.cea.fr:8080/api/vhf/packet/5a1c348a0a8b881f5e0f7e934be52cb3609f6d645f05fad9cd3555a1bba10deb\" "
		         , "}}"
		);
		try {
			Map<String, Object> amap = new HashMap<String, Object>();
			System.out.println("deserialize "+s);
			// convert JSON string to Map
			amap = jacksonMapper.readValue(s, new TypeReference<Map<String, Object>>(){});
			amap.forEach((k, v) -> System.out.println(String.format("%s ==>> %s",k, v)));
			System.out.println("===================== check if DataNotification ================");

			for (String kk : amap.keySet()) {
				System.out.println("look for key " + kk);
				if (amap.get(kk).equals("DataNotification")) {
					DataNotification amapnot = jacksonMapper.readValue(s, new TypeReference<DataNotification>(){});
					System.out.println("Create DataNotification from string: " + amapnot.toString());
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String pkt = "\"{\\\"CcsdsTmHeadr\\\": {\\\"CcsdsApid\\\": 576, \\\"CcsdsGFlag\\\": 3, \\\"CcsdsCounter\\\": 1, \\\"CcsdsPLength\\\": 87}, \\\"VhfTmHeader\\\": {\\\"DPacketTimeS\\\": 1547211062}, \\\"IObservId\\\": {\\\"IObsType\\\": 0, \\\"NObsNumber\\\": 1}, \\\"Alert_MsgCount\\\": 0, \\\"Alert_TimeTbAbsSeconds\\\": 325, \\\"DAlertTBAbF\\\": 4410433, \\\"Alert_TimeTb\\\": 1279611476, \\\"Alert_SlewRequest\\\": 76, \\\"Alert_SkyTheta\\\": 3211333, \\\"Alert_SkyPhi\\\": 4410433, \\\"Alert_CatSrcNum\\\": 19525, \\\"Alert_SkyYpix\\\": 82, \\\"Alert_SkyZpix\\\": 84, \\\"Alert_SkyYfit\\\": 19505, \\\"Alert_SkyZfit\\\": 69, \\\"Alert_SkyCnt\\\": 204.25506591796875, \\\"Alert_SkyCntFit\\\": 3365.2685546875, \\\"Alert_SkyVar\\\": 822101315, \\\"Alert_Quality\\\": 76, \\\"Alert_TriggerCriterium\\\": 16716, \\\"Alert_DetCounts\\\": 4543060, \\\"Alert_DetBkg\\\": 4993280, \\\"Sat_PositionX\\\": 3124.765869140625, \\\"Sat_PositionY\\\": 51726672.0, \\\"Sat_PositionZ\\\": 46399764.0, \\\"Sat_AttitudeQ0\\\": 204.25506591796875, \\\"Sat_AttitudeQ1\\\": 3365.2685546875, \\\"Sat_AttitudeQ2\\\": 1.862645149230957e-09, \\\"Sat_AttitudeQ3\\\": 0.0, \\\"Sat_AttQuality\\\": 0, \\\"Sat_AttRef\\\": 0, \\\"ZVhfPEC\\\": 52688}\"";
		try {
			jacksonMapper = new ObjectMapper();
			System.out.println("deserialize "+pkt);
			JsonNode node = jacksonMapper.readTree(pkt);
			// convert JSON string to JsonNode
			System.out.println("created node: "+node.asText());

			String pkt2 = "{\"CcsdsTmHeadr\": {\"CcsdsApid\": 576, \"CcsdsGFlag\": 3, "
						  + "\"CcsdsCounter\": 1, \"CcsdsPLength\": 87}, \"VhfTmHeader\": {\"DPacketTimeS\": "
						  + "1547211062}, \"IObservId\": {\"IObsType\": 0, \"NObsNumber\": 1}, \"Alert_MsgCount\": 0}";
			JsonNode node2 = jacksonMapper.readTree(pkt2);
			System.out.println("created node2: "+node2.asText());
			JsonNode ccsdsTmHeader = node2.path("CcsdsTmHeadr");
			System.out.println("extract node tmheader: "+node2.get("CcsdsTmHeadr").get("CcsdsApid").asText());
			System.out.println("extract node tmheader all: "+ccsdsTmHeader.get("CcsdsApid").asText() + " " + ccsdsTmHeader.get("CcsdsGFlag").asText());
			// JSON string
			String json = "{\"id\":1,\"name\":\"John Doe\"}";
// convert JSON string to `JsonNode`
			JsonNode node3 = jacksonMapper.readTree(json);
			System.out.println("extract node3: "+node3.asText());
			System.out.println("extract node3 id: "+node3.path("id").asText());

			String json4 = "{\n"
						   + "    \"id\": \"957c43f2-fa2e-42f9-bf75-6e3d5bb6960a\",\n"
						   + "    \"name\": \"The Best Product\",\n"
						   + "    \"brand\": {\n"
						   + "        \"id\": \"9bcd817d-0141-42e6-8f04-e5aaab0980b6\",\n"
						   + "        \"name\": \"ACME Products\",\n"
						   + "        \"owner\": {\n"
						   + "            \"id\": \"b21a80b1-0c09-4be3-9ebd-ea3653511c13\",\n"
						   + "            \"name\": \"Ultimate Corp, Inc.\"\n"
						   + "        }\n"
						   + "    }  \n"
						   + "}";
			JsonNode node4 = jacksonMapper.readTree(json4);
			System.out.println("Read id : " + node4.get("id").textValue());
			System.out.println("Read brand id: " + node4.get("brand").get("id").textValue());
			for (Iterator<String> it = node4.fieldNames(); it.hasNext();) {
				String i = it.next();
				System.out.println("Found field in json node " + i);
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("======= test with string from DataGenerator ============");
		//String jpktjson = DataGenerator.generateDecodedLC();
		String jpktjson = DataGenerator.generateDecodedLCNoSlash();
		System.out.println("Using jpktjson string as: " + jpktjson);

		final Map<String, String> decfields = DataModelsHelper
				.parseCcsds(jpktjson);
		decfields.forEach((k,v) -> System.out.println("decoded field: " + k + " = " + v));

		System.out.println("======= END test with string from DataGenerator ============");

		String newfmt = "{\"type\": \"Frame\", \"content\": {\"CcsdsTmHeadr\": {\"CcsdsApid\": 0, \"CcsdsGFlag\": 0, \"CcsdsCounter\": 0, \"CcsdsPLength\": 0}, \"VhfTmHeader\": {\"DPacketTimeS\": 1565687742}, \"ZVhfPEC\": 0}, \"padding\": \"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\"}";
		try {
			Map<String, Object> amap = new HashMap<String, Object>();
			System.out.println("deserialize "+newfmt);
			// convert JSON string to Map
			amap = jacksonMapper.readValue(newfmt, new TypeReference<Map<String, Object>>(){});
			amap.forEach((k, v) -> System.out.println(String.format("%s ==>> %s",k, v)));

			JsonNode node = jacksonMapper.readTree(newfmt);
			// convert JSON string to JsonNode
			System.out.println("created node: "+node);
			System.out.println("	    type is: "+node.get("type").asText()+"!!!");
			JsonNode content = node.get("content");
			String jsonpktcont = jacksonMapper.writeValueAsString(content);

			System.out.println("	    content is: "+jsonpktcont);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {
			String newfmt2 = "{\"CcsdsTmHeadr\": {\"CcsdsApid\": 0, \"CcsdsGFlag\": 0, \"CcsdsCounter\": 0, \"CcsdsPLength\": 0}, \"VhfTmHeader\": {\"DPacketTimeS\": 1565687742}, \"InstrumentMode\":255,\"CurrentObsId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":1280}}";
			String json2 = "{\"CcsdsTmHeadr\":{\"CcsdsApid\":612,\"CcsdsGFlag\":0,\"CcsdsCounter\":2,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1591178989},\"InstrumentMode\":16,\"CurrentObsId\":{\"VhfIObsType\":119,\"VhfNObsNumber\":1},\"TcRejectedCount\":0,\"TcReceivedCount\":0,\"LastAnomalyCode\":0,\"LastAnomalyTime\":0,\"OOLParamNumber\":0,\"CameraConf\":{\"WheelPos\":7,\"FeeMode\":1},\"GFeePower1\":{\"BitNotUsed15\":1,\"FeeLVSSS\":1,\"FeeLVSSD\":1,\"FeeLVSST\":1,\"FeeLVSS\":1,\"FeeLVDDT\":1,\"FeeLVDDD\":1,\"FeeLVDD\":0,\"BitNotUsed07\":1,\"FeeRVSSS\":1,\"FeeRVSSD\":1,\"FeeRVSST\":1,\"FeeRVSS\":1,\"FeeRVDDT\":1,\"FeeRVDDD\":1,\"FeeRVDD\":0},\"GFeePower2\":{\"BitNotUsed5X\":3,\"SImADC\":0,\"FeeFFDR\":0,\"FeeLlDVS\":0,\"FeeRLDVS\":0,\"FeeRedDAC\":0},\"IFDR\":0,\"ILSS\":0,\"SaaSlewStatus\":{\"SaaStatus\":0,\"SlewStatus\":0},\"EarthOccStatus\":255,\"DetectorTemperature\":-65.0348892211914,\"TDetBoard\":0,\"DetectorTemperatureTarget\":-65.0,\"OpticsTemperature\":-5.468810081481934,\"ColdTipTemperature\":-50.05377960205078,\"TubeTemperature\":-20.388172149658203,\"SoftwareResetcounter\":0,\"BadPixelTableVersionYear\":20,\"BadPixelTableVersionMonth\":1,\"BadPixelTableVersionDay\":1,\"OffsetTableVersionYear\":20,\"OffsetTableVersionMonth\":1,\"OffsetTableVersionDay\":1,\"LowLevelThrTableVersionYear\":20,\"LowLevelThrTableVersionMonth\":1,\"LowLevelThrTableVersionDay\":1,\"GainTableVersionYear\":20,\"GainTableVersionMonth\":1,\"GainTableVersionDay\":1,\"DeadPixTableVersionYear\":20,\"DeadPixTableVersionMonth\":1,\"DeadPixTableVersionDay\":1,\"VhfCrc\":0} ";
			//org.apache.commons.text.StringEscapeUtils.unescapeJson();
        	json2 = newfmt2;
            Map<String, Object> amap = new HashMap<String, Object>();
            System.out.println("Now deserialize "+json2);
            // convert JSON string to Map
            amap = jacksonMapper.readValue(json2, new TypeReference<Map<String, Object>>(){});
            amap.forEach((k, v) -> System.out.println(String.format("%s ==>> %s",k, v)));

            JsonNode node = jacksonMapper.readTree(json2);
            // convert JSON string to JsonNode
            System.out.println("created node: "+node);
            System.out.println("        InstrumentMode is: "+node.get("InstrumentMode").asText()+"!!!");
            System.out.println("        CurrentObsId is: "+node.get("CurrentObsId")+"!!!");
            JsonNode nodeobsid = node.get("CurrentObsId");
            System.out.println("created node: "+nodeobsid);
            System.out.println("        VhfIObsType is: "+nodeobsid.get("VhfIObsType").asText()+"!!!");
            System.out.println("        VhfNObsNumber is: "+nodeobsid.get("VhfNObsNumber").asText()+"!!!");
            if (node.get("pippo") == null) {
                System.out.println("node for key pippo does not exists");
            }
            
            String lc = "{\"CcsdsTmHeadr\":{\"CcsdsApid\":577,\"CcsdsGFlag\":1,\"CcsdsCounter\":0,\"CcsdsPLength\":87},\"VhfTmHeader\":{\"PacketTimeS\":1592721093},\"VhfIObservId\":{\"VhfIObsType\":0,\"VhfNObsNumber\":100},\"AlertTimeTb0AbsSeconds\":1592721091,\"AlertTimeTb0AbsMilliseconds\":69,\"LightCurvePacketNumber\":1,\"SatAttitudeLCQ0\":14058,\"SatAttitudeLCQ1\":21521,\"SatAttitudeLCQ2\":9840,\"SatAttitudeLCQ3\":17781,\"SatPositionLon\":69,\"SatPositionLat\":17228,\"Sample0Eband0IntCount\":8537,\"Sample0Eband1IntCount\":2722,\"Sample0Eband2IntCount\":2462,\"Sample0Eband3IntCount\":2818,\"Sample0EsatIntCount\":0,\"Sample0MultipleIntCount\":0,\"Sample1Eband0DiffCount\":2374,\"Sample1Eband1DiffCount\":801,\"Sample1Eband2DiffCount\":775,\"Sample1Eband3DiffCount\":895,\"Sample1EsatDiffCount\":0,\"Sample1MultipleDiffCount\":0,\"Sample2Eband0DiffCount\":2139,\"Sample2Eband1DiffCount\":647,\"Sample2Eband2DiffCount\":616,\"Sample2Eband3DiffCount\":687,\"Sample2EsatDiffCount\":0,\"Sample2MultipleDiffCount\":0,\"Sample3Eband0DiffCount\":2064,\"Sample3Eband1DiffCount\":615,\"Sample3Eband2DiffCount\":477,\"Sample3Eband3DiffCount\":586,\"Sample3EsatDiffCount\":0,\"Sample3MultipleDiffCount\":0,\"VhfCrc\":9995}";
            JsonNode nodelc = jacksonMapper.readTree(lc);
            System.out.println("Node for LC is : "+nodelc);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

		// Try to generate object node
		Map<String, Integer> notifcounter = new HashMap<>();
		notifcounter.put("count1", 100);
		notifcounter.put("count2", 200);
		try {
			ObjectNode objectNode = jacksonMapper.createObjectNode();
			JsonNode node = jacksonMapper.valueToTree(notifcounter);
			objectNode.set("notified", node);
			System.out.println("Created object node "+jacksonMapper.writeValueAsString(objectNode));
		}
		catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

}
