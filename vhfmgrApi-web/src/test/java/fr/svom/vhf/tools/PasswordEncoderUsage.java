package fr.svom.vhf.tools;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

public class PasswordEncoderUsage {

	public PasswordEncoderUsage() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			PasswordEncoder penc =  NoOpPasswordEncoder.getInstance();
			String passwd = penc.encode("password");
			System.out.println("new password is "+passwd);
			PasswordEncoder pencscryp =  new Pbkdf2PasswordEncoder();
			passwd = pencscryp.encode("password");
			System.out.println("new scrypt password is "+passwd);
			PasswordEncoder pencbcryp =  new BCryptPasswordEncoder();
			passwd = pencbcryp.encode("password");
			System.out.println("new bcrypt password is "+passwd);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
