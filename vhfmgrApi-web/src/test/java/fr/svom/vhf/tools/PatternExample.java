package fr.svom.vhf.tools;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PatternExample {

	private static Logger log = LoggerFactory.getLogger(PatternExample.class);
	private static final String QRY_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:|<|>|~)([a-zA-Z0-9_\\-\\/\\.\\*\\%]+?),";
	private static final String SORT_PATTERN = "([a-zA-Z0-9_\\-\\.]+?)(:)([ASC|DESC]+?),";

	public PatternExample() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		final String by = "apid:12,apidname:TestTm*Curve,another:Tm%Ecl,again~this";
		final Pattern pattern = Pattern.compile(QRY_PATTERN);
		final Matcher matcher = pattern.matcher(by + ",");
		log.debug("Pattern is {}",pattern);
		log.debug("Matcher is {}",matcher);
		while (matcher.find()) {
			log.info("Found {} {} {}",matcher.group(1), matcher.group(2), matcher.group(3));
		}
	}

}
