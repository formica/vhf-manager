package fr.svom.vhf.tools;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import fr.svom.vhf.server.test.generators.DataGenerator;
import fr.svom.vhf.server.test.handlers.VhfErrorHandler;

public class SimpleClient {

	private static Logger log = LoggerFactory.getLogger(SimpleClient.class);

	public SimpleClient() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		final RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new VhfErrorHandler());
		final String uri = "http://localhost:8080/vhfmgrapi/vhf/packet";
		final byte[] data = DataGenerator.generatePacket();
		String datastr = null;
		try {
			datastr = new String(data, "ISO-8859-1");
		} catch (final UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("String encoded is "+datastr+" with length "+datastr.length());
        log.info(">>>>> upload stream from byte array of length : "+data.length);
		assertThat(data.length).isEqualTo(128);
		
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
	}

}
