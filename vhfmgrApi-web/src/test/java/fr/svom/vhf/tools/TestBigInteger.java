/**
 * 
 */
package fr.svom.vhf.tools;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.bind.DatatypeConverter;

/**
 * @author formica
 *
 */
public class TestBigInteger {

	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Path p = FileSystems.getDefault().getPath("", "/tmp/vhftestpacket.bin");
		byte[] vhfData = null;
		try {
			vhfData = Files.readAllBytes(p);
			System.out.println("Dump input as hex: "+bytesToHex(vhfData));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//String num = "0240FFFF7FFF000043352C0000";
		//String num = "0240FFFF7FFF000043352C00000F040000000F00000100000000FF0000000000007FFF00000000000041B800000000000000000000FF00080000001000000000332BCC77C1728F5C41BB5C29000000003F8000004000000040400000FFFF002A";
		String num = bytesToHex(vhfData);
		BigInteger a = new BigInteger(num,16);
		//BigInteger b = new BigInteger(num);
		System.out.println("num as hexa = "+a);
				//+ " num as int "+b);
		String numhex = DatatypeConverter.printHexBinary(a.toByteArray());
		//String numint = DatatypeConverter.printHexBinary(b.toByteArray());
		byte[] numba = a.toByteArray();
		System.out.println("string => num as hexa = "+numhex);//+", num as int "+numint);
		System.out.println("string => num as bytes = "+bytesToHex(numba));

	}

}
