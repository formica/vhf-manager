/**
 *
 */
package fr.svom.vhf.tools;

/**
 * @author formica
 *
 */
public class TestString {

    public static void main(String args[]) {
//        String Str = new String("Welcome to Tutorialspoint.com or Alert");
        String Str = new String("Welcome to Hello.com or Alert");

        System.out.print("Return Value :");
        System.out.println(Str.matches("(.*)(Tutorials|Alert)(.*)"));

        System.out.print("Return Value :");
        System.out.println(Str.matches("Tutorials"));

        System.out.print("Return Value :");
        System.out.println(Str.matches("Welcome(.*)"));
    }
}