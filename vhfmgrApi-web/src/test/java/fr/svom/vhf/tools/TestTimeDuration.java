/**
 * 
 */
package fr.svom.vhf.tools;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

/**
 * @author formica
 *
 */
public class TestTimeDuration {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
        String duration = "P2DT1H";
        java.time.Duration d = java.time.Duration.parse(duration);
        System.out.println( d.get(java.time.temporal.ChronoUnit.SECONDS)*1000L);
        duration = "PT1H";
        d = java.time.Duration.parse(duration);
        System.out.println( d.get(java.time.temporal.ChronoUnit.SECONDS)*1000L);
        
        Instant now = Instant.now();
        d = java.time.Duration.parse("PT10M");
        Long r = now.toEpochMilli()%(d.get(java.time.temporal.ChronoUnit.SECONDS)*1000L);
        if (r>0) {
            System.out.println("Rest is "+r);
        }

        Instant t0 = Instant.now();
        ZonedDateTime zdt = ZonedDateTime.ofInstant(t0, ZoneId.of("UTC"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        String svomId = zdt.format(formatter);
        System.out.println("seconds are: "+zdt.get(ChronoField.SECOND_OF_DAY));
        int ff = (int) (100 * ((float)zdt.get(ChronoField.SECOND_OF_DAY) / 86400.));
        System.out.println("Create burst id: "+String.format("sb%s%02d", svomId, ff));
    }

}
