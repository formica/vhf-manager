/**
 * 
 */
package fr.svom.vhf.tools;

import java.util.Map;

/**
 * @author formica
 *
 */
public class TestUnsignedInt {

	private final static String hexVal = "FFFFFFFF";

	private static final Map<Integer, String> a = Map.of(1,"cccc");

	public static String readUnsigned(String val) {
	    final Integer v = Integer.parseUnsignedInt(val, 16);
	    return v.toString();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    final String vread = readUnsigned(hexVal);
	    System.out.println("Reading "+hexVal+" as "+vread);
	    final int tzero = 1 << 31;
	    final int tzero_1 = tzero - 1;
	    System.out.println("Reading tzero "+tzero+ " tzero-1 "+tzero_1);
	    
	    final Long obsidlong = 10L;
	    int obsid = obsidlong.intValue();
	    int val_written = obsid + tzero;
        long val_read = val_written + tzero;
        System.out.println("Write "+val_written+ " read as long "+val_read + " using obsid "+obsid);

        final Long obsidlong2 = 0x00000000ffffffffL;
        obsid = obsidlong2.intValue();
        final long tzerolong =  0x0000000080000000L;
        System.out.println("Reading tzero long "+tzerolong);

        val_written = obsid - tzero;
        val_read = val_written + tzerolong;
        System.out.println("Write "+val_written+ " read as long "+val_read + " using obsid "+obsid);

	}

}
